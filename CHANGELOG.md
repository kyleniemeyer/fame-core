# [TBA - v1.5()](https://gitlab.com/fame-framework/fame-core/-/releases/v1.5)
## breaking changes
## major changes
## minor changes
## bug fixes

# [Cigar - v1.4.2 (2022-12-02)](https://gitlab.com/fame-framework/fame-core/-/releases/v.1.4.2)
## bug fixes
* output of last tick is now written to file
* removed log4j.properties from packaged files

# [Cartwheel - v1.4 (2022-07-08)](https://gitlab.com/fame-framework/fame-core/-/releases/v1.4)
## breaking changes
* Changed: Format of output files - now contains a file header and stores multiple protobuf messages

## major changes
* Added: ComplexIndex mechanics to store more than one value per column for one agent and time step

## minor changes
* performance improvements for systems with more than 1000 agents

## / Compatibility
* FAME-protobuf: >= 1.2
* FAME-Io >= 1.6
