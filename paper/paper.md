---
title: 'FAME-Core: An open Framework for distributed Agent-based Modelling of Energy systems'
tags:
  - Java
  - agent-based
  - energy systems
  - modelling framework
  - parallelisation
authors:
  - name: Christoph Schimeczek^[corresponding author]
    orcid: 0000-0002-0791-9365
    affiliation: 1
  - name: Marc Deissenroth-Uhrig
    orcid: 0000-0002-9103-418X
    affiliation: "1, 2"
  - name: Ulrich Frey
    orcid: 0000-0002-9803-1336
    affiliation: 1
  - name: Benjamin Fuchs
    orcid: 0000-0002-7820-851X
    affiliation: 1
  - name: A. Achraf El Ghazi
    orcid: 0000-0001-5064-9148
    affiliation: 1
  - name: Kristina Nienhaus
    orcid: 0000-0003-4180-6767
    affiliation: 1

affiliations:
 - name: Department of Energy Systems Analysis, Institute of Networked Energy Systems, German Aerospace Center (DLR), Curiestraße 4, 70563 Stuttgart, Germany
   index: 1
 - name: School of Engineering, University of Applied Sciences Saarbrücken, Goebenstraße 40, 66117 Saarbrücken, Germany
   index: 2

date: 30 October 2021
bibliography: paper.bib
---

# Summary
This paper introduces FAME, an open Framework for distributed Agent-based Models of Energy systems.
FAME aims to reduce the required effort and programming skills for modelling and simulating energy systems and other networks with similar interaction structure.
It offers rapid development and fast simulation execution of complex agent-based models (ABM).

ABMs are employed for energy system simulations which enable investigating the dynamics of energy systems and their emerging variables like electricity prices.
Designing such models, however, requires in-depth knowledge of relevant actors, their interactions and decision-making processes.
In addition, specialised programming skills are necessary to ensure numeric integrity and fast simulation execution.
This holds especially true for computationally intense models that require parallelisation to keep computation times within acceptable limits.
FAME aims to reduce the need for such specialised skills, to make the code base of ABMs less complex and thus to ease the burden of ABM development and maintenance.

For user convenience and reasons of modularity, FAME is divided into several repositories according to coding language and purpose.
All parts work seamlessly together, with no extra effort involved.
This paper covers FAME-Core, a Java library to create and run ABM simulations.
Other components are:
1. FAME-Io^[https://gitlab.com/fame-framework/fame-io] – a set of Python tools to configure simulations and convert result data,
2. FAME-Gui^[https://gitlab.com/fame-framework/FAME-Gui] – a graphical user interface currently under development, and
3. FAME-Demo^[https://gitlab.com/fame-framework/fame-demo] – a small model example built on FAME that introduces the most important FAME-Core features by explaining their usage.

# Statement of need

In the field of energy system analysis, optimisation-based models are most common [@Hoekstra2017].
As also pointed out, e.g. by [@Trutnevyte2016], this type of modelling is not ideally suited to capture real-world market imperfections.
The social-planner approach employed in such models often disregards e.g., restricted foresight, complex (and maybe non-rational) actor behaviour, and market distortions due to policy instruments.
Thus, optimisation-model results may significantly deviate from outcomes of ABM simulations which can consider such aspects, see [@TorralbaDiaz2020].

The ABM method is not new to the field of energy systems analysis. 
Many research questions were addressed with this modelling technique, see e.g. [@Zhou2009; @Ringler2016].
However, most models are not actively developed over a longer period.
One possible cause is the high effort of keeping models up to date to an ever-evolving energy system with diverse actors ranging from plant owners over policy makers to household customers.
This diversity of actors in energy systems also affects the requirements for corresponding modelling tools: actors decide on many different time scales, ranging from seconds (e.g. grid-stabilisation) to decades (e.g. investment decisions).
In addition, some decisions may be addressed with computationally cheap rule sets, whereas other decisions might require complex and demanding numerical algorithms (e.g. dynamic programming).

Many platforms, languages, and libraries already exist for the development of ABM, see the CoMSES Network^[https://www.comses.net/].
In [@Frey2021] we assessed more than 40 candidate ABM frameworks for the purpose of energy system modelling.
We closely examined, e.g., Jade^[https://jade.tilab.com], MASS^[http://depts.washington.edu/dslab/MASS], Repast^[https://repast.github.io] and Akka^[https://akka.io/].
None of those, however, satisfied our needs to model energy systems with diverse actors of inhomogeneous numerical complexity in a fast and parallelisable way using Java or Python.
We therefore developed FAME to facilitate the development of new ABM and to improve their longevity and interoperability by streamlining common components.
Hence, it fills the gap of a modern, modular and parallelizable framework that is missing so far in energy systems analysis.

Due to the above-mentioned inhomogeneity of actors and hence agents, efficient parallelisation of such models can be tricky.
FAME’s multi-core mode (based on the Message Passing Interface) was specifically developed for this use case.
It enables fast execution of computationally intense simulation models featuring inhomogeneous agents with regard to time and complexity.
Despite this capability, model developers are not burdened with parallelisation aspects.
Every FAME model is parallelizable by default.
For small to medium-sized simulations single-core execution is recommended as it avoids parallelisation overhead.

FAME addresses management of data input, simulation output, scheduling of agent actions, and agent-to-agent communication.
As for configuration, the structure of data input and output need to be defined at one point only, i.e. when defining FAME agents.
Interfaces to the supporting FAME-Io and FAME-Gui tools are then derived   from these definitions.
A tool to automate this process is currently under development.

Scheduling of agent actions is not hard-coded in FAME.
Instead, actions are triggered by receiving messages from other agents.
Since many agents have fixed business relations with other agents with reoccurring patterns, FAME features “Contracts” to organise these interactions.
Contracts are defined in the model configuration and thus create a loose coupling between agents.
This allows to formulate models with higher configurational flexibility and fewer code changes – effectively supporting the separation of concerns between model developers and model users.
In addition, this also facilitates the continuous development of ABM models under ever changing scientific requirements and research foci.

# Use Case
FAME was designed for modelling energy systems.
However, it is not restricted to this field and may be employed for other agent-based simulations as well.
Albeit FAME is intended to drive numerically complex simulations, its rapid-prototyping features also support designing simpler or less demanding models.
FAME-Demo code may serve as a convenient starting point for any new model developments.

AMIRIS, an **A**gent-based **M**arket model for the **I**nvestigation of **R**enewable and **I**ntegrated energy **S**ystems, see [@Deissenroth2017], was ported to FAME.
This did not only reduce the required lines of code in AMIRIS, but also opened new configuration options to the model, which would not have been possible before, see [@Frey2021].
FAME proved to be numerically efficient.
In single-core mode, it executes the AMIRIS model with low overhead – performing a simulation of the German wholesale electricity market for one year in hourly resolution on a desktop computer within less than a minute.
This mode is recommended for small to medium-scale simulation setups.
In multi-core mode, FAME demonstrated high parallelisation efficiency for a setup of 16 computationally heavy agents: Computation wall time was proportional to $1/n$ as $n \in {1,2,4,8}$ cores were utilised.

# Acknowledgements
This project was funded by the German Aerospace Center (DLR).

# References