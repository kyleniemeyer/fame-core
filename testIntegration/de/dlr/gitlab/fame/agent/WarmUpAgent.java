package de.dlr.gitlab.fame.agent;

import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.time.TimeStamp;

public class WarmUpAgent extends Agent {
	private final int neededWarmUpRounds;
	private int currentNumberOfWarmUpRounds = 0;
	
	public WarmUpAgent(AgentDao dao, int neededWarmUpRounds) {
		super(new DataProvider(dao, null, mock(LocalServices.class)));
		this.neededWarmUpRounds = neededWarmUpRounds;
	}

	protected void execute(TimeStamp currentTime, ArrayList<Message> incomingMessages) {}
	
	@Override
	protected WarmUpStatus warmUp(TimeStamp t) {
		currentNumberOfWarmUpRounds++;
		if (currentNumberOfWarmUpRounds >= neededWarmUpRounds) {
			return WarmUpStatus.COMPLETED;
		} else {
			return WarmUpStatus.INCOMPLETE;
		}
	}
	
	public boolean warmUpPerformedAsNeeded() {
		return currentNumberOfWarmUpRounds == neededWarmUpRounds;
	}
}