package de.dlr.gitlab.fame.agent;

import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.Signal;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.service.MessageTransfer;
import de.dlr.gitlab.fame.time.TimeStamp;

public class DummyAgent extends Agent {
	private int numerOfReceivedMessages = 0;
	private int numerOfSentMessages = 0;
	
	public DummyAgent(AgentDao dao) {
		super(new DataProvider(dao, null, mock(LocalServices.class)));
	}

	protected void execute(TimeStamp currentTime, ArrayList<Message> incomingMessages) {
		numerOfReceivedMessages += incomingMessages.size();
		long receiverId = (getId() + 1 ) % ((long) MessageTransfer.AGENT_COUNT);
		int outgoingMessageCount = (int) getId() + 1;
		for (int i = 0; i < outgoingMessageCount; i++) {
			sendMessageTo(receiverId, new Signal(0));
		}
		numerOfSentMessages += outgoingMessageCount;
	}

	public int getNumerOfReceivedMessages() {
		return numerOfReceivedMessages;
	}

	public int getNumerOfSentMessages() {
		return numerOfSentMessages;
	}
}