package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin.Topic;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.Signal;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.ChannelTest;
import de.dlr.gitlab.fame.time.TimeStamp;

public class ChannelAgent extends Agent {
	private int numerOfReceivedMessages = 0;
	private int numerOfSentMessages = 0;
	
	public ChannelAgent(AgentDao dao) {
		super(new DataProvider(dao, null, null));
	}

	protected void execute(TimeStamp currentTime, ArrayList<Message> incomingMessages) {
		numerOfReceivedMessages += incomingMessages.size();
		int outgoingMessageCount = (int) getId() + 1;
		for (int i = 0; i < outgoingMessageCount; i++) {
			publish(MessageContext.NONE, new Signal(0));
		}
		numerOfSentMessages += outgoingMessageCount;
	}
	
	public void openChannel() {
		openChannel(MessageContext.NONE);
	}
	
	public void subscribeToPartnerChannel() {
		long channelOwner = (getId() + 1 ) % ((long) ChannelTest.AGENT_COUNT);
		sendMessageTo(channelOwner, new ChannelAdmin(Topic.SUBSCRIBE, MessageContext.NONE));
	}
	
	public void unsubscribeFromChannel() {
		long channelOwner = (getId() + 1 ) % ((long) ChannelTest.AGENT_COUNT);
		sendMessageTo(channelOwner, new ChannelAdmin(Topic.UNSUBSCRIBE, MessageContext.NONE));
	}
	
	public void closeChannel() {
		super.closeChannel(MessageContext.NONE);
	}

	public int getNumerOfReceivedMessages() {
		return numerOfReceivedMessages;
	}

	public int getNumerOfSentMessages() {
		return numerOfSentMessages;
	}
}