package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.HashMap;
import de.dlr.gitlab.fame.agent.WarmUpAgent;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.setup.Setup;

/** Tests warm-up of simulation: MPI, Scheduler */
public class SimulationWarmUp {
	private static final int MAX_WARM_UP_TICKS = 100;
	private static MpiFacade mpi = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE);
	
	public static void main(String[] args) {
		mpi.initialise(args);
		MpiManager mpiManager = new MpiManager(mpi);

		Setup setup = new Setup();
		TimeSeriesProvider timeSeriesProvider = new TimeSeriesProvider(new HashMap<Integer, TimeSeries>());
		PostOffice postOffice = new PostOffice(mpiManager, setup, timeSeriesProvider);

		Scheduler scheduler = new Scheduler(mpiManager);
		SimulationParam config = SimulationParam.newBuilder().setStartTime(0L).setStopTime(0L).setRandomSeed(0L).build();
		scheduler.initialise(config);
		UuidManager uuidManager = new UuidManager(mpiManager.getRank());
		
		ArrayList<WarmUpAgent> localAgents = new ArrayList<>();
		for (int i = 0; i < mpiManager.getRank(); i++) {
			AgentDao dao = AgentDao.newBuilder().setId(uuidManager.generateNext()).setClassName("WarmUpAgent").build();
			WarmUpAgent agent = new WarmUpAgent(dao, mpiManager.getRank() + i); 
			localAgents.add(agent);
			scheduler.registerAgent(agent);
		}

		int warmUpTick = 0;
		while (scheduler.needsFurtherWarmUp()) {
			postOffice.synchroniseAddressBookUpdates();
			postOffice.deliverMessages();
			scheduler.executeWarmUp();
			scheduler.sychroniseWarmUp();

			warmUpTick++;
			if (warmUpTick > MAX_WARM_UP_TICKS) {
				throw new RuntimeException("Simulation warm-up not completed after " + MAX_WARM_UP_TICKS + " steps.");
			}
		}
		for (WarmUpAgent agent : localAgents) {
			assert agent.warmUpPerformedAsNeeded() : "Warm-up not performed as expected";
		}
		System.out.println("Warm-up completed as expected after " + warmUpTick + " ticks.");
		mpi.finalize();
	}
}