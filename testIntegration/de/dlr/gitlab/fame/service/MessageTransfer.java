package de.dlr.gitlab.fame.service;

import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.DummyAgent;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class MessageTransfer {
	public static final int AGENT_COUNT = 7;
	private static MpiFacade mpi = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE);
	private static MpiManager mpiManager;
	private static ArrayList<ArrayList<Long>> globalAddressBook = new ArrayList<>();
	private static ArrayList<Agent> localAgents = new ArrayList<>();

	public static void main(String[] args) {
		mpi.initialise(args);
		setupAddessBook();
		MessageTransfer.mpiManager = new MpiManager(mpi);
		Setup mockSetup = mock(Setup.class);
		TimeSeriesProvider mockSeriesProvider = mock(TimeSeriesProvider.class);
		PostOffice postOffice = new PostOffice(mpiManager, mockSetup, mockSeriesProvider);

		for (Long agentId : globalAddressBook.get(mpiManager.getRank())) {
			DummyAgent agent = new DummyAgent(AgentDao.newBuilder().setId(agentId).setClassName("DummyAgent").build());
			localAgents.add(agent);
			postOffice.registerAgent(agent);
		}
		postOffice.synchroniseAddressBookUpdates();
		assertAddressBooksAreSychnronized(postOffice);

		int tickCount = 10;
		for (int tick = 0; tick < tickCount; tick++) {
			for (Agent agent : localAgents) {
				agent.executeActions(null);
			}
			postOffice.deliverMessages();
		}
		assertCorrectSendMessageCount(tickCount);
		assertCorrectReceiveMessageCount(tickCount);

		mpi.finalize();
	}

	private static void setupAddessBook() {
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(0L, 1L, 2L, 3L)));
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(4L, 5L)));
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(6L)));
		globalAddressBook.add(new ArrayList<Long>());
	}

	private static void assertAddressBooksAreSychnronized(PostOffice postOffice) {
		@SuppressWarnings("unchecked")
		HashMap<Long, Integer> processIdByAgent = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("processIdByAgent", postOffice);
		for (int processId = 0; processId < mpiManager.getProcessCount(); processId++) {
			for (Long agentId : globalAddressBook.get(processId)) {
				assert ((int) processIdByAgent.get(agentId)) == processId : "AddressBook not synchronised!";
			}
		}
	}

	private static void assertCorrectSendMessageCount(int tickCount) {
		for (Agent agent : localAgents) {
			DummyAgent dummy = (DummyAgent) agent;
			long expextedSent = (agent.getId() + 1) * tickCount;
			long actualSent = (long) dummy.getNumerOfSentMessages();
			assert expextedSent == actualSent : "Sent message was " + actualSent + " but expected: " + expextedSent;
		}
	}

	private static void assertCorrectReceiveMessageCount(int tickCount) {
		for (Agent agent : localAgents) {
			DummyAgent dummy = (DummyAgent) agent;
			long senderId = agent.getId() - 1;
			senderId = senderId < 0 ? senderId + AGENT_COUNT : senderId;
			long expextedReceived = (senderId + 1) * (tickCount - 1); // no receive in first tick
			long actualReceived = (long) dummy.getNumerOfReceivedMessages();
			assert expextedReceived == actualReceived : "Received message was " + actualReceived + " but expected: "
					+ expextedReceived;
		}
	}
}