package de.dlr.gitlab.fame.service;

import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.SpecialAgent;
import de.dlr.gitlab.fame.data.DataEntry;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Input.InputData.OutputParam;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

/**
 * Tests setup chain of simulation: Python:: CreateInput Java:: InputManager, InputData, MPI, AgentBuilder
 */
public class SimulationSetup {
	private static MpiFacade mpi = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE);
	private static final String INPUT_FILE_NAME = "./testIntegration/service/testInput.pb";
	private static final String SETUP_FILE_NAME = "./fameSetup.yaml";
	private static final RandomNumberGeneratorProvider rngProvider = mock(RandomNumberGeneratorProvider.class);
	private static final Scheduler scheduler = mock(Scheduler.class);
	private static final OutputManager outputManager = mock(OutputManager.class);

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		mpi.initialise(args);
		MpiManager mpiManager = new MpiManager(mpi);
		Setup setup = Simulator.loadAndDistributeSetup(mpiManager, SETUP_FILE_NAME);
		InputManager input = new InputManager(mpiManager);
		input.read(INPUT_FILE_NAME);
		input.distribute();

		TimeSeriesProvider timeSeriesProvider = new TimeSeriesProvider(input.getTimeSeries());
		PostOffice postOffice = new PostOffice(mpiManager, setup, timeSeriesProvider);
		
		ArrayList<String> agentPackages = new ArrayList<>();
		Notary notary = new Notary(mpiManager, agentPackages, input.getAgentConfigs());
		LocalServices localServices = new LocalServices(scheduler, postOffice, outputManager, notary, timeSeriesProvider, rngProvider);
		AgentBuilder agentBuilder = new AgentBuilder(mpiManager, agentPackages, timeSeriesProvider.getMapOfTimeSeries(), localServices);

		assertCorrectConfig(input.getConfig());
		assertCorrectOutputParameters(input.getOutputParameters());

		agentBuilder.createAgentsFromConfig(input.getAgentConfigs());
		HashMap<Long, Agent> localAgents = (HashMap<Long, Agent>) AccessPrivates.getPrivateFieldOf("localAgents", postOffice);
		assertCorrectLocalAgents(localAgents);

		// Assert Correct Contracts!
		notary.setInitialContracts(input.getContractPrototypes());

		mpi.finalize();
	}

	private static void assertCorrectConfig(SimulationParam config) {
		assert 0L == config.getStartTime() : "Wrong StartTime";
		assert 84600L == config.getStopTime() : "Wrong StopTime";
		assert 1L == config.getRandomSeed() : "Wrong RandomSeed";
	}

	private static void assertCorrectOutputParameters(OutputParam outputParameters) {
		assert 100 == outputParameters.getInterval() : "Wrong OutputInterval";
		assert 0 == outputParameters.getProcess() : "Wrong OutputProcess";
		for (String className : Arrays.asList("Exchange", "SpecialAgent", "Demand", "Supply")) {
			assert outputParameters.getActiveClassNameList().contains(className) : "Active class missing in List";
		}
	}

	@SuppressWarnings("unchecked")
	private static void assertCorrectLocalAgents(HashMap<Long, Agent> localAgents) {
		for (Entry<Long, Agent> entry : localAgents.entrySet()) {
			Agent agent = entry.getValue();
			assert agent instanceof SpecialAgent : "Agent not a SpecialAgent";
			SpecialAgent specialAgent = (SpecialAgent) agent;
			TimeSeries dummySeries = specialAgent.getDummySeries();
			assert dummySeries != null : "DummySeries is null";
			ArrayList<DataEntry> data = (ArrayList<DataEntry>) AccessPrivates.getPrivateFieldOf("data", dummySeries);
			assert data.size() == 100 : "Not exactly 100 items in TimeSeries";
			assert data.get(0).getTimeStep() == 0L : "First item not at timeIndex 0";
			assert data.get(99).getTimeStep() == 99L : "Last item not at timeIndex 99";
		}
	}
}