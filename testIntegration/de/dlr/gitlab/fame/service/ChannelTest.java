package de.dlr.gitlab.fame.service;

import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.ChannelAgent;
import de.dlr.gitlab.fame.agent.ChannelManager;
import de.dlr.gitlab.fame.communication.Channel;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin.Topic;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class ChannelTest {
	private static final int TICK_COUNT = 10;
	public static final int AGENT_COUNT = 7;
	private static MpiFacade mpi = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE);
	private static MpiManager mpiManager; 
	private static ArrayList<ArrayList<Long>> globalAddressBook = new ArrayList<>();
	private static ArrayList<Agent> localAgents = new ArrayList<>();

	public static void main(String[] args) {
		mpi.initialise(args);
		setupAddessBook();
		ChannelTest.mpiManager = new MpiManager(mpi);
		Setup mockSetup = mock(Setup.class);
		TimeSeriesProvider mockSeriesProvider = mock(TimeSeriesProvider.class);
		PostOffice postOffice = new PostOffice(mpiManager, mockSetup, mockSeriesProvider);
		createLocalAgentsAndSynchronise(postOffice);
		openAllChannels();
		
		SubscribeToAllChannels(postOffice);
		assertEachChannelHasOneSubscriber();
		
		UnsubscribeFromAllChannels(postOffice);
		assertEachChannelHasNoMoreSubscribers();
		
		SubscribeToAllChannels(postOffice);	
		CloseAllChannels(postOffice);
		assertAllChannelsAreClosed();
		assertCloseMessageReceived();
		
		clearInBoxes();
		
		openAllChannels();
		SubscribeToAllChannels(postOffice);
		
		executeTicks(postOffice);
		assertCorrectSendMessageCount();
		assertCorrectReceiveMessageCount();
		
		mpi.finalize();
	}

	private static void setupAddessBook() {
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(0L, 1L, 2L, 3L)));
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(4L, 5L)));
		globalAddressBook.add(new ArrayList<Long>(Arrays.asList(6L)));
		globalAddressBook.add(new ArrayList<Long>());
	}

	private static void createLocalAgentsAndSynchronise(PostOffice postOffice) {
		for (Long agentId : globalAddressBook.get(mpiManager.getRank())) {
			ChannelAgent agent = new ChannelAgent(AgentDao.newBuilder().setId(agentId).setClassName("DummyAgent").build());
			localAgents.add(agent);
			postOffice.registerAgent(agent);
		}
		postOffice.synchroniseAddressBookUpdates();
	}
	
	private static void openAllChannels() {
		for (Agent agent : localAgents) {
			((ChannelAgent) agent).openChannel();
		}
	}
	
	private static void SubscribeToAllChannels(PostOffice postOffice) {
		for (Agent agent : localAgents) {
			((ChannelAgent) agent).subscribeToPartnerChannel();
			AccessPrivates.callMethodOfClassXOnY("forwardMessages", Agent.class, agent);
		}
		postOffice.deliverMessages();
	}
	
	private static void assertEachChannelHasOneSubscriber() {
		for (Agent agent : localAgents) {
			ChannelAgent channelAgent = (ChannelAgent) agent; 
			ChannelManager manager = (ChannelManager) AccessPrivates.getPrivateFieldOf("channelManager", channelAgent);
			@SuppressWarnings("unchecked")
			HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates.getPrivateFieldOf("channels", manager);
			Channel channel = channels.get(MessageContext.NONE);
			int subscriberCount = ((ArrayList<?>) AccessPrivates.getPrivateFieldOf("subscribers", channel)).size();
			assert subscriberCount == 1 : agent + ": Count of subscribers not 1 but " + subscriberCount;
		}
	}
	
	private static void UnsubscribeFromAllChannels(PostOffice postOffice) {
		for (Agent agent : localAgents) {
			((ChannelAgent) agent).unsubscribeFromChannel();
			AccessPrivates.callMethodOfClassXOnY("forwardMessages", Agent.class, agent);
		}
		postOffice.deliverMessages();
	}	
	
	private static void assertEachChannelHasNoMoreSubscribers() {
		for (Agent agent : localAgents) {
			ChannelAgent channelAgent = (ChannelAgent) agent; 
			ChannelManager manager = (ChannelManager) AccessPrivates.getPrivateFieldOf("channelManager", channelAgent);
			@SuppressWarnings("unchecked")
			HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates.getPrivateFieldOf("channels", manager);
			Channel channel = channels.get(MessageContext.NONE);
			int subscriberCount = ((ArrayList<?>) AccessPrivates.getPrivateFieldOf("subscribers", channel)).size();
			assert subscriberCount == 0 : agent + ": Count of subscribers not 0 but " + subscriberCount;
		}
	}	

	private static void CloseAllChannels(PostOffice postOffice) {
		for (Agent agent : localAgents) {
			((ChannelAgent) agent).closeChannel();
		}
		postOffice.deliverMessages();
	}
	
	private static void assertAllChannelsAreClosed() {
		for (Agent agent : localAgents) {
			ChannelAgent channelAgent = (ChannelAgent) agent; 
			ChannelManager manager = (ChannelManager) AccessPrivates.getPrivateFieldOf("channelManager", channelAgent);
			@SuppressWarnings("unchecked")
			HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates.getPrivateFieldOf("channels", manager);
			int channelCount = channels.size();
			assert channelCount == 0 : agent + ": Count of Channels not 0 but " + channelCount;
		}
	}

	private static void assertCloseMessageReceived() {
		for (Agent agent : localAgents) {
			ChannelAgent channelAgent = (ChannelAgent) agent;
			@SuppressWarnings("unchecked")
			ArrayList<Message> inBox = (ArrayList<Message>) AccessPrivates.getPrivateFieldOf("inBox", channelAgent);
			assert inBox.size() == 1 : agent + ": More than one message in inBox";
			ChannelAdmin data = inBox.get(0).getDataItemOfType(ChannelAdmin.class);
			assert data.topic == Topic.CLOSE : agent + ": Has no close message.";
		}
	}
	
	private static void clearInBoxes() {
		for (Agent agent : localAgents) {
			ChannelAgent channelAgent = (ChannelAgent) agent;
			((ArrayList<?>) AccessPrivates.getPrivateFieldOf("inBox", channelAgent)).clear();
		}
	}
	
	private static void executeTicks(PostOffice postOffice) {
		for (int tick = 0; tick < TICK_COUNT; tick++) {
			for (Agent agent : localAgents) {
				agent.executeActions(null);
			}
			postOffice.deliverMessages();
		}
	}	
	
	private static void assertCorrectSendMessageCount() {
		for (Agent agent : localAgents) {
			ChannelAgent dummy = (ChannelAgent) agent;
			long expextedSent = (agent.getId() + 1) * TICK_COUNT;
			long actualSent = (long) dummy.getNumerOfSentMessages();
			assert expextedSent == actualSent : "Sent message was " + actualSent + " but expected: " + expextedSent;
		}
	}
	
	private static void assertCorrectReceiveMessageCount() {
		for (Agent agent : localAgents) {
			ChannelAgent channelAgent = (ChannelAgent) agent;
			long senderId = (channelAgent.getId() + 1 ) % ((long) ChannelTest.AGENT_COUNT);
			long expextedReceived = (senderId + 1) * (TICK_COUNT - 1); //no receive in first tick
			long actualReceived = (long) channelAgent.getNumerOfReceivedMessages();
			assert expextedReceived == actualReceived : "Received message was " + actualReceived + " but expected: " + expextedReceived;
		}
	}
}