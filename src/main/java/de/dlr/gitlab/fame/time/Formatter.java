package de.dlr.gitlab.fame.time;

/** Formats TimeStamp to String representation corresponding to FAME's internal normed time intervals
 *
 * @author Christoph Schimeczek */
public class Formatter {
	/** Encapsules result if integer division */
	private class DivisionResult {
		public final long multiple;
		public final long remainder;

		public DivisionResult(long multiple, long remainder) {
			this.multiple = multiple;
			this.remainder = remainder;
		}
	}

	/** Returns String representation of TimeStamp-step using norm length for every day(24h), week(168h), month(730h) and
	 * year(8760h) in the format YYYY-DDD(MM/WW)_hh:mm:ss, with
	 * <ul>
	 * <li>YYYY - year</li>
	 * <li>DDD - day of year</li>
	 * <li>MM - norm month - may change during a day</li>
	 * <li>WW - norm week - last week of year has only one day</li>
	 * <li>hh:mm:ss - time of day in hours:minutes:seconds</li>
	 * </ul>
	 * 
	 * @param step step of a TimeStamp to create the String representation from
	 * @return formatted String */
	String format(long step) {
		StringBuilder builder = new StringBuilder();

		DivisionResult yearDiv = division(step, Constants.STEPS_PER_YEAR);
		builder.append(String.format("%04d-", Constants.START_YEAR + yearDiv.multiple));

		DivisionResult dayDiv = division(yearDiv.remainder, Constants.STEPS_PER_DAY);
		builder.append(String.format("%03d", 1 + dayDiv.multiple));

		DivisionResult monthDiv = division(yearDiv.remainder, Constants.STEPS_PER_MONTH);
		DivisionResult weekDiv = division(yearDiv.remainder, Constants.STEPS_PER_WEEK);
		builder.append(String.format("(%02d/%02d)_", 1 + monthDiv.multiple, 1 + weekDiv.multiple));

		DivisionResult hourDiv = division(dayDiv.remainder, Constants.STEPS_PER_HOUR);
		DivisionResult minuteDiv = division(hourDiv.remainder, Constants.STEPS_PER_MINUTE);
		builder.append(String.format("%02d:%02d:%02d", hourDiv.multiple, minuteDiv.multiple, minuteDiv.remainder));

		return builder.toString();
	}

	/** Returns result of Euclidean division <code>dividend = multiple * divisor + remainder</code> for positive dividend, but
	 * deviates for negative dividends: the divisor is added until remainder gets positive
	 * 
	 * @return division result ensuring zero or positive remainder */
	private DivisionResult division(long dividend, long divisor) {
		long multiple;
		long remainder;
		if (dividend < 0) {
			multiple = dividend / divisor - 1;
			remainder = dividend - divisor * multiple;
		} else {
			multiple = dividend / divisor;
			remainder = dividend % divisor;
		}
		return new DivisionResult(multiple, remainder);
	}
}
