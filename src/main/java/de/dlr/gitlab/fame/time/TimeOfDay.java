package de.dlr.gitlab.fame.time;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;

/** Represents a time of day, i.e. a certain point of time repeated every day, i.e. a time span one has to wait for starting at
 * mid-night until that time of day is reached
 * 
 * @author Christoph Schimeczek */
public class TimeOfDay extends TimeSpan {
	public static final String INVALID = "Hours should range from 0-24, minutes and seconds from 0-60.";
	public static final String NEGATIVE = "Negative values for hour, minute or second are strongly discouraged.";
	public static final String TOO_BIG = "Time of day can only be between 00:00:00h and 23:59:59h";

	private static Logger logger = LoggerFactory.getLogger(TimeOfDay.class);
	/** Last second of each day */
	public static final TimeOfDay END_OF_DAY = new TimeOfDay(23, 59, 59);

	/** Create a {@link TimeOfDay} based on given time in hours, minutes and seconds
	 * 
	 * @param hour of day (0..23)
	 * @param minute of hours (0..59)
	 * @param second of minute (0..59) */
	public TimeOfDay(int hour, int minute, int second) {
		super(hour * Constants.STEPS_PER_HOUR + minute * Constants.STEPS_PER_MINUTE + second * Constants.STEPS_PER_SECOND);
		if (steps >= Constants.STEPS_PER_DAY) {
			Logging.logAndThrowFatal(logger, TOO_BIG);
		}
		if (hour < 0 || minute < 0 || second < 0) {
			logger.warn(NEGATIVE);
		}
		if (hour >= 24 || minute >= 60 || second >= 60) {
			logger.error(INVALID);
		}
	}

	/** Returns the hour of day for the given simulation time step
	 * 
	 * @param timeStep long representation of simulation time
	 * @return hour of day (0..23) for the given step */
	public static int calcHourOfDay(long timeStep) {
		long stepsOfToday = timeStep % Constants.STEPS_PER_DAY;
		return (int) (stepsOfToday / Constants.STEPS_PER_HOUR);
	}

	@Override
	public String toString() {
		long hours = steps / Constants.STEPS_PER_HOUR;
		long remainder = steps - hours * Constants.STEPS_PER_HOUR;
		long minutes = remainder / Constants.SECONDS_PER_MINUTE;
		long seconds = remainder - minutes * Constants.SECONDS_PER_MINUTE;
		return String.format("%02d:%02d:%02dh", hours, minutes, seconds);
	}
}