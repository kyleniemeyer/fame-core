package de.dlr.gitlab.fame.time;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/** Time constants
 * 
 * @author Christoph Schimeczek */
public final class Constants {
	public static enum Interval {
		SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS
	};

	/** Number of simulation steps corresponding to one real-world second */
	static final int STEPS_PER_SECOND = 1;
	static final int START_YEAR = 2000;
	static final ZonedDateTime START_IN_REAL_TIME = ZonedDateTime.of(START_YEAR, 1, 1, 0, 0, 0, 0,
			ZoneId.of("UTC+01:00"));
	static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public static final int SECONDS_PER_MINUTE = 60;
	public static final int MINUTES_PER_HOUR = 60;
	public static final int HOURS_PER_DAY = 24;
	public static final int DAYS_PER_WEEK = 7;
	public static final int DAYS_PER_YEAR = 365;
	public static final int MONTHS_PER_YEAR = 12;

	public static final int HOURS_PER_WEEK = HOURS_PER_DAY * DAYS_PER_WEEK;
	public static final int HOURS_PER_NORM_YEAR = HOURS_PER_DAY * DAYS_PER_YEAR;
	public static final int HOURS_PER_NORM_MONTH = HOURS_PER_NORM_YEAR / MONTHS_PER_YEAR;

	static final long STEPS_PER_MINUTE = STEPS_PER_SECOND * SECONDS_PER_MINUTE;
	public static final long STEPS_PER_HOUR = STEPS_PER_MINUTE * MINUTES_PER_HOUR;
	public static final long STEPS_PER_DAY = STEPS_PER_HOUR * HOURS_PER_DAY;
	static final long STEPS_PER_WEEK = STEPS_PER_DAY * DAYS_PER_WEEK;
	static final long STEPS_PER_MONTH = STEPS_PER_HOUR * HOURS_PER_NORM_MONTH;
	static final long STEPS_PER_YEAR = STEPS_PER_MONTH * MONTHS_PER_YEAR;

	/** Returns simulation step equivalent of specified simulation time {@link Interval}
	 * 
	 * @return simulation step equivalent of specified simulation time {@link Interval} */
	static long calcPeriodInSteps(Interval period) {
		switch (period) {
			case SECONDS:
				return STEPS_PER_SECOND;
			case MINUTES:
				return STEPS_PER_MINUTE;
			case HOURS:
				return STEPS_PER_HOUR;
			case DAYS:
				return STEPS_PER_DAY;
			case WEEKS:
				return STEPS_PER_WEEK;
			case MONTHS:
				return STEPS_PER_MONTH;
			case YEARS:
				return STEPS_PER_YEAR;
			default:
				throw new RuntimeException("Period " + period + " not implemented.");
		}
	}
}