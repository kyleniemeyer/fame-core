package de.dlr.gitlab.fame.util;

/** Collection of static utility methods
 * 
 * @author Christoph Schimeczek */
public final class Util {
	public static final String NO_INSTANCE = "Do not instantiate class: ";

	private Util() {
		throw new IllegalStateException(NO_INSTANCE + getClass().getCanonicalName());
	}

	/** In case the given Object is null: throws RuntimeException by concatenating given message part(s) 
	 * 
	 * @param objectToTest to be tested for existence
	 * @param messageParts any number of objects (e.g. Strings) to be transformed to a single String
	 * @throws RuntimeException if the given object is null */  
	public static void ensureNotNull(Object objectToTest, Object... messageParts) {
		if (objectToTest == null) {
			StringBuilder builder = new StringBuilder();
			for (Object messagePart : messageParts) {
				builder.append(messagePart);
			}
			throw new RuntimeException(builder.toString());
		}
	}
}