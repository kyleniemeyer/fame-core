package de.dlr.gitlab.fame.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.reflections.Reflections;
import org.reflections.ReflectionsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.setup.Constants;

/** Encapsulates reflections from org.reflections for a specified {@link ReflectionType type} of packages
 * 
 * @author Christoph Schimeczek, A. Achraf El Ghazi */
public class Reflection {
	public static enum ReflectionType {
		AGENT, COMMUNICATION, PORTABLE
	};

	public static final String ERR_ILLEGAL_PACKAGE = "Invalide package name: ";
	public static final String ERR_REFLECTION_TYPE_UNKNOWN = "Default packages for reflection type not implemented.";
	public static final String ERR_MODIFIERS_MISSING = " : ComplexIndex definitions must be 'static final' in class: ";
	public static final String ERR_FIELD_ACCESS = " : ComplexField could not be read of class: ";
	public static final String ERR_NOT_ENUM = " : is not an enum type, but was annotated with: ";
	public static final String ERR_DOUBLE_DECLARATION = " is already used for another enum in class hierarchy of: ";
	public static final String ERR_MULTI_USE = " : annotation was used more than once in class: ";

	private static Logger logger = LoggerFactory.getLogger(Reflection.class);
	private final ArrayList<Reflections> reflectionList = new ArrayList<>();

	/** Creates a {@link Reflection} for the specified list of package names; the given List is not modified; the FAME-specific
	 * package names are added to the given list, depending on the specified {@link ReflectionType}; if the given List is empty or
	 * null, only FAME's packages are used
	 * 
	 * @param type of Reflection, select from {@link ReflectionType}
	 * @param givenPackageNames list of fully qualified package names to consider */
	public Reflection(ReflectionType type, List<String> givenPackageNames) {
		List<String> packageNames = new ArrayList<String>();
		packageNames.add(getFamePackageNames(type));
		if (givenPackageNames != null) {
			packageNames.addAll(givenPackageNames);
		}
		for (String packageName : packageNames) {
			if (packageName != null) {
				reflectionList.add(createReflections(packageName));
			}
		}
	}

	/** @return {@link Reflections} for given packageName
	 * @throws RuntimeException if scan for any Object within given packageName raises an Exception */
	private Reflections createReflections(String packageName) {
		Reflections reflections = new Reflections(packageName);
		try {
			reflections.getSubTypesOf(Object.class);
		} catch (ReflectionsException e) {
			throw Logging.logFatalException(logger, ERR_ILLEGAL_PACKAGE + packageName);
		}
		return reflections;
	}

	/** Returns path to FAME's packages depending on the specified {@link ReflectionType} */
	private String getFamePackageNames(ReflectionType type) {
		switch (type) {
			case AGENT:
				return Constants.AGENT_PACKAGE_NAME;
			case COMMUNICATION:
				return Constants.MESSAGE_PACKAGE_NAME;
			case PORTABLE:
				return Constants.PORTABLE_PACKAGE_NAME;
			default:
				throw Logging.logFatalException(logger, ERR_REFLECTION_TYPE_UNKNOWN);
		}
	}

	/** Returns all child-classes of given classType in a HashMap, mapping the classes {@link Class#getSimpleName() SimpleName}
	 * 
	 * @param <T> type of class
	 * @param classType parent class
	 * @return HashMap of all child-classes of Agent mapping the classes SimpleName to the class */
	public <T> HashMap<String, Class<? extends T>> findAllChildsOfByName(Class<T> classType) {
		HashMap<String, Class<? extends T>> classNameToClass = new HashMap<>();
		Set<Class<? extends T>> allClassesExtendingClassType = getSubTypesOf(classType);
		for (Class<? extends T> clas : allClassesExtendingClassType) {
			Class<? extends T> classObject = clas;
			classNameToClass.put(classObject.getSimpleName(), classObject);
		}
		return classNameToClass;
	}

	/** Returns a Set of all {@link Class}es that are a subtype of the specified class in the packages of this {@link Reflection}
	 * 
	 * @param <T> parent class type
	 * @param classType parent class
	 * @return Set of all child classes in the considered packages */
	public <T> Set<Class<? extends T>> getSubTypesOf(Class<T> classType) {
		Set<Class<? extends T>> subTypes = new HashSet<Class<? extends T>>();
		for (Reflections reflections : reflectionList) {
			subTypes.addAll(reflections.getSubTypesOf(classType));
		}
		return subTypes;
	}

	/** Returns an {@link ArrayList} containing all non-abstract child classes of the specified {@link Class}
	 * 
	 * @param classType parent class
	 * @return {@link ArrayList} containing all non-abstract child classes of the specified {@link Class} */
	public ArrayList<Class<?>> findAllInstantiableChildClassesOf(Class<?> classType) {
		ArrayList<Class<?>> instantiableChilds = new ArrayList<>();
		Set<?> allClassesExtendingClassType = getSubTypesOf(classType);
		for (Object object : allClassesExtendingClassType) {
			Class<?> classObject = (Class<?>) object;
			if (!Modifier.isAbstract(classObject.getModifiers())) {
				instantiableChilds.add(classObject);
			}
		}
		return instantiableChilds;
	}

	/** Returns a {@link HashMap} that maps<br>
	 * - each child of the specified class type considered in this {@link Reflection}<br>
	 * - to a list of all {@link Enum}s with the specified {@link Annotation} defined in this class or any of its parent classes.
	 * 
	 * @param <T> parent class type
	 * @param annotationClass searched for Annotation type of enum(s)
	 * @param classType parent class
	 * @return HashMap matching each child class to all enums with given annotation defined in this class or any super class */
	public <T> HashMap<Class<? extends T>, ArrayList<Enum<?>>> findEnumHierarchyAnnotatedWithForSubtypes(
			Class<? extends Annotation> annotationClass, Class<T> classType) {
		HashMap<Class<?>, ArrayList<Enum<?>>> annotatedEnumsInClass = mapAnnotatedEnumsToEnclosingClass(annotationClass);
		Set<Class<? extends T>> classesInHierarchy = getSubTypesOf(classType);
		return joinEnumsByHierarchy(annotatedEnumsInClass, classesInHierarchy);
	}

	/** @return HashMap mapping a List of all {@link Enum}s with given {@link Annotation} to their enclosing class */
	private HashMap<Class<?>, ArrayList<Enum<?>>> mapAnnotatedEnumsToEnclosingClass(
			Class<? extends Annotation> annotation) {
		HashMap<Class<?>, ArrayList<Enum<?>>> annotatedEnumsInClass = new HashMap<>();
		for (Class<?> annotatedType : getTypesAnnotatedWith(annotation)) {
			String annotationName = annotation.getSimpleName();
			Class<?> enclosingClass = getEnclosingClass(annotatedType, annotationName);
			if (annotatedEnumsInClass.containsKey(enclosingClass)) {
				throw Logging.logFatalException(logger, annotationName + ERR_MULTI_USE + enclosingClass.getSimpleName());
			}
			ArrayList<Enum<?>> annotatedEnums = new ArrayList<>();
			for (Object entry : annotatedType.getEnumConstants()) {
				annotatedEnums.add((Enum<?>) entry);
			}
			annotatedEnumsInClass.put(enclosingClass, annotatedEnums);
		}
		return annotatedEnumsInClass;
	}

	/** @return the enclosing class of the given class annotated with the specified annotation by name
	 * @throws RuntimeException if the given class is not an inner type or not an {@link Enum} */
	private Class<?> getEnclosingClass(Class<?> clas, String annotationName) {
		Class<?> enclosingClass = clas.getEnclosingClass();
		if (enclosingClass == null || !clas.isEnum()) {
			throw Logging.logFatalException(logger, clas.getSimpleName() + ERR_NOT_ENUM + annotationName);
		}
		return enclosingClass;
	}

	/** @return {@link Set} of {@link Class}es covered by this {@link Reflection} that are annotated with the given annotation */
	private Set<Class<?>> getTypesAnnotatedWith(Class<? extends Annotation> annotation) {
		Set<Class<?>> annotatedClasses = new HashSet<Class<?>>();
		for (Reflections reflections : reflectionList) {
			annotatedClasses.addAll(reflections.getTypesAnnotatedWith(annotation));
		}
		return annotatedClasses;
	}

	/** Joins all given {@link ArrayList}s of {@link Enum}s based on the hierarchy of the {@link Set} of given classes;<br />
	 * All given enums defined in super classes are added to the list of their child classes.
	 * 
	 * @return HashMap of classes in hierarchy mapped to the joined ArrayLists of Enums based of the class hierarchy */
	private <T> HashMap<Class<? extends T>, ArrayList<Enum<?>>> joinEnumsByHierarchy(
			HashMap<Class<?>, ArrayList<Enum<?>>> enumsByClass, Set<Class<? extends T>> classesInHierarchy) {
		HashMap<Class<? extends T>, ArrayList<Enum<?>>> hierarchicalByClass = new HashMap<>();
		for (Class<? extends T> classInHierarchy : classesInHierarchy) {
			ArrayList<Enum<?>> enumList = new ArrayList<Enum<?>>();
			getAllEnumsOfClassAndSuperClasses(classInHierarchy, enumsByClass, enumList);
			hierarchicalByClass.put(classInHierarchy, enumList);
		}
		return hierarchicalByClass;
	}

	/** adds all Enum entries of a given class and, recursively, all its super-classes to the given ArrayList */
	private <T> void getAllEnumsOfClassAndSuperClasses(Class<? extends T> enclosingClass,
			HashMap<Class<?>, ArrayList<Enum<?>>> enumsByClass, ArrayList<Enum<?>> enumList) {
		ArrayList<Enum<?>> agentClassEnums = enumsByClass.getOrDefault(enclosingClass, new ArrayList<>(0));
		for (Enum<?> enumType : agentClassEnums) {
			if (listContainsEnumOfName(enumList, enumType.name())) {
				throw Logging.logFatalException(logger, enumType + ERR_DOUBLE_DECLARATION + enclosingClass.getSimpleName());
			}
			enumList.add(enumType);
		}
		Class<?> superClass = enclosingClass.getSuperclass();
		if (superClass != Object.class) {
			getAllEnumsOfClassAndSuperClasses(superClass, enumsByClass, enumList);
		}
	}

	/** @return true if given list of {@link Enum}s contains and entry with the specified name */
	private boolean listContainsEnumOfName(ArrayList<Enum<?>> enumList, String name) {
		for (Enum<?> enumInList : enumList) {
			if (name.equals(enumInList.name())) {
				return true;
			}
		}
		return false;
	}

	/** For all children of {@link Agent}, returns map of class names to a list of all ComplexIndex created in this class or any of
	 * its parents.
	 *
	 * @return HashMap of Agent class name to list of ComplexIndex */
	public HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> findComplexIndexHierarchyForAgents() {
		Set<Class<? extends Agent>> classesInHierarchy = getSubTypesOf(Agent.class);
		HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> result = new HashMap<>();
		for (Class<? extends Agent> clas : classesInHierarchy) {
			ArrayList<ComplexIndex<? extends Enum<?>>> indices = new ArrayList<>();
			result.put(clas.getSimpleName(), indices);
			addComplexIndicesOfClassAndSuperClasses(clas, indices);
		}
		return result;
	}

	/** Adds ComplexIndexes to the give List, that are created in the given Agent class or its super classes.
	 * 
	 * @param clas (and its super classes) to search for ComplexIndex definitions
	 * @param indices list to add the found ComplexIndexes to */
	@SuppressWarnings("unchecked")
	private void addComplexIndicesOfClassAndSuperClasses(
			Class<? extends Agent> clas, ArrayList<ComplexIndex<? extends Enum<?>>> indices) {
		for (Field field : clas.getDeclaredFields()) {
			if (ComplexIndex.class.isAssignableFrom(field.getType())) {
				ComplexIndex<?> complexIndex = getComplexIndexFromField(clas, field);
				indices.add(complexIndex);
			}
		}
		Class<?> superClass = clas.getSuperclass();
		if (Agent.class.isAssignableFrom(superClass)) {
			addComplexIndicesOfClassAndSuperClasses((Class<? extends Agent>) superClass, indices);
		}
	}

	/** @return the ComplexIndex defined in the given static field */
	@SuppressWarnings("unchecked")
	private ComplexIndex<?> getComplexIndexFromField(Class<?> clas, Field field) {
		int fieldModifiers = field.getModifiers();
		if (!Modifier.isFinal(fieldModifiers) || !Modifier.isStatic(fieldModifiers)) {
			throw new RuntimeException(field.getName() + ERR_MODIFIERS_MISSING + clas.getSimpleName());
		}

		try {
			field.setAccessible(true);
			return (ComplexIndex<? extends Enum<?>>) field.get(null);
		} catch (IllegalAccessException | RuntimeException e) {
			throw new RuntimeException(field.getName() + ERR_FIELD_ACCESS + clas.getSimpleName(), e);
		}
	}
}