package de.dlr.gitlab.fame.setup;

/** Setup constants
 * 
 * @author Christoph Schimeczek */
public final class Constants {
	public static final String DEFAULT_OUTPUT_PATH = "./result/";
	public static final String DEFAULT_FILE_PREFIX = "FameResult";
	public static final String AGENT_PACKAGE_NAME = "de.dlr.gitlab.fame.agent";
	public static final String MESSAGE_PACKAGE_NAME = "de.dlr.gitlab.fame.communication.message";
	public static final String PORTABLE_PACKAGE_NAME = null;
	public static final boolean DEFAULT_FILE_TIMESTAMP = true;
}