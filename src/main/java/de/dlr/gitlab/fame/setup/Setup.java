package de.dlr.gitlab.fame.setup;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.logging.Logging;

/** Contains setup information about packages and paths
 * 
 * @author Christoph Schimeczek */
public class Setup {
	private static Logger logger = LoggerFactory.getLogger(Setup.class);
	public static final String MISSING_AGENT_PACKAGES = "No package names for agents defined";
	public static final String MISSING_MESSAGE_PACKAGES = "No package names for messages defined";

	private final int maxPortableHierarchyDepth = 100;
	private String outputPath = Constants.DEFAULT_OUTPUT_PATH;
	private String outputFilePrefix = Constants.DEFAULT_FILE_PREFIX;
	private boolean outputFileTimeStamp = Constants.DEFAULT_FILE_TIMESTAMP;
	private List<String> agentPackages;
	private List<String> messagePackages;
	private List<String> portablePackages;

	public String getOutputPath() {
		return outputPath;
	}

	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public void setOutputFilePrefix(String outputFilePrefix) {
		this.outputFilePrefix = outputFilePrefix;
	}

	public String getOutputFilePrefix() {
		return outputFilePrefix;
	}

	public boolean getOutputFileTimeStamp() {
		return outputFileTimeStamp;
	}

	public void setOutputFileTimeStamp(boolean outputFileTimeStamp) {
		this.outputFileTimeStamp = outputFileTimeStamp;
	}

	public List<String> getAgentPackages() {
		return agentPackages;
	}

	public void setAgentPackages(List<String> agentPackages) {
		this.agentPackages = agentPackages;
	}

	public List<String> getMessagePackages() {
		return messagePackages;
	}

	public void setMessagePackages(List<String> messagePackages) {
		this.messagePackages = messagePackages;
	}

	public List<String> getPortablePackages() {
		return portablePackages;
	}

	public void setPortablePackages(List<String> portablePackages) {
		this.portablePackages = portablePackages;
	}

	/** @return maximum depth of hierarchy for {@link Portable} classes */
	public int getMaxPortableHierarchyDepth() {
		return maxPortableHierarchyDepth;
	}

	/** ensures that all required lists of packages are specified and or not null */
	public void ensurePackageListsAreConsistent() {
		if (agentPackages == null || agentPackages.isEmpty()) {
			throw Logging.logFatalException(logger, MISSING_AGENT_PACKAGES);
		}
		if (messagePackages == null || messagePackages.isEmpty()) {
			throw Logging.logFatalException(logger, MISSING_MESSAGE_PACKAGES);
		}
		if (portablePackages == null) {
			portablePackages = new ArrayList<String>();
		}
	}
}