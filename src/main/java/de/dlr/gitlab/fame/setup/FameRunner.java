package de.dlr.gitlab.fame.setup;

import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiInstantiator;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;
import de.dlr.gitlab.fame.service.Simulator;
import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/** Main class to load setup and execute FAME
 * 
 * @author Christoph Schimeczek */
public class FameRunner implements Runnable {
	@Parameters(paramLabel = "MpiArgs",
			description = "3 positional arguments to specify when PARALLEL mode is used (<myrank> <conf_file> <device_name>)") String[] mpiArgs;

	@Option(names = {"-h", "--help"}, usageHelp = true,
			description = "show a help message") private boolean helpRequested = false;

	@Option(names = {"-m", "--mode"},
			description = "select execution mode: [SINGLE_CORE(default), PARALLEL]") private MpiMode mode = MpiMode.SINGLE_CORE;

	@Option(names = {"-s", "--setup"},
			description = "path to fame setup file") private String pathToSetup = "./fameSetup.yaml";

	@Option(names = {"-f", "--file"}, required = true,
			description = "path to input protobuf file") private String pathToInputFile;

	/** execute FAME
	 * 
	 * @param args arguments for MPI and named options */
	public static void main(String[] args) {
		int exitCode = new CommandLine(new FameRunner()).execute(args);
		System.exit(exitCode);
	}

	@Override
	public void run() {
		MpiFacade mpi = MpiInstantiator.getMpi(mode);
		mpi.initialise(mpiArgs);

		Simulator simulator = new Simulator(mpi, pathToInputFile, pathToSetup);
		simulator.warmUp();
		simulator.run();

		mpi.finalize();
	}
}