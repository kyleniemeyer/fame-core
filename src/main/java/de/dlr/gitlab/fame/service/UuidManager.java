package de.dlr.gitlab.fame.service;

/**
 * Generates universal unique IDs for objects in the simulation. Each MPI-rank has {@link #ADDRESS_SPACE} UUIDs at
 * disposal starting at (rank+1) * {@link #ADDRESS_SPACE}. The first block of UUIDs from 0 to {@link #ADDRESS_SPACE} - 1
 * is reserved for external parameterised IDs from the configuration.
 * 
 * @author Christoph Schimeczek
 */
public class UuidManager {
	public static final long ADDRESS_SPACE = (long) Integer.MAX_VALUE;
	private long localObjectCount = 0;
	private final long processIdOffset;

	UuidManager(int mpiRank) {
		processIdOffset = (mpiRank + 1L) * ADDRESS_SPACE;
	}

	/**
	 * Returns the next generated UUID for this simulation
	 * 
	 * @return next generated UUID for this simulation
	 */
	long generateNext() {
		return processIdOffset + localObjectCount++;
	}
}