package de.dlr.gitlab.fame.service;

import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle.Builder;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.ProtoSetup;
import de.dlr.gitlab.fame.protobuf.Services.ScheduledTime;
import de.dlr.gitlab.fame.protobuf.Services.WarmUpMessage;
import de.dlr.gitlab.fame.setup.Setup;

/** Abstract representation of all services, which
 * <ul>
 * <li>are coordinated by the {@link Simulator}</li>
 * <li>exist on all simulation processes</li>
 * <li>coordinate via MPI</li>
 * <li>are known by all process-local agents</li>
 * </ul>
 * 
 * @author Christoph Schimeczek */
public abstract class Service {
	protected MpiManager mpi;
	private Builder bundleBuilder = Bundle.newBuilder();
	private de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage.Builder mpiMessageBuilder = MpiMessage.newBuilder();

	/** Create Service connected to the specified {@link MpiManager}
	 * 
	 * @param mpi an MpiManager to coordinate inter-process communication */
	public Service(MpiManager mpi) {
		this.mpi = mpi;
	}

	/** Create a {@link Bundle} to transmit protobuf input data
	 * 
	 * @param inputData to be transmitted
	 * @return created Bundle containing the given data */
	protected Bundle createBundleForInput(InputData inputData) {
		clearMessageBuilders();
		mpiMessageBuilder.setInput(inputData);
		return buildBundleFromBuilders();
	}

	private void clearMessageBuilders() {
		bundleBuilder.clear();
		mpiMessageBuilder.clear();
	}

	private Bundle buildBundleFromBuilders() {
		return bundleBuilder.addMessage(mpiMessageBuilder).build();
	}

	/** Returns a {@link Bundle} to transmit ScheduledTimes
	 * 
	 * @param scheduledTimeBuilder builder for protobuf ScheduledTimes
	 * @return created Bundle */
	protected Bundle createBundleForScheduledTime(ScheduledTime.Builder scheduledTimeBuilder) {
		clearMessageBuilders();
		mpiMessageBuilder.setScheduledTime(scheduledTimeBuilder);
		return buildBundleFromBuilders();
	}

	/** Returns a {@link Bundle} to transmit WarmUpMessages
	 * 
	 * @param warmUpBuilder builder for protobuf WarmUpMessages
	 * @return created Bundle */
	public Bundle createBundleForWarmUpMessage(WarmUpMessage.Builder warmUpBuilder) {
		clearMessageBuilders();
		mpiMessageBuilder.setWarmUp(warmUpBuilder);
		return buildBundleFromBuilders();
	}

	/** Returns a {@link Bundle} to transmit AddressBook data
	 * 
	 * @param addressBookBuilder builder for protobuf AddressBooks
	 * @return created Bundle */
	protected Bundle createBundleForAddressBook(AddressBook.Builder addressBookBuilder) {
		clearMessageBuilders();
		mpiMessageBuilder.setAddressBook(addressBookBuilder);
		return buildBundleFromBuilders();
	}

	/** Returns a {@link Bundle} to transmit Output dta
	 * 
	 * @param outputBuilder builder for protobuf Output
	 * @return created Bundle */
	protected Bundle createBundleForOutput(Output.Builder outputBuilder) {
		clearMessageBuilders();
		mpiMessageBuilder.setOutput(outputBuilder);
		return buildBundleFromBuilders();
	}

	/** Returns a {@link Bundle} to transmit simulation {@link Setup} data
	 * 
	 * @param setupBuilder builder for protobuf ProtoSetup
	 * @return created Bundle */
	protected Bundle createBundleForSetup(ProtoSetup.Builder setupBuilder) {
		clearMessageBuilders();
		mpiMessageBuilder.setSetup(setupBuilder);
		return buildBundleFromBuilders();
	}
}