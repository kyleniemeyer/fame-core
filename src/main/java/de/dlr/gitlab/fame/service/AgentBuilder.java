package de.dlr.gitlab.fame.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;
import de.dlr.gitlab.fame.logging.Logging;

/** Builds {@link Agent}s from given input
 * 
 * @author Christoph Schimeczek */
public final class AgentBuilder {
	static final String ERR_CREATION_FAILED = "Could not create Agent of type ";
	static final String ERR_ABSTRACT = "Is your class abstract?";
	static final String ERR_NOT_PUBLIC = "Constructor must be public!";
	static final String ERR_CLASS_NOT_FOUND = "Agent class not found: ";
	static final String ERR_NO_CONSTRUCTOR = "Public constructor with argument DataProvider is missing for class: ";
	static final String ERR_FAME_BROKEN = "This should not have happend. Please contact FAME developers.";
	static final String AGENT_CREATED = " created.";

	private static Logger logger = LoggerFactory.getLogger(AgentBuilder.class);
	private final Map<Integer, TimeSeries> mapOfTimeSeries;
	private HashMap<String, Class<? extends Agent>> mapOfAgentClasses = new HashMap<>();
	private final int processCount;
	private final int processRank;
	private final LocalServices localServices;

	AgentBuilder(MpiManager mpiManager, List<String> agentPackages, Map<Integer, TimeSeries> mapOfSeries,
			LocalServices localServices) {
		processCount = mpiManager.getProcessCount();
		processRank = mpiManager.getRank();
		this.mapOfTimeSeries = mapOfSeries;
		this.mapOfAgentClasses = mapAgentTypesToNames(agentPackages);
		this.localServices = localServices;
	}

	/** Uses {@link Reflection} to map all detected {@link Agent}-types in the specified packages to their simple name */
	private HashMap<String, Class<? extends Agent>> mapAgentTypesToNames(List<String> agentPackages) {
		Reflection reflection = new Reflection(ReflectionType.AGENT, agentPackages);
		return reflection.findAllChildsOfByName(Agent.class);
	}

	/** Builds {@link Agent agents} of this MPI process based on given {@link AgentDao} and registers them at simulation
	 * {@link Service services}
	 * 
	 * @return List of newly created, local {@link Agent}s */
	void createAgentsFromConfig(List<AgentDao> agentConfigs) {
		for (int agentCount = 0; agentCount < agentConfigs.size(); agentCount++) {
			AgentDao agentConfig = agentConfigs.get(agentCount);
			if (!buildAgentOnThisProcess(agentCount)) {
				continue;
			}
			Agent agent = createAgentFromConfig(agentConfig);
			logger.info(agent + AGENT_CREATED);
		}
	}

	/** Simple distribution method: Equal distribution of agents on all nodes
	 * 
	 * @return true if agent with specified ID is to be hosted on this node */
	private boolean buildAgentOnThisProcess(int counter) {
		return (counter % processCount) == processRank;
	}

	private Agent createAgentFromConfig(AgentDao agentConfig) {
		Class<?> clas = getClassFor(agentConfig.getClassName());
		Constructor<?> constructor = getConstructorFor(clas);
		return instantiate(constructor, agentConfig);
	}

	/** @return {@link Class} corresponding to the specified simple className in package agent */
	private Class<?> getClassFor(String className) {
		Class<?> clas = mapOfAgentClasses.get(className);
		if (clas == null) {
			throw Logging.logFatalException(logger, ERR_CLASS_NOT_FOUND + className);
		}
		return clas;
	}

	/** @return {@link Constructor} for the specified class extending {@link Agent} */
	private Constructor<?> getConstructorFor(Class<?> clas) {
		try {
			return clas.getDeclaredConstructor(DataProvider.class);
		} catch (NoSuchMethodException | SecurityException e) {
			throw Logging.logFatalException(logger, ERR_NO_CONSTRUCTOR + clas.getSimpleName());
		}
	}

	/** Instantiates a class using the given {@link Constructor} with the specified argument {@link AgentDao} */
	private Agent instantiate(Constructor<?> constructor, AgentDao agentConfig) {
		try {
			DataProvider inputParameters = new DataProvider(agentConfig, mapOfTimeSeries, localServices);
			return (Agent) constructor.newInstance(inputParameters);
		} catch (IllegalArgumentException e) {
			throw Logging.logFatalException(logger, ERR_FAME_BROKEN);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			String message = ERR_CREATION_FAILED + agentConfig.getClassName() + ": ";
			if (e instanceof InvocationTargetException) {
				message += e.getCause().getMessage();
			} else if (e instanceof IllegalAccessException) {
				message += ERR_NOT_PUBLIC;
			} else if (e instanceof InstantiationException) {
				message += ERR_ABSTRACT;
			}
			throw Logging.logFatalException(logger, message);
		}
	}
}