package de.dlr.gitlab.fame.service.scheduling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import de.dlr.gitlab.fame.agent.Agent;

/** Associates Agents and reasons for their scheduling
 * 
 * @author Christoph Schimeczek */
public class ScheduleSlot {
	private final HashMap<Agent, ArrayList<Enum<?>>> entries = new HashMap<>();

	public void addEntry(Agent agent, Enum<?> reason) {
		ensureHasKey(agent);
		ArrayList<Enum<?>> storedReasons = entries.get(agent);
		storedReasons.add(reason);
	}

	/** Adds an empty ArrayList to {@link #entries} if not yet present */
	private void ensureHasKey(Agent agent) {
		if (!entries.containsKey(agent)) {
			entries.put(agent, new ArrayList<Enum<?>>());
		}
	}

	/** Get scheduling reasons stored for given Agent
	 * 
	 * @param agent that is scheduled
	 * @return List of reasons for scheduling */
	public ArrayList<Enum<?>> getReasons(Agent agent) {
		return entries.get(agent);
	}

	/** @return List of all Agents that registered scheduling reasons */
	public Set<Agent> getScheduleAgents() {
		return entries.keySet();
	}
}
