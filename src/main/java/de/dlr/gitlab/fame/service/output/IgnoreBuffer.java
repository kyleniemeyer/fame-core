package de.dlr.gitlab.fame.service.output;

import de.dlr.gitlab.fame.protobuf.Services.Output.Series;

/** An <b>inactive</b> data output buffer of a single agent; all given data is dumped directly
 * 
 * @author Christoph Schimeczek */
public final class IgnoreBuffer extends OutputBuffer {
	static final String EX_NON_REACHABLE = "Must not get called for non-active buffers.";

	/** Constructs a {@link IgnoreBuffer}
	 * 
	 * @param className of the associated agent
	 * @param agentId of the associated agent */
	public IgnoreBuffer(String className, long agentId) {
		super(className, agentId);
	}

	/** dummy method; all given data is <b>not</b> stored but ignored */
	@Override
	public void store(Enum<?> type, double value) {}

	/** dummy method; all given data is <b>not</b> stored but ignored */
	@Override
	public void store(ComplexIndex<?> complexField, Object value) {}

	@Override
	/** must not get called */
	protected Series getSeries() {
		throw new RuntimeException(EX_NON_REACHABLE);
	}

	/** does nothing */
	@Override
	protected void tick(long timeStep) {
		throw new RuntimeException(EX_NON_REACHABLE);
	}
}