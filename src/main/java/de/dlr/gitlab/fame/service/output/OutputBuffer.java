package de.dlr.gitlab.fame.service.output;

import de.dlr.gitlab.fame.protobuf.Services.Output.Series;

/** Abstract data output buffer of a single agent
 * 
 * @author Christoph Schimeczek */
public abstract class OutputBuffer {
	private final String className;
	/** ID of the agent that writes to this {@link OutputBuffer} */
	protected final long agentId;

	/** Constructs an {@link OutputBuffer} for the specified agent with the given className
	 * 
	 * @param className name string of the agent class
	 * @param agentId UUID of the agent with the given class */
	protected OutputBuffer(String className, long agentId) {
		this.className = className;
		this.agentId = agentId;
	}

	/** Stores data in a buffer for later write-out
	 * 
	 * @param column {@link Enum} to identify the column associated with the given value
	 * @param value to be stored in the given column */
	public abstract void store(Enum<?> column, double value);

	/** Stores data of a complex column for later write out
	 * 
	 * @param complexField contains all keys and their values
	 * @param value to be stored in the given column as string - make sure string conversion is meaningful */
	public abstract void store(ComplexIndex<?> complexField, Object value);

	/** Concludes writing of a buffered line
	 * 
	 * @param timeStep current time (at the end) of the tick */
	protected abstract void tick(long timeStep);

	/** Returns the ID of the associated agent
	 * 
	 * @return ID of the associated agent */
	final long getAgentId() {
		return agentId;
	};

	/** Returns class name of the associated agent
	 * 
	 * @return class name of the associated agent */
	final String getClassName() {
		return className;
	}

	/** @return series containing all stored data */
	protected abstract Series getSeries();
}