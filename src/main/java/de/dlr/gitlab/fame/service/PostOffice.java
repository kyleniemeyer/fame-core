package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.MessageBuilder;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle.Builder;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook;
import de.dlr.gitlab.fame.setup.Setup;

/** Manages delivery of messages between agents on any processes
 * 
 * @author Christoph Schimeczek */
public class PostOffice extends Service {
	enum AGENT_STATUS_CHANGE {
		REGISTER, UNREGISTER
	};

	/** stores processID for every {@link Agent} in simulation */
	private final HashMap<Long, Integer> processIdByAgent = new HashMap<>();
	/** unsorted list of all messages from local agents to be dispatched this tick */
	private final ArrayList<ProtoMessage> messageHeap = new ArrayList<>();
	private final HashMap<Long, Agent> localAgents = new HashMap<>();
	private final HashMap<AGENT_STATUS_CHANGE, List<Long>> agentUpdates = new HashMap<>();
	/** stores messages bundled by processID, ready to be built into a {@link Bundle} */
	private final ArrayList<Builder> postBoxesBuilders = new ArrayList<>();
	private final ArrayList<Bundle> postBoxes;
	private final MessageBuilder messageBuilder;

	/** Construct a {@link PostOffice} that uses the specified {@link MpiManager} */
	PostOffice(MpiManager mpi, Setup setup, TimeSeriesProvider timeSeriesProvider) {
		super(mpi);
		messageBuilder = new MessageBuilder(setup, timeSeriesProvider);
		prepareProcessSpecificPostBoxBuilders();
		postBoxes = new ArrayList<>(mpi.getProcessCount());
	}

	/** Creates a new {@link Builder Bundle_builder} for each active process, including own process */
	private void prepareProcessSpecificPostBoxBuilders() {
		for (int processId = 0; processId < mpi.getProcessCount(); processId++) {
			postBoxesBuilders.add(Bundle.newBuilder());
		}
	}

	/** called by {@link Simulator}; delivers messages to their respective target {@link Agent Agents} thereby synchronises across
	 * all processes */
	void deliverMessages() {
		clearPostBoxesBuilders();
		sortMessagesByPostOffice();
		messageHeap.clear();
		buildPostBoxes();

		Bundle myMessageBundle = mpi.individualAllToAll(postBoxes, Tag.POST);
		for (MpiMessage mpiMessage : myMessageBundle.getMessageList()) {
			ProtoMessage message = mpiMessage.getMessage();
			long receiverId = message.getReceiverId();
			Agent receiver = localAgents.get(receiverId);
			Message agentMessage = new Message(message);
			receiver.receive(agentMessage);
		}
	}

	/** clears all {@link #postBoxesBuilders} from messages of previous tick */
	private void clearPostBoxesBuilders() {
		for (Builder postBoxBuilder : postBoxesBuilders) {
			postBoxBuilder.clear();
		}
	}

	/** distributes all {@link ProtoMessage}s of the {@link #messageHeap} to the postBoxBuilder of the receiver's process */
	private void sortMessagesByPostOffice() {
		MpiMessage.Builder mpiMessageBuilder = MpiMessage.newBuilder();
		for (ProtoMessage message : messageHeap) {
			long receiverId = message.getReceiverId();
			int processIdOfReceiver = processIdByAgent.get(receiverId);
			mpiMessageBuilder.clear();
			MpiMessage mpiMessage = mpiMessageBuilder.setMessage(message).build();
			postBoxesBuilders.get(processIdOfReceiver).addMessage(mpiMessage);
		}
	}

	/** Builds {@link Bundle Bundles} from {@link #postBoxesBuilders BundleBuilders} */
	private void buildPostBoxes() {
		postBoxes.clear();
		for (Builder bundleBuilder : postBoxesBuilders) {
			postBoxes.add(bundleBuilder.build());
		}
	}

	/** Register an {@link Agent} at this {@link PostOffice};
	 * 
	 * @param agent to register */
	void registerAgent(Agent agent) {
		Long agentId = agent.getId();
		localAgents.put(agentId, agent);
		addAgentStatusChange(agentId, AGENT_STATUS_CHANGE.REGISTER);
	}
	
	/** adds given status change for the given agentID */
	private void addAgentStatusChange(long agentId, AGENT_STATUS_CHANGE statusChange) {
		agentUpdates.putIfAbsent(statusChange, new ArrayList<>());
		agentUpdates.get(statusChange).add(agentId);
	}

	/** Return true if given agent ID is registered as a local agent
	 * 
	 * @param agentId to check if it is a local agent
	 * @return true if agent ID matches a local agent, false otherwise */
	boolean isLocalAgent(long agentId) {
		return localAgents.containsKey(agentId);
	}

	/** Return true if given agent ID is scheduled for the given status change
	 * 
	 * @param agentId the agent to check for the expected status change
	 * @param update the expected status change
	 * @return true if the given agent is scheduled for the given status change */
	boolean agentHasStatusChange(long agentId, AGENT_STATUS_CHANGE update) {
		List<Long> agentsWithGivenUpdate = agentUpdates.get(update);
		return agentsWithGivenUpdate != null && agentsWithGivenUpdate.contains(agentId);
	}

	/** synchronises updates for address books from all processes */
	void synchroniseAddressBookUpdates() {
		List<Long> myAgentUpdates = getAgentsWithStatusChange(AGENT_STATUS_CHANGE.REGISTER);
		AddressBook.Builder addressBookBuilder = AddressBook.newBuilder().setProcessId(mpi.getRank())
				.addAllAgentId(myAgentUpdates);
		Bundle bundle = createBundleForAddressBook(addressBookBuilder);
		bundle = mpi.aggregateAll(bundle, Tag.ADDRESSES);
		updateAddressBook(bundle);
		agentUpdates.clear();
	}

	/** @return ArrayList with IDs of local agents scheduled with given status change */
	List<Long> getAgentsWithStatusChange(AGENT_STATUS_CHANGE update) {
		return agentUpdates.getOrDefault(update, Collections.<Long>emptyList());		
	}

	/** update own addressBook {@link #processIdByAgent} using the given {@link Bundle} that contains all agents and the processes
	 * they reside on */
	private void updateAddressBook(Bundle bundle) {
		for (MpiMessage mpiMessage : bundle.getMessageList()) {
			AddressBook addresses = mpiMessage.getAddressBook();
			Integer processId = addresses.getProcessId();
			for (Long agentId : addresses.getAgentIdList()) {
				processIdByAgent.put(agentId, processId);
			}
		}
	}

	/** Send a {@link Message} to specified receiver containing all given {@link Portable}s and {@link DataItem}s
	 * 
	 * @param senderId UUID of sending agent
	 * @param receiverId UUID of receiving agent
	 * @param portable 0..1 {@link Portable} to include in message
	 * @param dataItems 0..N {@link DataItem}s to include in message */
	public void sendMessage(long senderId, long receiverId, Portable portable, DataItem... dataItems) {
		messageBuilder.clear();
		messageBuilder.setSenderId(senderId).setReceiverId(receiverId);
		addItems(portable, dataItems);
		post(messageBuilder.build());
	}

	/** adds {@link DataItem}s and {@link Portable}s to the {@link #messageBuilder} */
	private void addItems(Portable portable, DataItem... dataItems) {
		for (DataItem dataItem : dataItems) {
			messageBuilder.add(dataItem);
		}
		if (portable != null) {
			messageBuilder.add(portable);
		}
	}

	/** saves given {@link Message} to {@link #messageHeap} */
	private void post(Message message) {
		ProtoMessage protobufRepresentation = message.createProtobufRepresentation();
		messageHeap.add(protobufRepresentation);
	}
}