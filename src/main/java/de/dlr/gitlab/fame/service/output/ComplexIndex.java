package de.dlr.gitlab.fame.service.output;

import java.util.Arrays;

/** Defines a complex output column that has inner keys to distinguish multiple values
 * 
 * @author A. Achraf El Ghazi, Christoph Schimeczek */
public final class ComplexIndex<T extends Enum<T>> {
	public static final String ERROR_WRONG_ENUM = " was not defined during the original field initialisation with class ";
	public static final String ERROR_MISSING_KEY = "Required key for complex column was not set: ";
	public static final String ERR_NO_OUTPUT = "Column used to declare a ComplexIndex was not an Enum declared with @Output: ";

	private final Enum<?> field;
	private final Class<T> keyClass;
	private final Object[] values;

	/** Defines a {@link ComplexIndex}
	 * 
	 * @param outputColumn Enum entry of the output column that holds the inner map
	 * @param keyClass enum class that defines all required keys */
	private ComplexIndex(Enum<?> outputColumn, Class<T> keyClass) {
		this.field = outputColumn;
		this.keyClass = keyClass;
		values = new Object[keyClass.getEnumConstants().length];
	}

	/** Defines a {@link ComplexIndex}
	 * 
	 * @param <T> enum defining the inner index of the column
	 * @param outputColumn enum column to map the inner index to
	 * @param keyClass enum class defining the inner index of the column
	 * @return new {@link ComplexIndex} */
	public static <T extends Enum<T>> ComplexIndex<T> build(Enum<?> outputColumn, Class<T> keyClass) {
		if (outputColumn.getClass().getAnnotation(Output.class) == null) {
			throw new RuntimeException(ERR_NO_OUTPUT + outputColumn);
		}
		return new ComplexIndex<T>(outputColumn, keyClass);
	}

	/** Set value of one specific key of this {@link ComplexIndex}; call once per key
	 * 
	 * @param key key to set the value for
	 * @param value value of the key
	 * @return this {@link ComplexIndex} to set another key or value */
	public ComplexIndex<T> key(T key, Object value) {
		values[key.ordinal()] = value;
		return this;
	}

	/** @return all key values of the corresponding Enum class */
	Object[] getKeyValues() {
		ensureHasRequiredKeys();
		return values;
	}

	/** @return all key labels of the corresponding Enum class */
	T[] getKeyLabels() {
		return keyClass.getEnumConstants();
	}

	/** @throws RuntimeException if not all required keys are set */
	private void ensureHasRequiredKeys() {
		for (int i = 0; i < values.length; i++) {
			if (values[i] == null) {
				throw new RuntimeException(ERROR_MISSING_KEY + keyClass.getEnumConstants()[i]);
			}
		}
	}

	/** clears all currently stored key bindings and the associated value */
	void clear() {
		Arrays.fill(values, null);
	}

	/** @return name of the field this complex index is associated with */
	String getFieldName() {
		return field.name();
	}

	/** @return associated output column */
	public Enum<?> getField() {
		return field;
	}
}