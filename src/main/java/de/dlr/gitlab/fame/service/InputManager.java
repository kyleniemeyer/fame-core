package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.mpi.Constants;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.OutputParam;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.service.input.FileReader;
import de.dlr.gitlab.fame.logging.Logging;

/** Reads input files on root and distributes data to other processes
 * 
 * @author Christoph Schimeczek */
public class InputManager extends Service {
	public static final String TIME_SERIES_INDEX_COLLISION = "TimeSeries Index already used: ";
	static Logger logger = LoggerFactory.getLogger(InputManager.class);
	private InputData inputData;

	/** Create new InputManager connected to a given {@link MpiManager} */
	InputManager(MpiManager mpi) {
		super(mpi);
	}

	/** Reads given file by name to root's {@link InputData} storage */
	void read(String inputFileName) {
		if (mpi.isRoot()) {
			inputData = FileReader.read(inputFileName);
		}
	}

	/** Distributes {@link InputData} from root to all other processes */
	void distribute() {
		Bundle bundle = null;
		if (mpi.isRoot()) {
			bundle = createBundleForInput(inputData);
		}
		bundle = mpi.broadcast(bundle, Constants.ROOT);
		inputData = bundle.getMessage(0).getInput();
	}

	/** Returns output parameters read from input file
	 * 
	 * @return {@link OutputParam} read from input file */
	OutputParam getOutputParameters() {
		return inputData.getOutput();
	}

	/** Returns simulation parameters read from input file
	 * 
	 * @return {@link SimulationParam} read from input file */
	public SimulationParam getConfig() {
		return inputData.getSimulation();
	}

	/** Returns an unmodifiable Map linking {@link TimeSeries} to their ID as specified in the input file
	 * 
	 * @return unmodifiable {@link Map} linking {@link TimeSeries} to their ID as specified in the input file */
	public Map<Integer, TimeSeries> getTimeSeries() {
		HashMap<Integer, TimeSeries> timeSeriesMap = new HashMap<>();
		for (TimeSeriesDao series : inputData.getTimeSeriesList()) {
			TimeSeries timeSeries = new TimeSeries(series);
			Integer id = series.getSeriesId();
			if (timeSeriesMap.containsKey(id)) {
				throw Logging.logFatalException(logger, TIME_SERIES_INDEX_COLLISION + id);
			}
			timeSeriesMap.put(id, timeSeries);
		}
		return Collections.unmodifiableMap(timeSeriesMap);
	}

	/** Returns List of all {@link AgentDao AgentConfigurations} read from protobuf file
	 * 
	 * @return List of all agent configurations read from protobuf file */
	public List<AgentDao> getAgentConfigs() {
		return inputData.getAgentList();
	}

	/** Returns a List of all {@link ProtoContract}s read from protobuf file
	 * 
	 * @return List of all {@link ProtoContract}s read from protobuf file */
	public ArrayList<ProtoContract> getContractPrototypes() {
		ArrayList<ProtoContract> prototypes = new ArrayList<>();
		prototypes.addAll(inputData.getContractList());
		return prototypes;
	}
}