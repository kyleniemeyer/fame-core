package de.dlr.gitlab.fame.service.scheduling;

import java.util.Map;
import java.util.TreeMap;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Schedule of all actions of local agents
 * 
 * @author Christoph Schimeczek */
public class Schedule {
	public static final String ERR_SCHEDULED_TO_PAST = "Actions can only be scheduled for the future.";

	private final TimeStamp initialTime;
	private final TimeStamp finalTime;
	private TimeStamp currentTime;
	private final TreeMap<TimeStamp, ScheduleSlot> scheduleBoard = new TreeMap<>();

	/** Create a {@link Schedule} with specified initial and final simulation time
	 * 
	 * @param initialTime first TimeStamp to be simulated
	 * @param finalTime last TimeStamp to be simulated */
	public Schedule(long initialTime, long finalTime) {
		this.currentTime = new TimeStamp(initialTime - 1);
		this.finalTime = new TimeStamp(finalTime);
		this.initialTime = new TimeStamp(initialTime);
	}

	/** Register a new action for the specified {@link Agent} at given {@link TimeStamp}
	 * 
	 * @param agent that plans the action
	 * @param scheduledTime TimeStamp for when to execute the action
	 * @param reason hint returned to Agent when called to Action to remind it what it wanted to execute */
	public void addSingleActionAt(Agent agent, TimeStamp scheduledTime, Enum<?> reason) {
		checkScheduleTimeIsInFuture(scheduledTime);
		ensureScheduleContainsKey(scheduledTime);
		ScheduleSlot scheduleSlot = scheduleBoard.get(scheduledTime);
		scheduleSlot.addEntry(agent, reason);
	}

	/** @throws RuntimeException if given {@link TimeStamp} is later than {@link #currentTime} */
	private void checkScheduleTimeIsInFuture(TimeStamp timeStamp) {
		if (timeStamp.isLessEqualTo(currentTime)) {
			throw new RuntimeException(ERR_SCHEDULED_TO_PAST);
		}
	}

	/** Inserts new Array into {@link #scheduleBoard} if it doesn't exist already for the specified scheduled time */
	private void ensureScheduleContainsKey(TimeStamp scheduledTime) {
		if (!scheduleBoard.containsKey(scheduledTime)) {
			scheduleBoard.put(scheduledTime, new ScheduleSlot());
		}
	}

	/** Returns next higher Entry in the schedule; updates {@link #currentTime}; removes this Entry from {@link #scheduleBoard}
	 * 
	 * @return next higher Entry in the schedule or, if no higher entry is present, an empty list of agents */
	public ScheduleSlot getNextScheduledEntry() {
		Map.Entry<TimeStamp, ScheduleSlot> entry = scheduleBoard.higherEntry(currentTime);
		if (entry != null) {
			currentTime = entry.getKey();
			scheduleBoard.remove(currentTime);
			return entry.getValue();
		}
		return new ScheduleSlot();
	}

	/** Returns {@link TimeStamp} of current simulation time
	 * 
	 * @return {@link TimeStamp} of current simulation time */
	public TimeStamp getCurrentTime() {
		return currentTime;
	}

	/** Returns the {@link TimeStamp} of the next higher simulation time in which actions are scheduled, or of a time in a very
	 * distant future, i.e. {@link Long#MAX_VALUE}, if no tasks are scheduled
	 * 
	 * @return {@link TimeStamp} of the next higher simulation time in which actions are scheduled, or of a time in a very distant
	 *         future, i.e. {@link Long#MAX_VALUE}, if no tasks are scheduled */
	public TimeStamp getNextTimeInSchedule() {
		Map.Entry<TimeStamp, ScheduleSlot> entry = scheduleBoard.higherEntry(currentTime);
		if (entry != null) {
			return entry.getKey();
		}
		return new TimeStamp(Long.MAX_VALUE);
	}

	/** In case no actions are scheduled at the given {@link TimeStamp}, an empty event is inserted into this {@link Schedule} if
	 * before {@link #finalTime} of the schedule
	 * 
	 * @param nextScheduledTime TimeStamp to insert an event if none is already present */
	public void insertTimeWhenNecessary(TimeStamp nextScheduledTime) {
		if (nextScheduledTime.isLessEqualTo(finalTime) && !scheduleBoard.containsKey(nextScheduledTime)) {
			scheduleBoard.put(nextScheduledTime, new ScheduleSlot());
		}
	}

	/** Returns true if tasks remain to be scheduled up to final time of the simulation
	 * 
	 * @return true if tasks remain to be scheduled up to final time of the simulation */
	public boolean hasTasksRemaining() {
		return !scheduleBoard.isEmpty() && getNextTimeInSchedule().isLessEqualTo(finalTime);
	}

	/** Returns initial {@link TimeStamp} of simulation
	 * 
	 * @return initial {@link TimeStamp} of simulation */
	public TimeStamp getInitialTime() {
		return initialTime;
	}
}