package de.dlr.gitlab.fame.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Services.ProtoSetup;
import de.dlr.gitlab.fame.setup.Setup;

/** Manages cross-process distribution of the simulation's {@link Setup}
 * 
 * @author Christoph Schimeczek */
public class SetupManager extends Service {
	public static final String ERROR_DEFAULT_SETUP = "No fameSetup.yaml found - proceeding with default.";

	private Setup setup;
	private static List<String> MessagePackageNames = new ArrayList<String>();
	private static Logger logger = LoggerFactory.getLogger(SetupManager.class);

	/** Creates a new {@link SetupManager} using the specified {@link MpiManager} to communicate with other processes
	 * 
	 * @param mpi */
	SetupManager(MpiManager mpi) {
		super(mpi);
	}

	/** Root process loads setup file and saves content to {@link #setup} member
	 * 
	 * @param setupFileName path to setup file */
	public void rootLoadSetup(String setupFileName) {
		if (mpi.isRoot()) {
			setup = loadSetup(setupFileName);
		}
	}

	/** Loads {@link #FAME_SETUP_YAML} from file system; if specified file is not found, a default setup is created
	 * 
	 * @return {@link Setup} */
	private Setup loadSetup(String setupFileName) {
		Yaml yaml = new Yaml(new Constructor(Setup.class));
		try {
			InputStream input = new FileInputStream(new File(setupFileName));
			return yaml.loadAs(input, Setup.class);
		} catch (FileNotFoundException e) {
			logger.error(ERROR_DEFAULT_SETUP);
			return new Setup();
		}
	}

	public void distributeSetup() {
		if (mpi.isRoot()) {
			setup.ensurePackageListsAreConsistent();
			Bundle bundle = createSetupBundle();
			mpi.broadcast(bundle, de.dlr.gitlab.fame.mpi.Constants.ROOT);
		} else {
			Bundle bundle = mpi.broadcast(null, de.dlr.gitlab.fame.mpi.Constants.ROOT);
			this.setup = setupFromProto(bundle.getMessage(0).getSetup());
		}
		SetupManager.MessagePackageNames = setup.getMessagePackages();
	}

	/** Transforms local {@link #setup} into a {@link Bundle} */
	private Bundle createSetupBundle() {
		ProtoSetup.Builder builder = ProtoSetup.newBuilder();
		builder.setOutputPath(setup.getOutputPath());
		builder.setOutputFilePrefix(setup.getOutputFilePrefix());
		builder.setOutputFileTimeStamp(setup.getOutputFileTimeStamp());
		builder.addAllAgentPackageNames(setup.getAgentPackages());
		builder.addAllMessagePackageNames(setup.getMessagePackages());
		builder.addAllPortablePackageNames(setup.getPortablePackages());
		return createBundleForSetup(builder);
	}

	/** Transforms {@link ProtoSetup} into {@link Setup} */
	private Setup setupFromProto(ProtoSetup protobuf) {
		Setup setup = new Setup();
		setup.setOutputPath(protobuf.getOutputPath());
		setup.setOutputFilePrefix(protobuf.getOutputFilePrefix());
		setup.setOutputFileTimeStamp(protobuf.getOutputFileTimeStamp());
		setup.setAgentPackages(protobuf.getAgentPackageNamesList());
		setup.setMessagePackages(protobuf.getMessagePackageNamesList());
		setup.setPortablePackages(protobuf.getPortablePackageNamesList());
		return setup;
	}

	public Setup getSetup() {
		return setup;
	}

	/** Returns an unmodifiable List of all package names containing types derived from {@link DataItem}
	 * 
	 * @return unmodifiable {@link List} of packageName {@link String}s */
	public static List<String> getMessagePackageNames() {
		return Collections.unmodifiableList(MessagePackageNames);
	}
}