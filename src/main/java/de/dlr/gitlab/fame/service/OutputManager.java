package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.HashMap;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.OutputParam;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Services.Output.Builder;
import de.dlr.gitlab.fame.service.input.FileReader;
import de.dlr.gitlab.fame.service.output.ActiveBuffer;
import de.dlr.gitlab.fame.service.output.BufferManager;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.FileWriter;
import de.dlr.gitlab.fame.service.output.Output;
import de.dlr.gitlab.fame.service.output.OutputBuffer;
import de.dlr.gitlab.fame.service.output.OutputSerializer;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.time.TimeStamp;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;

/** Coordinates {@link BufferManager}s on all processes and the {@link OutputSerializer} for output files
 * 
 * @author Christoph Schimeczek */
public class OutputManager extends Service {
	private int centralOutputProcess;
	private OutputSerializer outputSerializer;
	private BufferManager bufferManager = new BufferManager();
	private int outputInterval = 0;

	/** Constructs an {@link OutputManager} that uses the specified {@link MpiManager} */
	OutputManager(MpiManager mpi) {
		super(mpi);
	}

	/** Set relevant output parameters */
	void setOutputParameters(OutputParam outputParameters, Setup setup, String inputFileName) {
		centralOutputProcess = outputParameters.getProcess();
		if (mpi.isRank(centralOutputProcess)) {
			outputSerializer = new OutputSerializer(new FileWriter(setup));
			outputSerializer.writeInput(FileReader.read(inputFileName));
		}
		outputInterval = outputParameters.getInterval();

		Reflection reflection = new Reflection(ReflectionType.AGENT, setup.getAgentPackages());
		HashMap<Class<? extends Agent>, ArrayList<Enum<?>>> outputColumnLists = reflection
				.findEnumHierarchyAnnotatedWithForSubtypes(Output.class, Agent.class);
		HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> complexColumnLists = reflection
				.findComplexIndexHierarchyForAgents();
		bufferManager.setBufferParameters(outputParameters.getActiveClassNameList(), outputColumnLists, complexColumnLists);
	}

	/** tick {@link ActiveBuffer} indices and gather and write data to output files if buffers are full */
	void tickBuffersAndWriteOutData(long tick, TimeStamp timeStamp) {
		bufferManager.finishTick(timeStamp.getStep());
		if (isLastTickInInverval(tick)) {
			writeAllBufferDataToFile();
		}
	}

	/** @return true {@link #outputInterval} ticks have passed in interval */
	private boolean isLastTickInInverval(long tick) {
		return tick % outputInterval == 0;
	}

	/** gather output from all processes and write to file at central process */
	private void writeAllBufferDataToFile() {
		Bundle bundledOutputData = gatherOutputDataAtOutputProcess();
		if (mpi.isRank(centralOutputProcess)) {
			outputSerializer.writeBundledOutput(bundledOutputData);
		}
	}

	/** gather output data from all processes at central output process */
	private Bundle gatherOutputDataAtOutputProcess() {
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		Bundle bundle = createBundleForOutput(outputBuilder);
		return mpi.aggregateMessagesAt(bundle, centralOutputProcess, Tag.OUTPUT);
	}

	/** Flushes all data to the output file and then closes the file output
	 * 
	 * @param timeStamp time of the last tick */
	public void flushAndCloseAll(TimeStamp timeStamp) {
		bufferManager.finishTick(timeStamp.getStep());
		writeAllBufferDataToFile();
		if (outputSerializer != null) {
			outputSerializer.close();
		}
	}

	/** registers given agent and returns dedicated output buffer.
	 * 
	 * @param agent to register
	 * @return created buffer for that agent */
	public OutputBuffer registerAgent(Agent agent) {
		return bufferManager.createBuffer(agent.getClass(), agent.getId());
	}
}