package de.dlr.gitlab.fame.service.output;

import java.util.HashMap;
import java.util.List;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line.Column;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line.Column.Map;

/** A data output buffer of a single agent that buffers data for later storage to disc
 * 
 * @author Christoph Schimeczek, A. Achraf El Ghazi */
public final class ActiveBuffer extends OutputBuffer {
	static final String EX_OVERWRITE_COLUMN = " tried to overwrite stored column: ";
	static final String EX_MIXED_COLUMN = " tried to mix simple and complex output in column: ";
	static final String EX_UNKNOWN_COLUMN = " is not a known column of agent: ";

	private static final HashMap<String, HashMap<String, Integer>> COLUMN_NAME_MAP = new HashMap<>();
	private final HashMap<String, Integer> columnNames;
	private final Series.Builder seriesBuilder = Series.newBuilder();
	private final Line.Builder lineBuilder = Line.newBuilder();
	private final Column.Builder[] columnBuilders;
	private final Map.Builder mapBuilder = Map.newBuilder();

	/** Constructs a {@link ActiveBuffer}
	 * 
	 * @param className of the associated agent
	 * @param agentId of the associated agent
	 * @param columnList all possible output column {@link Enum}s of the associated agent */
	public ActiveBuffer(String className, long agentId, List<Enum<?>> columnList) {
		super(className, agentId);
		columnNames = getOrCreateClassSpecificMap(className);
		updateColumnNamesIfRequired(columnList);
		columnBuilders = new Column.Builder[columnNames.size()];
		for (int i = 0; i < columnNames.size(); i++) {
			columnBuilders[i] = Column.newBuilder();
		}
	}

	/** @return the output columnName to output columnId HashMap for the given class name; if the entry does not exist, a new entry
	 *         is created and returned */
	private HashMap<String, Integer> getOrCreateClassSpecificMap(String className) {
		if (!COLUMN_NAME_MAP.containsKey(className)) {
			COLUMN_NAME_MAP.put(className, new HashMap<String, Integer>());
		}
		return COLUMN_NAME_MAP.get(className);
	}

	/** adds the given output columns to the class specific HashMap, if no mapping exists yet */
	private void updateColumnNamesIfRequired(List<Enum<?>> columnList) {
		if (columnNames.isEmpty() && columnList != null) {
			for (int i = 0; i < columnList.size(); i++) {
				columnNames.put(columnList.get(i).name(), i);
			}
		}
	}

	/** stores data in a buffer for later write-out */
	@Override
	public void store(Enum<?> type, double value) {
		Integer columnId = columnNames.get(type.name());
		if (columnId == null) {
			throw new RuntimeException(type + EX_UNKNOWN_COLUMN + agentId);
		}

		Column.Builder columnBuilder = columnBuilders[columnId];
		if (columnBuilder.hasFieldId()) {
			if (columnBuilder.hasValue()) {
				throw new RuntimeException(agentId + EX_OVERWRITE_COLUMN + type);
			} else {
				throw new RuntimeException(agentId + EX_MIXED_COLUMN + type);
			}
		}

		columnBuilder.setFieldId(columnId).setValue(value);
	}

	@Override
	public void store(ComplexIndex<?> complexIndex, Object value) {
		Integer columnId = columnNames.get(complexIndex.getFieldName());
		if (columnId == null) {
			throw new RuntimeException(complexIndex.getFieldName() + EX_UNKNOWN_COLUMN + agentId);
		}

		Column.Builder columnBuilder = columnBuilders[columnId];
		if (columnBuilder.hasValue()) {
			throw new RuntimeException(agentId + EX_MIXED_COLUMN + complexIndex.getFieldName());
		}

		mapBuilder.setValue(value.toString());
		for (Object indexValue : complexIndex.getKeyValues()) {
			mapBuilder.addIndexValue(indexValue.toString());
		}

		if (!columnBuilder.hasFieldId()) {
			columnBuilder.setFieldId(columnId);
		}
		columnBuilder.addEntry(mapBuilder);
		mapBuilder.clear();
		complexIndex.clear();
	}

	@Override
	protected void tick(long timeStep) {
		addColumnsToLineAndClear();
		if (!lineBuilder.getColumnList().isEmpty()) {
			lineBuilder.setTimeStep(timeStep);
			seriesBuilder.addLine(lineBuilder.build());
			lineBuilder.clear();
		}
	}

	/** adds all non-empty columns to the current line and clears column builders */
	private void addColumnsToLineAndClear() {
		for (int columnId = 0; columnId < columnBuilders.length; columnId++) {
			Column.Builder columnBuilder = columnBuilders[columnId];
			if (columnBuilder.isInitialized()) {
				lineBuilder.addColumn(columnBuilder);
				columnBuilder.clear();
			}
		}
	}

	@Override
	protected Series getSeries() {
		seriesBuilder.setAgentId(getAgentId()).setClassName(getClassName());
		Series series = seriesBuilder.build();
		seriesBuilder.clear();
		return series;
	}
}