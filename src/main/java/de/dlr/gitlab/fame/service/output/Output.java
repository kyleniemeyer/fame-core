package de.dlr.gitlab.fame.service.output;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** Use to annotate output columns of agents
 * 
 * @author Christoph Schimeczek */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Output {}