package de.dlr.gitlab.fame.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;

/** Assigns {@link Contract}s specified in the configuration file to their sender and receiver
 * 
 * @author Christoph Schimeczek */
public class Notary extends Service {
	public static final String AGENT_NOT_FOUND = "Contracted Agent not configured - AgentID: ";
	public static final String NO_PRODUCTS_FOUND = "Could not find any products for class: ";

	private static Logger logger = LoggerFactory.getLogger(Notary.class);
	private final HashMap<Long, Class<?>> allAgentClasses = new HashMap<>();
	private final ArrayList<Agent> localAgents = new ArrayList<>();
	private final HashMap<Class<? extends Agent>, ArrayList<Enum<?>>> instantiableAgentProducts;
	private final ArrayList<Class<?>> instantiableAgents;

	/** Constructs a new {@link Notary}
	 * 
	 * @param mpi MpiManager to coordinate inter-process communication
	 * @param agentPackages list of package names that may contain Agent classes used in this simulation
	 * @param agentConfigs list of protobuf representations of Agent configurations */
	public Notary(MpiManager mpi, List<String> agentPackages, List<AgentDao> agentConfigs) {
		super(mpi);
		Reflection reflection = new Reflection(ReflectionType.AGENT, agentPackages);
		this.instantiableAgents = reflection.findAllInstantiableChildClassesOf(Agent.class);
		this.instantiableAgentProducts = reflection.findEnumHierarchyAnnotatedWithForSubtypes(Product.class, Agent.class);
		registerAllAgents(agentConfigs);
	}

	/** Register all agents by Id and ClassName of this simulation - necessary match Products by agent-Id only */
	private void registerAllAgents(List<AgentDao> agentConfigs) {
		for (AgentDao agentConfig : agentConfigs) {
			storeAgentClassAndId(agentConfig.getId(), agentConfig.getClassName());
		}
	}

	/** Register a new {@link Agent} by Id and {@link Class#getSimpleName() SimpleName} of its class */
	void storeAgentClassAndId(long agentId, String className) {
		for (Class<?> agentClass : instantiableAgents) {
			if (agentClass.getSimpleName().equals(className)) {
				allAgentClasses.put(agentId, agentClass);
				return;
			}
		}
		throw Logging.logFatalException(logger, "Could not find agent class of name:" + className);
	}

	/** Register a local {@link Agent} managed by this {@link Notary}
	 * 
	 * @param agent to be registered */
	public void registerLocalAgent(Agent agent) {
		localAgents.add(agent);
	}

	/** Add all specified {@link Contract}s to their sender and receiver agents' contract list
	 * 
	 * @param contractPrototypes list of protobuf representations for Contracts */
	public void setInitialContracts(ArrayList<ProtoContract> contractPrototypes) {
		ensureAllContractedAgentsExist(contractPrototypes);
		ArrayList<Contract> contractList = buildContractsFromProto(contractPrototypes);
		HashMap<Long, ArrayList<Contract>> contractMap = assignContractsToAgentId(contractList);
		updateContractsOfLocalAgents(contractMap);
	}

	/** Ensures that all referenced {@link Agent}s in the given List of {@link ProtoContract}s have been reported for this
	 * simulation
	 * 
	 * @throws RuntimeException if any contracted sending or receiving agent is not reported */
	private void ensureAllContractedAgentsExist(ArrayList<ProtoContract> contractPrototypes) {
		for (ProtoContract contract : contractPrototypes) {
			ensureAgentIdIsReported(contract.getSenderId());
			ensureAgentIdIsReported(contract.getReceiverId());
		}
	}

	/** Ensures that the specified AgentId has been reported to the {@link Notary} before
	 * 
	 * @throws RuntimeException if given agentId is not contained in {@link #allAgentClasses} */
	private void ensureAgentIdIsReported(long agentId) {
		if (!allAgentClasses.containsKey(agentId)) {
			throw Logging.logFatalException(logger, AGENT_NOT_FOUND + agentId);
		}
	}

	/** @return an ArrayList of {@link Contract}s built from the available data of {@link Agent}s and {@link ProtoContract} */
	private ArrayList<Contract> buildContractsFromProto(ArrayList<ProtoContract> contractPrototypes) {
		ArrayList<Contract> contracts = new ArrayList<>(2 * contractPrototypes.size());
		long contractId = 0;
		for (ProtoContract protoContract : contractPrototypes) {
			Enum<?> product = getProductOfAgent(protoContract.getSenderId(), protoContract.getProductName());
			contracts.add(new Contract(protoContract, contractId, product));
			contractId++;
		}
		return contracts;
	}

	private Enum<?> getProductOfAgent(long senderId, String productName) {
		Class<?> classOfSender = allAgentClasses.get(senderId);
		return getEnumOfProduct(productName, classOfSender);
	}

	private Enum<?> getEnumOfProduct(String productName, Class<?> classOfSender) {
		if (!instantiableAgentProducts.containsKey(classOfSender)) {
			throw Logging.logFatalException(logger, NO_PRODUCTS_FOUND + classOfSender.getSimpleName());
		}
		for (Enum<?> product : instantiableAgentProducts.get(classOfSender)) {
			if (productName.equals(product.name())) {
				return product;
			}
		}
		throw Logging.logFatalException(logger, productName + " is not a known @Product of " + classOfSender.getSimpleName());
	}

	/** @return a HashMap assigning all contracts to both their sender and receiver agent UUIDs */
	private HashMap<Long, ArrayList<Contract>> assignContractsToAgentId(ArrayList<Contract> contractList) {
		HashMap<Long, ArrayList<Contract>> contractMap = new HashMap<>();
		for (Contract contract : contractList) {
			addContractToListOfAgent(contractMap, contract, contract.getSenderId());
			addContractToListOfAgent(contractMap, contract, contract.getReceiverId());
		}
		return contractMap;
	}

	/** adds the given {@link Contract} to the specified HashMap of Contracts for the given agentId */
	private void addContractToListOfAgent(HashMap<Long, ArrayList<Contract>> contractMap, Contract contract,
			long agentId) {
		if (!contractMap.containsKey(agentId)) {
			contractMap.put(agentId, new ArrayList<>());
		}
		ArrayList<Contract> contracts = contractMap.get(agentId);
		contracts.add(contract);
	}

	/** {@link Agent#addContract(Contract) Adds} all contracts to their corresponding {@link Agent} on this process */
	private void updateContractsOfLocalAgents(HashMap<Long, ArrayList<Contract>> contractMap) {
		for (Agent agent : localAgents) {
			ArrayList<Contract> agentContracts = contractMap.getOrDefault(agent.getId(), new ArrayList<>(0));
			for (Contract contract : agentContracts) {
				agent.addContract(contract);
			}
		}
	}
}