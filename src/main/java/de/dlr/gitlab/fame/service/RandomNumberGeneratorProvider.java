package de.dlr.gitlab.fame.service;

import java.util.HashMap;
import java.util.Random;

/**
 * Provides random number generators based on the agent's id and the simulations seed
 *
 * @author Christoph Schimeczek
 */
public class RandomNumberGeneratorProvider {
	private static final long SEED_SHIFT_PER_CALL = 300997L; // some prime number, not Mersenne
	private final long randomSeed;
	private HashMap<Long, Integer> callsByAgentId = new HashMap<>();

	RandomNumberGeneratorProvider(Long randomSeed) {
		this.randomSeed = randomSeed;
	}

	/**
	 * Returns a new {@link Random} seeded according to the agent's id, the simulation seed and that agent's number of calls to that function
	 * 
	 * @param agentId
	 *          UUID of the calling agent
	 * @return a new {@link Random} with a new seed
	 */
	public Random getNewRandomNumberGeneratorForAgent(long agentId) {
		return new Random(updateAndGetNewSeedForAgent(agentId));
	}

	/** @return next seed for the given agent, can be negative due to overflow - that's o.k. */
	private long updateAndGetNewSeedForAgent(long agentId) {
		int previousCallsByAgent = callsByAgentId.getOrDefault(agentId, 0);
		int callsByAgent = previousCallsByAgent + 1;
		callsByAgentId.put(agentId, callsByAgent);
		return randomSeed + agentId + callsByAgent * SEED_SHIFT_PER_CALL;
	}
}