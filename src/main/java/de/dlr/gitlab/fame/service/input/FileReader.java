package de.dlr.gitlab.fame.service.input;

import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.io.Files;
import com.google.protobuf.InvalidProtocolBufferException;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;
import de.dlr.gitlab.fame.service.InputManager;
import de.dlr.gitlab.fame.logging.Logging;

/** Reads given input file
 *
 * @author Christoph Schimeczek */
public abstract class FileReader {
	public static final String INPUT_FILE_NOT_FOUND = "Could not open input file: ";
	public static final String PARSER_ERROR = "Could not parse content of input file: ";

	private static Logger logger = LoggerFactory.getLogger(InputManager.class);

	/** Reads the predefined protobuffer file and returns the corresponding section for input data
	 * 
	 * @param inputFileName path to input file
	 * @return InputData read from file */
	public static InputData read(String inputFileName) {
		byte[] inputAsBytes = readFileToByteArray(inputFileName);
		try {
			return parseByteInput(inputAsBytes);
		} catch (InvalidProtocolBufferException e) {
			throw Logging.logFatalException(logger, PARSER_ERROR + inputFileName);
		}
	}

	/** @return All content of the specified file as byte array */
	private static byte[] readFileToByteArray(String inputFileName) {
		try {
			return Files.toByteArray(new File(inputFileName));
		} catch (IOException e) {
			throw Logging.logFatalException(logger, INPUT_FILE_NOT_FOUND + inputFileName);
		}
	}

	/** @return {@link InputData} of given binary representation of {DataStorage} */
	private static InputData parseByteInput(byte[] inputAsBytes) throws InvalidProtocolBufferException {
		DataStorage dataStorage = DataStorage.parseFrom(inputAsBytes);
		return dataStorage.getInput();
	}
}
