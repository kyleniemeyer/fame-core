package de.dlr.gitlab.fame.service.output;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType.Field;
import de.dlr.gitlab.fame.protobuf.Services.Output.Builder;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;

/** Manages all local output {@link ActiveBuffer}s and their storage operations
 * 
 * @author Christoph Schimeczek */
public class BufferManager {
	static Logger logger = LoggerFactory.getLogger(BufferManager.class);
	private List<String> activeClasses;
	private HashMap<Class<? extends Agent>, ArrayList<Enum<?>>> outputColumnLists;
	private HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> complexColumnLists;
	private ArrayList<OutputBuffer> activeBuffers = new ArrayList<>();
	private HashMap<String, ArrayList<String>> classHeaders = new HashMap<>();
	private ArrayList<String> newClassHeaders = new ArrayList<>();
	private AgentType.Builder agentBuilder = AgentType.newBuilder();
	private AgentType.Field.Builder fieldBuilder = Field.newBuilder();
	private Builder outputBuilder = Output.newBuilder();

	/** Set parameters for output buffer
	 * 
	 * @param activeClasses List of Strings with class names whose output will be written to file<br>
	 *          if <code>null</code>, <b>all</b> available {@link ActiveBuffer}s will write to disc
	 * @param outputColumnLists map of agent classes to their respective output column {@link Enum}s
	 * @param complexColumnLists map of agent class names to their complex output columns */
	public void setBufferParameters(List<String> activeClasses,
			HashMap<Class<? extends Agent>, ArrayList<Enum<?>>> outputColumnLists,
			HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> complexColumnLists) {
		this.activeClasses = activeClasses;
		this.outputColumnLists = outputColumnLists;
		this.complexColumnLists = complexColumnLists;
	}

	/** Register a class instance via class name, agentId and column enum
	 * 
	 * @param classType type of class of the registering agent
	 * @param agentId of the registering agent
	 * @return an OutputBuffer which may be fed with data if the column enum is not null */
	public OutputBuffer createBuffer(Class<?> classType, long agentId) {
		ArrayList<Enum<?>> columnList = outputColumnLists.get(classType);
		String className = classType.getSimpleName();
		if (columnList == null || columnList.isEmpty()) {
			logger.warn("AgentType " + className + " misses entries for output columns.");
			return null;
		}

		OutputBuffer newBuffer;
		if (isActive(className)) {
			storeColumnNamesForClass(className, columnList);
			newBuffer = new ActiveBuffer(className, agentId, columnList);
			activeBuffers.add(newBuffer);
		} else {
			logger.info("Output for AgentType " + className + " suppressed by config.");
			newBuffer = new IgnoreBuffer(className, agentId);
		}
		return newBuffer;
	}

	/** saves output columns names for the specified class */
	private void storeColumnNamesForClass(String className, ArrayList<Enum<?>> columnList) {
		if (!classHeaders.containsKey(className)) {
			newClassHeaders.add(className);
			ArrayList<String> headers = new ArrayList<>();
			for (Enum<?> entry : columnList) {
				headers.add(entry.name());
			}
			classHeaders.put(className, headers);
		}
	}

	/** @return true if the specified class is on the list of classes with activated output */
	boolean isActive(String agentTypeName) {
		return activeClasses == null || activeClasses.size() == 0 || activeClasses.contains(agentTypeName);
	}

	/** Returns content of all connected buffers, clears all buffers, resets buffer index to 0
	 * 
	 * @return content of all connected buffers as {@link Builder} for a {@link Output}-Protobuf */
	public Builder flushAllBuffersToOutputBuilder() {
		outputBuilder.clear();
		outputBuilder.addAllAgentType(getNewAgentTypes());
		outputBuilder.addAllSeries(getSeries());
		return outputBuilder;
	}

	/** @return {@link AgentType}s containing the column names for all local agent types with active output not yet written */
	private ArrayList<AgentType> getNewAgentTypes() {
		ArrayList<AgentType> agentTypes = new ArrayList<>();
		for (String className : newClassHeaders) {
			agentTypes.add(buildProtobufForHeaders(className));
		}
		newClassHeaders.clear();
		return agentTypes;
	}

	/** @return {@link AgentType} header definition based on given className and columnNames using {@link #agentBuilder} &
	 *         {@link #fieldBuilder} */
	private AgentType buildProtobufForHeaders(String className) {
		agentBuilder.clear();
		agentBuilder.setClassName(className);

		ArrayList<String> columnNames = classHeaders.get(className);
		ArrayList<ComplexIndex<? extends Enum<?>>> complexIndices = complexColumnLists.get(className);
		for (int fieldId = 0; fieldId < columnNames.size(); fieldId++) {
			fieldBuilder.clear();
			String columName = columnNames.get(fieldId);
			fieldBuilder.setFieldId(fieldId).setFieldName(columName);
			if (complexIndices != null) {
				for (ComplexIndex<? extends Enum<?>> index : complexIndices) {
					if (columName == index.getFieldName()) {
						for (Enum<?> keyLabel : index.getKeyLabels()) {
							fieldBuilder.addIndexName(keyLabel.name());
						}
					}
				}
			}
			agentBuilder.addField(fieldBuilder.build());
		}
		return agentBuilder.build();
	}

	/** creates an ArrayList of {@link Series} containing the data of all active buffers */
	private ArrayList<Series> getSeries() {
		ArrayList<Series> seriesList = new ArrayList<>();
		for (OutputBuffer buffer : activeBuffers) {
			seriesList.add(buffer.getSeries());
		}
		return seriesList;
	}

	/** Finalises data recording for the current tick in all active {@link OutputBuffer}s
	 * 
	 * @param timeStep time step equivalent of the current tick */
	public void finishTick(long timeStep) {
		for (OutputBuffer buffer : activeBuffers) {
			buffer.tick(timeStep);
		}
	}
}