package de.dlr.gitlab.fame.service.output;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;

/** Manages writing operations of output data to file stream
 * 
 * @author Christoph Schimeczek */
public class OutputSerializer {
	private final HashMap<String, Boolean> headersWrittenForAgent = new HashMap<>();
	private final DataStorage.Builder dataStorageBuilder = DataStorage.newBuilder();
	private final Output.Builder outputBuilder = Output.newBuilder();
	private final FileWriter fileWriter;

	/** Constructs a {@link OutputSerializer} using given FileWriter to write to output files
	 * 
	 * @param fileWriter manages streaming data to output file */
	public OutputSerializer(FileWriter fileWriter) {
		this.fileWriter = fileWriter;
	}

	/** Writes all output data from this Bundle to file
	 * 
	 * @param bundledOutputData Bundle containing data for output */
	public void writeBundledOutput(Bundle bundledOutputData) {
		dataStorageBuilder.clear();
		outputBuilder.clear();
		List<Output> outputs = extractOutputList(bundledOutputData);
		for (Output output : outputs) {
			updateAndAddNewHeadersToOutput(output);
			outputBuilder.addAllSeries(output.getSeriesList());
		}
		dataStorageBuilder.setOutput(outputBuilder);
		DataStorage dataStorage = dataStorageBuilder.build();
		byte[] serialisedData = dataStorage.toByteArray();
		fileWriter.write(serialisedData);
	}

	/** @return List of {@link Output} protobuf extracted from given {@link Bundle} of messages */
	private List<Output> extractOutputList(Bundle bundledOutputData) {
		return bundledOutputData
				.getMessageList()
				.stream()
				.map(MpiMessage::getOutput)
				.collect(Collectors.toList());
	}

	/** adds new {@link AgentType headers} to {@link #dataStorageBuilder} and marks them as written in
	 * {@link #headersWrittenForAgent} */
	private void updateAndAddNewHeadersToOutput(Output output) {
		for (AgentType agentType : output.getAgentTypeList()) {
			String className = agentType.getClassName();
			if (!headersWrittenForAgent.containsKey(className)) {
				outputBuilder.addAgentType(agentType);
				headersWrittenForAgent.put(className, true);
			}
		}
	}

	/** Closes the associated file stream */
	public void close() {
		fileWriter.close();
	}

	/** Stores content of given InputData to the output file
	 * 
	 * @param inputData to be written to the output file */
	public void writeInput(InputData inputData) {
		dataStorageBuilder.clear();
		dataStorageBuilder.setInput(inputData);
		DataStorage dataStorage = dataStorageBuilder.build();
		fileWriter.write(dataStorage.toByteArray());
	}
}