package de.dlr.gitlab.fame.service.output;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.setup.Setup;

/** Writes binary data to an output file
 *
 * @author Christoph Schimeczek, Achraf A. El Ghazi */
public final class FileWriter {
	static final String ERR_ILLEGAL_PATH = "Illegal Path to output file: ";
	static final String ERR_WRITE_FAILED = "Could not write to output file!";
	static final String ERR_CLOSE_FAILED = "Could not close output file!";
	static final String ERR_MISSING_PATH = "Output path is missing, empty or all whitespaces.";
	static final String ERR_MISSING_FILE_PREFIX = "Output file prefix is missing, empty or all whitespaces.";
	static final String WARN_EMPTY_CONTENT = "Output FileWriter received no data to write - nothing written.";

	static final String FILE_HEADER = "famecoreprotobufstreamfile";
	static final String FILE_HEADER_VERSION = "v001";	
	
	private static final String OUT_FILE_ENDING = ".pb";
	private static final String PATH_SEPARATOR = "/";
	private static final String TIMESTAMP_SEPARATOR = ".";
	private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd_HH-mm-ss";

	static Logger logger = LoggerFactory.getLogger(FileWriter.class);

	private final FileOutputStream fileStream;

	/** Create a FileWriter based on the given Setup parameters
	 * 
	 * @param setup defining the output parameters */
	public FileWriter(Setup setup) {
		this(getFileOutputStream(fullFileName(setup)));
	}

	/** Creates a FileWriter for the given fileStream and writes file header
	 * 
	 * @param fileStream to write to */
	FileWriter(FileOutputStream fileStream) {
		this.fileStream = fileStream;
		try {
			fileStream.write((FILE_HEADER + FILE_HEADER_VERSION).getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new RuntimeException(ERR_WRITE_FAILED);
		}
	}

	/** Creates a FileOutputStream for writing data to
	 * 
	 * @param fileName including fully qualified path - overwrites file (if existing)
	 * @return created file stream */
	private static FileOutputStream getFileOutputStream(String fileName) {
		try {
			return new FileOutputStream(fileName);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(ERR_ILLEGAL_PATH + fileName);
		}
	}

	/** @return the full file name, including its path, name-prefix, an optional TimeStamp and the file ending */
	static String fullFileName(Setup setup) {
		String path = setup.getOutputPath();
		if (path == null || path.trim().isEmpty()) {
			throw new IllegalArgumentException(ERR_MISSING_PATH);
		}
		String filePrefix = setup.getOutputFilePrefix();
		if (filePrefix == null || filePrefix.trim().isEmpty()) {
			throw new IllegalArgumentException(ERR_MISSING_FILE_PREFIX);
		}

		String fileName = path + PATH_SEPARATOR + filePrefix;
		if (setup.getOutputFileTimeStamp()) {
			fileName += TIMESTAMP_SEPARATOR + getCurrentTimeStamp();
		}
		return fileName + OUT_FILE_ENDING;
	}

	/** @return {@link String} representing the wall-time in human readable format */
	private static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(TIMESTAMP_FORMAT);
		return sdfDate.format(new Date());
	}

	/** Write an array of bytes to {@link #fileStream}, preceded by the length of the bytes */
	void write(byte[] bytes) {
		if (bytes == null || bytes.length == 0) {
			logger.warn(WARN_EMPTY_CONTENT);
		} else {
			try {
				fileStream.write(byteRepresentationOf(bytes.length));
				fileStream.write(bytes);
			} catch (IOException e) {
				throw new RuntimeException(ERR_WRITE_FAILED, e);
			}
		}
	}

	/** @return byte[] representation of given int in BigEndian */
	private byte[] byteRepresentationOf(int number) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.putInt(number);
		return buffer.array();
	}

	/** Closes the associated file stream */
	void close() {
		try {
			fileStream.close();
		} catch (IOException e) {
			throw new RuntimeException(ERR_CLOSE_FAILED, e);
		}
	}
}
