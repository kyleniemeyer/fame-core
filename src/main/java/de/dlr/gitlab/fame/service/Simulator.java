package de.dlr.gitlab.fame.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.logging.Logging;

/** Runs the simulation ticks and coordinates all {@link Service Services}
 * 
 * @author Christoph Schimeczek */
public class Simulator {
	private static final int MAX_WARM_UP_TICKS = 100;
	private static final Logger logger = LoggerFactory.getLogger(Simulator.class);
	private final boolean processIsRoot;
	private final OutputManager outputManager;
	private final Scheduler scheduler;
	private final PostOffice postOffice;

	/** Sets up a simulation
	 * 
	 * @param mpi MpiManager to coordinate inter-process communication
	 * @param inputFileName path to simulation input data file
	 * @param setupFileName path to FAME setup file */
	public Simulator(MpiFacade mpi, String inputFileName, String setupFileName) {
		MpiManager mpiManager = new MpiManager(mpi);
		processIsRoot = mpiManager.isRoot();

		outputManager = new OutputManager(mpiManager);
		scheduler = new Scheduler(mpiManager);

		Setup setup = loadAndDistributeSetup(mpiManager, setupFileName);
		InputManager inputManager = new InputManager(mpiManager);
		inputManager.read(inputFileName);
		inputManager.distribute();

		TimeSeriesProvider timeSeriesProvider = new TimeSeriesProvider(inputManager.getTimeSeries());
		postOffice = new PostOffice(mpiManager, setup, timeSeriesProvider);

		outputManager.setOutputParameters(inputManager.getOutputParameters(), setup, inputFileName);
		scheduler.initialise(inputManager.getConfig());

		long randomSeed = inputManager.getConfig().getRandomSeed();
		RandomNumberGeneratorProvider rngProvider = new RandomNumberGeneratorProvider(randomSeed);

		List<String> agentPackages = setup.getAgentPackages();
		Notary notary = new Notary(mpiManager, agentPackages, inputManager.getAgentConfigs());
		LocalServices localServices = new LocalServices(scheduler, postOffice, outputManager, notary, timeSeriesProvider,
				rngProvider);

		AgentBuilder agentBuilder = new AgentBuilder(mpiManager, agentPackages, timeSeriesProvider.getMapOfTimeSeries(),
				localServices);
		agentBuilder.createAgentsFromConfig(inputManager.getAgentConfigs());
		localServices.armActions();

		notary.setInitialContracts(inputManager.getContractPrototypes());
	}

	/** Loads simulation Setup from file and distributes it among all nodes;
	 * 
	 * @return {@link Setup} loaded from file */
	static Setup loadAndDistributeSetup(MpiManager mpi, String setupFileName) {
		SetupManager setupManager = new SetupManager(mpi);
		setupManager.rootLoadSetup(setupFileName);
		setupManager.distributeSetup();
		return setupManager.getSetup();
	}

	/** executes initial steps of agents, until all agents report "ready to run" */
	public void warmUp() {
		int warmUpTick = 0;
		while (scheduler.needsFurtherWarmUp()) {
			postOffice.synchroniseAddressBookUpdates();
			postOffice.deliverMessages();
			scheduler.executeWarmUp();
			scheduler.sychroniseWarmUp();

			warmUpTick++;
			if (warmUpTick > MAX_WARM_UP_TICKS) {
				for (Agent agent : scheduler.getRemainingAgentsForWarmUp()) {
					logger.error(Logging.LOGGER_FATAL, "Warm-up not completed for " + agent);
				}
				throw new RuntimeException("Simulation warm-up not completed after " + MAX_WARM_UP_TICKS + " steps.");
			}
		}
		scheduler.sychronise();
		if (processIsRoot) {
			System.out.println("Warm-up completed after " + warmUpTick + " ticks.");
		}
	}

	/** executes the simulation */
	public void run() {
		long startTime = System.currentTimeMillis();

		long tick = 0;
		proceedWithSchedule();
		while (scheduler.hasTasksRemaining()) {
			tick++;
			postOffice.synchroniseAddressBookUpdates();
			postOffice.deliverMessages();
			outputManager.tickBuffersAndWriteOutData(tick, scheduler.getCurrentTime());
			proceedWithSchedule();
		}
		outputManager.flushAndCloseAll(scheduler.getCurrentTime());

		long stopTime = System.currentTimeMillis();
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		Date date = new Date();
		if (processIsRoot) {
			System.out.println(dateFormat.format(date) + " :: Ran " + tick + " ticks in: " + (stopTime - startTime) + " ms");
		}
	}

	/** schedules next actions & synchronises schedulers, so that each scheduler knows the time of the next simulation step. */
	private void proceedWithSchedule() {
		scheduler.scheduleNext();
		scheduler.sychronise();
	}
}