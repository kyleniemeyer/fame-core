package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.Channel;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.service.PostOffice;
import de.dlr.gitlab.fame.service.Scheduler;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;
import de.dlr.gitlab.fame.service.output.OutputBuffer;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Abstract representation of an autonomous agent in the simulation
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public abstract class Agent {
	public static final String ERROR_UNHANDLED_MESSAGE = "Unhandled message from: ";
	public static final String ERR_NO_ACTION = " has no action for reason ";
	public static final String LOG_EXECUTION = " executes contract action at ";
	public static final String INFO_NO_FOLLOW_UP = " schedules no direct subsequent action(s) at ";
	private static final long MESSAGE_OVERFLOW_THRESHOLD = (long) 1E5;

	public enum WarmUpStatus {
		INCOMPLETE, COMPLETED
	};

	protected static Logger logger = LoggerFactory.getLogger(Agent.class);
	private final long id;
	private final LocalServices localServices;
	private final ChannelManager channelManager = new ChannelManager(this);
	private final ActionManager actionManager = new ActionManager();
	private final MessageManager messageManager = new MessageManager(MESSAGE_OVERFLOW_THRESHOLD);
	protected final ContractManager contractManager;
	private OutputBuffer outputBuffer;

	/** Construct an agent from the specified data provider object
	 * 
	 * @param dataProvider input for this agent read from file */
	public Agent(DataProvider dataProvider) {
		id = dataProvider.getAgentId();
		contractManager = new ContractManager(id);
		localServices = dataProvider.getLocalServices();
		outputBuffer = localServices.registerAgent(this);
	}

	/** Returns unique ID of this agent
	 * 
	 * @return Id unique in this simulation */
	public long getId() {
		return id;
	}

	/** Add a {@link Contract} to this {@link Agent}'s list of Contracts
	 * 
	 * @param contract associated with this agent */
	public void addContract(Contract contract) {
		contractManager.addContract(contract);
		messageManager.registerContract(contract);
	}

	/** Write a value to the agent's associated output file.<br>
	 * Value will appear in the specified column and with the time stamp matching the simulation time when this method is called.
	 * 
	 * @param column an Enum denoting the column to associate the value with; Enum definition must be annotated with {@link Output}
	 * @param value value to be written to file */
	protected void store(Enum<?> column, double value) {
		outputBuffer.store(column, value);
	}

	/** Stores data of a complex column for later write out
	 * 
	 * @param complexField that contains the data to write - will be reset during this call - no extra call to clear() is required.
	 * @param value to be stored in the given column as string */
	protected void store(ComplexIndex<?> complexField, Object value) {
		outputBuffer.store(complexField, value);
	}

	/** Internal FAME function - <b>do not call</b> in simulations!<br>
	 * Called by {@link Scheduler} to trigger an action of this agent at a given {@link TimeStamp}.
	 * 
	 * @param reasons list of reasons for why this Agent is being scheduled */
	public final void executeActions(ArrayList<Enum<?>> reasons) {
		TimeStamp currentTime = localServices.getCurrentTime();
		for (Enum<?> reason : reasons) {
			executeAction(reason, currentTime);
		}
		ArrayList<PlannedAction> nextActions = contractManager.updateAndGetNextContractActions(currentTime);
		transmitPlannedActionsToScheduler(currentTime, nextActions);
	}

	/** Executes an Action for a specific scheduling reason at the current time */
	private final void executeAction(Enum<?> reason, TimeStamp currentTime) {
		BiConsumer<ArrayList<Message>, List<Contract>> action = actionManager.getActionFor(reason);
		if (action == null) {
			throw new RuntimeException(this + ERR_NO_ACTION + reason);
		}
		ArrayList<Message> inputMessages = messageManager.getMessagesForTrigger(reason, currentTime);
		List<Contract> contractsToFulfil = contractManager.getContractsForProduct(reason);
		if (logger.isDebugEnabled()) {
			logger.debug(this + LOG_EXECUTION + currentTime);
		}
		action.accept(inputMessages, contractsToFulfil);
	}

	/** Reports given list of planned actions to {@link Scheduler}; issues a warning if no actions reported */
	private void transmitPlannedActionsToScheduler(TimeStamp currentTime, ArrayList<PlannedAction> nextActions) {
		if (nextActions.size() == 0) {
			logger.info(this + INFO_NO_FOLLOW_UP + currentTime);
		} else {
			for (PlannedAction plannedAction : nextActions) {
				localServices.addActionAt(this, plannedAction);
			}
		}
	}

	/** Internal FAME function - <b>do not call</b> in simulations!<br>
	 * Called by {@link PostOffice} to deliver a {@link Message} to this agent.<br>
	 * {@link Channel} (un-)subscriptions are handled by the {@link ChannelManager}; <br>
	 * Messages regarding active {@link Contract}s are handled by the {@link MessageManager}
	 * 
	 * @param message to be received and handled */
	public final void receive(Message message) {
		message = channelManager.handleMessage(message);
		message = messageManager.handleMessage(message);
		if (message != null) {
			handleMessage(message);
		}
	}

	/** Function to react to non-contract messages;<br>
	 * By default, such messages are not handled and an error is logged. Override this method to handle non-contract messages (i.e.
	 * that have no {@link ContractData} item).
	 * 
	 * @param message message to be handled */
	protected void handleMessage(Message message) {
		logger.error(ERROR_UNHANDLED_MESSAGE + message.getSenderId());
	}

	/** Manually dispatch a non-contract {@link Message}. To send a message connected with a contract, use
	 * {@link #fulfilNext(Contract, DataItem...)} instead.
	 * 
	 * @param receiverId the targeted agent to receive this message
	 * @param dataItems the contents of this message */
	protected void sendMessageTo(long receiverId, DataItem... dataItems) {
		localServices.sendMessage(id, receiverId, null, dataItems);
	}

	/** Manually dispatch a non-contract {@link Message}. To send a message connected with a contract, use
	 * {@link #fulfilNext(Contract, DataItem...)} instead.
	 * 
	 * @param receiverId the targeted agent to receive this message
	 * @param portable a single complex {@link Portable} content of this message
	 * @param dataItems the contents of this message */
	protected void sendMessageTo(long receiverId, Portable portable, DataItem... dataItems) {
		localServices.sendMessage(id, receiverId, portable, dataItems);
	}

	/** Fulfills the given {@link Contract} by sending a contract-related {@link Message} to the receiver of the given contract, by
	 * adding to the specified {@link DataItem}s a {@link ContractData}-item which matches the next contracted {@link TimeStamp
	 * time} after the current time.
	 * 
	 * @param contract to be fulfilled
	 * @param dataItems to be sent to the contracted receiver */
	protected final void fulfilNext(Contract contract, DataItem... dataItems) {
		fulfilNext(contract, null, dataItems);
	}

	/** Fulfills the given {@link Contract} by sending a contract-related {@link Message} to the receiver of the given contract, by
	 * adding to the specified {@link DataItem}s a {@link ContractData}-item which matches the next contracted {@link TimeStamp
	 * time} after the current time.
	 * 
	 * @param contract to be fulfilled
	 * @param portable a single complex {@link Portable} to be sent to the receiver
	 * @param dataItems to be sent to the contracted receiver */
	protected final void fulfilNext(Contract contract, Portable portable, DataItem... dataItems) {
		DataItem[] extendedItems = new DataItem[dataItems.length + 1];
		extendedItems[0] = contract.fulfilAfter(now());
		System.arraycopy(dataItems, 0, extendedItems, 1, dataItems.length);
		sendMessageTo(contract.getReceiverId(), portable, extendedItems);
	}

	/** @return the {@link PostOffice} that handle's this agents messages */
	public PostOffice getPostOffice() {
		return localServices.getPostOffice();
	}

	/** <b> Warning: functionality is missing </b>! Create a new {@link Channel} for the given {@link MessageContext}. Use
	 * {@link #publish(MessageContext, DataItem...)} to send messages to registered channel clients.
	 * 
	 * @param context of the channel to be opened */
	protected void openChannel(MessageContext context) {
		channelManager.openChannel(context);
	}

	/** <b> Warning: functionality is missing </b>! If a {@link Channel} exists for the specified {@link MessageContext}, it is
	 * closed and clients are informed. After closing, messages can no longer be published.
	 * 
	 * @param context of the channel to be closed */
	protected void closeChannel(MessageContext context) {
		channelManager.closeChannel(context);
	}

	/** <b> Warning: functionality is missing </b>! Publish the given message on the channel with the given {@link MessageContext}
	 * 
	 * @param context of the channel where this message is to be published
	 * @param dataItems contents of the message to be send to registered channel clients */
	protected void publish(MessageContext context, DataItem... dataItems) {
		// TODO Implement functionality of publishing via channel
		throw new RuntimeException("Publish via channel not implemented!");
	}

	@Override
	public String toString() {
		return "Agent(" + getClass().getSimpleName() + ", " + id + ")";
	}

	/** Executes warm-up functionalities at the beginning of the simulation.<br>
	 * This function is called by the {@link Scheduler} and must not be called by the {@link Agent} itself!<br>
	 * In order to create agent-specific warm-up behaviour override function {@link #warmUp(TimeStamp)}.
	 * 
	 * @param initialTime first time step to be considered by the simulation
	 * @return {@link WarmUpStatus#COMPLETED} if warm-up has been completed<br>
	 *         {@link WarmUpStatus#INCOMPLETE} if more warm-up steps are required */
	public final WarmUpStatus executeWarmUp(TimeStamp initialTime) {
		ArrayList<PlannedAction> nextActions = contractManager.scheduleDeliveriesBeginningAt(initialTime);
		WarmUpStatus needsFurtherWarmUp = warmUp(initialTime);
		transmitPlannedActionsToScheduler(initialTime, nextActions);
		return needsFurtherWarmUp;
	}

	/** <b>Override</b> this method to define agent-specific warm-up. <br>
	 * Executes warm-up functionalities at the beginning of the simulation.
	 * 
	 * @param initialTime first time step to be considered by the simulation
	 * @return {@link WarmUpStatus#COMPLETED} if warm-up has been completed, this method is not called again;<br>
	 *         {@link WarmUpStatus#INCOMPLETE} if more warm-up steps are required, this method is called again after exchanging
	 *         messages with all other {@link Agent}s */
	protected WarmUpStatus warmUp(TimeStamp initialTime) {
		return WarmUpStatus.COMPLETED;
	}

	/** @return the {@link TimeStamp} of the current time as defined by the scheduler */
	protected final TimeStamp now() {
		return localServices.getCurrentTime();
	}

	/** Creates an ActionBuilder to define a new scheduled Action. A scheduled Action will call the specified method when the
	 * defined {@link ActionBuilder#on(Enum) trigger} is met and provide all messages with the defined
	 * {@link ActionBuilder#use(Enum...) product(s)}.
	 * 
	 * @param actionToExecute a function pointer defining which function to be executed when conditions are met
	 * @return new ActionBuilder for the given method */
	protected final ActionBuilder call(BiConsumer<ArrayList<Message>, List<Contract>> actionToExecute) {
		ActionBuilder builder = new ActionBuilder(actionToExecute);
		localServices.registerActionBuilder(this, builder);
		return builder;
	}

	/** Register the scheduled action defined in this builder */
	void activateAction(ActionBuilder builder) {
		builder.arm(contractManager, actionManager, messageManager);
	}

	/** Returns a new random number generator with a new seed. The seeds of the returned generator differ for each call of this
	 * function. The seeds are generated based on the simulation seed specified in the config file and the agent's UUID. Thus, the
	 * sequence of returned number generators is identical for every execution of the simulation with the same simulation seed -
	 * regardless of the number of computational nodes that are used to run the simulation.
	 * 
	 * @return a {@link Random} seeded with an new value that is reproducible independent of the structure of employed computation
	 *         nodes */
	protected final Random getNextRandomNumberGenerator() {
		return localServices.getNewRandomNumberGeneratorForAgent(id);
	}
}