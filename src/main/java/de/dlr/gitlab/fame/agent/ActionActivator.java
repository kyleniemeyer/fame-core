package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.HashMap;

/** Gathers all ActionBuilders for all Agents during their initialisation and arms their actions after the Agents have been built
 *
 * @author Christoph Schimeczek */
public class ActionActivator {
	private HashMap<Agent, ArrayList<ActionBuilder>> buildersPerAgent = new HashMap<>();

	/** Register the given ActionBuilder for the specified Agent to be armed() after the Agent has been initialised
	 * 
	 * @param agent associated with the final action  
	 * @param builder the builder of the action */
	public void registerBuilder(Agent agent, ActionBuilder builder) {
		ensureAgentEntry(agent);
		ArrayList<ActionBuilder> builders = buildersPerAgent.get(agent);
		builders.add(builder);
	}

	/** Ensures that {@link #buildersPerAgent} has an entry for the given Agent */
	private void ensureAgentEntry(Agent agent) {
		if (!buildersPerAgent.containsKey(agent)) {
			buildersPerAgent.put(agent, new ArrayList<>());
		}
	}

	/** Arms all actions of registered Agents and their ActionBuilders; Builders are removed after Building of the actions */
	public void armAllActions() {
		for (Agent agent : buildersPerAgent.keySet()) {
			ArrayList<ActionBuilder> builders = buildersPerAgent.get(agent);
			for (ActionBuilder builder : builders) {
				agent.activateAction(builder);
			}
			builders.clear();
		}
	}
}
