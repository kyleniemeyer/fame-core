package de.dlr.gitlab.fame.agent.input;

import java.util.ArrayList;
import java.util.List;

/** Builds a parameter tree group that contains inner parameter groups and or elements
 *
 * @author Christoph Schimeczek */
public final class GroupBuilder extends ElementBuilder {
	static final String NAME_NOT_UNIQUE = "Name was already used within this group: ";

	private List<ElementOrBuilder> innerElementsOrBuilders = new ArrayList<>();

	@Override
	protected GroupBuilder setName(String name) {
		super.setName(name);
		return this;
	}

	/** Sets this Group to be treated as List: i.e. all inner elements can be repeated in a list format.<br>
	 * If inner items shall be unique - simply do not call this method.<br>
	 * Note that the Tree group annotated with {@link Input} cannot be a list itself.
	 * 
	 * @return the GroupBuilder of which this method was call from */
	@Override
	public GroupBuilder list() {
		super.setList();
		return this;
	}

	/** Adds the given Element(s) or their respective Builders to be inner elements of this group
	 * 
	 * @param items any amount of items to be added to this group as inner elements
	 * @return the GroupBuilder of which this method was call from */
	public GroupBuilder add(ElementOrBuilder... items) {
		for (ElementOrBuilder item : items) {
			ensureUnique(item.getName());
			innerElementsOrBuilders.add(item);
		}
		return this;
	}

	/** Adds the given element under the given name to this group - can be used to insert another parameter tree
	 * 
	 * @param name the new name attached to the given element
	 * @param element (root) element from another parameter tree
	 * @return the GroupBuilder of which this method was call from */
	public GroupBuilder addAs(String name, TreeElement element) {
		ensureNotNullOrEmpty(name);
		ensureUnique(name);
		element.setName(name);
		innerElementsOrBuilders.add(element);
		return this;
	}

	/** @throws RuntimeException if given name is already used for an element in this group */
	private void ensureUnique(String name) {
		for (ElementOrBuilder elementOrBuilder : innerElementsOrBuilders) {
			if (elementOrBuilder.getName().equals(name)) {
				throw new RuntimeException(NAME_NOT_UNIQUE + name);
			}
		}
	}

	@Override
	protected Group build() {
		Group group = new Group();
		fillGroup(group);
		return group;
	}

	private void fillGroup(Group group) {
		super.setAttributes(group);
		ArrayList<TreeElement> innerElements = new ArrayList<>();
		for (ElementOrBuilder elementOrBuilder : innerElementsOrBuilders) {
			TreeElement innerElement = elementOrBuilder.get();
			innerElement.setParent(group);
			innerElements.add(innerElement);
		}
		group.setInnerElements(innerElements);
	}

	/** Returns a new {@link Tree} built from this group and all its inner elements
	 * 
	 * @return created {@link Tree} */
	public Tree buildTree() {
		name = null;
		Tree tree = new Tree();
		fillGroup(tree);
		return tree;
	}
}
