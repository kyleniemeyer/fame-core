package de.dlr.gitlab.fame.agent;

import de.dlr.gitlab.fame.time.TimeStamp;

/** An action planned at a certain {@link TimeStamp} for a List of scheduling reasons
 * 
 * @author Ben Fuchs, Ulrich Frey, Christoph Schimeczek */
public class PlannedAction {
	private final TimeStamp timeStamp;
	private final Enum<?> schedulingReason;

	public PlannedAction(TimeStamp timeStamp, Enum<?> schedulingReason) {
		this.timeStamp = timeStamp;
		this.schedulingReason = schedulingReason;
	}

	public TimeStamp getTimeStamp() {
		return timeStamp;
	}

	public Enum<?> getSchedulingReason() {
		return schedulingReason;
	}
}