package de.dlr.gitlab.fame.agent.exception;

/** Indicates that the requested TimeSeries has not been included in the configuration */
public class MissingSeriesException extends LoggedException {
	private static final long serialVersionUID = 1L;

	public MissingSeriesException(String message) {
		super(message);
	}
}