package de.dlr.gitlab.fame.agent.input;

/** Provides access to parameter tree element builders
 *
 * @author Christoph Schimeczek */
public final class Make {
	static final String DONT_INSTANTIATE = "Do not instantiate ";

	/** Supported input data types */
	public enum Type {
		DOUBLE, INTEGER, LONG, TIMESTAMP, TIMESERIES, STRING, ENUM
	}

	private Make() {
		throw new IllegalStateException(DONT_INSTANTIATE + Make.class.getCanonicalName());
	}

	/** Returns new {@link GroupBuilder} to create a new parameter tree group with the provided name. This {@link GroupBuilder}
	 * should be added to any other GroupBuilder (or root GroupBuilder) without calling its get() method.<br>
	 * Call {@link GroupBuilder#buildTree()} only in case a stand-alone parameter group shall be obtained, e.g. for reuse in
	 * multiple parameter trees.
	 * 
	 * @param name of the parameter group to be created - must not be empty or null
	 * @return new Builder to create the group */
	public static GroupBuilder newGroup(String name) {
		return (new GroupBuilder()).setName(name);
	}

	/** Returns new {@link GroupBuilder} to create a root group of a new parameter tree.<br>
	 * To complete parameter tree definition call {@link GroupBuilder#buildTree()}.
	 * 
	 * @return new {@link GroupBuilder} to create an unnamed group serving as root element of a new parameter tree */
	public static GroupBuilder newTree() {
		return new GroupBuilder();
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter. This {@link ParameterBuilder} should be added to any
	 * GroupBuilder without calling its get() method.<br />
	 * Call {@link ParameterBuilder#get()} only in case a stand-alone Parameter shall be obtained, e.g. for reuse in multiple
	 * parameter trees.<br />
	 * To take effect, Parameter or their Builders need to be added to a ParameterGroup.<br>
	 * One can also use the Type-specific ShortCuts {@link #newInt(String)}, {@link #newDouble(String)}, {@link #newString(String)}
	 * {@link #newSeries(String)} or {@link #newEnum(String, Class)}
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @param type {@link Type} of the parameter - if {@link Type#ENUM} is used ParameterBuilder
	 * @return new {@link ParameterBuilder} to create the parameter */
	private static ParameterBuilder newParam(String name, Type type) {
		return (new ParameterBuilder()).setName(name).setType(type);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#INTEGER}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newInt(String name) {
		return newParam(name, Type.INTEGER);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#DOUBLE}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newDouble(String name) {
		return newParam(name, Type.DOUBLE);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#STRING}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newString(String name) {
		return newParam(name, Type.STRING);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#TIMESERIES}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newSeries(String name) {
		return newParam(name, Type.TIMESERIES);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#LONG}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newLong(String name) {
		return newParam(name, Type.LONG);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#TIMESTAMP}.
	 * 
	 * @param name of the new parameter - must not be empty or null
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static ParameterBuilder newTimeStamp(String name) {
		return newParam(name, Type.TIMESTAMP);
	}

	/** Returns new {@link ParameterBuilder} to create a new parameter of {@link Type#ENUM}.
	 * 
	 * @param <T> any Enum can be used
	 * @param name of the new parameter - must not be empty or null
	 * @param enumType type of Enum that the parameter corresponds to
	 * @return new {@link ParameterBuilder} to create the parameter
	 * @see #newParam(String, Type) */
	public static <T extends Enum<T>> ParameterBuilder newEnum(String name, Class<T> enumType) {
		return newParam(name, Type.ENUM).setEnum(enumType);
	}
}
