package de.dlr.gitlab.fame.agent.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.time.TimeStamp;
import de.dlr.gitlab.fame.logging.Logging;

/** Handles data associated with a {@link Tree}
 * 
 * @author Ulrich Frey, Achraf El Ghazi, Christoph Schimeczek */
public class ParameterData {
	static final String WRONG_TYPE = " is not of type ";
	static final String EX_UNKNOWN_PARAM = "No data or wrong path for parameter: ";
	static final String GROUP_MISSING = "Group could not be found: ";
	static final String DEFAULT_USED = "Default value used for parameter: ";
	static final String GROUP_IS_LIST = "Group is a list - use getGroupList(<Path>) instead for: ";
	static final String GROUP_NOT_LIST = "Group is not a list - use getGroup(<Path>) instead for: ";

	private static final Logger logger = LoggerFactory.getLogger(ParameterData.class);

	public final String parentPath;
	private final HashMap<String, Object> data;

	/** Indicates that data is missing */
	public class MissingDataException extends Exception {
		private static final long serialVersionUID = 1L;

		public MissingDataException(String parameterName) {
			super(parameterName);
		}
	}

	/** Creates a new {@link ParameterData} linking to the given data and path in the associated {@link Tree} */
	ParameterData(HashMap<String, Object> data, String path) {
		this.data = data;
		this.parentPath = path;
	}

	/** Returns a new {@link ParameterData} linking to the subset of data available at the given path of the associated
	 * {@link Tree}.<br>
	 * <b>Disallowed</b> for Groups that are lists - use {@link #getGroupList(String)} for those instead.
	 * 
	 * @param path of the group in the {@link Tree}
	 * @return new {@link ParameterData} linking to the input data subset
	 * @throws MissingDataException if no element address could be found to start with the given path */
	public ParameterData getGroup(String path) throws MissingDataException {
		path = parentPath + path + ".";
		ensurePathHasElements(path);
		ensureNotList(path);
		return new ParameterData(data, path);
	}

	/** @throws MissingDataException if no element address could be found to start with the given path */
	private void ensurePathHasElements(String path) throws MissingDataException {
		if (!hasElements(path)) {
			throw new MissingDataException(GROUP_MISSING + path);
		}
	}

	/** @return true if any entry in {@link #data} starts with the given path */
	private boolean hasElements(String path) {
		return data.keySet().stream().anyMatch(s -> s.startsWith(path));
	}

	/** @throws RuntimeException if a direct subgroup with name "0" is present */
	private void ensureNotList(String path) {
		if (hasElements(path + "0.")) {
			Logging.logAndThrowFatal(logger, GROUP_IS_LIST + path);
		}
	}

	/** Returns a new List of {@link ParameterData}. Each List entry refers to the n-th subset of data available at the given path
	 * of the associated {@link Tree}.<br>
	 * <b>Disallowed</b> for Groups that aren't lists - use {@link #getGroup(String)} for those instead.
	 * 
	 * @param path of the group-list in the {@link Tree}
	 * @return new List of {@link ParameterData} with a separate group for each list entry in the input file
	 * @throws MissingDataException if no element address could be found to start with the given path */
	public List<ParameterData> getGroupList(String path) throws MissingDataException {
		path = parentPath + path + ".";
		ensurePathHasElements(path);
		ensureIsList(path);
		return extractSubgroups(path);
	}

	/** @throws RuntimeException if no direct subgroup with name "0" is present */
	private void ensureIsList(String path) {
		if (!hasElements(path + "0.")) {
			Logging.logAndThrowFatal(logger, GROUP_NOT_LIST + path);
		}
	}

	/** @return new List of {@link ParameterData} - each n-th entry matching the n-th subGroup at the given path */
	private List<ParameterData> extractSubgroups(String path) {
		List<ParameterData> list = new ArrayList<>();
		int listId = 0;
		while (hasElements(path + listId + ".")) {
			list.add(new ParameterData(data, path + listId + "."));
			listId++;
		}
		return list;
	}

	/** @return the value of given class type T if an element is present at the path built from the given subPath and the
	 *         {@link #parentPath} of this {@link ParameterData} (in case this ParamContainer represents a group).
	 * @throws MissingDataException in case no element is present at the specified path
	 * @throws RuntimeException in case element at the specified path cannot be cast to given class type */
	private <T> T getParameterOrThrow(String subPath, Class<T> clas) throws MissingDataException {
		String path = parentPath + subPath;
		if (!data.containsKey(path)) {
			throw new MissingDataException(EX_UNKNOWN_PARAM + path);
		}
		return getParameter(path, clas);
	}

	/** @return data of given class type at the specified address
	 * @throws RuntimeException in case element at the specified path cannot be cast to given class type
	 * @throws NullPointerException in case no element is present at the given address */
	private <T> T getParameter(String address, Class<T> clas) {
		try {
			return clas.cast(data.get(address));
		} catch (ClassCastException e) {
			throw Logging.logFatalException(logger, address + WRONG_TYPE + clas.getSimpleName());
		}
	}

	/** Returns {@link TimeSeries} at the given subPath of the associated {@link Tree} group
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @return stored TimeSeries
	 * @throws MissingDataException in case no data is stored at the given path */
	public TimeSeries getTimeSeries(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, TimeSeries.class);
	}

	/** Returns {@link TimeSeries} at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public TimeSeries getTimeSeriesOrDefault(String subPath, TimeSeries defaultValue) {
		return getOrDefault(subPath, TimeSeries.class, defaultValue);
	}

	/** @return if present: Parameter data of given class type at the specified subPath, given default value otherwise */
	private <T> T getOrDefault(String subPath, Class<T> clas, T defaultValue) {
		String path = parentPath + subPath;
		if (data.containsKey(path)) {
			return getParameter(path, clas);
		} else {
			logger.info(DEFAULT_USED + path + " -> " + defaultValue);
			return defaultValue;
		}
	}

	/** Returns Double at the given subPath of the associated {@link Tree} group
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @return stored double value
	 * @throws MissingDataException in case no data is stored at the given path */
	public Double getDouble(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, Double.class);
	}

	/** Returns Double at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public Double getDoubleOrDefault(String subPath, Double defaultValue) {
		return getOrDefault(subPath, Double.class, defaultValue);
	}

	/** Returns String at the given subPath of the associated {@link Tree} group
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @return stored String
	 * @throws MissingDataException in case no data is stored at the given path */
	public String getString(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, String.class);
	}

	/** Returns String at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public String getStringOrDefault(String subPath, String defaultValue) {
		return getOrDefault(subPath, String.class, defaultValue);
	}

	/** Returns Integer at the given subPath of the associated {@link Tree} group
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @return stored Integer value
	 * @throws MissingDataException in case no data is stored at the given path */
	public Integer getInteger(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, Integer.class);
	}

	/** Returns Integer at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public Integer getIntegerOrDefault(String subPath, Integer defaultValue) {
		return getOrDefault(subPath, Integer.class, defaultValue);
	}

	/** Returns Enum of the given type T at the specified subPath of the associated {@link Tree} group
	 * 
	 * @param <T> type of the enum
	 * @param subPath Path within the associated {@link Tree} group
	 * @param clas type of the Enum to be retrieved
	 * @return stored Enum
	 * @throws MissingDataException in case no data is stored at the given path */
	public <T extends Enum<T>> T getEnum(String subPath, Class<T> clas) throws MissingDataException {
		return getParameterOrThrow(subPath, clas);
	}

	/** Returns Enum of given Type at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param <T> type of the enum
	 * @param subPath Path within the associated {@link Tree} group
	 * @param clas type of the Enum to be retrieved
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public <T extends Enum<T>> T getEnumOrDefault(String subPath, Class<T> clas, T defaultValue) {
		return getOrDefault(subPath, clas, defaultValue);
	}

	/** Returns Long at the given subPath of the associated {@link Tree} group
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @return stored Long value
	 * @throws MissingDataException in case no data is stored at the given path */
	public Long getLong(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, Long.class);
	}

	/** Returns Long at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public Long getLongOrDefault(String subPath, Long defaultValue) {
		return getOrDefault(subPath, Long.class, defaultValue);
	}

	/** Returns {@link TimeStamp} at the given subPath of the associated {@link Tree} group
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @return stored {@link TimeStamp}
	 * @throws MissingDataException in case no data is stored at the given path */
	public TimeStamp getTimeStamp(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, TimeStamp.class);
	}

	/** Returns {@link TimeStamp} at the given subPath of the associated {@link Tree} group or a given default
	 * 
	 * @param subPath Path within the associated {@link Tree} group
	 * @param defaultValue to be returned if no item is found at specified subPath
	 * @return value at given subPath or given default */
	public TimeStamp getTimeStampOrDefault(String subPath, TimeStamp defaultValue) {
		return getOrDefault(subPath, TimeStamp.class, defaultValue);
	}

	/** Returns List of values of given type at the specified subPath of the associated {@link Tree} group.<br>
	 * This method performs a type check ensuring that the specified type applies to all List elements.
	 * 
	 * @param <T> supported types are {@link Integer}, {@link Double}, {@link Long}, {@link TimeStamp}, {@link String} and
	 *          {@link Enum}
	 * @param subPath Path within the associated {@link Tree} group
	 * @param type class specifying the type of list elements
	 * @return List of stored values of given type
	 * @throws MissingDataException in case no data is stored at the given path */
	public <T> List<T> getList(String subPath, Class<T> type) throws MissingDataException {
		return typeCheckList(getListOrThrow(subPath), type);
	}

	/** @return typed list with checked casts for each inner element of given list */
	private <T> List<T> typeCheckList(List<?> list, Class<T> type) {
		List<T> typedList = new ArrayList<>();
		for (Object entry : list) {
			try {
				typedList.add(type.cast(entry));
			} catch (ClassCastException e) {
				throw Logging.logFatalException(logger, WRONG_TYPE + type.getSimpleName());
			}
		}
		return (List<T>) typedList;
	}

	/** @return List stored for the specified subPath or an empty list if nothing is stored
	 * @throws MissingDataException if no value is stored at the given subPath */
	private List<?> getListOrThrow(String subPath) throws MissingDataException {
		return getParameterOrThrow(subPath, List.class);
	}

	/** Returns List of values of given type at the specified subPath of the associated {@link Tree} group or a given default.<br>
	 * This method performs a type check ensuring that the specified type applies to all List elements.
	 * 
	 * @param <T> supported types are {@link Integer}, {@link Double}, {@link Long}, {@link TimeStamp}, {@link String} and
	 *          {@link Enum}
	 * @param subPath Path within the associated {@link Tree} group
	 * @param type class specifying the type of list elements
	 * @param defaultValues List that is to be returned if no item is found at specified subPath
	 * @return List of values - either from the input or the provided default values */
	public <T> List<T> getListOrDefault(String subPath, Class<T> type, List<T> defaultValues) {
		return typeCheckList(getListOrDefault(subPath, defaultValues), type);
	}

	/** @return List for given subPath or the specified default if nothing is stored */
	private List<?> getListOrDefault(String subPath, List<?> defaultValues) {
		return getOrDefault(subPath, List.class, defaultValues);
	}

	/** @return a Set of all parameters (by address) that were retrieved from the input file */
	Set<String> getAvailableParameters() {
		return data.keySet();
	}
}