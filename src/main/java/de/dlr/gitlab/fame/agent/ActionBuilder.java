package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.util.Util;
import de.dlr.gitlab.fame.logging.Logging;

/** Sets up a single scheduled action for an {@link Agent}
 * 
 * @author Christoph Schimeczek */
public class ActionBuilder {
	static final String ARGUMENT_IS_NULL = "Function reference must not be null.";
	static final String MISSING_TRIGGER = "Action cannot be activated: Missing a triggering product.";

	private static final Logger logger = LoggerFactory.getLogger(ActionBuilder.class);
	private final BiConsumer<ArrayList<Message>, List<Contract>> actionToExecute;
	private Enum<?> triggeringProduct;
	private ArrayList<Enum<?>> productsOfInputMessages = new ArrayList<>();

	/** Creates an ActionBuilder to build a scheduled Action calling the given method when the {@link #on(Enum) trigger} is met */
	ActionBuilder(BiConsumer<ArrayList<Message>, List<Contract>> actionToExecute) {
		Util.ensureNotNull(actionToExecute, ARGUMENT_IS_NULL);
		this.actionToExecute = actionToExecute;
	}

	/** Defines a trigger on which the scheduled action is executed.
	 * 
	 * @param triggeringProduct must be a {@link Product} of either this Agent or any other {@link Agent}
	 * @return the same {@link ActionBuilder} to further configure the scheduled action */
	public ActionBuilder on(Enum<?> triggeringProduct) {
		Util.ensureNotNull(triggeringProduct, ARGUMENT_IS_NULL);
		this.triggeringProduct = triggeringProduct;
		return this;
	}

	/** Defines which {@link Message}s are handed over to the function called by this action. Messages of {@link Contract}s that
	 * match the specified {@link Product}s and correspond to times earlier or equal to the current simulation time will be
	 * selected. You can specify more than one product and can also reuse the products for other actions - both as trigger and / or
	 * input.
	 * 
	 * @param productsOfInputMessages a bunch of products to look out for: if any of these products are addressed in a contract, the
	 *          messages referring to those contract(s) are handed over to the associated action.
	 * @return the same {@link ActionBuilder} to further configure the scheduled action */
	public ActionBuilder use(Enum<?>... productsOfInputMessages) {
		for (Enum<?> product : productsOfInputMessages) {
			this.productsOfInputMessages.add(product);
		}
		return this;
	}

	/** Completes the definition of this scheduled action, after which the method will be called whenever its trigger is met. */
	void arm(ContractManager contractManager, ActionManager actionManager, MessageManager messageManager) {
		ensureHasTrigger();
		actionManager.configure(triggeringProduct, actionToExecute);
		contractManager.consider(triggeringProduct);
		messageManager.connectInputsToTrigger(triggeringProduct, productsOfInputMessages);
	}

	/** throws a {@link RuntimeException} if trigger of scheduled action is missing */
	private void ensureHasTrigger() {
		if (triggeringProduct == null) {
			Logging.logAndThrowFatal(logger, MISSING_TRIGGER);
		}
	}
}