package de.dlr.gitlab.fame.agent.input;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Field.NestedField;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.time.TimeStamp;
import de.dlr.gitlab.fame.logging.Logging;

/** Wraps all input data of {@link Agent agents} e.g. from protobuf and local services
 * 
 * @author Christoph Schimeczek */
public class DataProvider {
	static final String UNIMPLEMENTED = "Reading not implemented for DataType ";
	static final String NOT_SINGLE_VALUE = " contains not exactly one value of type ";
	static final String NO_LIST = "Field contains no entry for list: ";
	static final String MISSING_SERIES = "Specified TimeSeries not found for field: ";
	static final String VALUE_NOT_ALLOWED = "Cannot match given String to any allowed value of defined ENUM. String was: ";

	private static Logger logger = LoggerFactory.getLogger(DataProvider.class);

	private final AgentDao agentDao;
	private final LocalServices localServices;
	private final Map<Integer, TimeSeries> mapOfTimeSeries;

	public DataProvider(AgentDao agentDao, Map<Integer, TimeSeries> mapOfTimeSeries, LocalServices localServices) {
		this.agentDao = agentDao;
		this.mapOfTimeSeries = mapOfTimeSeries;
		this.localServices = localServices;
	}

	/** @return Id of the connected Agent as specified in the input file */
	public long getAgentId() {
		return agentDao.getId();
	}

	/** @return List of top-level input parameters for the connected Agent (possibly with further inner components) */
	List<NestedField> getFields() {
		return agentDao.getFieldList();
	}

	/** @return Link to the associated computation node's {@link LocalServices} */
	public LocalServices getLocalServices() {
		return localServices;
	}

	/** @return Object stored for the given field with corresponding detail */
	Object getValue(Parameter parameter, NestedField field) {
		switch (parameter.getDataType()) {
			case DOUBLE:
				return getDoubleOrList(parameter, field);
			case INTEGER:
				return getIntegerOrList(parameter, field);
			case LONG:
				return getLongOrList(parameter, field);
			case TIMESTAMP:
				return getTimeStampOrList(parameter, field);
			case STRING:
				return getStringOrList(parameter, field);
			case TIMESERIES:
				return getTimeSeries(field);
			case ENUM:
				return getEnumOrList(parameter, field);
			default:
				throw Logging.logFatalException(logger, UNIMPLEMENTED + parameter.getDataType());
		}
	}

	/** @return a single Double or List of Doubles (if given parameter is list) from given field */
	private Object getDoubleOrList(Parameter parameter, NestedField field) {
		checkValueCount(parameter, field.getDoubleValueCount());
		return parameter.isList() ? field.getDoubleValueList() : field.getDoubleValue(0);
	}

	/** @throws RuntimeException if no value is present or more than one value but not list */
	private void checkValueCount(Parameter parameter, int valueCount) {
		if (parameter.isList()) {
			if (valueCount == 0) {
				Logging.logAndThrowFatal(logger, NO_LIST + parameter.getAddress());
			}
		} else if (valueCount != 1) {
			Logging.logAndThrowFatal(logger, parameter.getAddress() + NOT_SINGLE_VALUE + parameter.getDataType());
		}
	}

	/** @return a single Integer or List of Integers (if given parameter is list) from given field */
	private Object getIntegerOrList(Parameter parameter, NestedField field) {
		checkValueCount(parameter, field.getIntValueCount());
		return parameter.isList() ? field.getIntValueList() : field.getIntValue(0);
	}

	/** @return a single Long or List of Longs (if given parameter is list) from given field */
	private Object getLongOrList(Parameter parameter, NestedField field) {
		checkValueCount(parameter, field.getLongValueCount());
		return parameter.isList() ? field.getLongValueList() : field.getLongValue(0);
	}

	/** @return a single TimeStamp or List of TimeStamps (if given parameter is list) from given field */
	private Object getTimeStampOrList(Parameter parameter, NestedField field) {
		checkValueCount(parameter, field.getLongValueCount());
		return parameter.isList() ? toTimeStamps(field.getLongValueList()) : new TimeStamp(field.getLongValue(0));
	}

	/** @return a List {@link TimeStamp}s containing {@link TimeStamp} created from given List of long values */
	private List<TimeStamp> toTimeStamps(List<Long> longValues) {
		return longValues.stream().map(x -> new TimeStamp(x)).collect(Collectors.toList());
	}

	/** @return a single String or List of Strings (if given parameter is list) from given field */
	private Object getStringOrList(Parameter parameter, NestedField field) {
		checkValueCount(parameter, field.getStringValueCount());
		return parameter.isList() ? field.getStringValueList() : field.getStringValue(0);
	}

	/** @return a single TimeSeries from given field */
	private TimeSeries getTimeSeries(NestedField field) {
		if (!field.hasSeriesId()) {
			throw Logging.logFatalException(logger, field.getFieldName() + NOT_SINGLE_VALUE + "TimeSeries");
		}
		Integer seriesId = field.getSeriesId();
		if (!mapOfTimeSeries.containsKey(seriesId)) {
			throw Logging.logFatalException(logger, MISSING_SERIES + field.getFieldName());
		}
		return mapOfTimeSeries.get(seriesId);
	}

	/** @return a single Enum or List of Enums (if given parameter is list) from given field */
	private Object getEnumOrList(Parameter parameter, NestedField field) {
		checkValueCount(parameter, field.getStringValueCount());
		Enum<?>[] allowed = parameter.getAllowedValues();
		return parameter.isList() ? toEnums(allowed, field.getStringValueList()) : toEnum(allowed, field.getStringValue(0));
	}

	/** @return List of Enums with given enumNames, each matching an entry in given allowedValues */
	private List<Enum<?>> toEnums(Enum<?>[] allowedValues, List<String> enumNames) {
		return enumNames.stream().map(x -> toEnum(allowedValues, x)).collect(Collectors.toList());
	}

	private Enum<?> toEnum(Enum<?>[] allowedValues, String enumName) {
		for (Enum<?> allowedValue : allowedValues) {
			if (allowedValue.toString().equals(enumName)) {
				return allowedValue;
			}
		}
		throw new IllegalArgumentException(VALUE_NOT_ALLOWED + enumName);
	}
}