package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.Product;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Sorts incoming {@link Message}s according to its configuration and provides them according to scheduling triggers
 * 
 * @author Christoph Schimeczek, Ulrich Frey */
public class MessageManager {
	public static final String NO_MESSAGES = "No messages for action scheduled on product: ";
	public static final String IGNORE_NO_FOLLOW_UP = "Message ignored - No action uses input product: ";
	public static final String IGNORE_UNREGISTERED = "Message ignored - Contains reference to unregistered contract";
	public static final String IGNORE_TRIGGER_DISABLED = "Message ignored - Trigger not contracted: ";
	public static final String MESSAGE_OVERFLOW = "Lots of messages stored! Causes could be many contract partners, "
			+ "many individual messages or a terminated Action that was supposed to use these message. Check Actions triggered by: ";

	private final long overflowThreshold;
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageManager.class);

	/** ContractId -> ProductInContract */
	private final HashMap<Long, Enum<?>> productByContractId = new HashMap<>();
	/** Trigger -> ListOfMessagesWithAssociatedInputs */
	private final HashMap<Enum<?>, ArrayList<Message>> messagesWithProduct = new HashMap<>();
	/** InputProduct -> RequiredByTrigger(s) */
	private final HashMap<Enum<?>, ArrayList<Enum<?>>> triggersForInput = new HashMap<>();
	/** Trigger -> ListOfRequiredInputProducts */
	private final HashMap<Enum<?>, List<Enum<?>>> inputNeededByTrigger = new HashMap<>();

	/** Construct a {@link MessageManager}
	 * 
	 * @param messageOverflowThreshold soft limit of messages per trigger - warning is issued when reached */
	public MessageManager(long messageOverflowThreshold) {
		this.overflowThreshold = messageOverflowThreshold;
	}
	
	/** Returns a List of {@link Message}s that are connected to the given trigger and have a delivery {@link TimeStamp} less than
	 * or equal to the given current time.
	 * 
	 * @return {@link ArrayList} of Messages according to the scheduling trigger valid for times <= currentTime */
	ArrayList<Message> getMessagesForTrigger(Enum<?> trigger, TimeStamp currentTime) {
		ArrayList<Message> messagesToReturn = new ArrayList<>();
		if (requiresInput(trigger)) {
			extractMessagesOfProductBeforeCurrentTime(trigger, currentTime, messagesToReturn);
		}
		return messagesToReturn;
	}

	/** @return true if given trigger requires input */
	private boolean requiresInput(Enum<?> trigger) {
		List<Enum<?>> requiredInputs = inputNeededByTrigger.get(trigger);
		return requiredInputs != null && !requiredInputs.isEmpty();
	}

	/** Retrieves all Messages for the given Product received for delivery times before given current time and moves them from
	 * {@link #messagesWithProduct} to the given target List */
	private void extractMessagesOfProductBeforeCurrentTime(Enum<?> trigger, TimeStamp now, List<Message> target) {
		ArrayList<Message> messagesForProduct = messagesWithProduct.get(trigger);
		if (messagesForProduct == null || messagesForProduct.isEmpty()) {
			LOGGER.warn(NO_MESSAGES + trigger);
		} else {
			extractMessagesBeforeCurrentTime(messagesForProduct, now, target);
		}
	}

	/** Extracts Messages from given storedMessages for delivery times before given current time and moves them to target List */
	private void extractMessagesBeforeCurrentTime(List<Message> storedMessages, TimeStamp now, List<Message> target) {
		ListIterator<Message> iterator = storedMessages.listIterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			TimeStamp deliveryTime = message.getDataItemOfType(ContractData.class).getDeliveryTime();
			if (deliveryTime.isLessEqualTo(now)) {
				target.add(message);
				iterator.remove();
			}
		}
	}

	/** Prepares this MessageManager to store arriving {@link Message}s for the given {@link Contract} */
	void registerContract(Contract contract) {
		Enum<?> product = contract.getProduct();
		productByContractId.put(contract.getContractId(), product);
		addMessageSlotIfTrigger(product);
	}

	/** adds empty List to store incoming messages for given trigger if given trigger requires input */
	private void addMessageSlotIfTrigger(Enum<?> trigger) {
		List<Enum<?>> requiredInputs = inputNeededByTrigger.get(trigger);
		if (requiredInputs != null && !requiredInputs.isEmpty()) {
			if (!messagesWithProduct.containsKey(trigger)) {
				messagesWithProduct.put(trigger, new ArrayList<Message>());
			}
		}
	}

	/** Stores given Message if it is associated with a {@link Contract} of this {@link Agent} and is needed for input of an armed
	 * action;<br />
	 * if message is not associated with any Contract, the message is returned <br/>
	 * in any other case the message is deleted
	 * 
	 * @return the given message if it is not associated with any Contract, otherwise <code>null</code> */
	Message handleMessage(Message message) {
		if (message == null) {
			return null;
		}
		ContractData contractData = message.getDataItemOfType(ContractData.class);
		if (contractData == null) {
			return message;
		}
		Enum<?> product = productByContractId.get(contractData.getContractId());
		if (product == null) {
			LOGGER.error(IGNORE_UNREGISTERED);
			return null;
		}
		List<Enum<?>> associatedTriggers = triggersForInput.get(product);
		if (associatedTriggers == null || associatedTriggers.isEmpty()) {
			LOGGER.error(IGNORE_NO_FOLLOW_UP + product);
			return null;
		}
		storeMessageToTriggers(message, associatedTriggers);
		return null;
	}

	/** saves copy of given message to specified associated triggers; logs error for any trigger that has not been contracted */
	private void storeMessageToTriggers(Message message, List<Enum<?>> triggers) {
		boolean messageUsed = false;
		for (Enum<?> trigger : triggers) {
			ArrayList<Message> messageStorage = messagesWithProduct.get(trigger);
			if (messageStorage == null) {
				LOGGER.error(IGNORE_TRIGGER_DISABLED + trigger);
				continue;
			}
			messageStorage.add(messageUsed ? message.deepCopy() : message);
			warnOnOverflow(messageStorage, trigger);
			messageUsed = true;
		}
	}

	/** raises warning if {@link #overflowThreshold} is exceeded for given trigger */
	private void warnOnOverflow(ArrayList<Message> messageStorage, Enum<?> trigger) {
		if (messageStorage.size() == overflowThreshold + 1) {
			LOGGER.warn(MESSAGE_OVERFLOW + trigger);
		}
	}

	/** Connects a triggering {@link Product}s of {@link Contract}s to their required input Products.<br />
	 * Any connection made previously for the given triggering product is overridden. */
	void connectInputsToTrigger(Enum<?> trigger, List<Enum<?>> requiredInputProducts) {
		removePreviousBindingsInputsToTrigger(trigger);
		bindInputsToTrigger(requiredInputProducts, trigger);
	}

	/** Removes all bindings of input products to given trigger from {@link #triggersForInput} */
	private void removePreviousBindingsInputsToTrigger(Enum<?> trigger) {
		Iterator<Entry<Enum<?>, ArrayList<Enum<?>>>> iterator = triggersForInput.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Enum<?>, ArrayList<Enum<?>>> entry = iterator.next();
			ArrayList<Enum<?>> boundTriggers = entry.getValue();
			boundTriggers.remove(trigger);
			if (boundTriggers.isEmpty()) {
				iterator.remove();
			}
		}
	}

	/** Mutually binds all input products to given trigger and vice versa */
	private void bindInputsToTrigger(List<Enum<?>> requiredInputProducts, Enum<?> trigger) {
		for (Enum<?> inputProduct : requiredInputProducts) {
			getOrInsertBoundTriggers(inputProduct).add(trigger);
		}
		inputNeededByTrigger.put(trigger, requiredInputProducts);
	}

	/** @return ArrayList of bound triggers for given input Product - inserted into {@link #triggersForInput} if not yet present */
	private ArrayList<Enum<?>> getOrInsertBoundTriggers(Enum<?> inputProduct) {
		if (!triggersForInput.containsKey(inputProduct)) {
			triggersForInput.put(inputProduct, new ArrayList<>());
		}
		return triggersForInput.get(inputProduct);
	}
	
	long getOverflowThreshold() {
		return overflowThreshold;
	}
}