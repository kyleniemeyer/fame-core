package de.dlr.gitlab.fame.agent.input;

/** One element of a parameter tree definition or its associated builder
 * 
 * @author Christoph Schimeczek */
public abstract class ElementOrBuilder {
	/** @return a readily built Tree Element */
	protected abstract TreeElement get();

	/** @return name of this Tree Element */
	abstract String getName();
}
