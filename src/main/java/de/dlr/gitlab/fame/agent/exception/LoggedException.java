package de.dlr.gitlab.fame.agent.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Super class of logged exceptions
 * 
 * @author Christoph Schimeczek */
public abstract class LoggedException extends Exception {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(LoggedException.class);

	public LoggedException(String message) {
		super(message);
		logger.error(message);
	}
}