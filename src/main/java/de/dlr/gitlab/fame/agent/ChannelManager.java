package de.dlr.gitlab.fame.agent;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.protobuf.Extension.MessageType;
import de.dlr.gitlab.fame.communication.Channel;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin.Topic;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.logging.Logging;

/** Manages all {@link Channel channels} of an {@link Agent}
 * 
 * @author Christoph Schimeczek, Ben Fuchs */
public class ChannelManager {
	static final String ERROR_WRONG_TOPIC = "Topic not one of SUBSCRIBE / UNSUBSCRIBE.";
	static final String DOUBLE_CHANNEL = "Cannot open channel! Channel already exists with context: ";
	static final String PUBLISH_MISSING = "Cannot publish! Channel context unknown: ";
	static final String REMOVE_MISSING = "Cannot remove channel! Channel context unknown: ";
	static final String SUBSCRIBE_MISSING = " has no channel with context: ";
	
	private static Logger logger = LoggerFactory.getLogger(ChannelManager.class);
	private final HashMap<MessageContext, Channel> channels = new HashMap<>();
	private final Agent owner;

	/** Constructs an {@link ChannelManager} */
	ChannelManager(Agent owner) {
		this.owner = owner;
	}

	/** Creates a new {@link Channel} for the given {@link MessageContext};
	 * 
	 * @throws RuntimeException if a channel already exists for this {@link MessageContext} */
	void openChannel(MessageContext context) {
		if (channels.containsKey(context)) {
			Logging.logAndThrowFatal(logger, DOUBLE_CHANNEL + context);
		}
		channels.put(context, new Channel(owner, context));
	}

	/** If a {@link Channel} exists for the specified {@link MessageContext}, it is closed and removed */
	void closeChannel(MessageContext context) {
		if (channels.containsKey(context)) {
			Channel channel = channels.remove(context);
			channel.close();
		} else {
			logger.warn(REMOVE_MISSING + context);
		}
	}

	/** Handles given {@link Message} if it regards subscription of a {@link Channel} of this {@link Agent}; <br />
	 * if message cannot be handled, the message is returned, otherwise null
	 * 
	 * @return the given message if it has not been handled, otherwise <code>null</code> */
	Message handleMessage(Message message) {
		if (message == null) {
			return null;
		} else if (canHandleMessage(message)) {
			manageSubscriptionMessage(message);
			return null;
		} else {
			return message;
		}
	}

	/** Returns True if the given message regards subscription of a {@link Channel} of this {@link Agent}
	 * 
	 * @return true if {@link Message} is of type {@link MessageType#CHANNEL_MESSAGE CHANNEL} with Topic {@link Topic#SUBSCRIBE
	 *         SUBSCRIBE} or {@link Topic#UNSUBSCRIBE UNSUBSCRIBE} */
	private boolean canHandleMessage(Message message) {
		if (message.containsType(ChannelAdmin.class)) {
			ChannelAdmin channelAdminData = message.getDataItemOfType(ChannelAdmin.class);
			return channelAdminData.topic == Topic.SUBSCRIBE || channelAdminData.topic == Topic.UNSUBSCRIBE;
		}
		return false;
	}

	/** Subscribes or unsubscribes sender of given {@link ChannelMessage message} to or from one of the open {@link Channel}s
	 * corresponding to the {@link MessageContext} specified in message;
	 * <p>
	 * If the corresponding channel is not open, an error is logged and a message informing about Channel closure is returned to the
	 * sender of the original subscription message
	 * 
	 * @throws RuntimeException if the message's {@link Topic} is not {@link Topic#SUBSCRIBE SUBSCRIBE} or {@link Topic#UNSUBSCRIBE
	 *           UNSUBSCRIBE} */
	private void manageSubscriptionMessage(Message message) {
		ChannelAdmin channelAdmin = message.getDataItemOfType(ChannelAdmin.class);
		MessageContext messageContext = channelAdmin.context;
		Channel channel = channels.get(messageContext);
		if (channel == null) {
			logger.error(owner + SUBSCRIBE_MISSING + messageContext);
			submitCloseMessage(message, messageContext);
			return;
		}
		long senderId = message.getSenderId();
		switch (channelAdmin.topic) {
			case SUBSCRIBE:
				channel.subscribe(senderId);
				break;
			case UNSUBSCRIBE:
				channel.unsubscribe(senderId);
				break;
			default:
				throw Logging.logFatalException(logger, ERROR_WRONG_TOPIC);
		}
	}

	/** Submits a {@link Topic#CLOSE}-message to sender of specified message */
	private void submitCloseMessage(Message message, MessageContext channelName) {
		owner.getPostOffice().sendMessage(owner.getId(), message.getSenderId(), null,
				new ChannelAdmin(Topic.CLOSE, channelName));
	}

	/** Publishes the given {@link Message} on the channel of its {@link MessageContext context}
	 * 
	 * @throws RuntimeException if no channel exists for the context of the given message */
	void publish(MessageContext context, DataItem... dataItems) {
		Channel channel = channels.get(context);
		if (channel == null) {
			Logging.logAndThrowFatal(logger, PUBLISH_MISSING + context);
		}
		channel.publish(dataItems);
	}
}