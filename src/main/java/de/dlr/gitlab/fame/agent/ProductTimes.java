package de.dlr.gitlab.fame.agent;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.lang3.Validate;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Holds all products and their next execution time
 * 
 * @author Christoph Schimeczek */
public class ProductTimes {
	static final String ERROR_NULL_PRODUCT = "ProductTimes cannot store a NULL Product.";
	static final String ERROR_NULL_TIME = "ProductTimes cannot assign to a NULL TimeStamp.";
	
	private final HashMap<TimeStamp, ArrayList<Enum<?>>> productTimes = new HashMap<>();

	/** Extracts all Products for the given time; calling this function again with same time will result in an empty list
	 * 
	 * @param timeStamp to check for products
	 * @return List of all Products at the given time, list is empty if no Products were registered */
	ArrayList<Enum<?>> drawProductsForTime(TimeStamp timeStamp) {
		ArrayList<Enum<?>> products = productTimes.remove(timeStamp);
		return products != null ? products : new ArrayList<>(0);
	}

	/** Link the given Product with the given time
	 * 
	 * @param timeStamp to be associated with the product
	 * @param product to be associated with the time - must not be null */
	void linkProductToTime(TimeStamp timeStamp, Enum<?> product) {
		Validate.notNull(product, ERROR_NULL_PRODUCT);
		Validate.notNull(timeStamp, ERROR_NULL_TIME);
		productTimes.putIfAbsent(timeStamp, new ArrayList<Enum<?>>());
		productTimes.get(timeStamp).add(product);
	}
}
