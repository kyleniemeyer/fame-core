package de.dlr.gitlab.fame.agent.input;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import de.dlr.gitlab.fame.agent.Agent;

/** Use to annotate input {@link Tree}s of {@link Agent}s
 * 
 * @author Christoph Schimeczek */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.FIELD)
public @interface Input {}