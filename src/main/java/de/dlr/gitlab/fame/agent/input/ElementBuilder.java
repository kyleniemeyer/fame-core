package de.dlr.gitlab.fame.agent.input;

import javax.lang.model.SourceVersion;

/** Builds a {@link TreeElement}
 *
 * @author Christoph Schimeczek */
public abstract class ElementBuilder extends ElementOrBuilder {
	static final String NAME_NEEDED = "Name must not be null or empty.";
	static final String NAME_ILLEGAL = "Name must be a valid Java identifier but is: ";

	protected String name;
	private boolean isList = false;

	@Override
	protected TreeElement get() {
		return build();
	}

	/** @return {@link TreeElement} built from this builder */
	protected abstract TreeElement build();

	String getName() {
		return name;
	}

	public abstract ElementBuilder list();

	/** sets {@link #isList} to true */
	protected void setList() {
		isList = true;
	}

	/** Sets {@link #name} to give value if not null or empty
	 * 
	 * @param name to be assigned to this element
	 * @return the ElementBuilder this function is called from */
	protected ElementBuilder setName(String name) {
		ensureNotNullOrEmpty(name);
		ensureIsValidIdentifier(name);
		this.name = name;
		return this;
	}

	/** @throws RuntimeException if given String is null or empty
	 * @param name to be checked */
	protected void ensureNotNullOrEmpty(String name) {
		if (name == null || name.isEmpty()) {
			throw new RuntimeException(NAME_NEEDED);
		}
	}

	/** @throws RuntimeException if given String has illegal characters */
	private void ensureIsValidIdentifier(String name) {
		if (!SourceVersion.isName(name)) {
			throw new RuntimeException(NAME_ILLEGAL + name);
		}
	}

	/** Copies this Builder's attributes (name, isList) to the given Element
	 * 
	 * @param element to filled with this Builder's attributes */
	protected void setAttributes(TreeElement element) {
		element.setName(name);
		element.setIsList(isList);
	}
}
