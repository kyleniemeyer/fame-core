package de.dlr.gitlab.fame.agent.input;

/** The root group of a Parameter Tree - containing {@link Group}s and / or {@link Parameter}s
 *
 * @author Christoph Schimeczek */
public final class Tree extends Group {
	/** Reads input data from the given {@link DataProvider}, checks them for completeness and stores them into a
	 * {@link ParameterData} for easy access. Throws a {@link RuntimeException} in case a Parameter is mandatory but not present in
	 * the given {@link DataProvider}.
	 * 
	 * @param dataProvider a {@link DataProvider} with content with matching structure of this {@link Tree}
	 * @return {@link ParameterData} filled with the data of the given {@link DataProvider} */
	public ParameterData join(DataProvider dataProvider) {
		return Joiner.join(this, dataProvider);
	}
}
