package de.dlr.gitlab.fame.agent.input;

import java.util.List;

/** One element of a parameter tree definition
 * 
 * @author Christoph Schimeczek */
public abstract class TreeElement extends ElementOrBuilder {
	static final String ADDRESS_SEPARATOR = ".";

	private TreeElement parent;
	private String name;
	private boolean isList;

	/** @return {@link #deepCopy()} of this TreeElement */
	@Override
	protected final TreeElement get() {
		return deepCopy();
	}

	/** @return a deep copy of this TreeElement without a parent */
	abstract TreeElement deepCopy();

	/** Sets this members values to the provided copy
	 * 
	 * @param copy to be filled with this elements attributes */
	protected final void setAttributes(TreeElement copy) {
		copy.setName(name);
		copy.setIsList(isList);
	}

	/** @return parent element if any - or null if element {@link #isRoot()} */
	final TreeElement getParent() {
		return parent;
	}

	/** Defines the given Element as parent of this Element
	 * 
	 * @param parent element to be the parent of this */
	protected final void setParent(TreeElement parent) {
		this.parent = parent;
	}

	/** @return name of this element or null if {@link #isRoot()} */
	protected final String getName() {
		return name;
	}

	final void setName(String name) {
		this.name = name;
	}

	/** @return true if this element or its inner Elements can be repeatedly defined */
	final boolean isList() {
		return isList;
	}

	final void setIsList(boolean isList) {
		this.isList = isList;
	}

	/** @return root element of the parameter tree this element is part of */
	final TreeElement getRoot() {
		return isRoot() ? this : parent.getRoot();
	}

	/** @return true if element has no {@link #parent} */
	final boolean isRoot() {
		return parent == null;
	}

	/** @return address of this element, with all names of parent elements prepended and separated by "." */
	final String getAddress() {
		if (isRoot()) {
			return "";
		} else if (parent.isRoot()) {
			return name;
		} else {
			return parent.getAddress() + ADDRESS_SEPARATOR + name;
		}
	}

	/** @return List of inner elements - or an empty List if no inner elements are present */
	abstract List<TreeElement> getInnerElements();

	/** @return true if the Element or any of its inner Parameters is mandatory */
	abstract boolean isPropagatedMandatory();
}
