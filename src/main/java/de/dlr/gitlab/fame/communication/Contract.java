package de.dlr.gitlab.fame.communication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.time.TimeSpan;
import de.dlr.gitlab.fame.time.TimeStamp;
import de.dlr.gitlab.fame.logging.Logging;

/** An information delivery contract between two agents
 * 
 * @author Christoph Schimeczek */
public class Contract {
	static final String ERR_PRODUCT_NAME = "Product names do no match: ";
	private static Logger logger = LoggerFactory.getLogger(Contract.class);
	private final long contractId;
	private final long senderId;
	private final long receiverId;
	private final Enum<?> product;
	private final TimeStamp firstDeliveryTime;
	private final TimeSpan deliveryInterval;
	private final TimeStamp expirationTime;

	public Contract(ProtoContract protoType, long contractId, Enum<?> product) {
		this.contractId = contractId;
		this.senderId = protoType.getSenderId();
		this.receiverId = protoType.getReceiverId();
		this.product = product;
		ensureEqualOrThrow(protoType.getProductName(), product.name());
		this.firstDeliveryTime = new TimeStamp(protoType.getFirstDeliveryTime());
		this.deliveryInterval = new TimeSpan(protoType.getDeliveryIntervalInSteps());
		this.expirationTime = new TimeStamp(protoType.getExpirationTime());
	}

	/** Ensures that the given two {@link String}s are identical
	 * 
	 * @throws RuntimeException if the two strings are not identical */
	private void ensureEqualOrThrow(String productName, String nameOfProduct) {
		if (!productName.equals(nameOfProduct)) {
			throw Logging.logFatalException(logger, ERR_PRODUCT_NAME + productName);
		}
	}

	public long getContractId() {
		return contractId;
	}

	public long getSenderId() {
		return senderId;
	}

	public long getReceiverId() {
		return receiverId;
	}

	/** Returns the {@link Product} of the sender agent this contract refers to
	 * 
	 * @return {@link Product} of the sender agent this contract refers to */
	public Enum<?> getProduct() {
		return product;
	}

	/** Returns the {@link TimeStamp} of the first delivery, i.e. where the sender guarantees the receiver to have received the
	 * contract's information
	 * 
	 * @return {@link TimeStamp} of first delivery */
	public TimeStamp getFirstDeliveryTime() {
		return firstDeliveryTime;
	}

	/** Returns the {@link TimeSpan} of the delivery interval: The sender guarantees the delivery of update informations each
	 * "delivery interval" times
	 * 
	 * @return {@link TimeStamp} delivery interval of promised information */
	public TimeSpan getDeliveryInterval() {
		return deliveryInterval;
	}

	/** Returns the {@link TimeStamp} at which the contract will automatically expire and end without further notice, i.e. no
	 * further information is to be delivered <b>after</b> the expiration time.
	 * 
	 * @return expiration TimeStamp */
	public TimeStamp getExpirationTime() {
		return expirationTime;
	}

	/** Returns the next {@link TimeStamp} where at this contract guarantees delivery <b>after</b> the given TimeStamp
	 * 
	 * @param givenTime the given TimeStamp
	 * @return next {@link TimeStamp} where at this contract guarantees delivery <b>after</b> the given TimeStamp. <br>
	 *         If the contract will be expired by then, the largest possible future time is returned<br>
	 *         If the given TimeStamp is before the first delivery time, the initial first time of delivery is returned. */
	public TimeStamp getNextTimeOfDeliveryAfter(TimeStamp givenTime) {
		long timeAfterContractBegin = givenTime.getStep() - firstDeliveryTime.getStep();
		if (timeAfterContractBegin < 0) {
			return firstDeliveryTime;
		} else if (timeAfterContractBegin == 0) {
			TimeStamp nextDelivery = firstDeliveryTime.laterBy(deliveryInterval);
			return nextDelivery.isLessEqualTo(expirationTime) ? nextDelivery : TimeStamp.LATEST;
		} else {
			long timeAfterLastDelivery = timeAfterContractBegin % deliveryInterval.getSteps();
			long remainingTimeToNextDelivery = deliveryInterval.getSteps() - timeAfterLastDelivery;
			TimeStamp nextDelivery = givenTime.laterBy(new TimeSpan(remainingTimeToNextDelivery));
			return nextDelivery.isLessEqualTo(expirationTime) ? nextDelivery : TimeStamp.LATEST;
		}
	}

	/** Returns true if given AgentId is sender in this Contract
	 * 
	 * @param agentId id to be checked
	 * @return true if given Id is the sender */
	public boolean isSender(long agentId) {
		return agentId == senderId;
	}

	/** Returns true if this {@link Contract} is active at the given {@link TimeStamp time}, i.e. the given time is greater than or
	 * equal to the {@link #firstDeliveryTime} and lesser than or equal to {@link #expirationTime}
	 * 
	 * @param time at which the Contract is tested for activity
	 * @return true, if the Contract is active at the given time */
	public boolean isActiveAt(TimeStamp time) {
		return time.isLessEqualTo(expirationTime) && time.isGreaterEqualTo(firstDeliveryTime);
	}

	/** Returns a {@link ContractData} item that can be added to a {@link Message} to indicate that this {@link Contract} is to be
	 * fulfilled; the TimeStamp of fulfillment is searched based on the given {@link TimeStamp}: the next greater time of contract
	 * fulfillment is selected.
	 * 
	 * @param time used to search the next time of delivery after this TimeStamp
	 * @return {@link ContractData} matching this {@link Contract} */
	public ContractData fulfilAfter(TimeStamp time) {
		return new ContractData(getContractId(), getNextTimeOfDeliveryAfter(time));
	}
}