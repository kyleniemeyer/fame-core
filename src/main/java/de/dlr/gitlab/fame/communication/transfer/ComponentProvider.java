package de.dlr.gitlab.fame.communication.transfer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.data.TimeSeries;

/** Provides components of a {@link Portable} object, allowing its population with values
 * 
 * @author Christoph Schimeczek */
public final class ComponentProvider {
	private Iterator<Boolean> booleanIterator;
	private Iterator<Integer> intIterator;
	private Iterator<Long> longIterator;
	private Iterator<Float> floatIterator;
	private Iterator<Double> doubleIterator;
	private Iterator<String> stringIterator;
	private Iterator<TimeSeries> timeSeriesIterator;
	private ListIterator<Portable> componentIterator;

	/** saves values of all primitives to this component provider */
	void setPrimitives(NestedItem item) {
		booleanIterator = item.getBoolValueList().iterator();
		intIterator = item.getIntValueList().iterator();
		longIterator = item.getLongValueList().iterator();
		floatIterator = item.getFloatValueList().iterator();
		doubleIterator = item.getDoubleValueList().iterator();
		stringIterator = item.getStringValueList().iterator();
	}

	void setBoolArray(List<Boolean> bools) {
		booleanIterator = bools.iterator();
	}

	void setIntArray(List<Integer> ints) {
		intIterator = ints.iterator();
	}

	void setLongArray(List<Long> longs) {
		longIterator = longs.iterator();
	}

	void setFloatArray(List<Float> floats) {
		floatIterator = floats.iterator();
	}

	void setDoubleArray(List<Double> doubles) {
		doubleIterator = doubles.iterator();
	}

	void setStringArray(List<String> strings) {
		stringIterator = strings.iterator();
	}

	void setTimeSeriesArray(List<TimeSeries> timeSeries) {
		timeSeriesIterator = timeSeries.iterator();
	}

	void setComponentArray(List<Portable> components) {
		componentIterator = components.listIterator();
	}

	/** Return next stored boolean value
	 * 
	 * @return next stored boolean value
	 * @throws NoSuchElementException if no more elements are stored */
	public boolean nextBoolean() {
		return booleanIterator.next();
	}

	/** Return next stored int value
	 * 
	 * @return next stored int value
	 * @throws NoSuchElementException if no more elements are stored */
	public int nextInt() {
		return intIterator.next();
	}

	/** Return next stored long value
	 * 
	 * @return next stored long value
	 * @throws NoSuchElementException if no more elements are stored */
	public long nextLong() {
		return longIterator.next();
	}

	/** Return next stored float value
	 * 
	 * @return next stored float value
	 * @throws NoSuchElementException if no more elements are stored */
	public float nextFloat() {
		return floatIterator.next();
	}

	/** Return next stored double value
	 * 
	 * @return next stored double value
	 * @throws NoSuchElementException if no more elements are stored */
	public double nextDouble() {
		return doubleIterator.next();
	}

	/** Return next stored String
	 * 
	 * @return next stored String
	 * @throws NoSuchElementException if no more elements are stored */
	public String nextString() {
		return stringIterator.next();
	}

	/** Return next stored {@link TimeSeries}
	 * 
	 * @return next stored TimeSeries
	 * @throws NoSuchElementException if no more elements are stored */
	public TimeSeries nextTimeSeries() {
		return timeSeriesIterator.next();
	}

	/** Test for any remaining component(s)
	 * 
	 * @return true if this {@link ComponentProvider} contains further components */
	public boolean hasNextComponent() {
		return componentIterator.hasNext();
	}

	/** Test if next component is of given type
	 * 
	 * @param type class of {@link Portable} in question
	 * @return <b>true</b> if the next component to provide can be cast to given class type<br>
	 *         <b>false</b> otherwise or if no next component exists */
	public boolean hasNextComponent(Class<? extends Portable> type) {
		if (componentIterator.hasNext()) {
			Portable nextItem = componentIterator.next();
			componentIterator.previous();
			return type.isAssignableFrom(nextItem.getClass());
		} else {
			return false;
		}
	}

	/** Return next stored component and casts to given class type; <br>
	 * Test with {@link #hasNextComponent(Class)} whether the next element can be cast to that type
	 * 
	 * @param<T> type of Portable
	 * @param type class of {@link Portable} to be extracted
	 * @return next stored component
	 * @throws NoSuchElementException if no more components are stored
	 * @throws ClassCastException if the next element cannot be cast to given class type */
	public <T extends Portable> T nextComponent(Class<T> type) {
		return type.cast(componentIterator.next());
	}

	/** Returns a List of all the next components that are of the given class type, until a component of a different class type or
	 * no further component is found.
	 * 
	 * @param<T> type of Portable
	 * @param type class of the {@link Portable}(s) to be extracted
	 * @return List of components of similar class type; list may be empty if the next component is not of given class type or if no
	 *         more components are stored. */
	public <T extends Portable> ArrayList<T> nextComponentList(Class<T> type) {
		ArrayList<T> list = new ArrayList<>();
		while (hasNextComponent(type)) {
			list.add(nextComponent(type));
		}
		return list;
	}
}