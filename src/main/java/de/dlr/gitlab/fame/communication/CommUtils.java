package de.dlr.gitlab.fame.communication;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.logging.Logging;

/** Helper functions for tasks related to Messages and Contracts
 *
 * @author Christoph Schimeczek */
public final class CommUtils {
	public static final String NOT_ONE_ENTRY = "List has not exactly one entry!";
	public static final String NO_MESSAGES = "Nothing to extract data from - given message list missing or empty.";
	public static final String NO_TYPE = "No type of DataItem specified.";
	private static final Logger logger = LoggerFactory.getLogger(CommUtils.class);

	private CommUtils() {
		throw new IllegalStateException(CommUtils.class.getName() + " must not be instantiated.");
	}

	/** Returns the one and only entry in the given list or throws Exception
	 * 
	 * @param <T> inner type of the given List
	 * @param list must contain exactly one entry
	 * @return the entry in the given List; ensures that list has exactly one entry
	 * @throws RuntimeException if not exactly one entry is is present in the given List */
	public static <T> T getExactlyOneEntry(List<T> list) {
		ensureExactlyOneEntry(list);
		return list.get(0);
	}

	/** Ensures that given List contains exactly one entry or throws Exception
	 * 
	 * @throws RuntimeException if not exactly one entry is is present in the given List */
	private static void ensureExactlyOneEntry(List<?> list) {
		if (list == null || list.size() != 1) {
			throw new RuntimeException(NOT_ONE_ENTRY);
		}
	}

	/** Returns all Messages where the sender has the given agentId<br>
	 * <b>Important:</b> Returned Messages are removed from the provided ArrayList
	 * 
	 * @param messages List of messages to search and filter from - will be modified
	 * @param senderId id of the sender whose messages are to be extracted
	 * @return new ArrayList of Messages from given ArrayList where the sender has the given senderId; */
	public static ArrayList<Message> extractMessagesFrom(ArrayList<Message> messages, long senderId) {
		ArrayList<Message> messagesFromContractReceiver = new ArrayList<>();
		if (messages == null || messages.size() == 0) {
			logger.warn(NO_MESSAGES);
		} else {
			ListIterator<Message> iterator = messages.listIterator();
			while (iterator.hasNext()) {
				Message message = iterator.next();
				if (message.getSenderId() == senderId) {
					messagesFromContractReceiver.add(message);
					iterator.remove();
				}
			}
		}
		return messagesFromContractReceiver;
	}

	/** Returns all Messages from given messages that contain specified type of {@link DataItem}<br>
	 * <b>Important:</b> Returned Messages are removed from the provided ArrayList
	 * 
	 * @param messages List of messages to search and filter from - will be modified
	 * @param clas type of DataItem to filter for
	 * @return new ArrayList of Messages from given ArrayList where each Message contains the requested DataItem */
	public static ArrayList<Message> extractMessagesWith(ArrayList<Message> messages, Class<? extends DataItem> clas) {
		ArrayList<Message> messagesWithRequestedDataItem = new ArrayList<>();
		if (clas == null) {
			throw Logging.logFatalException(logger, NO_TYPE);
		} else if (messages == null || messages.size() == 0) {
			logger.warn(NO_MESSAGES);
		} else {
			ListIterator<Message> iterator = messages.listIterator();
			while (iterator.hasNext()) {
				Message message = iterator.next();
				if (message.containsType(clas)) {
					messagesWithRequestedDataItem.add(message);
					iterator.remove();
				}
			}
		}
		return messagesWithRequestedDataItem;
	}
}
