package de.dlr.gitlab.fame.communication;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin.Topic;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.service.PostOffice;

/** Multiplies messages to a List of subscribers
 * 
 * @author Ulrich Frey, Christoph Schimeczek */
public class Channel {
	static final String DOUBLE_SUBSCRIPTION = " :Channel has already a subscriber with that AgentId: ";
	static final String UNSUBSCRIBER_MISSING = " :Channel cannot unsubscribe - has no registered agentId: ";

	private static Logger logger = LoggerFactory.getLogger(Channel.class);
	private final ArrayList<Long> subscribers = new ArrayList<>();
	private final Agent owner;
	private final MessageContext context;

	/** Constructs a {@link Channel} for a given owner with a specific context of messages
	 * 
	 * @param owner the Agent that can send messages over this channel
	 * @param context identifying the channel */
	public Channel(Agent owner, MessageContext context) {
		this.owner = owner;
		this.context = context;
	}

	/** Subscribe specified agent to this channel; the agent will receive all subsequent published messages until
	 * {@link #unsubscribe(long)} is called; issues a warning if agent has already subscribed to this channel
	 * 
	 * @param agentId ID of the subscribing agent */
	public void subscribe(long agentId) {
		if (!subscribers.contains(agentId)) {
			subscribers.add(agentId);
		} else {
			logger.warn(this + DOUBLE_SUBSCRIPTION + agentId);
		}
	}

	/** Unsubscribe specified agent from this channel; the agent will no longer receive messages published here; if the agent hasn't
	 * {@link #subscribe(long) subscribed} to this channel, a warning is issued.
	 * 
	 * @param agentId the Id of the agent that no longer wants to receive messages from this channel */
	public void unsubscribe(long agentId) {
		if (!subscribers.remove(agentId)) {
			logger.warn(this + UNSUBSCRIBER_MISSING + agentId);
		}
	}

	/** Send given {@link DataItem}s to all subscribers of this {@link Channel}
	 * 
	 * @param dataItems to be published to all subscribers */
	public void publish(DataItem... dataItems) {
		PostOffice postOffice = owner.getPostOffice();
		long senderId = owner.getId();
		for (long receiverId : subscribers) {
			postOffice.sendMessage(senderId, receiverId, null, dataItems);
		}
	}

	/** Emits close message to all subscribers; removes all subscribers from list */
	public void close() {
		PostOffice postOffice = owner.getPostOffice();
		for (Long receiverId : subscribers) {
			postOffice.sendMessage(owner.getId(), receiverId, null, new ChannelAdmin(Topic.CLOSE, context));
		}
		subscribers.clear();
	}

	@Override
	public String toString() {
		return "Channel(" + owner.getId() + ", " + context + ")";
	}

	/** Returns true if a given agent is listed as subscriber
	 * 
	 * @param agentId subscriber to search for
	 * @return true if given agent is a listed subscriber */
	boolean hasSubscriber(long agentId) {
		return subscribers.contains(agentId);
	}

	/** Returns true if any agent is registered
	 * 
	 * @return true if any agent is registered */
	boolean hasAnySubscribers() {
		return !subscribers.isEmpty();
	}
}