package de.dlr.gitlab.fame.communication.message;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;

/**
 * Content regarding subscriptions / closing of channels
 * 
 * @author Christoph Schimeczek
 */
public class ChannelAdmin extends DataItem {
	public static enum Topic {
		/** The sender (owner of this channel) informs the receiver that this channel has been closed. No more data will be transmitted. */
		CLOSE,
		/** The sender wants to subscribe to a message channel of type {@link MessageContext} owned by the receiver. */
		SUBSCRIBE,
		/** The sender wants to unsubscribe to a message channel of type {@link MessageContext} owned by the receiver. */
		UNSUBSCRIBE
	};
	public final Topic topic;
	public final MessageContext context;

	public ChannelAdmin(Topic topic, MessageContext context) {
		this.topic = topic;
		this.context = context;
	}

	ChannelAdmin(ProtoDataItem protoData) {
		this.topic = Topic.values()[protoData.getIntValue(0)];
		this.context = MessageContext.values()[protoData.getIntValue(1)];
	}

	@Override
	protected void fillDataFields(Builder builder) {
		builder.addIntValue(topic.ordinal());
		builder.addIntValue(context.ordinal());
	}
}