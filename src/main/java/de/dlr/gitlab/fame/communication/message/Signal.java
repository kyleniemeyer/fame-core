package de.dlr.gitlab.fame.communication.message;

import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;

/** A signal carrying one "tag" of type <code>int</code>
 * 
 * @author Christoph Schimeczek, Ulrich Frey, Ben Fuchs */
public final class Signal extends DataItem {
	public final int tag;

	/** Constructs a new {@link Signal} with the given tag
	 * 
	 * @param tag integer to be transmitted in this DataItem */
	public Signal(int tag) {
		this.tag = tag;
	}

	/** Constructs a new {@link Signal} from ProtoBuffer */
	Signal(ProtoDataItem protoData) {
		this.tag = protoData.getIntValue(0);
	}

	@Override
	protected void fillDataFields(Builder builder) {
		builder.addIntValue(tag);
	}
}