package de.dlr.gitlab.fame.communication.message;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.service.TimeSeriesProvider;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.logging.Logging;

/** Used to construct new {@link Message}s
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public class MessageBuilder {
	public static final String TYPE_ALREADY_EXISTS = "DataItem of that type already exists in this message factory!";
	public static final String INCOMPLETE = "Each message must contain at a receiverId, senderId, and at least one DataItem.";
	private static Logger logger = LoggerFactory.getLogger(MessageBuilder.class);
	private long senderId = Long.MIN_VALUE;
	private long receiverId = Long.MIN_VALUE;
	private ArrayList<DataItem> dataItems = new ArrayList<>();
	private ArrayList<Portable> portables = new ArrayList<>();
	private final Composer composer;

	/** Creates a {@link MessageBuilder factory} to produce {@link Message}s
	 * 
	 * @param setup simulation {@link Setup} containing required classes
	 * @param timeSeriesProvider provides TimeSeries from input data */
	public MessageBuilder(Setup setup, TimeSeriesProvider timeSeriesProvider) {
		composer = new Composer(setup, timeSeriesProvider);
		Message.setComposer(composer);
	}

	/** Adds the given {@link DataItem} to this {@link MessageBuilder} of {@link Message}; any type of DataItem may only occur once,
	 * otherwise a {@link RuntimeException} is thrown; null pointers are ignored and not added to the list
	 * 
	 * @param dataItem to add to the message to be built
	 * @return the modified MessageBuilder */
	public MessageBuilder add(DataItem dataItem) {
		if (dataItem == null) {
			return this;
		} else if (typeIsalreadyListed(dataItem)) {
			throw Logging.logFatalException(logger, TYPE_ALREADY_EXISTS);
		}
		dataItems.add(dataItem);
		return this;
	}

	/** @return true if any {@link DataItem} of the same type has already been added to this {@link MessageBuilder} */
	private boolean typeIsalreadyListed(DataItem dataItem) {
		for (DataItem dataItemInList : dataItems) {
			if (dataItem.getClass() == dataItemInList.getClass()) {
				return true;
			}
		}
		return false;
	}

	/** Adds the given {@link Portable} to this {@link MessageBuilder}; Any number of portables can be added;
	 * 
	 * @param portable to be added to the message to be built
	 * @return the modified MessageBuilder */
	public MessageBuilder add(Portable portable) {
		portables.add(portable);
		return this;
	}

	/** Returns the {@link Message} containing sender, receiver and all {@link DataItem}s and {@link Portable}s added to this
	 * {@link MessageBuilder}
	 * 
	 * @return built Message
	 * @throws RuntimeException in case that Message {@link #isNotComplete()} */
	public Message build() {
		if (isNotComplete()) {
			throw Logging.logFatalException(logger, INCOMPLETE);
		}
		ProtoDataItem[] protoItems = getProtoDataItems();
		NestedItem[] portableItems = getProtoPortables();
		Message message = new Message(senderId, receiverId, protoItems, portableItems);
		return message;
	}

	/** @return true if either sender or receiver is missing or if no DataItem is present */
	private boolean isNotComplete() {
		return senderId == Long.MIN_VALUE || receiverId == Long.MIN_VALUE || dataItems.isEmpty();
	}

	/** @return Array of {@link ProtoDataItem}s for all entries of {@link #dataItems} */
	private ProtoDataItem[] getProtoDataItems() {
		ProtoDataItem[] protoItems = new ProtoDataItem[dataItems.size()];
		for (int index = 0; index < dataItems.size(); index++) {
			protoItems[index] = dataItems.get(index).getProtobuf();
		}
		return protoItems;
	}

	/** @return Array of {@link NestedItem}s for all entries of {@link #portables} */
	private NestedItem[] getProtoPortables() {
		NestedItem[] portableItems = new NestedItem[portables.size()];
		for (int index = 0; index < portables.size(); index++) {
			portableItems[index] = composer.decompose(portables.get(index));
		}
		return portableItems;
	}

	/** Sets senderId and receiverId to {@link Long#MIN_VALUE} and empties List of stored {@link DataItem}s and {@link Portable}s
	 * 
	 * @return the modified MessageBuilder */
	public MessageBuilder clear() {
		dataItems.clear();
		portables.clear();
		senderId = Long.MIN_VALUE;
		receiverId = Long.MIN_VALUE;
		return this;
	}

	/** Sets the UUID of the sender for the {@link Message} under construction
	 * 
	 * @param senderId id of the sender for the Message to be built
	 * @return the modified MessageBuilder */
	public MessageBuilder setSenderId(long senderId) {
		this.senderId = senderId;
		return this;
	}

	/** Sets the UUID of the receiver for the {@link Message} under construction
	 * 
	 * @param receiverId id of the receiver for the Message to be built
	 * @return the modified MessageBuilder */
	public MessageBuilder setReceiverId(long receiverId) {
		this.receiverId = receiverId;
		return this;
	}
}