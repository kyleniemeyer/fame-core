package de.dlr.gitlab.fame.communication.message;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.communication.transfer.DataUidManager;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;
import de.dlr.gitlab.fame.service.SetupManager;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;
import de.dlr.gitlab.fame.logging.Logging;

/** Abstract base class for data items as part of a {@link Message}; Extend this class if you wish to create a new type of data
 * payload to be transferred with {@link Message}s.<br>
 * All non-abstract <b>subtypes must implement</b> a Constructor with signature ({@link ProtoDataItem}) -&gt; &lt;? extends
 * DataItem&gt;
 * 
 * @author Christoph Schimeczek, Ben Fuchs, Ulrich Frey */
public abstract class DataItem {
	public static final String CLASS_NOT_KNOWN = "Class not registered - ensure messagePackages cover class ";
	public static final String ERR_MISSING_CONSTRUCTOR = "Cannot find Constructor for DataItem ";

	private static Logger logger = LoggerFactory.getLogger(DataItem.class);
	private static Builder builder = ProtoDataItem.newBuilder();
	private static HashMap<Class<? extends DataItem>, Integer> mapClassToId = mapClassesToUniqueId();
	private static HashMap<Integer, Constructor<?>> mapIdToConstructor = mapIdsToConstructors();

	/** Assigns each subtype of {@link DataItem} a unique id */
	private static HashMap<Class<? extends DataItem>, Integer> mapClassesToUniqueId() {
		Reflection reflection = new Reflection(ReflectionType.COMMUNICATION, SetupManager.getMessagePackageNames());
		Set<Class<? extends DataItem>> dataItemClasses = reflection.getSubTypesOf(DataItem.class);
		ArrayList<Class<? extends DataItem>> sortedItemClasses = DataUidManager.sortClassTypesByName(dataItemClasses);
		return DataUidManager.createClassToIdMap(sortedItemClasses);
	}

	/** @return a {@link HashMap} mapping internal IDs of {@link DataItem} subtypes to their {@link Constructor} */
	private static HashMap<Integer, Constructor<?>> mapIdsToConstructors() {
		HashMap<Integer, Constructor<?>> map = new HashMap<>();
		for (Entry<Class<? extends DataItem>, Integer> entry : mapClassToId.entrySet()) {
			try {
				Constructor<? extends DataItem> constructor = entry.getKey().getDeclaredConstructor(ProtoDataItem.class);
				map.put(entry.getValue(), constructor);
			} catch (NoSuchMethodException | SecurityException e) {
				String message = ERR_MISSING_CONSTRUCTOR + entry.getKey().getName();
				throw Logging.logFatalException(logger, message);
			}
		}
		return map;
	}

	/** @return {@link ProtoDataItem} for this {@link DataItem} */
	public ProtoDataItem getProtobuf() {
		builder.clear();
		Integer dataItemClassNumber = mapClassToId.get(getClass());
		if (dataItemClassNumber == null) {
			throw Logging.logFatalException(logger, CLASS_NOT_KNOWN + getClass().getName());
		}
		builder.setDataTypeId(dataItemClassNumber);
		fillDataFields(builder);
		return builder.build();
	}

	/** Fills given {@link Builder} with all data of this {@link DataItem}
	 * 
	 * @param builder to be filled with data */
	protected abstract void fillDataFields(Builder builder);

	/** Creates a {@link DataItem} from the matching {@link ProtoDataItem}
	 * 
	 * @return {@link DataItem} from the matching {@link ProtoDataItem} */
	static DataItem createFromProtobuf(ProtoDataItem protoDataItem) {
		Integer classId = protoDataItem.getDataTypeId();
		return constructFromDataAndId(protoDataItem, classId);
	}

	/** constructs a DataItem from its protobuf representation */
	private static DataItem constructFromDataAndId(ProtoDataItem data, int classId) {
		Constructor<?> constructor = mapIdToConstructor.get(classId);
		try {
			return (DataItem) constructor.newInstance(data);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String message = "Cannot create DataItem " + constructor.getName();
			throw Logging.logFatalException(logger, message);
		}
	}

	/** @return unique ID of the specified subclass of {@link DataItem} */
	static int getIdOfType(Class<? extends DataItem> classType) {
		Integer result = mapClassToId.get(classType);
		if (result == null) {
			throw new RuntimeException(CLASS_NOT_KNOWN + classType.getSimpleName());
		}
		return result;
	}
}