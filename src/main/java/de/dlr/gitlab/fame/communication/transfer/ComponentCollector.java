package de.dlr.gitlab.fame.communication.transfer;

import java.util.ArrayList;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem.Builder;
import de.dlr.gitlab.fame.data.TimeSeries;

/** Collects components and primitives of {@link Portable} objects to enable their transfer to other {@link Agent}s; acts as a
 * Facade for underlying protobuf structure
 * 
 * @author Christoph Schimeczek, Marc Deissenroth */
public class ComponentCollector {
	private Builder builder;
	private ArrayList<Portable> storedComponents = new ArrayList<>();

	public ComponentCollector(Builder builder) {
		this.builder = builder;
	}

	public void storeBooleans(boolean... booleans) {
		for (boolean bool : booleans) {
			builder.addBoolValue(bool);
		}
	}

	public void storeInts(int... ints) {
		for (int integer : ints) {
			builder.addIntValue(integer);
		}
	}

	public void storeLongs(long... longs) {
		for (long longValue : longs) {
			builder.addLongValue(longValue);
		}
	}

	public void storeFloats(float... floats) {
		for (float floatValue : floats) {
			builder.addFloatValue(floatValue);
		}
	}

	public void storeDoubles(double... doubles) {
		for (double doubleValue : doubles) {
			builder.addDoubleValue(doubleValue);
		}
	}

	public void storeStrings(String... strings) {
		for (String string : strings) {
			builder.addStringValue(string);
		}
	}

	public void storeTimeSeries(TimeSeries... timeSeries) {
		for (TimeSeries aTimeSeries : timeSeries) {
			builder.addTimeSeriesId(aTimeSeries.getId());
		}
	}

	public void storeComponents(Portable... components) {
		for (Portable component : components) {
			storedComponents.add(component);
		}
	}

	ArrayList<Portable> getSubComponents() {
		return storedComponents;
	}
}