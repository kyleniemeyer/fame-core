package de.dlr.gitlab.fame.communication;

import de.dlr.gitlab.fame.communication.message.Message;

/** Communication constants
 * 
 * @author Christoph Schimeczek, Ulrich Frey */
public final class Constants {
	/** Available contexts for {@link Message}s */
	public static enum MessageContext {
		NONE, DEMAND_ASK, CLEARING_PRICE
	}
}