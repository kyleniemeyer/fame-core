package de.dlr.gitlab.fame.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao.Row;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Represents a time series of double values, offering different interpolation mechanics
 * 
 * @author Christoph Schimeczek */
public class TimeSeries {
	static Logger logger = LoggerFactory.getLogger(TimeSeries.class);
	private final ArrayList<DataEntry> data = new ArrayList<>();
	private final String name;
	private final int id;

	/** Construct a {@link TimeSeries} from its protobuf {@link TimeSeriesDao representation}
	 * 
	 * @param rawData protobuf representation of a TimeSeries */
	public TimeSeries(TimeSeriesDao rawData) {
		name = rawData.getSeriesName();
		id = rawData.getSeriesId();
		addRowData(rawData.getRowList());
		Collections.sort(data);
		checkNoDuplicates();
	}

	/** Adds data entries from given List of {@link Row}s to {@link #data} */
	private void addRowData(List<Row> rows) {
		for (Row row : rows) {
			data.add(new DataEntry(row.getTimeStep(), row.getValue()));
		}
	}

	/** Ensures no duplicate items for same timeStep exist;
	 * <p>
	 * Assumes that {@link DataEntry}s are sorted by timeStep
	 * </p>
	 * 
	 * @throws RuntimeException in case duplicate entries are found */
	private void checkNoDuplicates() {
		for (int i = 1; i < data.size(); i++) {
			long timeStep = data.get(i).timeStep;
			long previousTimeStep = data.get(i - 1).timeStep;
			if (timeStep == previousTimeStep) {
				throw Logging.logFatalException(logger, "Duplicate time steps found in series " + name);
			}
		}
	}

	@Override
	public String toString() {
		return "Series(" + id + ": " + name + ")";
	}

	/** Returns value for the specified timeStamp;
	 * <p>
	 * If no value is stored for the specified time the value of the next earlier time is returned.
	 * <p>
	 * If no element exists for the next earlier time, the value of the first element is returned.
	 * 
	 * @param timeStamp the time for which to return a value
	 * @return Value for specified time, next lower time or lowest time element stored */
	public double getValueLowerEqual(TimeStamp timeStamp) {
		return getValueLowerEqual(timeStamp.getStep());
	}

	/** @return Value for specified time, next lower time or lowest time element stored */
	private double getValueLowerEqual(long timeStep) {
		int arrayIndex = getArrayIndexLowerEqual(timeStep);
		return data.get(arrayIndex).value;
	}

	/** Returns the index of a {@link DataEntry} at the specified time.
	 * <p>
	 * If no DataItem with the specified time exists, the index of the next earlier time will be returned.
	 * <p>
	 * If the specified time is earlier as the earliest DataItem the index of the earliest DataItem is returned.
	 * <p>
	 * If the specified time is later as the latest DataItem the index of the latest DataItem is returned.
	 *
	 * @return index */
	private int getArrayIndexLowerEqual(long timeIndex) {
		int searchResult = Collections.binarySearch(data, new DataEntry(timeIndex, 0L));
		if (searchResult < 0) {
			int lowerElementIndex = -(searchResult + 2);
			return lowerElementIndex >= 0 ? lowerElementIndex : 0;
		} else {
			return searchResult;
		}
	}

	/** Returns value for the specified timeStamp;
	 * <p>
	 * If no value is stored for the specified time the value of the next later time is returned.
	 * <p>
	 * If no element exists for the next later time, the value of the last element is returned.
	 * 
	 * @param timeStamp the time for which to return a value
	 * @return Value for specified time, next higher time or highest time element stored */
	public double getValueHigherEqual(TimeStamp timeStamp) {
		return getValueHigherEqual(timeStamp.getStep());
	}

	/** @return Value for specified time, next higher time or highest time element stored */
	private double getValueHigherEqual(long timeStep) {
		int arrayIndex = getArrayIndexLowerEqual(timeStep);
		if (isLastDataIndex(arrayIndex)) {
			return data.get(arrayIndex).value;
		}
		long timeOfElement = data.get(arrayIndex).timeStep;
		if (timeStep > timeOfElement) {
			return data.get(arrayIndex + 1).value;
		}
		return data.get(arrayIndex).value;
	}

	/** @return true if the specified index is the last one in the {@link #data} array */
	private boolean isLastDataIndex(int index) {
		return index == (data.size() - 1);
	}

	/** Returns value for the specified timeStamp;
	 * <p>
	 * If no value is stored for the specified time the value is interpolated from direct neighbouring elements
	 * <p>
	 * If no element is stored for lower times, the value of the lowest element is returned.
	 * <p>
	 * If no element is stored for higher times, the value of the highest element is returned.
	 * 
	 * @param timeStamp the time for which to return a value
	 * @return Interpolated value for specified time, lowest time or highest time stored */
	public double getValueLinear(TimeStamp timeStamp) {
		return getValueLinear(timeStamp.getStep());
	}

	private double getValueLinear(long timeStep) {
		int arrayIndex = getArrayIndexLowerEqual(timeStep);
		if (isLastDataIndex(arrayIndex)) {
			return data.get(arrayIndex).value;
		}
		long timeOfElement = data.get(arrayIndex).timeStep;
		if (arrayIndex == 0 && (timeStep < timeOfElement)) {
			return data.get(arrayIndex).value;
		}
		return interpolateBetween(timeStep, data.get(arrayIndex), data.get(arrayIndex + 1));
	}

	/** @return Interpolated value at timeStep, where timeStep lies between time of lowerTimeItem and upperTimeItem or at the
	 *         boundary of this interval */
	private double interpolateBetween(long requestedTime, DataEntry lowerTimeItem, DataEntry upperTimeItem) {
		long t0 = lowerTimeItem.timeStep;
		long t1 = upperTimeItem.timeStep;
		double y0 = lowerTimeItem.value;
		double y1 = upperTimeItem.value;
		double gradient = (y1 - y0) / (t1 - t0);
		long timeDelta = requestedTime - t0;
		return y0 + gradient * timeDelta;
	}

	/** @return unique Id of this {@link TimeSeries} as specified in the input file */
	public int getId() {
		return id;
	}
}