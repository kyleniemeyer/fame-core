package de.dlr.gitlab.fame.mpi;

/** Constants of package mpi
 * 
 * @author Christoph Schimeczek */
public class Constants {
	/** ID of the root process */
	public static final int ROOT = 0;

	/** ID representing a purpose of the MPI communication */
	public static enum Tag {
		SCHEDULE, ADDRESSES, POST, OUTPUT, WARM_UP, CONTRACTS
	};
}