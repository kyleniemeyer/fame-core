package de.dlr.gitlab.fame.mpi;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.protobuf.InvalidProtocolBufferException;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle.Builder;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.logging.Logging;

/** Manages all (faked or real) MPI communication between processes
 * 
 * @author Christoph Schimeczek */
public class MpiManager {
	public static final String PARSER_ERROR = "Unable to parse message bundle - data corruption during transmission occured.";
	private final Logger logger = LoggerFactory.getLogger(MpiManager.class);
	private final MpiFacade mpi;
	protected final int rank;
	protected final int processCount;
	private Builder bundleBuilder = Bundle.newBuilder();

	/** Construct a {@link MpiManager}
	 * 
	 * @param mpi the MPI-Facade to be managed */
	public MpiManager(MpiFacade mpi) {
		this.mpi = mpi;
		this.rank = mpi.getRank();
		this.processCount = mpi.getSize();

		if (isRoot()) {
			System.out.println("Starting up " + processCount + " processes.");
		}
	}

	/** @return true if the process is the MPI root process */
	public boolean isRoot() {
		return rank == Constants.ROOT;
	}

	/** Returns true if the process has the specified MPI rank
	 * 
	 * @param rank to test for
	 * @return true if the process has the specified MPI rank */
	public boolean isRank(int rank) {
		return this.rank == rank;
	}

	/** @return the MPI process rank */
	public int getRank() {
		return rank;
	}

	/** @return total number of MPI processes */
	public int getProcessCount() {
		return processCount;
	}

	/** Send a {@link Bundle} of {@link MpiMessage}s from the source process to all other processes
	 * 
	 * @param bundle the data to be sent
	 * @param source processID of the data source
	 * @return the transmitted (source) / received Bundle (other processes) */
	public Bundle broadcast(Bundle bundle, int source) {
		if (processCount > 1) {
			byte[] bytes = null;
			if (rank == source) {
				bytes = bundle.toByteArray();
			}
			bytes = mpi.broadcastBytes(bytes, source);
			if (rank != source) {
				bundle = parseBundleFrom(bytes);
			}
		}
		return bundle;
	}

	/** Parse an array of bytes to it's corresponding representation as {@link Bundle}
	 * 
	 * @param bytes data to be parsed
	 * @return parsed Bundle */
	private Bundle parseBundleFrom(byte[] bytes) {
		try {
			return Bundle.parseFrom(bytes);
		} catch (InvalidProtocolBufferException e) {
			e.printStackTrace();
			throw Logging.logFatalException(logger, PARSER_ERROR);
		}
	}

	/** Send a {@link Bundle} of messages from all processes to a single target under a given {@link Tag}
	 * 
	 * @param bundle data to be send
	 * @param target ID of the target process
	 * @param tag data stream identifier
	 * @return bundle input + on target process: Aggregated messages from all received bundles */
	public Bundle aggregateMessagesAt(Bundle bundle, int target, Tag tag) {
		if (rank != target) {
			byte[] bytes = bundle.toByteArray();
			mpi.sendBytesTo(bytes, target, tag.ordinal());
		} else {
			bundle = addBundlesFromAllOtherProcessesToMy(bundle, tag);
		}
		return bundle;
	}

	/** Receive {@link Bundle}s from all other processes and adds their messages to the messages in the given Bundle
	 * 
	 * @param myBundle container to add incoming data to; previous content is kept
	 * @param tag identifier for data streams to receive
	 * @return updated Bundle with previous + received content */
	private Bundle addBundlesFromAllOtherProcessesToMy(Bundle myBundle, Tag tag) {
		bundleBuilder.clear();
		bundleBuilder.addAllMessage(myBundle.getMessageList());
		for (int count = 0; count < processCount - 1; count++) {
			byte[] bytes = mpi.receiveBytesWithTag(tag.ordinal());
			Bundle receivedBundle = parseBundleFrom(bytes);
			bundleBuilder.addAllMessage(receivedBundle.getMessageList());
		}
		return bundleBuilder.build();
	}

	/** Send {@link Bundle bundles} <b>from all</b> processes aggregated <b>to all</b> processes
	 * 
	 * @param bundle on each process: data to be shared with other processes
	 * @param tag data stream identifier
	 * @return on each process: one Bundle containing all messages from all processes */
	public Bundle aggregateAll(Bundle bundle, Tag tag) {
		bundle = aggregateMessagesAt(bundle, Constants.ROOT, tag);
		return broadcast(bundle, Constants.ROOT);
	}

	/** Each process sends an <b>individual</b> {@link Bundle} <b>to each other</b> process
	 * 
	 * @param bundles must contain one Bundle for each process including own process; position in ArrayList must correspond to MPI
	 *          rank
	 * @param tag data stream identifier
	 * @return on each process: one Bundle with aggregated messages sent specifically to this process */
	public Bundle individualAllToAll(ArrayList<Bundle> bundles, Tag tag) {
		ArrayList<MpiRequestFacade> sendRequests = new ArrayList<>(processCount);
		for (int offset = 1; offset < processCount; offset++) {
			int targetProcess = (rank + offset) % processCount;
			byte[] sendBuffer = bundles.get(targetProcess).toByteArray();
			MpiRequestFacade request = mpi.iSendBytesTo(sendBuffer, targetProcess, tag.ordinal());
			sendRequests.add(request);
		}
		Bundle newBundle = addBundlesFromAllOtherProcessesToMy(bundles.get(rank), tag);
		waitForAll(sendRequests);
		return newBundle;
	}

	/** Wait for completion of a given list of requests
	 * 
	 * @param requests list of requests to wait for completion */
	private void waitForAll(ArrayList<MpiRequestFacade> requests) {
		for (MpiRequestFacade request : requests) {
			request.waitForCompletion();
		}
	}
}