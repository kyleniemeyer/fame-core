package de.dlr.gitlab.fame.mpi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

/** 
 * Tests for {@link MpiManager}
 *
 * @author Christoph Schimeczek
 */
public class MpiManagerTest {
	private MpiFacade mpi;
	private MpiManager mpiManager;

	@Before
	public void setup() {
		mpi = mock(MpiFacade.class);
	}

	@Test
	public void testIsRootTrue() {
		setMpiManager(Constants.ROOT, 0);
		assertTrue(mpiManager.isRoot());
	}

	/** set {@link #mpiManager} with given rank and process count */
	private void setMpiManager(int rank, int count) {
		when(mpi.getSize()).thenReturn(count);
		when(mpi.getRank()).thenReturn(rank);
		mpiManager = new MpiManager(mpi);
	}

	@Test
	public void testIsRootFalse() {
		setMpiManager(Constants.ROOT + 1, 0);
		assertFalse(mpiManager.isRoot());
	}

	@Test
	public void testIsRankTrue() {
		int myRank = 5;
		setMpiManager(myRank, 0);
		assertTrue(mpiManager.isRank(myRank));
	}

	@Test
	public void testIsRankFalse() {
		int myRank = 5;
		setMpiManager(myRank, 0);
		assertFalse(mpiManager.isRank(myRank + 1));
	}

	@Test
	public void testGetRank() {
		int myRank = 42;
		setMpiManager(myRank, 0);
		assertEquals(myRank, mpiManager.getRank());
	}

	@Test
	public void testGetProcessCount() {
		int processCount = 11;
		setMpiManager(0, processCount);
		assertEquals(processCount, mpiManager.getProcessCount());
	}

	@Test
	public void testBroadcastReturnInputIfNoOtherProcess() {
		setMpiManager(0, 1);
		Bundle bundle = mock(Bundle.class);

		Bundle returnValue = mpiManager.broadcast(bundle, Integer.MIN_VALUE);
		assertEquals(bundle, returnValue);
	}

	@Test
	public void testBroadcastSenderReturnOwnInput() {
		int rankOfSender = 1;
		setMpiManager(rankOfSender, 2);
		Bundle bundle = mock(Bundle.class);

		Bundle returnValue = mpiManager.broadcast(bundle, rankOfSender);
		assertEquals(bundle, returnValue);
	}

	@Test
	public void testBroadcastReturnBundleFromSender() {
		int rankOfSender = 0;
		int myRank = 1;
		setMpiManager(myRank, 2);
		Bundle bundle = createEmptyBundle();
		when(mpi.broadcastBytes(any(), anyInt())).thenReturn(bundle.toByteArray());

		Bundle returnValue = mpiManager.broadcast(null, rankOfSender);
		assertEquals(bundle, returnValue);
	}

	/** @return {@link Bundle} with no content */
	private Bundle createEmptyBundle() {
		return Bundle.newBuilder().build();
	}

	@Test
	public void testParseBundleFromInvalidBytes() {
		mpiManager = new MpiManager(mpi);
		byte[] bytes = new byte[] {-5}; // negative length of first entry causes the protobuffer to be invalid
		try {
			AccessPrivates.callPrivateMethodOn("parseBundleFrom", mpiManager, bytes);
			fail("Exception expected!");
		} catch (RuntimeException e) {
			assertEquals(MpiManager.PARSER_ERROR, e.getMessage());
		}
	}

	@Test
	public void testAggregateMessagesAtNotTarget() {
		int processCount = 3;
		int myRank = 2;
		int rankOfTarget = 0;
		setMpiManager(myRank, processCount);
		Bundle bundle = mock(Bundle.class);

		Bundle returnValue = mpiManager.aggregateMessagesAt(bundle, rankOfTarget, mock(Tag.class));
		assertEquals(bundle, returnValue);
	}

	@Test
	public void testAggregateMessagesAtIsTarget() {
		int processCount = 3;
		int myRank = 2;
		setMpiManager(myRank, processCount);
		Bundle bundle = createBundleWithOneMessage(0L);
		Tag tag = mockTag(5);
		when(mpi.receiveBytesWithTag(5)).thenReturn(createBundleWithOneMessage(1L).toByteArray())
				.thenReturn(createBundleWithOneMessage(2L).toByteArray());

		Bundle returnedBundle = mpiManager.aggregateMessagesAt(bundle, myRank, tag);
		assertEquals(processCount, returnedBundle.getMessageCount());
		assertTrue(messageListContainsMessagesWithSenderIds(returnedBundle.getMessageList(), 0L, 1L, 2L));
	}

	/** @return a mocked {@link Tag} with given id */
	private Tag mockTag(int id) {
		Tag tag = mock(Tag.class);
		int tagId = 5;
		when(tag.ordinal()).thenReturn(tagId);
		return tag;
	}

	/** @return {@link Bundle} with a single empty {@link Message} with sender = receiver = given number */
	private Bundle createBundleWithOneMessage(long number) {
		return Bundle.newBuilder()
				.addMessage(
						MpiMessage.newBuilder().setMessage(ProtoMessage.newBuilder().setSenderId(number).setReceiverId(number)))
				.build();
	}

	/** @return true if the given message list contains messages from all given senderIds */
	private boolean messageListContainsMessagesWithSenderIds(List<MpiMessage> messages, long... senderIds) {
		List<Long> checkList = new ArrayList<Long>(3);
		for (Long value : senderIds) {
			checkList.add(value);
		}
		for (MpiMessage message : messages) {
			Long senderId = message.getMessage().getSenderId();
			if (!checkList.contains(senderId)) {
				return false;
			}
			checkList.remove(senderId);
		}
		return true;
	}

	@Test
	public void testIndividualAllToAll() {
		setMpiManager(0, 3);
		ArrayList<Bundle> inputBundles = new ArrayList<>();
		inputBundles.add(createBundleWithOneMessage(0));
		inputBundles.add(createBundleWithOneMessage(1));
		inputBundles.add(createBundleWithOneMessage(2));
		when(mpi.iSendBytesTo(any(), anyInt(), anyInt())).thenReturn(mock(MpiRequestFacade.class));
		Tag tag = mockTag(5);
		when(mpi.receiveBytesWithTag(5)).thenReturn(createBundleWithOneMessage(1L).toByteArray())
				.thenReturn(createBundleWithOneMessage(2L).toByteArray());
		
		Bundle result = mpiManager.individualAllToAll(inputBundles, tag);
		assertEquals(3, result.getMessageCount());
		assertTrue(messageListContainsMessagesWithSenderIds(result.getMessageList(), 0L, 1L, 2L));	
	}
}