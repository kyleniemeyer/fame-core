package de.dlr.gitlab.fame.time;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for {@link Formatter}
 * 
 * Tests for null argument not needed as Formatter is called from existing TimeStamp objects only
 *
 * @author Christoph Schimeczek
 */
public class FormatterTest {
	private Formatter formatter;

	@Before
	public void setUp() {
		formatter = new Formatter();
	}

	@Test
	public void testFormatZero() {
		assertEquals("2000-001(01/01)_00:00:00", formatter.format(0));
	}

	@Test
	public void testFormatNegativeSeconds() {
		assertEquals("1999-365(12/53)_23:59:55", formatter.format(-5));
	}

	@Test
	public void testFormatNegativeMinutes() {
		assertEquals("1999-365(12/53)_23:57:55", formatter.format(-125));
	}

	@Test
	public void testFormatNegativeHours() {
		assertEquals("1999-365(12/53)_20:57:55", formatter.format(-10925));
	}

	@Test
	public void testFormatNegativeDays() {
		assertEquals("1999-364(12/52)_20:57:55", formatter.format(-97325));
	}

	@Test
	public void testFormatNegativeMonths() {
		assertEquals("1999-334(11/48)_20:57:55", formatter.format(-2689325));
	}

	@Test
	public void testFormatNegativeYears() {
		assertEquals("1994-334(11/48)_20:57:55", formatter.format(-160369325L));
	}

	@Test
	public void testFormatSeconds() {
		assertEquals("2000-001(01/01)_00:00:17", formatter.format(17));
	}

	@Test
	public void testFormatMinutes() {
		assertEquals("2000-001(01/01)_00:23:17", formatter.format(1397));
	}

	@Test
	public void testFormatHours() {
		assertEquals("2000-001(01/01)_07:23:17", formatter.format(26597));
	}

	@Test
	public void testFormatDays() {
		assertEquals("2000-005(01/01)_07:23:17", formatter.format(372197));
	}

	@Test
	public void testFormatWeek() {
		assertEquals("2000-009(01/02)_07:23:17", formatter.format(717797));
	}

	@Test
	public void testFormatMonths() {
		assertEquals("2000-040(02/06)_07:23:17", formatter.format(3396197));
	}

	@Test
	public void testFormatYears() {
		assertEquals("6331-040(02/06)_07:23:17", formatter.format(136585812197L));
	}
}
