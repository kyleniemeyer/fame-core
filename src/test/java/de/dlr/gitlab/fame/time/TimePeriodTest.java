package de.dlr.gitlab.fame.time;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TimePeriodTest {

	private TimeStamp timeStampA = new TimeStamp(17);
	private TimeStamp timeStampB = new TimeStamp(19);
	private TimeSpan timeSpanA = new TimeSpan(23);
	private TimeSpan timeSpanB = new TimeSpan(29);

	private TimePeriod periodAA = new TimePeriod(timeStampA, timeSpanA);
	private TimePeriod periodAAStar = new TimePeriod(timeStampA, timeSpanA);
	private TimePeriod periodBB = new TimePeriod(timeStampB, timeSpanB);
	private TimePeriod periodAB = new TimePeriod(timeStampA, timeSpanB);
	private TimePeriod periodBA = new TimePeriod(timeStampB, timeSpanA);

	@Test
	public void testLastValidTime() {
		long lastValidTimeStep = periodAA.getStartTime().getStep() + periodAA.getDuration().getSteps() - 1L;
		assertEquals(lastValidTimeStep, periodAA.getLastTime().getStep());
	}

	@Test
	public void testShiftByDurationFailsNegative() {
		assertThrowsFatalMessage(TimeSpan.ERR_NOT_POSITIVE, () -> periodAA.shiftByDuration(-1));
	}

	@Test
	public void testShiftByDurationZero() {
		assertEquals(periodAA, periodAA.shiftByDuration(0));
	}

	@Test
	public void testShiftByDurationPositive() {
		int shift = 5;
		TimePeriod result = periodAA.shiftByDuration(shift);
		TimeSpan duration = periodAA.getDuration();
		assertEquals(duration, result.getDuration());
		long newStart = periodAA.getStartTime().getStep() + duration.getSteps() * shift;
		assertEquals(newStart, result.getStartTime().getStep());
		long newEnd = periodAA.getLastTime().getStep() + duration.getSteps() * shift;
		assertEquals(newEnd, result.getLastTime().getStep());
	}

	@Test
	public void testCompareToFailsWrongDuration() {
		assertThrowsFatalMessage(TimePeriod.UNALLOWED_COMPARISON, () -> periodAA.compareTo(periodAB));
	}

	@Test
	public void testCompareToIdentical() {
		assertEquals(0, periodAA.compareTo(periodAA));
	}

	@Test
	public void testCompareToSimilar() {
		assertEquals(0, periodAA.compareTo(periodAAStar));
	}

	@Test
	public void testCompareToLater() {
		assertTrue(periodAA.compareTo(periodBA) < 0);
	}

	@Test
	public void testCompareToEarlier() {
		assertTrue(periodBA.compareTo(periodAA) > 0);
	}

	@Test
	public void testIsComparableTo() {
		assertTrue(periodAA.isComparableTo(periodBA));
		assertFalse(periodAA.isComparableTo(periodAB));
	}

	@Test
	public void testEqualsNull() {
		assertFalse(periodAA.equals(null));
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEqualsWrongType() {
		assertFalse(periodAA.equals(666));
	}

	@Test
	public void testEqualsIdentical() {
		assertTrue(periodAA.equals(periodAA));
	}

	@Test
	public void testEqualsSimilar() {
		assertTrue(periodAA.equals(periodAAStar));
	}

	@Test
	public void testEqualsOther() {
		assertFalse(periodAA.equals(periodAB));
		assertFalse(periodAA.equals(periodBA));
		assertFalse(periodAA.equals(periodBB));
	}

	@Test
	public void testToString() {
		String result = periodAA.toString();
		assertThat(result, containsString(timeStampA.toString()));
		assertThat(result, containsString(periodAA.getLastTime().toString()));
	}

	@Test
	public void testHashCodeIdentical() {
		assertEquals(periodAA.hashCode(), periodAA.hashCode());
	}

	@Test
	public void testHashCodeSimilar() {
		assertEquals(periodAA.hashCode(), periodAAStar.hashCode());
	}
}
