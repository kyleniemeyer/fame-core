package de.dlr.gitlab.fame.time;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.time.Constants.Interval;

public class TimeStampTest {
	TimeStamp simulationBegin = new TimeStamp(0);

	@Test
	public void testFirstHour() {
		assertEquals("2000-01-01 00:00:00", simulationBegin.toCalendar());
	}

	@Test
	public void testFirstDay() {
		TimeStamp timeStamp = simulationBegin.laterBy(new TimeSpan(1, Interval.DAYS));
		assertEquals("2000-01-02 00:00:00", timeStamp.toCalendar());
	}

	@Test
	public void testFirstMonth() {
		TimeStamp timeStamp = simulationBegin.laterBy(new TimeSpan(1, Interval.MONTHS));
		assertEquals("2000-01-31 10:00:00", timeStamp.toCalendar());
	}

	@Test
	public void testLeapYearA() {
		TimeStamp timeStamp = simulationBegin.laterBy(new TimeSpan(1, Interval.YEARS));
		assertEquals("2001-01-01 00:00:00", timeStamp.toCalendar());
	}

	@Test
	public void testLeapYearLastHour() {
		TimeStamp timeStamp = simulationBegin.laterBy(new TimeSpan(1, Interval.YEARS))
				.earlierBy(new TimeSpan(1, Interval.SECONDS));
		assertEquals("2000-12-30 23:59:59", timeStamp.toCalendar());
	}

	@Test
	public void test2018_08_14_11_55_12() {
		TimeSpan timeSpan = TimeSpan.combine(new TimeSpan(18, Interval.YEARS), new TimeSpan(225, Interval.DAYS),
				new TimeSpan(11, Interval.HOURS), new TimeSpan(55, Interval.MINUTES), new TimeSpan(12, Interval.SECONDS));
		TimeStamp timeStamp = simulationBegin.laterBy(timeSpan);
		assertEquals("2018-08-14 11:55:12", timeStamp.toCalendar());
	}

	@Test
	public void testGreaterThan() {
		TimeStamp lesserTime = new TimeStamp(0);
		TimeStamp higherTime = new TimeStamp(1);
		assertTrue(higherTime.isGreaterThan(lesserTime));
		assertFalse(lesserTime.isGreaterThan(higherTime));
		assertFalse(higherTime.isGreaterThan(higherTime));
	}

	@Test
	public void testCompareTo() {
		TimeStamp lesserTime = new TimeStamp(0);
		TimeStamp higherTime = new TimeStamp(1);

		assertTrue(lesserTime.compareTo(higherTime) < 0);
		assertTrue(higherTime.compareTo(higherTime) == 0);
		assertTrue(higherTime.compareTo(lesserTime) > 0);
	}

	@Test
	public void testNextTimeOfDayLaterToday() {
		TimeStamp currentTime = simulationBegin.laterBy(new TimeSpan(5, Interval.HOURS));
		TimeOfDay timeOfDay = new TimeOfDay(15, 0, 0);
		TimeStamp nextTime = currentTime.nextTimeOfDay(timeOfDay);
		assertEquals(simulationBegin.getStep() + timeOfDay.getSteps(), nextTime.getStep());
	}

	@Test
	public void testNextTimeOfDayTomorrow() {
		TimeStamp currentTime = simulationBegin.laterBy(new TimeSpan(15, Interval.HOURS));
		TimeOfDay timeOfDay = new TimeOfDay(5, 0, 0);
		TimeStamp nextTime = currentTime.nextTimeOfDay(timeOfDay);
		assertEquals(simulationBegin.getStep() + timeOfDay.getSteps() + Constants.STEPS_PER_DAY, nextTime.getStep());
	}

	@Test
	public void testNextTimeOfDayMatchesNow() {
		TimeStamp currentTime = simulationBegin.laterBy(new TimeSpan(15, Interval.HOURS));
		TimeOfDay timeOfDay = new TimeOfDay(15, 0, 0);
		TimeStamp nextTime = currentTime.nextTimeOfDay(timeOfDay);
		assertEquals(simulationBegin.getStep() + timeOfDay.getSteps() + Constants.STEPS_PER_DAY, nextTime.getStep());
	}

	@Test
	public void testIsLessThanLesser() {
		TimeStamp later = new TimeStamp(10L);
		assertFalse(later.isLessThan(new TimeStamp(0L)));
	}

	@Test
	public void testIsLessThanGreater() {
		TimeStamp later = new TimeStamp(0L);
		assertTrue(later.isLessThan(new TimeStamp(10L)));
	}

	@Test
	public void testIsLessThanEqual() {
		TimeStamp later = new TimeStamp(10L);
		assertFalse(later.isLessThan(new TimeStamp(10L)));
	}

	@Test
	public void testIsLessEqualTo_Less() {
		TimeStamp earlier = new TimeStamp(5L);
		TimeStamp later = new TimeStamp(10L);
		assertTrue(earlier.isLessEqualTo(later));
	}

	@Test
	public void testIsLessEqualTo_Equal() {
		TimeStamp now = new TimeStamp(10L);
		TimeStamp alsoNow = new TimeStamp(10L);
		assertTrue(now.isLessEqualTo(alsoNow));
	}

	@Test
	public void testIsLessEqualTo_Greater() {
		TimeStamp earlier = new TimeStamp(5L);
		TimeStamp later = new TimeStamp(10L);
		assertFalse(later.isLessEqualTo(earlier));
	}

	@Test
	public void testIsGreaterEqualThanLess() {
		TimeStamp now = new TimeStamp(10L);
		assertTrue(now.isGreaterEqualTo(now.earlierByOne()));
	}

	@Test
	public void testIsGreaterEqualThanEqual() {
		TimeStamp now = new TimeStamp(10L);
		assertTrue(now.isGreaterEqualTo(new TimeStamp(10L)));
	}

	@Test
	public void testIsGreaterEqualThanGreater() {
		TimeStamp now = new TimeStamp(10L);
		assertFalse(now.isGreaterEqualTo(now.laterByOne()));
	}

	@Test
	public void testGetStep_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).getStep());
	}

	@Test
	public void testToString_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).toCalendar());
	}

	@Test
	public void testLaterBy_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).laterBy(new TimeSpan(1)));
	}

	@Test
	public void testLaterByOne_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).laterByOne());
	}

	@Test
	public void testEarlierBy_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).earlierBy(new TimeSpan(1)));
	}

	@Test
	public void testIsLessEqualTo_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).isLessEqualTo(simulationBegin));
	}

	@Test
	public void testIsLessThan_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).isLessThan(simulationBegin));
	}

	@Test
	public void testIsGreaterEqualTo_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).isGreaterEqualTo(simulationBegin));
	}

	@Test
	public void testIsGreaterThan_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).isGreaterThan(simulationBegin));
	}

	@Test
	public void testNextTimeOfDay_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).nextTimeOfDay(new TimeOfDay(0, 0, 0)));
	}

	@Test
	public void testHashCode_FailWhenInvalid() {
		assertThrowsFatalMessage(TimeStamp.ERROR_INVALID, () -> (new TimeStamp()).hashCode());
	}

	@Test
	public void testHashCode_IdenticalObjects() {
		assertEquals(simulationBegin.hashCode(), simulationBegin.hashCode());
	}

	@Test
	public void testEquals_IdenticalObjects() {
		assertTrue(simulationBegin.equals(simulationBegin));
	}

	@Test
	public void testHashCode_SameTimes() {
		TimeStamp a = new TimeStamp(42L);
		TimeStamp b = new TimeStamp(42L);
		assertEquals(a.hashCode(), b.hashCode());
	}

	@Test
	public void testEquals_SameTimes() {
		TimeStamp a = new TimeStamp(42L);
		TimeStamp b = new TimeStamp(42L);
		assertTrue(a.equals(b));
	}

	@Test
	public void testEquals_DifferentTimes() {
		TimeStamp a = new TimeStamp(451L);
		TimeStamp b = new TimeStamp(19L);
		assertFalse(a.equals(b));
	}

	@Test
	public void testEquals_Null() {
		assertFalse(simulationBegin.equals(null));
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEquals_NoInstance() {
		TimeStamp timeStamp = new TimeStamp(21L);
		assertFalse(timeStamp.equals("NotATimeStamp"));
	}

	@Test
	public void testAddComponentsTo() {
		ComponentCollector mockCollector = mock(ComponentCollector.class);
		TimeStamp timeStamp = new TimeStamp(7L);
		timeStamp.addComponentsTo(mockCollector);
		verify(mockCollector).storeLongs(7L);
	}

	@Test
	public void testPopulate() {
		TimeStamp timeStamp = new TimeStamp();
		ComponentProvider mockProvider = mock(ComponentProvider.class);
		when(mockProvider.nextLong()).thenReturn(76L);
		timeStamp.populate(mockProvider);
		verify(mockProvider, times(1)).nextLong();
		assertEquals(76L, timeStamp.getStep());
	}

	@Test
	public void testLaterByMultiples() {
		TimeStamp timeStamp = new TimeStamp(0);
		TimeStamp result = timeStamp.laterBy(new TimeSpan(54), 33);
		assertEquals(54 * 33, result.getStep());
	}

	@Test
	public void testLaterByMultiplesFailNegative() {
		TimeStamp timeStamp = new TimeStamp(0);
		assertThrowsFatalMessage(TimeStamp.ERR_NOT_POSITIVE, () -> timeStamp.laterBy(new TimeSpan(2), -2));
	}

	@Test
	public void testEarlierByMultiples() {
		TimeStamp timeStamp = new TimeStamp(0);
		TimeStamp result = timeStamp.earlierBy(new TimeSpan(54), 33);
		assertEquals(-54 * 33, result.getStep());
	}

	@Test
	public void testEarlierByMultiplesFailNegative() {
		TimeStamp timeStamp = new TimeStamp(0);
		assertThrowsFatalMessage(TimeStamp.ERR_NOT_POSITIVE, () -> timeStamp.earlierBy(new TimeSpan(2), -2));
	}

	@Test
	public void testToStringCallsFormatter() {
		long step = 55;
		TimeStamp timeStamp = new TimeStamp(step);
		Formatter formatterMock = mock(Formatter.class);
		AccessPrivates.setPrivateFinalField("FORMATTER", timeStamp, formatterMock);
		timeStamp.toString();
		verify(formatterMock, times(1)).format(step);
		// fix static field to have normal formatter again!
		AccessPrivates.setPrivateFinalField("FORMATTER", timeStamp, new Formatter());
	}

	@Test
	public void testToStringReturnsFormatterResult() {
		long step = 389748923745L;
		TimeStamp timeStamp = new TimeStamp(step);
		String result = timeStamp.toString();
		Formatter formatter = new Formatter();
		assertEquals(formatter.format(step), result);
	}
}