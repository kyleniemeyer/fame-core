package de.dlr.gitlab.fame.time;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import de.dlr.gitlab.fame.time.Constants.Interval;

public class TimeSpanTest {

	@Test
	public void testNegativeFail() {
		assertThrowsMessage(IllegalArgumentException.class, TimeSpan.ERR_NOT_POSITIVE, () -> new TimeSpan(-1));
	}

	@Test
	public void testZeroA() {
		TimeSpan timeSpan = new TimeSpan(0);
		assertEquals(0, timeSpan.getSteps());
	}

	@Test
	public void testZeroB() {
		TimeSpan timeSpan = new TimeSpan(0, Interval.SECONDS);
		assertEquals(0, timeSpan.getSteps());
	}

	@Test
	public void testSeconds() {
		TimeSpan timeSpan = new TimeSpan(14, Interval.SECONDS);
		assertEquals(14, timeSpan.getSteps());
	}

	@Test
	public void testMinutes() {
		TimeSpan timeSpan = new TimeSpan(3, Interval.MINUTES);
		assertEquals(3 * 60, timeSpan.getSteps());
	}

	@Test
	public void testHours() {
		TimeSpan timeSpan = new TimeSpan(2, Interval.HOURS);
		assertEquals(2 * 3600, timeSpan.getSteps());
	}

	@Test
	public void testDays() {
		TimeSpan timeSpan = new TimeSpan(5, Interval.DAYS);
		assertEquals(5 * 3600 * 24, timeSpan.getSteps());
	}

	@Test
	public void testWeeks() {
		TimeSpan timeSpan = new TimeSpan(99, Interval.WEEKS);
		assertEquals(99 * 3600 * 168, timeSpan.getSteps());
	}

	@Test
	public void testMonths() {
		TimeSpan timeSpan = new TimeSpan(15, Interval.MONTHS);
		assertEquals(15 * 3600 * 730, timeSpan.getSteps());
	}

	@Test
	public void testYears() {
		TimeSpan timeSpan = new TimeSpan(88, Interval.YEARS);
		assertEquals(88L * 3600 * 24 * 365, timeSpan.getSteps());
	}

	@Test
	public void testCombine() {
		TimeSpan timeSpan = TimeSpan.combine(new TimeSpan(1, Interval.YEARS), new TimeSpan(25, Interval.DAYS),
				new TimeSpan(11, Interval.HOURS), new TimeSpan(2, Interval.SECONDS));
		assertEquals(3600 * 24 * (365 + 25) + 3600 * 11 + 2, timeSpan.getSteps());
	}

	@Test
	public void testEqualsNull() {
		TimeSpan timeSpan = new TimeSpan(0);
		assertFalse(timeSpan.equals(null));
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEqualsForeignClass() {
		TimeSpan timeSpan = new TimeSpan(0);
		assertFalse(timeSpan.equals(0));
	}

	@Test
	public void testEqualsThis() {
		TimeSpan timeSpan = new TimeSpan(21);
		assertTrue(timeSpan.equals(timeSpan));
	}

	@Test
	public void testEqualsSimilar() {
		TimeSpan timeSpanA = new TimeSpan(21);
		TimeSpan timeSpanB = new TimeSpan(21);
		assertTrue(timeSpanA.equals(timeSpanB));
	}

	@Test
	public void testEqualsNotSimilar() {
		TimeSpan timeSpanA = new TimeSpan(21);
		TimeSpan timeSpanB = new TimeSpan(99);
		assertFalse(timeSpanA.equals(timeSpanB));
	}
	
	@Test
	public void testHashCode_Identical() {
		TimeSpan timeSpan = new TimeSpan(54);
		assertEquals(timeSpan.hashCode(), timeSpan.hashCode());
	}
	
	@Test
	public void testHashCode_Similar() {
		TimeSpan timeSpanA = new TimeSpan(66);
		TimeSpan timeSpanB = new TimeSpan(66);
		assertEquals(timeSpanA.hashCode(), timeSpanB.hashCode());
	}
}