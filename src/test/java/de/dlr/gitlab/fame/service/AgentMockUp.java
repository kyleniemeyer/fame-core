package de.dlr.gitlab.fame.service;

import static org.mockito.Mockito.mock;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;

/** 
 * Mock-up for {@link Agent}
 * 
 * @author Christoph Schimeczek
 */
public class AgentMockUp extends Agent {
	public AgentMockUp(long id) {
		super(new DataProvider(AgentDao.newBuilder().setClassName("None").setId(id).build(), null, mock(LocalServices.class)));
	}
}