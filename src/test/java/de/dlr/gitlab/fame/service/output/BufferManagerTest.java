package de.dlr.gitlab.fame.service.output;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedConstruction;
import org.mockito.Mockito;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Services.Output.Builder;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;

public class BufferManagerTest {
	private BufferManager bufferManager;
	private List<String> activeClasses = Arrays.asList(ActiveClassA.class.getSimpleName(),
			ActiveClassB.class.getSimpleName());

	private enum Columns {
		A, B, C
	};

	private enum OtherColumns {
		X, Y, Z
	};

	private ArrayList<Enum<?>> columnList = new ArrayList<>();
	private ArrayList<Enum<?>> otherColumnList = new ArrayList<>();
	private HashMap<Class<? extends Agent>, ArrayList<Enum<?>>> outputColumnLists;
	private HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> complexColumnLists;

	public class ActiveClassA extends Agent {
		public ActiveClassA(DataProvider param) {
			super(param);
		}
	};

	public class ActiveClassB extends Agent {
		public ActiveClassB(DataProvider param) {
			super(param);
		}
	};

	public class InactiveClass extends Agent {
		public InactiveClass(DataProvider param) {
			super(param);
		}
	};

	@Before
	public void setup() {
		bufferManager = new BufferManager();
		outputColumnLists = new HashMap<>();
		complexColumnLists = new HashMap<>();

		bufferManager.setBufferParameters(activeClasses, outputColumnLists, complexColumnLists);
		columnList.clear();
		columnList.addAll(Arrays.asList(Columns.values()));
		otherColumnList.clear();
		otherColumnList.addAll(Arrays.asList(OtherColumns.values()));
	}

	@Test
	public void createBuffer_returnsNullIfAgentNotRegistered() {
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 0);
		assertNull(buffer);
	}

	@Test
	public void createBuffer_returnsNullIfAgentHasNoOutputColumns() {
		outputColumnLists.put(ActiveClassA.class, new ArrayList<Enum<?>>());
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 0);
		assertNull(buffer);
	}

	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_containsAgentClassHeader() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		bufferManager.createBuffer(ActiveClassA.class, 4);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		AgentType agentType = outputBuilder.getAgentType(0);
		assertEquals("ActiveClassA", agentType.getClassName());
		assertEquals("A", agentType.getField(0).getFieldName());
		assertEquals("B", agentType.getField(1).getFieldName());
		assertEquals("C", agentType.getField(2).getFieldName());
	}

	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_notRepeatClassHeader() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		bufferManager.createBuffer(ActiveClassA.class, 4);
		bufferManager.flushAllBuffersToOutputBuilder();
		bufferManager.createBuffer(ActiveClassA.class, 2);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		assertEquals(0, outputBuilder.getAgentTypeCount());
	}

	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_noHeaderForInactiveClass() {
		outputColumnLists.put(InactiveClass.class, columnList);
		bufferManager.createBuffer(InactiveClass.class, 4);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		assertEquals(0, outputBuilder.getAgentTypeCount());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void createBuffer_flushAllBuffersToOutputBuilder_HeaderHasComplexColumn() {
		ArrayList<ComplexIndex<? extends Enum<?>>> indices = new ArrayList<>();
		ComplexIndex<OtherColumns> complexIndex = mock(ComplexIndex.class);
		indices.add(complexIndex);
		complexColumnLists.put(ActiveClassA.class.getSimpleName(), indices);
		when(complexIndex.getFieldName()).thenReturn("A");
		when(complexIndex.getKeyLabels()).thenReturn(OtherColumns.values());
		
		outputColumnLists.put(ActiveClassA.class, columnList);
		bufferManager.createBuffer(ActiveClassA.class, 4);
		Builder outputBuilder = bufferManager.flushAllBuffersToOutputBuilder();
		AgentType agentType = outputBuilder.getAgentType(0);
		assertTrue(agentType.getField(0).getIndexNameList().contains("X"));
		assertFalse(agentType.getField(1).getIndexNameList().contains("X"));
		assertFalse(agentType.getField(2).getIndexNameList().contains("X"));
	}

	@Test
	public void createBuffer_returnsIgnoreBufferForInactiveClass() {
		outputColumnLists.put(InactiveClass.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(InactiveClass.class, 4);
		assertTrue(buffer instanceof IgnoreBuffer);
	}

	@Test
	public void createBuffer_returnsBufferForActiveClass() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 4);
		assertTrue(buffer instanceof ActiveBuffer);
	}

	@Test
	public void createBuffer_ForwardsAgentId() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer buffer = bufferManager.createBuffer(ActiveClassA.class, 99);
		assertEquals(99L, buffer.getAgentId());
	}

	@Test
	public void flushAllBuffersToOutputBuilder_FetchSeriesForActiveClass() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		OutputBuffer mockedBuffer;
		try (MockedConstruction<ActiveBuffer> mocked = Mockito.mockConstruction(ActiveBuffer.class)) {
			mockedBuffer = bufferManager.createBuffer(ActiveClassA.class, 4);
		}
		when(mockedBuffer.getSeries()).thenReturn(mock(Series.class));

		bufferManager.flushAllBuffersToOutputBuilder();
		verify(mockedBuffer, times(1)).getSeries();
	}

	@Test
	public void flushAllBuffersToOutputBuilder_IgnoreInactiveClass() {
		outputColumnLists.put(InactiveClass.class, columnList);
		OutputBuffer mockedBuffer;
		try (MockedConstruction<IgnoreBuffer> mocked = Mockito.mockConstruction(IgnoreBuffer.class)) {
			mockedBuffer = bufferManager.createBuffer(InactiveClass.class, 4);
		}
		when(mockedBuffer.getSeries()).thenReturn(mock(Series.class));

		bufferManager.flushAllBuffersToOutputBuilder();
		verify(mockedBuffer, times(0)).getSeries();
	}

	@Test
	public void isActive_TrueForActiveClass() {
		assertTrue(bufferManager.isActive(ActiveClassA.class.getSimpleName()));
	}

	@Test
	public void isActive_FalseForInactiveClass() {
		assertFalse(bufferManager.isActive(InactiveClass.class.getSimpleName()));
	}

	@Test
	public void isActive_TrueAllClassesIfListIsNull() {
		bufferManager.setBufferParameters(null, outputColumnLists, complexColumnLists);
		assertTrue(bufferManager.isActive(InactiveClass.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassA.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassB.class.getSimpleName()));
	}

	@Test
	public void isActive_TrueAllClassesIfListIsEmpty() {
		bufferManager.setBufferParameters(new ArrayList<String>(), outputColumnLists, complexColumnLists);
		assertTrue(bufferManager.isActive(InactiveClass.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassA.class.getSimpleName()));
		assertTrue(bufferManager.isActive(ActiveClassB.class.getSimpleName()));
	}

	@Test
	public void finishTick_ticksAllActiveBuffers() {
		outputColumnLists.put(ActiveClassA.class, columnList);
		outputColumnLists.put(ActiveClassB.class, otherColumnList);

		OutputBuffer bufferA;
		OutputBuffer bufferB;
		try (MockedConstruction<ActiveBuffer> mocked = Mockito.mockConstruction(ActiveBuffer.class)) {
			bufferA = bufferManager.createBuffer(ActiveClassA.class, 4);
			bufferB = bufferManager.createBuffer(ActiveClassA.class, 2);
		}
		bufferManager.finishTick(42L);
		verify(bufferA, times(1)).tick(42L);
		verify(bufferB, times(1)).tick(42L);
	}

	@Test
	public void finishTick_notTickInactiveBuffers() {
		outputColumnLists.put(InactiveClass.class, columnList);
		OutputBuffer mockedBuffer;
		try (MockedConstruction<IgnoreBuffer> mocked = Mockito.mockConstruction(IgnoreBuffer.class)) {
			mockedBuffer = bufferManager.createBuffer(InactiveClass.class, 4);
		}
		bufferManager.finishTick(42L);
		verify(mockedBuffer, times(0)).tick(42L);
	}
}