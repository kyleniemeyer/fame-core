package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.dummy.AgentDummy;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.ContractTest;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class NotaryTest {
	private enum Products {
		ProductA, ProductB, ProductC, ProductD
	}

	private MpiManager mockMpiManager;
	private Notary notary;
	private ArrayList<String> agentPackages = new ArrayList<>();
	private HashMap<Long, Class<?>> spyOnAllAgentClasses;

	@SuppressWarnings("unchecked")
	@Before
	public void setup() {
		this.mockMpiManager = mock(MpiManager.class);
		this.notary = new Notary(mockMpiManager, agentPackages, new ArrayList<AgentDao>());
		this.spyOnAllAgentClasses = (HashMap<Long, Class<?>>) AccessPrivates.getPrivateFieldOf("allAgentClasses", notary);
	}

	@Test
	public void testReportAgentClass() {
		long agentId = 27L;
		notary.storeAgentClassAndId(agentId, "AgentDummy");
		@SuppressWarnings("unchecked")
		HashMap<Long, Class<?>> agentClasses = (HashMap<Long, Class<?>>) AccessPrivates.getPrivateFieldOf("allAgentClasses",
			notary);
		assertTrue(agentClasses.containsKey(agentId));
		assertEquals(AgentDummy.class, agentClasses.get(agentId));
	}

	@Test
	public void testReportAgentClassFail() {
		try {
			notary.storeAgentClassAndId(0L, "NoAgentClass");
			fail("Exception exptected.");
		} catch (RuntimeException e) {
			assertEquals("Could not find agent class of name:NoAgentClass", e.getMessage());
		}
	}

	@Test
	public void testRegisterAgent() {
		Agent mockedAgent = mock(Agent.class);
		notary.registerLocalAgent(mockedAgent);
		@SuppressWarnings("unchecked")
		ArrayList<Agent> agents = (ArrayList<Agent>) AccessPrivates.getPrivateFieldOf("localAgents", notary);
		assertTrue(agents.contains(mockedAgent));
	}

	@Test
	public void testGetEnumOfProductFailClass() {
		HashMap<Class<?>, ArrayList<Enum<?>>> instantiableAgentProducts = new HashMap<>();
		AccessPrivates.setPrivateField("instantiableAgentProducts", notary, instantiableAgentProducts);
		try {
			AccessPrivates.callPrivateMethodOn("getEnumOfProduct", notary, "noProduct", Agent.class);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals(Notary.NO_PRODUCTS_FOUND + "Agent", e.getMessage());
		}
	}

	@Test
	public void testGetEnumOfProductFailProduct() {
		HashMap<Class<?>, ArrayList<Enum<?>>> instantiableAgentProducts = new HashMap<>();
		instantiableAgentProducts.put(Agent.class, new ArrayList<>());
		AccessPrivates.setPrivateField("instantiableAgentProducts", notary, instantiableAgentProducts);
		try {
			AccessPrivates.callPrivateMethodOn("getEnumOfProduct", notary, "noProduct", Agent.class);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals("noProduct is not a known @Product of Agent", e.getMessage());
		}
	}

	@Test
	public void testGetEnumOfProduct() {
		HashMap<Class<?>, ArrayList<Enum<?>>> instantiableAgentProducts = new HashMap<>();
		ArrayList<Enum<?>> enums = new ArrayList<>();
		enums.add(Products.ProductB);
		enums.add(Products.ProductA);
		instantiableAgentProducts.put(Agent.class, enums);
		AccessPrivates.setPrivateField("instantiableAgentProducts", notary, instantiableAgentProducts);
		Enum<?> product = (Enum<?>) AccessPrivates.callPrivateMethodOn("getEnumOfProduct", notary, "ProductA",
			Agent.class);
		assertEquals(Products.ProductA, product);
	}

	@Test
	public void testGetProductOfAgent() {
		prepareReflectionResults();
		Enum<?> product = (Enum<?>) AccessPrivates.callPrivateMethodOn("getProductOfAgent", notary, 0L,
			"ProductA");
		assertEquals(Products.ProductA, product);
	}

	private void prepareReflectionResults() {
		HashMap<Class<?>, ArrayList<Enum<?>>> instantiableAgentProducts = new HashMap<>();
		ArrayList<Enum<?>> enums = new ArrayList<>();
		enums.add(Products.ProductA);
		instantiableAgentProducts.put(Agent.class, enums);
		AccessPrivates.setPrivateField("instantiableAgentProducts", notary, instantiableAgentProducts);

		HashMap<Long, Class<?>> agentClasses = new HashMap<>();
		agentClasses.put(0L, Agent.class);
		AccessPrivates.setPrivateField("allAgentClasses", notary, agentClasses);
	}

	@Test
	public void testBuildContractsFromProto() {
		prepareReflectionResults();
		List<ProtoContract> contractPrototypes = new ArrayList<>();
		ProtoContract proto = ContractTest.getContractBuilder(0L, 0L, "ProductA", 0L, 1L, 0L).build();
		contractPrototypes.add(proto);
		@SuppressWarnings("unchecked")
		ArrayList<Contract> contracts = (ArrayList<Contract>) AccessPrivates.callPrivateMethodOn("buildContractsFromProto",
			notary, contractPrototypes);
		assertEquals(1, contracts.size());
		Contract contract = contracts.get(0);
		assertEquals(Products.ProductA, contract.getProduct());
	}

	@Test
	public void testAssignContractsToAgentId() {
		ArrayList<Contract> contracts = new ArrayList<>();
		Contract contractA = ContractTest.buildContract(1L, 2L, "ProductA", 0L, 0L, 1L, 0L, Products.ProductA);
		Contract contractB = ContractTest.buildContract(2L, 4L, "ProductB", 0L, 0L, 1L, 0L, Products.ProductB);
		Contract contractC = ContractTest.buildContract(2L, 3L, "ProductC", 0L, 0L, 1L, 0L, Products.ProductC);
		Contract contractD = ContractTest.buildContract(4L, 5L, "ProductD", 0L, 0L, 1L, 0L, Products.ProductD);
		contracts.add(contractA);
		contracts.add(contractB);
		contracts.add(contractC);
		contracts.add(contractD);
		@SuppressWarnings("unchecked")
		HashMap<Long, ArrayList<Contract>> contractMap = (HashMap<Long, ArrayList<Contract>>) AccessPrivates
			.callPrivateMethodOn("assignContractsToAgentId", notary, contracts);
		assertTrue(contractMap.get(1L).contains(contractA));
		assertTrue(contractMap.get(2L).contains(contractA));
		assertTrue(contractMap.get(2L).contains(contractB));
		assertTrue(contractMap.get(4L).contains(contractB));
		assertTrue(contractMap.get(2L).contains(contractC));
		assertTrue(contractMap.get(3L).contains(contractC));
		assertTrue(contractMap.get(4L).contains(contractD));
		assertTrue(contractMap.get(5L).contains(contractD));
	}

	@Test
	public void testUpdateContractsOfLocalAgents() {
		Agent mockedAgentA = mock(Agent.class);
		Agent mockedAgentB = mock(Agent.class);
		setLocalAgents(mockedAgentA, mockedAgentB);

		HashMap<Long, ArrayList<Contract>> contractMap = new HashMap<>();
		ArrayList<Contract> contractsA = new ArrayList<>();
		ArrayList<Contract> contractsB = new ArrayList<>();
		Contract contractA = mock(Contract.class);
		Contract contractB = mock(Contract.class);
		contractsA.add(contractA);
		contractsA.add(contractB);
		contractsB.add(contractB);
		contractMap.put(0L, contractsA);
		contractMap.put(1L, contractsB);
		AccessPrivates.callPrivateMethodOn("updateContractsOfLocalAgents", notary, contractMap);
		verify(mockedAgentA, times(1)).addContract(contractA);
		verify(mockedAgentA, times(1)).addContract(contractB);
		verify(mockedAgentB, times(1)).addContract(contractB);
	}

	/** sets Id (starting at 0) for given mocked {@link Agent}s and adds them to localAgents of {@link Notary} */
	private void setLocalAgents(Agent... mockedAgents) {
		long id = 0;
		ArrayList<Agent> localAgents = new ArrayList<>();
		for (Agent agent : mockedAgents) {
			when(agent.getId()).thenReturn(id);
			localAgents.add(agent);
			id++;
		}
		AccessPrivates.setPrivateField("localAgents", notary, localAgents);
	}

	@Test
	public void testEnsureAgentIdIsReportedWorks() {
		addAgentsOfClass(Agent.class, 0L);
		AccessPrivates.callPrivateMethodOn("ensureAgentIdIsReported", notary, 0L);
	}

	/** Adds all given Ids to "allAgentClasses" map of {@link Notary} linking to the given {@link Agent}-classType */
	private void addAgentsOfClass(Class<?> classType, Long... ids) {
		for (Long id : ids) {
			spyOnAllAgentClasses.put(id, classType);
		}
	}

	@Test
	public void testEnsureAgentIdIsReportedFail() {
		addAgentsOfClass(Agent.class, 0L);
		try {
			AccessPrivates.callPrivateMethodOn("ensureAgentIdIsReported", notary, 1L);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals(Notary.AGENT_NOT_FOUND + "1", e.getMessage());
		}
	}

	@Test
	public void testEnsureAllContractedAgentsExistAllAgentsOk() {
		addAgentsOfClass(Agent.class, 0L, 1L, 2L);
		ArrayList<ProtoContract> contractList = new ArrayList<>();
		contractList.add(ContractTest.getContractBuilder(0L, 1L, "ProductA", 0L, 1L, 0L).build());
		contractList.add(ContractTest.getContractBuilder(2L, 1L, "ProductA", 0L, 1L, 0L).build());
		AccessPrivates.callPrivateMethodOn("ensureAllContractedAgentsExist", notary, contractList);
	}

	@Test
	public void testEnsureAllContractedAgentsExistMissingSender() {
		addAgentsOfClass(Agent.class, 0L, 1L, 2L);
		ArrayList<ProtoContract> contractList = new ArrayList<>();
		contractList.add(ContractTest.getContractBuilder(0L, 1L, "ProductA", 0L, 1L, 0L).build());
		contractList.add(ContractTest.getContractBuilder(3L, 2L, "ProductA", 0L, 1L, 0L).build());
		try {
			AccessPrivates.callPrivateMethodOn("ensureAllContractedAgentsExist", notary, contractList);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals(Notary.AGENT_NOT_FOUND + 3, e.getMessage());
		}
	}

	@Test
	public void testEnsureAllContractedAgentsExistMissingReceiver() {
		addAgentsOfClass(Agent.class, 0L, 1L, 2L);
		ArrayList<ProtoContract> contractList = new ArrayList<>();
		contractList.add(ContractTest.getContractBuilder(0L, 1L, "ProductA", 0L, 1L, 0L).build());
		contractList.add(ContractTest.getContractBuilder(2L, 3L, "ProductA", 0L, 1L, 0L).build());
		try {
			AccessPrivates.callPrivateMethodOn("ensureAllContractedAgentsExist", notary, contractList);
			fail("Exception expected.");
		} catch (RuntimeException e) {
			assertEquals(Notary.AGENT_NOT_FOUND + 3, e.getMessage());
		}
	}

	@Test
	public void testSetInitialContracts() {
		AgentDummy mockedAgent0 = mock(AgentDummy.class);
		when(mockedAgent0.getId()).thenReturn(0L);
		AgentDummy mockedAgent1 = mock(AgentDummy.class);
		when(mockedAgent1.getId()).thenReturn(1L);
		AgentDummy mockedAgent2 = mock(AgentDummy.class);
		when(mockedAgent2.getId()).thenReturn(2L);

		ArrayList<AgentDummy> localAgents = new ArrayList<>();
		localAgents.add(mockedAgent0);
		localAgents.add(mockedAgent1);
		localAgents.add(mockedAgent2);
		AccessPrivates.setPrivateField("localAgents", notary, localAgents);

		notary.storeAgentClassAndId(0L, "AgentDummy");
		notary.storeAgentClassAndId(1L, "AgentDummy");
		notary.storeAgentClassAndId(2L, "AgentDummy");
		notary.storeAgentClassAndId(3L, "AgentDummy");

		ArrayList<ProtoContract> initialContracts = new ArrayList<>();
		initialContracts.add(ProtoContract.newBuilder().setSenderId(0L).setReceiverId(1L)
			.setProductName(AgentDummy.MyProducts.ProdA.name())
			.setFirstDeliveryTime(0L).setDeliveryIntervalInSteps(1L).build());
		initialContracts.add(ProtoContract.newBuilder().setSenderId(3L).setReceiverId(0L)
			.setProductName(AgentDummy.MyProducts.ProdA.name())
			.setFirstDeliveryTime(0L).setDeliveryIntervalInSteps(1L).build());
		notary.setInitialContracts(initialContracts);

		verify(mockedAgent0, times(2)).addContract(any());
		verify(mockedAgent1, times(1)).addContract(any());
		verify(mockedAgent2, times(0)).addContract(any());
	}

	@Test
	public void testSetInitialContractsNoContracts() {
		Agent mockedAgentA = mock(Agent.class);
		setLocalAgents(mockedAgentA);
		ArrayList<ProtoContract> initialContracts = new ArrayList<>();
		notary.setInitialContracts(initialContracts);
	}
}