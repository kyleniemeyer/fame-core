package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.Agent.WarmUpStatus;
import de.dlr.gitlab.fame.agent.PlannedAction;
import de.dlr.gitlab.fame.mpi.Constants;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.ScheduledTime;
import de.dlr.gitlab.fame.protobuf.Services.WarmUpMessage;
import de.dlr.gitlab.fame.service.scheduling.Schedule;
import de.dlr.gitlab.fame.service.scheduling.ScheduleTest.Reasons;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;
import de.dlr.gitlab.fame.time.TimeStamp;

public class SchedulerTest {
	private Scheduler scheduler;
	private Schedule mockSchedule;
	private MpiManager mockedMpi;
	private LogChecker logChecker = new LogChecker(Scheduler.class);

	@Before
	public void beforeEachTest() {
		mockedMpi = mock(MpiManager.class);
		scheduler = new Scheduler(mockedMpi);
		SimulationParam config = SimulationParam.newBuilder().setStartTime(0L).setStopTime(10L).setRandomSeed(0).build();
		scheduler.initialise(config);
		mockSchedule = mock(Schedule.class);
		AccessPrivates.setPrivateField("schedule", scheduler, mockSchedule);
		logChecker.clear();
	}

	@Test
	public void testRegisterAgent() {
		Agent mockAgent = mock(Agent.class);
		scheduler.registerAgent(mockAgent);
		@SuppressWarnings("unchecked")
		ArrayList<Agent> agentsForWarmUp = (ArrayList<Agent>) AccessPrivates.getPrivateFieldOf("agentsForWarmUp",
				scheduler);
		assertEquals(1, agentsForWarmUp.size());
		assertTrue(agentsForWarmUp.contains(mockAgent));
	}

	@Test
	public void testAddActionAt() {
		Agent agent = new AgentMockUp(0);
		scheduler.addActionAt(agent, mockAction(55, Reasons.Reason1));
		verify(mockSchedule, times(1)).addSingleActionAt(eq(agent), any(TimeStamp.class), eq(Reasons.Reason1));
		int numberOfScheduledActions = (int) AccessPrivates.callPrivateMethodOn("getNumOfScheduledActions", scheduler, 0L);
		assertEquals(1, numberOfScheduledActions);
	}

	private PlannedAction mockAction(long time, Enum<?> reason) {
		PlannedAction mockAction = mock(PlannedAction.class);
		doReturn(reason).when(mockAction).getSchedulingReason();
		when(mockAction.getTimeStamp()).thenReturn(new TimeStamp(time));
		return mockAction;
	}

	@Test
	public void testHasTasksRemaining() {
		scheduler.hasTasksRemaining();
		verify(mockSchedule).hasTasksRemaining();
	}

	@Test
	public void testGetCurrentTime() {
		scheduler.getCurrentTime();
		verify(mockSchedule).getCurrentTime();
	}

	@Test
	public void testGetNextTimeStep() {
		TimeStamp nextStamp = new TimeStamp(3L);
		doReturn(nextStamp).when(mockSchedule).getNextTimeInSchedule();
		TimeStamp returnedStamp = scheduler.getNextTimeStep();
		verify(mockSchedule).getNextTimeInSchedule();
		assertEquals(0, nextStamp.compareTo(returnedStamp));
	}

	@Test
	public void testFindLowestNextScheduledTime() {
		List<MpiMessage> messageList = createMessageList();
		Method method = AccessPrivates.accessMethod("findLowestNextScheduledTime", scheduler.getClass(), List.class);
		long nextTime = (long) AccessPrivates.invokeMethodOn(method, scheduler, messageList);
		assertEquals(1L, nextTime);
	}

	private List<MpiMessage> createMessageList() {
		List<MpiMessage> messageList = new ArrayList<MpiMessage>();
		messageList.add(buildScheduledTimeMessage(5L));
		messageList.add(buildScheduledTimeMessage(7L));
		messageList.add(buildScheduledTimeMessage(5L));
		messageList.add(buildScheduledTimeMessage(2L));
		messageList.add(buildScheduledTimeMessage(1L));
		messageList.add(buildScheduledTimeMessage(10L));
		return messageList;
	}

	private MpiMessage buildScheduledTimeMessage(long time) {
		MpiMessage.Builder messageBuilder = MpiMessage.newBuilder();
		ScheduledTime.Builder scheduledTimeBuilder = ScheduledTime.newBuilder();
		return messageBuilder.setScheduledTime(scheduledTimeBuilder.setTimeStep(time)).build();
	}

	@Test
	public void testReceiveNextScheduledTimeFromRoot() {
		long nextScheduledStep = 5L;
		Bundle bundle = Bundle.newBuilder().addMessage(buildScheduledTimeMessage(nextScheduledStep)).build();
		when(mockedMpi.broadcast(any(), eq(Constants.ROOT))).thenReturn(bundle);
		TimeStamp nextTimeStamp = (TimeStamp) AccessPrivates.callPrivateMethodOn("receiveNextScheduledTimeFromRoot",
				scheduler, bundle);
		verify(mockedMpi).broadcast(bundle, Constants.ROOT);
		assertEquals(0, nextTimeStamp.compareTo(new TimeStamp(nextScheduledStep)));
	}

	@Test
	public void testWhenRootThenBundleNextTimeIsRoot() {
		when(mockedMpi.isRoot()).thenReturn(true);
		Bundle inputBundle = Bundle.newBuilder().addAllMessage(createMessageList()).build();
		Bundle outputBundle = (Bundle) AccessPrivates.callPrivateMethodOn("whenRootThenBundleNextTime", scheduler,
				inputBundle);
		List<MpiMessage> messageList = outputBundle.getMessageList();
		assertEquals(1, messageList.size());
		long minimumOfInput = 1L;
		long nextStepFrom = messageList.get(0).getScheduledTime().getTimeStep();
		assertEquals(minimumOfInput, nextStepFrom);
	}

	@Test
	public void testWhenRootThenBundleNextTimeNotRoot() {
		when(mockedMpi.isRoot()).thenReturn(false);
		long nextScheduledStep = 5L;
		Bundle inputBundle = Bundle.newBuilder().addMessage(buildScheduledTimeMessage(nextScheduledStep)).build();
		Bundle outputBundle = (Bundle) AccessPrivates.callPrivateMethodOn("whenRootThenBundleNextTime", scheduler,
				inputBundle);
		assertEquals(inputBundle, outputBundle);
	}

	@Test
	public void testReportNextScheduledTimeToRoot() {
		long nextTimeStep = 5L;
		TimeStamp nextTimeStamp = new TimeStamp(nextTimeStep);
		when(mockedMpi.aggregateMessagesAt(any(Bundle.class), eq(Constants.ROOT), eq(Tag.SCHEDULE)))
				.then(returnsFirstArg());
		Bundle outputBundle = (Bundle) AccessPrivates.callPrivateMethodOn("reportNextScheduledTimeToRoot", scheduler,
				nextTimeStamp);
		long transmittedTimeStep = outputBundle.getMessage(0).getScheduledTime().getTimeStep();
		assertEquals(nextTimeStep, transmittedTimeStep);
		verify(mockedMpi).aggregateMessagesAt(outputBundle, Constants.ROOT, Tag.SCHEDULE);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRemoveActionFromRegister() {
		long agentId = 1233L;
		int startCount = 10;
		HashMap<Long, Integer> scheduledActionCount = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("scheduledActionsByAgentId", scheduler);
		scheduledActionCount.put(agentId, startCount);

		AccessPrivates.callPrivateMethodOn("removeActionFromRegister", scheduler, agentId);
		assertEquals(startCount - 1, (int) scheduledActionCount.get(agentId));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRemoveActionFromRegisterFailAssertion() {
		long agentId = 1;
		int startCount = 0;
		HashMap<Long, Integer> scheduledActionCount = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("scheduledActionsByAgentId", scheduler);
		scheduledActionCount.put(agentId, startCount);
		AccessPrivates.callPrivateMethodOn("removeActionFromRegister", scheduler, agentId);
		logChecker.assertLogsContain(Scheduler.ERR_ACTION_COUNT_NEGATIVE);
	}

	@Test
	public void testNeedsFurtherWarmUp() {
		assertTrue(scheduler.needsFurtherWarmUp());
		AccessPrivates.setPrivateField("needsFurtherWarmUp", scheduler, false);
		assertFalse(scheduler.needsFurtherWarmUp());
	}

	@Test
	public void testExecuteWarmUpIncomplete() {
		Agent mockAgent = mock(Agent.class);
		when(mockAgent.executeWarmUp(any(TimeStamp.class))).thenReturn(WarmUpStatus.INCOMPLETE);

		@SuppressWarnings("unchecked")
		ArrayList<Agent> agentsForWarmUp = (ArrayList<Agent>) AccessPrivates.getPrivateFieldOf("agentsForWarmUp",
				scheduler);
		agentsForWarmUp.add(mockAgent);
		scheduler.executeWarmUp();
		verify(mockAgent, times(1)).executeWarmUp(any());
		assertTrue(agentsForWarmUp.contains(mockAgent));
	}

	@Test
	public void testExecuteWarmUpCompleted() {
		Agent mockAgent = mock(Agent.class);
		when(mockAgent.executeWarmUp(any(TimeStamp.class))).thenReturn(WarmUpStatus.COMPLETED);
		@SuppressWarnings("unchecked")
		ArrayList<Agent> agentsForWarmUp = (ArrayList<Agent>) AccessPrivates.getPrivateFieldOf("agentsForWarmUp",
				scheduler);
		agentsForWarmUp.add(mockAgent);
		when(mockSchedule.getInitialTime()).thenReturn(new TimeStamp(45));
		scheduler.executeWarmUp();
		verify(mockAgent, times(1)).executeWarmUp(any());
		assertFalse(agentsForWarmUp.contains(mockAgent));
	}

	@Test
	public void testAnyProcessNeedsWarmUpFalseFalse() {
		Bundle inputBundle = Bundle.newBuilder()
				.addMessage(MpiMessage.newBuilder().setWarmUp(WarmUpMessage.newBuilder().setNeeded(false)))
				.addMessage(MpiMessage.newBuilder().setWarmUp(WarmUpMessage.newBuilder().setNeeded(false))).build();
		Bundle outputBundle = (Bundle) AccessPrivates.callPrivateMethodOn("anyProcessNeedsWarmUp", scheduler, inputBundle);
		assertFalse(outputBundle.getMessage(0).getWarmUp().getNeeded());
	}

	@Test
	public void testAnyProcessNeedsWarmUpFalseTrueFalse() {
		Bundle inputBundle = Bundle.newBuilder()
				.addMessage(MpiMessage.newBuilder().setWarmUp(WarmUpMessage.newBuilder().setNeeded(false)))
				.addMessage(MpiMessage.newBuilder().setWarmUp(WarmUpMessage.newBuilder().setNeeded(true)))
				.addMessage(MpiMessage.newBuilder().setWarmUp(WarmUpMessage.newBuilder().setNeeded(false))).build();
		Bundle outputBundle = (Bundle) AccessPrivates.callPrivateMethodOn("anyProcessNeedsWarmUp", scheduler, inputBundle);
		assertTrue(outputBundle.getMessage(0).getWarmUp().getNeeded());
	}

	@Test
	public void testAnyProcessNeedsWarmUpTrue() {
		Bundle inputBundle = Bundle.newBuilder()
				.addMessage(MpiMessage.newBuilder().setWarmUp(WarmUpMessage.newBuilder().setNeeded(true))).build();
		Bundle outputBundle = (Bundle) AccessPrivates.callPrivateMethodOn("anyProcessNeedsWarmUp", scheduler, inputBundle);
		assertTrue(outputBundle.getMessage(0).getWarmUp().getNeeded());
	}

	@Test
	public void testGetAgentForWarmup() {
		ArrayList<Agent> agents = new ArrayList<>();
		AccessPrivates.setPrivateField("agentsForWarmUp", scheduler, agents);
		assertEquals(agents, scheduler.getRemainingAgentsForWarmUp());
	}

	@Test
	public void testSynchroniseWarmupYesRoot() {
		ArrayList<Agent> agents = new ArrayList<>();
		AccessPrivates.setPrivateField("agentsForWarmUp", scheduler, agents);
		agents.add(mock(Agent.class));
		when(mockedMpi.isRoot()).thenReturn(true);
		when(mockedMpi.aggregateMessagesAt(any(), eq(Constants.ROOT), eq(Tag.WARM_UP))).then(returnsFirstArg());
		when(mockedMpi.broadcast(any(), eq(Constants.ROOT))).then(returnsFirstArg());

		scheduler.sychroniseWarmUp();
		verify(mockedMpi, times(1)).broadcast(any(Bundle.class), eq(Constants.ROOT));
		assertTrue((boolean) AccessPrivates.getPrivateFieldOf("needsFurtherWarmUp", scheduler));
	}

	@Test
	public void testSynchroniseWarmupNoYesRoot() {
		ArrayList<Agent> agents = new ArrayList<>();
		AccessPrivates.setPrivateField("agentsForWarmUp", scheduler, agents);
		when(mockedMpi.isRoot()).thenReturn(false);
		when(mockedMpi.aggregateMessagesAt(any(), eq(Constants.ROOT), eq(Tag.WARM_UP))).then(returnsFirstArg());
		when(mockedMpi.broadcast(any(), eq(Constants.ROOT))).then(returnsFirstArg());

		scheduler.sychroniseWarmUp();
		verify(mockedMpi, times(1)).broadcast(any(Bundle.class), eq(Constants.ROOT));
		assertFalse((boolean) AccessPrivates.getPrivateFieldOf("needsFurtherWarmUp", scheduler));
	}
}