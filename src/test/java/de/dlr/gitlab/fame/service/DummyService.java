package de.dlr.gitlab.fame.service;

import de.dlr.gitlab.fame.mpi.MpiManager;

/** 
 * Dummy child of {@link Service} to test its functions
 *
 * @author Christoph Schimeczek
 */
public class DummyService extends Service {

	public DummyService(MpiManager mpi) {
		super(mpi);
	}
}