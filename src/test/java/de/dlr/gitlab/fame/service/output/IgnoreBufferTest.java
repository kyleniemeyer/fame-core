package de.dlr.gitlab.fame.service.output;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.testUtils.ExceptionTesting;

public class IgnoreBufferTest {
	private enum DummyEnum {
		A, B, C
	};

	private long myId = 11L;
	private String className = "MyClassName";
	private IgnoreBuffer ignoreBuffer;

	@Before
	public void setUp() {
		ignoreBuffer = new IgnoreBuffer(className, myId);
	}

	@Test
	public void constructor_savesClassname() {
		assertEquals(className, ignoreBuffer.getClassName());
	}

	@Test
	public void constructor_savesId() {
		assertEquals(myId, ignoreBuffer.getAgentId());
	}

	@Test
	public void store_simple_isCallable() {
		ignoreBuffer.store(DummyEnum.A, 5.);
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void store_complex_isCallabale() {
		ComplexIndex complexColumn = mock(ComplexIndex.class);
		ignoreBuffer.store(complexColumn, "HelloWorld");
	}

	@Test
	public void getSeries_throwsRuntimeException() {
		ExceptionTesting.assertThrowsFatalMessage(IgnoreBuffer.EX_NON_REACHABLE, () -> ignoreBuffer.getSeries());
	}

	@Test
	public void tick_isCallable() {
		ExceptionTesting.assertThrowsFatalMessage(IgnoreBuffer.EX_NON_REACHABLE, () -> ignoreBuffer.tick(5L));
	}
}