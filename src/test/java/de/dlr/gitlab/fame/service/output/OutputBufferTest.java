package de.dlr.gitlab.fame.service.output;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class OutputBufferTest {
	class BufferTester extends OutputBuffer {

		BufferTester(String className, long agentId) {
			super(className, agentId);
		}

		@Override
		public void store(Enum<?> type, double value) {}

		@Override
		public void store(ComplexIndex<?> complexField, Object value) {}

		@Override
		protected void tick(long timeStep) {}

		@Override
		protected Series getSeries() {
			return null;
		}
	}

	private BufferTester buffer;

	@Before
	public void setup() {
		buffer = new BufferTester("testClass", 14L);
	}

	@Test
	public void testAgentId() {
		long agentId = (long) AccessPrivates.callMethodOfClassXOnY("getAgentId", OutputBuffer.class, buffer);
		assertEquals(14L, agentId);
	}

	@Test
	public void testClassName() {
		String className = (String) AccessPrivates.callMethodOfClassXOnY("getClassName", OutputBuffer.class, buffer);
		assertEquals("testClass", className);
	}
}