package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle.Builder;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook;
import de.dlr.gitlab.fame.service.PostOffice.AGENT_STATUS_CHANGE;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.MpiMock;

public class PostOfficeTest {
	private static final int MY_RANK = 0;
	private static final int PROCESS_COUNT = 2;

	private PostOffice postOffice;
	private MpiManager mockMpi;
	private Setup mockSetup;
	private TimeSeriesProvider mockSeriesProvider;

	public class DummyAgent extends Agent {
		public DummyAgent(AgentDao dao) {
			super(new DataProvider(dao, null, mock(LocalServices.class)));
		}
	}

	@Before
	public void setUp() {
		mockMpi();
		mockSetup = mock(Setup.class);
		when(mockSetup.getPortablePackages()).thenReturn(Arrays.asList("de.dlr.gitlab.fame.communication.transfer"));
		mockSeriesProvider = mock(TimeSeriesProvider.class);
		postOffice = new PostOffice(mockMpi, mockSetup, mockSeriesProvider);
	}

	private void mockMpi() {
		mockMpi = MpiMock.newMockMpi();
		when(mockMpi.getProcessCount()).thenReturn(PROCESS_COUNT);
		when(mockMpi.getRank()).thenReturn(MY_RANK);
	}

	@Test
	public void testSendMessageWithDataItem() {
		@SuppressWarnings("unchecked") ArrayList<ProtoMessage> messageHeap = (ArrayList<ProtoMessage>) AccessPrivates
				.getPrivateFieldOf("messageHeap",
						postOffice);
		messageHeap.clear();
		postOffice.sendMessage(0L, 1L, null, new DummyDataItem());
		ProtoMessage createdMessage = messageHeap.get(0);
		assertEquals(0L, createdMessage.getSenderId());
		assertEquals(1L, createdMessage.getReceiverId());
		assertEquals(1, createdMessage.getDataItemCount());
	}

	@Test
	public void testSendMessageWithPortable() {
		@SuppressWarnings("unchecked") ArrayList<ProtoMessage> messageHeap = (ArrayList<ProtoMessage>) AccessPrivates
				.getPrivateFieldOf("messageHeap",
						postOffice);
		messageHeap.clear();
		postOffice.sendMessage(2L, 13L, new DummyPortableEmpty(), new DummyDataItem());
		ProtoMessage createdMessage = messageHeap.get(0);
		assertEquals(2L, createdMessage.getSenderId());
		assertEquals(13L, createdMessage.getReceiverId());
		assertEquals(1, createdMessage.getDataItemCount());
		assertEquals(1, createdMessage.getNestedItemCount());
	}

	@Test
	public void testClearPostBoxesBuilders() {
		@SuppressWarnings("unchecked") ArrayList<Builder> postBoxesBuilders = (ArrayList<Builder>) AccessPrivates
				.getPrivateFieldOf("postBoxesBuilders",
						postOffice);
		postBoxesBuilders.get(0).addMessage(MpiMessage.newBuilder());
		postBoxesBuilders.get(1).addMessage(MpiMessage.newBuilder());
		AccessPrivates.callPrivateMethodOn("clearPostBoxesBuilders", postOffice);
		assertEquals(0, postBoxesBuilders.get(0).getMessageCount());
		assertEquals(0, postBoxesBuilders.get(1).getMessageCount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSortMessagesByPostOffice() {
		ArrayList<ProtoMessage> messageHeap = (ArrayList<ProtoMessage>) AccessPrivates.getPrivateFieldOf("messageHeap",
				postOffice);
		ProtoMessage.Builder builder = ProtoMessage.newBuilder();
		messageHeap.add(builder.clear().setSenderId(99L).setReceiverId(0L).build());
		messageHeap.add(builder.clear().setSenderId(99L).setReceiverId(1L).build());
		messageHeap.add(builder.clear().setSenderId(99L).setReceiverId(2L).build());

		HashMap<Long, Integer> processIdByAgent = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("processIdByAgent", postOffice);
		processIdByAgent.put(0L, 0);
		processIdByAgent.put(1L, 1);
		processIdByAgent.put(2L, 0);

		AccessPrivates.callPrivateMethodOn("sortMessagesByPostOffice", postOffice);
		ArrayList<Builder> postBoxesBuilders = (ArrayList<Builder>) AccessPrivates.getPrivateFieldOf("postBoxesBuilders",
				postOffice);
		assertEquals(2, postBoxesBuilders.get(0).getMessageCount());
		assertEquals(1, postBoxesBuilders.get(1).getMessageCount());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testBuildPostBoxes() {
		ArrayList<Builder> postBoxesBuilders = (ArrayList<Builder>) AccessPrivates.getPrivateFieldOf("postBoxesBuilders",
				postOffice);
		ArrayList<Bundle> postBoxes = (ArrayList<Bundle>) AccessPrivates.getPrivateFieldOf("postBoxes", postOffice);
		MpiMessage.Builder builder = MpiMessage.newBuilder();
		postBoxesBuilders.get(0)
				.addMessage(builder.clear().setMessage(ProtoMessage.newBuilder().setSenderId(99L).setReceiverId(0L)).build());
		postBoxesBuilders.get(0)
				.addMessage(builder.clear().setMessage(ProtoMessage.newBuilder().setSenderId(99L).setReceiverId(2L)).build());
		postBoxesBuilders.get(1)
				.addMessage(builder.clear().setMessage(ProtoMessage.newBuilder().setSenderId(99L).setReceiverId(1L)).build());

		AccessPrivates.callPrivateMethodOn("buildPostBoxes", postOffice);
		assertEquals(2, postBoxes.get(0).getMessageCount());
		assertEquals(1, postBoxes.get(1).getMessageCount());
	}

	@Test
	public void isLocalAgent_FalseOnEmptyList() {
		assertFalse(postOffice.isLocalAgent(5L));
	}

	@Test
	public void isLocalAgent_FalseOnMissingList() {
		registerAgents(11L);
		assertFalse(postOffice.isLocalAgent(5L));
	}

	/** registers any amount of mocked agents with given IDs */
	private void registerAgents(Long first, Long... agentIds) {
		Agent mockAgent = mock(Agent.class);
		when(mockAgent.getId()).thenReturn(first, agentIds);
		for (int i = 0; i < agentIds.length + 1; i++) {
			postOffice.registerAgent(mockAgent);
		}
	}

	@Test
	public void isLocalAgent_TrueWhenRegistered() {
		registerAgents(1L, 5L, 11L);
		assertTrue(postOffice.isLocalAgent(5L));
	}

	@Test
	public void agentHasStatusChange_FalseOnEmptyList() {
		assertFalse(postOffice.agentHasStatusChange(1L, AGENT_STATUS_CHANGE.REGISTER));
	}

	@Test
	public void agentHasStatusChange_FalseOnMissingAgent() {
		registerAgents(1L);
		assertFalse(postOffice.agentHasStatusChange(2L, AGENT_STATUS_CHANGE.REGISTER));
	}

	@Test
	public void agentHasStatusChange_FalseOnNullSTATUS() {
		registerAgents(1L);
		assertFalse(postOffice.agentHasStatusChange(1L, null));
	}

	@Test
	public void agentHasStatusChange_FalseOnOtherSTATUS() {
		registerAgents(1L);
		assertFalse(postOffice.agentHasStatusChange(1L, AGENT_STATUS_CHANGE.UNREGISTER));
	}

	@Test
	public void agentHasStatusChange_TrueOnIdAndStatusMatch() {
		registerAgents(1L);
		assertTrue(postOffice.agentHasStatusChange(1L, AGENT_STATUS_CHANGE.REGISTER));
	}

	@Test
	public void testUpdateAddressBook() {
		Bundle.Builder bundleBuilder = Bundle.newBuilder();
		AddressBook.Builder builder = AddressBook.newBuilder();
		bundleBuilder.addMessage(
				MpiMessage.newBuilder().setAddressBook(builder.addAgentId(11L).addAgentId(33L).setProcessId(0).build()));
		bundleBuilder
				.addMessage(MpiMessage.newBuilder().setAddressBook(builder.clear().addAgentId(22L).setProcessId(1).build()));

		AccessPrivates.callPrivateMethodOn("updateAddressBook", postOffice, bundleBuilder.build());
		@SuppressWarnings("unchecked") HashMap<Long, Integer> processIdByAgent = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("processIdByAgent", postOffice);

		assertEquals(0, (int) processIdByAgent.get(11L));
		assertEquals(1, (int) processIdByAgent.get(22L));
		assertEquals(0, (int) processIdByAgent.get(33L));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testDeliverMessages() {
		DummyAgent dummyAgent = new DummyAgent(AgentDao.newBuilder().setClassName("Dummy").setId(99L).build());
		HashMap<Long, Agent> localAgents = (HashMap<Long, Agent>) AccessPrivates.getPrivateFieldOf("localAgents",
				postOffice);
		localAgents.put(11L, dummyAgent);
		localAgents.put(33L, dummyAgent);

		HashMap<Long, Integer> processIdByAgent = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("processIdByAgent", postOffice);
		processIdByAgent.put(11L, 0);
		processIdByAgent.put(22L, 1);
		processIdByAgent.put(33L, 0);

		ArrayList<ProtoMessage> messageHeap = (ArrayList<ProtoMessage>) AccessPrivates.getPrivateFieldOf("messageHeap",
				postOffice);
		messageHeap.add(ProtoMessage.newBuilder().setSenderId(99L).setReceiverId(11L).build());
		messageHeap.add(ProtoMessage.newBuilder().setSenderId(99L).setReceiverId(22L).build());
		messageHeap.add(ProtoMessage.newBuilder().setSenderId(99L).setReceiverId(33L).build());

		// postOffice.deliverMessages();
		// ArrayList<Message> inBox = (ArrayList<Message>) AccessPrivates.getPrivateFieldOf("inBox", dummyAgent);
		// assertEquals(2, inBox.size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void synchroniseAddressBooks() {
		registerAgents(11L, 33L);	
		Bundle otherProcessAddressBundle = buildAddressBookBundle(1, 22L);
		HashMap<Long, Integer> processIdByAgent = (HashMap<Long, Integer>) AccessPrivates
				.getPrivateFieldOf("processIdByAgent", postOffice);
		processIdByAgent.put(99L, MY_RANK);
		
		when(mockMpi.aggregateAll(any(), eq(Tag.ADDRESSES))).thenAnswer(new Answer<Bundle>() {
			 @Override
		    public Bundle answer(InvocationOnMock invocation) throws Throwable {
				 Object[] functionArgs = invocation.getArguments();
				 return joinBundles((Bundle) functionArgs[0], otherProcessAddressBundle);
			 }
		});

		postOffice.synchroniseAddressBookUpdates();
		assertEquals(1, (int) processIdByAgent.get(22L));
		assertEquals(0, (int) processIdByAgent.get(11L));
		assertEquals(0, (int) processIdByAgent.get(33L));
		assertEquals(0, (int) processIdByAgent.get(99L));
	}

	/** @return Bundle with AddressBook of the given process with its agents */
	private Bundle buildAddressBookBundle(int processId, long... agentIds) {
		Bundle.Builder bundleBuilder = Bundle.newBuilder();
		AddressBook.Builder addressBuilder = AddressBook.newBuilder();
		MpiMessage.Builder messageBuilder = MpiMessage.newBuilder();
		addressBuilder.setProcessId(processId);
		for (long agentId : agentIds) {
			addressBuilder.addAgentId(agentId);
		}
		return bundleBuilder.addMessage(messageBuilder.setAddressBook(addressBuilder)).build();
	}
	
	/** Joins any amount of Bundles to a single bundle
	 * 
	 * @param bundles to join
	 * @return new bundle containing all the content from given bundles */
	private static Bundle joinBundles(Bundle... bundles) {
		Builder bundleBuilder = Bundle.newBuilder();
		for (Bundle bundle : bundles) {
			bundleBuilder.addAllMessage(bundle.getMessageList());
		}
		return bundleBuilder.build();
	}
}
