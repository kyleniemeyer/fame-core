package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Services.AddressBook;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.ProtoSetup;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType.Field;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line;
import de.dlr.gitlab.fame.protobuf.Services.Output.Series.Line.Column;
import de.dlr.gitlab.fame.protobuf.Services.ScheduledTime;
import de.dlr.gitlab.fame.protobuf.Services.WarmUpMessage;

/** Tests for class {@link Service} */
public class ServiceTest {
	private Service service;

	@Before
	public void setup() {
		service = new DummyService(mock(MpiManager.class));
	}

	@Test
	public void testCreateBundleForInput() {
		InputData data = InputData.newBuilder()
			.setRunId(0L)
			.setSimulation(SimulationParam.newBuilder()
				.setStartTime(0L)
				.setStopTime(1L)
				.setRandomSeed(0L))
			.build();
		Bundle bundle = service.createBundleForInput(data);
		InputData result = bundle.getMessage(0).getInput();
		assertEquals(data, result);
	}

	@Test
	public void testCreateBundleForScheduledTime() {
		ScheduledTime.Builder data = ScheduledTime.newBuilder().setTimeStep(11L);
		Bundle bundle = service.createBundleForScheduledTime(data);
		ScheduledTime result = bundle.getMessage(0).getScheduledTime();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForWarmUpMessage() {
		WarmUpMessage.Builder data = WarmUpMessage.newBuilder().setNeeded(true);
		Bundle bundle = service.createBundleForWarmUpMessage(data);
		WarmUpMessage result = bundle.getMessage(0).getWarmUp();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForAddressBook() {
		AddressBook.Builder data = AddressBook.newBuilder().setProcessId(55).addAgentId(21L).addAgentId(42L);
		Bundle bundle = service.createBundleForAddressBook(data);
		AddressBook result = bundle.getMessage(0).getAddressBook();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForOutput() {
		Output.Builder data = Output.newBuilder()
			.addAgentType(AgentType.newBuilder()
				.setClassName("DummyClass")
				.addField(Field.newBuilder().setFieldId(0).setFieldName("EmptyField")))
			.addSeries(Series.newBuilder()
				.setClassName("DummyClass")
				.setAgentId(33L)
				.addLine(Line.newBuilder()
					.setTimeStep(15L)
					.addColumn(Column.newBuilder().setFieldId(13).setValue(15.55d))));
		Bundle bundle = service.createBundleForOutput(data);
		Output result = bundle.getMessage(0).getOutput();
		assertEquals(data.build(), result);
	}

	@Test
	public void testCreateBundleForSetup() {
		ProtoSetup.Builder data = ProtoSetup.newBuilder()
			.setOutputPath("A")
			.addAgentPackageNames("B")
			.addMessagePackageNames("C")
			.addPortablePackageNames("XX");
		Bundle bundle = service.createBundleForSetup(data);
		ProtoSetup result = bundle.getMessage(0).getSetup();
		assertEquals(data.build(), result);
	}
}