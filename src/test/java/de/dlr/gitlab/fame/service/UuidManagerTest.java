package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import java.util.Random;
import org.junit.Test;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class UuidManagerTest {
	
	
	@Test
	public void testConstructor0() {
		UuidManager uuidManager = (UuidManager) AccessPrivates.usePrivateConstructorOn(UuidManager.class, 0);
		long processIdOffset = (long) AccessPrivates.getPrivateFieldOf("processIdOffset", uuidManager);
		assertEquals(UuidManager.ADDRESS_SPACE, processIdOffset);
	}
	
	@Test
	public void testConstructor4() {
		UuidManager uuidManager = (UuidManager) AccessPrivates.usePrivateConstructorOn(UuidManager.class, 4);
		long processIdOffset = (long) AccessPrivates.getPrivateFieldOf("processIdOffset", uuidManager);
		assertEquals(5L * UuidManager.ADDRESS_SPACE, processIdOffset);
	}
	
	@Test
	public void testGenerateNext() {
		UuidManager uuidManager = (UuidManager) AccessPrivates.usePrivateConstructorOn(UuidManager.class, 0);
		Random random = new Random(173954739L);
		long expectedUuid = UuidManager.ADDRESS_SPACE - 1;
		for (int i = 0; i < 10; i++) {
			long uuid = 0L;
			for (int numberOfGenerations = 0; numberOfGenerations <= random.nextInt(3); numberOfGenerations++) {
				uuid = (long) AccessPrivates.callPrivateMethodOn("generateNext", uuidManager);
				expectedUuid++;
			}
			assertEquals(expectedUuid, uuid);
		}
	}
}