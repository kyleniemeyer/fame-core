package de.dlr.gitlab.fame.service.output;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class FileWriterTest {
	public static final String WORKING_PATH = "./src/test/resources/keepThisFolder";
	private final String illegalPath = "KK:/illegal/path/";
	private final String fileNamePrefix = "test";

	private Setup mockSetup;
	private FileOutputStream mockFos;
	private FileWriter fileWriter;
	private LogChecker logChecker = new LogChecker(FileWriter.class);

	@Before
	public void setup() {
		mockFos = mock(FileOutputStream.class);
		fileWriter = new FileWriter(mockFos);
		logChecker.clear();
	}

	@Test
	public void FileWriter_illegalPath() {
		initialiseSetup(illegalPath, fileNamePrefix, false);
		assertThrowsFatalMessage(FileWriter.ERR_ILLEGAL_PATH, () -> new FileWriter(mockSetup));
	}

	/** Updates the mocked Setup with given parameters */
	private void initialiseSetup(String path, String prefix, boolean withTimeStamp) {
		mockSetup = mock(Setup.class);
		when(mockSetup.getOutputPath()).thenReturn(path);
		when(mockSetup.getOutputFilePrefix()).thenReturn(prefix);
		when(mockSetup.getOutputFileTimeStamp()).thenReturn(withTimeStamp);
	}

	@Test
	public void FileWriter_overrideExistingFile() throws IOException {
		initialiseSetup(WORKING_PATH, fileNamePrefix, false);
		FileWriter fileWriter = new FileWriter(mockSetup);
		String outFileName = FileWriter.fullFileName(mockSetup);
		fileWriter.close();

		try (FileOutputStream fos = new FileOutputStream(outFileName)) {
			fos.write(new byte[] {1, 2, 3, 1, 1, 2, 33, 22, 11, 1});
		}

		fileWriter = new FileWriter(mockSetup);
		fileWriter.close();
		int expected_length = FileWriter.FILE_HEADER.length() + FileWriter.FILE_HEADER_VERSION.length();
		assertEquals(expected_length, Files.size(Paths.get(outFileName)));
	}

	@Test
	public void FileWriter_NullPath() {
		initialiseSetup(null, fileNamePrefix, false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_PATH, () -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_EmptyPath() {
		initialiseSetup("", fileNamePrefix, false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_PATH, () -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_BlankPath() {
		initialiseSetup(" 	", fileNamePrefix, false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_PATH, () -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_NullPrefix() {
		initialiseSetup(WORKING_PATH, null, false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_FILE_PREFIX,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_EmptyPrefix() {
		initialiseSetup(WORKING_PATH, "", false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_FILE_PREFIX,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void FileWriter_BlankPrefix() {
		initialiseSetup(WORKING_PATH, "	 ", false);
		assertThrowsMessage(IllegalArgumentException.class, FileWriter.ERR_MISSING_FILE_PREFIX,
				() -> new FileWriter(mockSetup));
	}

	@Test
	public void fullFileName_FileNameNoTimeStamp() {
		initialiseSetup(WORKING_PATH, "abc", false);
		String result = FileWriter.fullFileName(mockSetup);
		assertEquals(WORKING_PATH + "/abc.pb", result);
	}

	@Test
	public void fullFileName_FileNameWithTimeStamp() {
		initialiseSetup(WORKING_PATH, "abc", true);
		String result = FileWriter.fullFileName(mockSetup);
		assertTrue(result.matches(WORKING_PATH + "/abc.[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}.pb"));
	}

	@Test
	public void FileWriter_WritesHeader() throws IOException {
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockFos, times(1)).write(captor.capture());
		byte[] result = captor.getValue();
		String expected = FileWriter.FILE_HEADER + FileWriter.FILE_HEADER_VERSION;
		assertEquals(expected, new String(result, StandardCharsets.UTF_8));
	}
	
	@Test
	public void FileWriter_ThrowsOnErrorWriteHeader() throws IOException {
		doThrow(IOException.class).when(mockFos).write(any(byte[].class));
		assertThrowsFatalMessage(FileWriter.ERR_WRITE_FAILED, () -> new FileWriter(mockFos));
	}

	@Test
	public void write_NullData() {
		fileWriter.write(null);
		logChecker.assertLogsContain(FileWriter.WARN_EMPTY_CONTENT);
	}

	@Test
	public void write_EmptyData() {
		fileWriter.write(new byte[] {});
		logChecker.assertLogsContain(FileWriter.WARN_EMPTY_CONTENT);
	}

	@Test
	public void write_SingleByte_WritesHeaderFirst_OneByte() throws IOException {
		byte[] payload = new byte[] {1};
		fileWriter.write(payload);
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockFos, times(3)).write(captor.capture());
		List<byte[]> results = captor.getAllValues();
		int bytesAsInt = ByteBuffer.wrap(results.get(1)).getInt();
		assertEquals(payload.length, bytesAsInt);
		assertEquals(payload, results.get(2));
	}

	@Test
	public void write_SingleByte_WritesHeaderFirst_ManyBytes() throws IOException {
		byte[] payload = new byte[] {1, 22, 1, 3, 2, 3, 4};
		fileWriter.write(payload);
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockFos, times(3)).write(captor.capture());
		List<byte[]> results = captor.getAllValues();
		int bytesAsInt = ByteBuffer.wrap(results.get(1)).getInt();
		assertEquals(payload.length, bytesAsInt);
		assertEquals(payload, results.get(2));
	}

	@Test
	public void write_ExceptionOnError_Header() throws IOException {
		doThrow(IOException.class).when(mockFos).write(any(byte[].class));
		assertThrowsFatalMessage(FileWriter.ERR_WRITE_FAILED, () -> fileWriter.write(new byte[] {1}));
	}

	@Test
	public void write_ExceptionOnError_Content() throws IOException {
		doNothing().doThrow(IOException.class).when(mockFos).write(any(byte[].class));
		assertThrowsFatalMessage(FileWriter.ERR_WRITE_FAILED, () -> fileWriter.write(new byte[] {1}));
	}

	@Test
	public void close_Fail() throws IOException {
		doThrow(IOException.class).when(mockFos).close();
		assertThrowsFatalMessage(FileWriter.ERR_CLOSE_FAILED, () -> fileWriter.close());
	}
}