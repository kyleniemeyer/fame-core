package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.ContractTest;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Input.InputData.SimulationParam;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao.Builder;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao.Row;
import de.dlr.gitlab.fame.service.input.FileReaderTest;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.MpiMock;

public class InputManagerTest {
	private InputManager inputManager;
	private MpiManager mockMpiManager;

	@Before
	public void beforeEachTest() {
		mockMpiManager = MpiMock.newMockMpi();
		inputManager = new InputManager(mockMpiManager);
	}

	@Test
	public void testReadIsRoot() {
		inputManager.read(FileReaderTest.inputFilePath);
		InputData inputData = (InputData) AccessPrivates.getPrivateFieldOf("inputData", inputManager);
		assertNotNull(inputData);
	}

	@Test
	public void testReadIsNotRoot() {
		when(mockMpiManager.isRoot()).thenReturn(false);
		inputManager.read(FileReaderTest.inputFilePath);
		InputData inputData = (InputData) AccessPrivates.getPrivateFieldOf("inputData", inputManager);
		assertNull(inputData);
	}

	@Test
	public void testGetContractPrototypes() {
		InputData inputData = InputData.newBuilder()
				.setRunId(0L)
				.setSimulation(InputData.SimulationParam.newBuilder()
						.setStartTime(0L)
						.setStopTime(0L)
						.setRandomSeed(0L))
				.addContract(ContractTest.getContractBuilder(1L, 2L, "ProductA", 3L, 4L, 5L))
				.addContract(ContractTest.getContractBuilder(6L, 7L, "ProductB", 8L, 9L, 10L))
				.build();
		AccessPrivates.setPrivateField("inputData", inputManager, inputData);
		List<ProtoContract> contracts = inputManager.getContractPrototypes();
		assertEquals(2, contracts.size());
		ProtoContract contract = contracts.get(0);
		assertEquals(1L, contract.getSenderId());
		assertEquals(2L, contract.getReceiverId());
		contract = contracts.get(1);
		assertEquals(6L, contract.getSenderId());
		assertEquals(7L, contract.getReceiverId());
	}

	@Test
	public void testGetTimeSeriesIndexCollision() {
		InputData inputData = InputData.newBuilder()
				.setRunId(0L)
				.setSimulation(createConfig(0, 1, 0, 1))
				.addTimeSeries(createTimeSeries(15, "seriesA"))
				.addTimeSeries(createTimeSeries(15, "seriesB"))
				.build();
		AccessPrivates.setPrivateField("inputData", inputManager, inputData);

		try {
			inputManager.getTimeSeries();
			fail("ExceptionExpected");
		} catch (RuntimeException e) {
			assertEquals(InputManager.TIME_SERIES_INDEX_COLLISION + 15, e.getMessage());
		}
	}

	@Test
	public void testGetTimeSeriesNoIndexCollision() {
		InputData inputData = InputData.newBuilder()
				.setRunId(0L)
				.setSimulation(createConfig(0, 1, 0, 1))
				.addTimeSeries(createTimeSeries(15, "seriesA"))
				.addTimeSeries(createTimeSeries(16, "seriesB"))
				.build();
		AccessPrivates.setPrivateField("inputData", inputManager, inputData);
		Map<Integer, TimeSeries> tsMap = inputManager.getTimeSeries();
		assertTrue(tsMap.containsKey(15));
		assertTrue(tsMap.containsKey(16));
	}

	private TimeSeriesDao createTimeSeries(int id, String name) {
		Builder builder = TimeSeriesDao.newBuilder();
		builder.clear();
		builder.addRow(Row.newBuilder().setTimeStep(-10L).setValue(-20.0))
				.addRow(Row.newBuilder().setTimeStep(0L).setValue(-10.0))
				.addRow(Row.newBuilder().setTimeStep(10L).setValue(30.0));
		builder.setSeriesId(id);
		builder.setSeriesName(name);
		return builder.build();
	}

	private SimulationParam createConfig(long start, long stop, long seed, int forecast) {
		return SimulationParam.newBuilder()
				.setStartTime(start).setStopTime(stop).setRandomSeed(seed)
				.build();
	}
}