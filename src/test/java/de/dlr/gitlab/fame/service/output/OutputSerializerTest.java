package de.dlr.gitlab.fame.service.output;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Input.InputData;
import de.dlr.gitlab.fame.protobuf.Mpi.Bundle;
import de.dlr.gitlab.fame.protobuf.Mpi.MpiMessage;
import de.dlr.gitlab.fame.protobuf.Services.Output;
import de.dlr.gitlab.fame.protobuf.Services.Output.AgentType;
import de.dlr.gitlab.fame.protobuf.Storage.DataStorage;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

public class OutputSerializerTest {
	private FileWriter fileWriterMock;
	private OutputSerializer serializer;
	private DataStorage.Builder dataStorageBuilderMock;
	private Output.Builder outputBuilderMock;
	private DataStorage dataStorageMock;

	@Before
	public void setUp() {
		fileWriterMock = mock(FileWriter.class);
		serializer = new OutputSerializer(fileWriterMock);
		dataStorageBuilderMock = mock(DataStorage.Builder.class);
		AccessPrivates.setPrivateFinalField("dataStorageBuilder", serializer, dataStorageBuilderMock);
		outputBuilderMock = mock(Output.Builder.class);
		AccessPrivates.setPrivateFinalField("outputBuilder", serializer, outputBuilderMock);
		dataStorageMock = mock(DataStorage.class);
		when(dataStorageBuilderMock.build()).thenReturn(dataStorageMock);
	}

	@Test
	public void testClose() throws IOException {
		serializer.close();
		verify(fileWriterMock, times(1)).close();
	}

	@Test
	public void testWriteInput() throws IOException {
		InputData mockInput = mock(InputData.class);
		serializer.writeInput(mockInput);
		verify(dataStorageBuilderMock).setInput(mockInput);
		verify(dataStorageBuilderMock).build();
		verify(fileWriterMock).write(any());
	}

	@Test
	public void testWriteBundledOutputNoDataNoCrash() {
		serializer.writeBundledOutput(createBundleMock());
	}

	/** @eturn mocked Bundle for the given output messages */
	private Bundle createBundleMock(Output... outputs) {
		Bundle bundleMock = mock(Bundle.class);
		ArrayList<MpiMessage> messages = new ArrayList<>();
		for (Output output : outputs) {
			MpiMessage messageMock = mock(MpiMessage.class);
			when(messageMock.getOutput()).thenReturn(output);
			messages.add(messageMock);
		}
		when(bundleMock.getMessageList()).thenReturn(messages);
		return bundleMock;
	}

	@Test
	public void testWriteBundledOutputAddNewAgentTypeOnceOnly() {
		Output outputMock = mock(Output.class);
		AgentType agentTypeMock = createAgentMock("MyClassName");
		when(outputMock.getAgentTypeList()).thenReturn(Arrays.asList(agentTypeMock, agentTypeMock, agentTypeMock));
		serializer.writeBundledOutput(createBundleMock(outputMock));
		verify(outputBuilderMock, times(1)).addAgentType(agentTypeMock);
	}

	/** @return mock of AgentType answering to "getClassName()" with given String */
	private AgentType createAgentMock(String className) {
		AgentType agentTypeMock = mock(AgentType.class);
		when(agentTypeMock.getClassName()).thenReturn(className);
		return agentTypeMock;
	}

	@Test
	public void testWriteBundledOutputAddMultipleAgents() {
		AgentType agentTypeMock1 = createAgentMock("MyClassName1");
		AgentType agentTypeMock2 = createAgentMock("MyClassName2");
		AgentType agentTypeMock3 = createAgentMock("MyClassName3");
		Output outputMock = mock(Output.class);
		when(outputMock.getAgentTypeList()).thenReturn(Arrays.asList(agentTypeMock1, agentTypeMock2, agentTypeMock3));
		serializer.writeBundledOutput(createBundleMock(outputMock));
		verify(outputBuilderMock, times(1)).addAgentType(agentTypeMock1);
		verify(outputBuilderMock, times(1)).addAgentType(agentTypeMock2);
		verify(outputBuilderMock, times(1)).addAgentType(agentTypeMock3);
	}
}