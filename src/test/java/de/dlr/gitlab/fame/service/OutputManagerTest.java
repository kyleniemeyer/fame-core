package de.dlr.gitlab.fame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.mpi.MpiManager;
import de.dlr.gitlab.fame.mpi.Constants.Tag;
import de.dlr.gitlab.fame.protobuf.Input.InputData.OutputParam;
import de.dlr.gitlab.fame.protobuf.Services;
import de.dlr.gitlab.fame.service.output.BufferManager;
import de.dlr.gitlab.fame.service.output.OutputSerializer;
import de.dlr.gitlab.fame.service.output.FileWriterTest;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Tests for class {@link OutputManager}
 *
 * @author Christoph Schimeczek */
public class OutputManagerTest {
	private static final String INPUT_FILE_DUMMY = "src/test/resources/inputData/config.pb";
	private final String outputFilePrefix = "MyFilePrefix";
	private final int rank = 1;
	private final int outputInterval = 21;
	private OutputManager outputManager;
	private MpiManager mockMpi;
	private OutputSerializer serializer;
	private BufferManager bufferManager;
	private OutputParam outputParameters;
	private Setup setup;

	@Before
	public void setup() {
		mockMpi = mock(MpiManager.class);
		when(mockMpi.isRank(anyInt())).thenReturn(true);
		outputManager = new OutputManager(mockMpi);
		bufferManager = mock(BufferManager.class);
		AccessPrivates.setPrivateField("bufferManager", outputManager, bufferManager);
		outputParameters = updateOutputParameters(rank);
		setup = mock(Setup.class);
		when(setup.getOutputPath()).thenReturn(FileWriterTest.WORKING_PATH);
		when(setup.getOutputFilePrefix()).thenReturn(outputFilePrefix);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		serializer = (OutputSerializer) AccessPrivates.getPrivateFieldOf("outputSerializer", outputManager);
	}

	/** @return {@link OutputParameters} created with given central output process */
	private OutputParam updateOutputParameters(int outputProcess) {
		OutputParam outputParameters = OutputParam.newBuilder()
				.setProcess(outputProcess)
				.setInterval(outputInterval)
				.addActiveClassName("TestClass")
				.build();
		return outputParameters;
	}

	@Test
	public void setOutputParameters_ForRankRoot() {
		assertNotNull(serializer);
		assertEquals(rank, AccessPrivates.getPrivateFieldOf("centralOutputProcess", outputManager));
		assertEquals(outputInterval, AccessPrivates.getPrivateFieldOf("outputInterval", outputManager));
		verify(bufferManager).setBufferParameters(any(), any(), any());
	}

	@Test
	public void setOutputParameters_NotRankRoot() {
		when(mockMpi.isRank(anyInt())).thenReturn(false);
		outputManager = new OutputManager(mockMpi);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		serializer = (OutputSerializer) AccessPrivates.getPrivateFieldOf("outputSerializer", outputManager);
		AccessPrivates.setPrivateField("bufferManager", outputManager, bufferManager);

		assertNull(serializer);
		assertEquals(rank, AccessPrivates.getPrivateFieldOf("centralOutputProcess", outputManager));
		assertEquals(outputInterval, AccessPrivates.getPrivateFieldOf("outputInterval", outputManager));
		verify(bufferManager).setBufferParameters(any(), any(), any());
	}

	@Test
	public void tickBuffersAndWriteOutData_TimeStepForwardedToBufferManager() {
		outputManager = new OutputManager(mockMpi);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		AccessPrivates.setPrivateField("bufferManager", outputManager, bufferManager);

		outputManager.tickBuffersAndWriteOutData(3L, new TimeStamp(14L));
		verify(bufferManager).finishTick(14L);

	}

	@Test
	public void tickBuffersAndWriteOutData_WritesOutput() {
		outputManager = new OutputManager(mockMpi);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		OutputSerializer mockSerialiser = mock(OutputSerializer.class);
		AccessPrivates.setPrivateField("outputSerializer", outputManager, mockSerialiser);

		outputManager.tickBuffersAndWriteOutData(outputInterval, new TimeStamp(14L));
		verify(mockMpi, times(1)).aggregateMessagesAt(any(), eq(rank), eq(Tag.OUTPUT));
	}

	@Test
	public void tickBuffersAndWriteOutData_noWriteIfNotOutputProcess() {
		when(mockMpi.isRank(anyInt())).thenReturn(false);
		outputManager = new OutputManager(mockMpi);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		OutputSerializer mockSerialiser = mock(OutputSerializer.class);
		AccessPrivates.setPrivateField("outputSerializer", outputManager, mockSerialiser);
		outputManager.tickBuffersAndWriteOutData(outputInterval, new TimeStamp(14L));
		verify(mockSerialiser, times(0)).writeBundledOutput(any());
	}

	@Test
	public void flushAndCloseAll_writesToFile() {
		outputManager = new OutputManager(mockMpi);
		when(mockMpi.isRank(anyInt())).thenReturn(false);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		outputManager.flushAndCloseAll(new TimeStamp(5L));
		verify(mockMpi, times(1)).aggregateMessagesAt(any(), eq(rank), eq(Tag.OUTPUT));
	}

	@Test
	public void flushAndCloseAll_flushesBuffers() {
		when(mockMpi.isRank(anyInt())).thenReturn(false);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		when(bufferManager.flushAllBuffersToOutputBuilder()).thenReturn(Services.Output.newBuilder());
		outputManager.flushAndCloseAll(new TimeStamp(5L));
		verify(bufferManager, times(1)).finishTick(5L);
	}

	@Test
	public void flushAndCloseAll_CloseFile() {
		outputManager = new OutputManager(mockMpi);
		outputManager.setOutputParameters(outputParameters, setup, INPUT_FILE_DUMMY);
		OutputSerializer mockSerialiser = mock(OutputSerializer.class);
		AccessPrivates.setPrivateField("outputSerializer", outputManager, mockSerialiser);
		outputManager.flushAndCloseAll(new TimeStamp(5L));
		verify(mockSerialiser, times(1)).close();
	}
}