package de.dlr.gitlab.fame.service.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Input.InputData;

public class FileReaderTest {
	public static final String inputFilePath = "./src/test/resources/inputData/InputData.bin";
	private final String corruptFilePath = "./src/test/resources/inputData/corrupt.bin";
	private final String illegalPath = "KK:/illegal/path/missing.pb";

	@Test
	public void testRead() {
		InputData data = FileReader.read(inputFilePath);
		assertNotNull(data);
	}

	@Test
	public void testReadFailNoFile() {
		assertThrowsFatalMessage(FileReader.INPUT_FILE_NOT_FOUND, () -> FileReader.read(illegalPath));
	}

	@Test
	public void testReadFailFileCorrupt() {
		assertThrowsFatalMessage(FileReader.PARSER_ERROR, () -> FileReader.read(corruptFilePath));
	}
}
