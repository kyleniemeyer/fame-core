package de.dlr.gitlab.fame.communication;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.service.PostOffice;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class ChannelTest {
	private Channel channel;
	private Agent mockedOwner;
	private LogChecker logChecker = new LogChecker(Channel.class);
	private PostOffice mockedPostOffice;
	private final long ownerAgentId = 1701L;
	
	@Before
	public void setup() {
		mockedOwner = mock(Agent.class);
		when(mockedOwner.getId()).thenReturn(ownerAgentId);
		channel = new Channel(mockedOwner, MessageContext.NONE);
		logChecker.clear();
		mockedPostOffice = mock(PostOffice.class);
		when(mockedOwner.getPostOffice()).thenReturn(mockedPostOffice);
	}
	
	@Test
	public void testSubscribe() {
		channel.subscribe(123L);
		assertTrue(channel.hasSubscriber(123L));
	}
	
	@Test
	public void testSubscribe_LogDoubleSubscription() {
		channel.subscribe(123L);
		channel.subscribe(123L);
		logChecker.assertLogsContain(Channel.DOUBLE_SUBSCRIPTION);
	}
	
	@Test
	public void testUnsubscribe() {
		channel.subscribe(123L);
		channel.unsubscribe(123L);
		assertFalse(channel.hasSubscriber(123L));
	}
	
	@Test
	public void testUnsubscribe_LogMissingSubscriber() {
		channel.unsubscribe(123L);
		logChecker.assertLogsContain(Channel.UNSUBSCRIBER_MISSING);
	}
	
	@Test
	public void testClose() {
		long[] receivers = new long[] {1, 5, 10};
		addSubscribers(receivers);
		channel.close();
		verify(mockedPostOffice).sendMessage(eq(ownerAgentId), eq(receivers[0]), eq(null), any(ChannelAdmin.class));
		verify(mockedPostOffice).sendMessage(eq(ownerAgentId), eq(receivers[1]), eq(null), any(ChannelAdmin.class));
		verify(mockedPostOffice).sendMessage(eq(ownerAgentId), eq(receivers[2]), eq(null), any(ChannelAdmin.class));
		assertFalse(channel.hasAnySubscribers());
	}
	
	/** adds given subscribers to channel */
	private void addSubscribers(long[] receivers) {
		for (long receiver: receivers) {
			channel.subscribe(receiver);
		}		
	}
	
	@Test
	public void testPublish() {
		long[] receivers = new long[] {1, 5, 10};
		addSubscribers(receivers);

		DataItem[] dataItems = new DataItem[3];
		channel.publish(dataItems);
		verify(mockedPostOffice).sendMessage(ownerAgentId, receivers[0], null, dataItems);
		verify(mockedPostOffice).sendMessage(ownerAgentId, receivers[1], null, dataItems);
		verify(mockedPostOffice).sendMessage(ownerAgentId, receivers[2], null, dataItems);
	}
}