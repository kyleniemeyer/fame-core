package de.dlr.gitlab.fame.communication.transfer.portablesA;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/**
 * Dummy object for testing of {@link Portable}
 * 
 * @author Christoph Schimeczek, Marc Deissenroth
 */
public class DummyPortableEmpty implements Portable {

	/** adds no components */
	@Override
	public void addComponentsTo(ComponentCollector collector) {}

	/** Does nothing */
	@Override
	public void populate(ComponentProvider provider) {}
}