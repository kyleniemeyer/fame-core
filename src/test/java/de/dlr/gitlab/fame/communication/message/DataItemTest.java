package de.dlr.gitlab.fame.communication.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.DummyBrokenDataItem;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.reflection.DummyDataItemB;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;

/** 
 * Tests for {@link DataItem}
 * 
 * @author Christoph Schimeczek
 */
public class DataItemTest {
	private class Dummy {
		@SuppressWarnings("unused")
		public Dummy() {}
	}

	@Test
	public void testMapIdsToConstructorsFail() {
		DummyDataItem ddi = new DummyDataItem();
		@SuppressWarnings("unchecked")
		HashMap<Class<?>, Integer> map = (HashMap<Class<?>, Integer>) AccessPrivates.getPrivateFieldOf("mapClassToId", ddi);
		map.put(Dummy.class, -1);
		try {
			AccessPrivates.callMethodOfClassXOnY("mapIdsToConstructors", DataItem.class, ddi);
			fail("Exception expected");
		} catch (RuntimeException e) {
			assertEquals(DataItem.ERR_MISSING_CONSTRUCTOR + Dummy.class.getName(), e.getMessage());
		}
		map.remove(Dummy.class);
	}
	
	@Test
	public void testGetProtobufFailUnknownClass() {
		DummyBrokenDataItem ddi = new DummyBrokenDataItem();
		try {
			ddi.getProtobuf();
			fail("Exception expected");
		} catch (RuntimeException e) {
			assertEquals(DataItem.CLASS_NOT_KNOWN + DummyBrokenDataItem.class.getName(), e.getMessage());
		}
	}
	
	@Test
	public void testMapIdToConstructorFail() throws NoSuchMethodException, SecurityException {
		DummyDataItem ddi = new DummyDataItem();
		@SuppressWarnings("unchecked")
		HashMap<Integer, Constructor<?>> map = (HashMap<Integer, Constructor<?>>) AccessPrivates.getPrivateFieldOf("mapIdToConstructor", ddi);		
		Constructor<?> constructor = Dummy.class.getConstructor(DataItemTest.class);
		map.put(-1, constructor);
		ProtoDataItem data = ProtoDataItem.newBuilder().setDataTypeId(-1).build();
		try {
			AccessPrivates.callMethodOfClassXOnY("constructFromDataAndId", DataItem.class, ddi, data, -1);
			fail("Exception expected");
		} catch (RuntimeException e) {
			assertEquals("Cannot create DataItem " + constructor.getName(), e.getMessage());
		}
		map.remove(-1);
	}
	
	@Test
	public void testGetIdOfType_UnknownClass() {
		try {
			DataItem.getIdOfType(DummyDataItemB.class);
			fail("Exception expected");
		} catch (RuntimeException e) {
			assertEquals(DataItem.CLASS_NOT_KNOWN + DummyDataItemB.class.getSimpleName(), e.getMessage());
		}
	}
}