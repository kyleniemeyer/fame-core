package de.dlr.gitlab.fame.communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.message.ContractData;
import de.dlr.gitlab.fame.protobuf.Contracts.ProtoContract;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.ExceptionTesting;
import de.dlr.gitlab.fame.testUtils.LogChecker;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Test cases for class {@link Contract}
 * 
 * @author Christoph Schimeczek */
public class ContractTest {
	private enum Products {
		NoProduct, ProductA
	};

	@Test
	public void testEnsureEqualOrThrowEqual() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 0L, 0L, 0L, 0L, Products.NoProduct);
		AccessPrivates.callPrivateMethodOn("ensureEqualOrThrow", contract, "X", "X");
	}

	public static Contract buildContract(long senderId, long receiverId, String productName, long firstDeliveryTime,
			long deliveryInterval, long expirationTime, long contractId, Enum<?> product) {
		ProtoContract prototype = getContractBuilder(senderId, receiverId, productName, firstDeliveryTime, deliveryInterval,
				expirationTime).build();
		return new Contract(prototype, contractId, product);
	}

	@Test
	public void testEnsureEqualOrThrowDifferent() {
		LogChecker logChecker = new LogChecker(Contract.class);
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 0L, 0L, 0L, 0L, Products.NoProduct);
		ExceptionTesting.assertThrowsFatalMessage(Contract.ERR_PRODUCT_NAME,
				() -> AccessPrivates.callPrivateMethodOn("ensureEqualOrThrow", contract, "Y", "X"));
		logChecker.assertLogsContain(Contract.ERR_PRODUCT_NAME);
	}

	@Test
	public void testContractId() {
		Contract contract = buildContract(55L, 0L, Products.NoProduct.name(), 0L, 0L, 0L, 77L, Products.NoProduct);
		assertEquals(77L, contract.getContractId());
	}

	@Test
	public void testSender() {
		Contract contract = buildContract(55L, 0L, Products.NoProduct.name(), 0L, 0L, 0L, 0L, Products.NoProduct);
		assertEquals(55L, contract.getSenderId());
	}

	/** @return a {@link ProtoContract.Builder Builder} for {@link ProtoContract}s, i.e. protobuf representation of
	 *         {@link Contract} */
	public static ProtoContract.Builder getContractBuilder(long senderId, long receiverId, String productName,
			long firstDeliveryTime, long deliveryInterval, long expirationTime) {
		return ProtoContract.newBuilder().setSenderId(senderId).setReceiverId(receiverId).setProductName(productName)
				.setFirstDeliveryTime(firstDeliveryTime).setDeliveryIntervalInSteps(deliveryInterval)
				.setExpirationTime(expirationTime);
	}

	@Test
	public void testReceiver() {
		Contract contract = buildContract(0L, 99L, Products.NoProduct.name(), 0L, 0L, 0L, 0L, Products.NoProduct);
		assertEquals(99L, contract.getReceiverId());
	}

	@Test
	public void testProduct() {
		Contract contract = buildContract(0L, 0L, Products.ProductA.name(), 0L, 0L, 0L, 0L, Products.ProductA);
		assertEquals(Products.ProductA, contract.getProduct());
	}

	@Test
	public void testFirstDeliveryTime() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 77L, 0L, 0L, 0L, Products.NoProduct);
		assertEquals(77L, contract.getFirstDeliveryTime().getStep());
	}

	@Test
	public void testDeliveryIntervalInSteps() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 0L, 10L, 0L, 0L, Products.NoProduct);
		assertEquals(10L, contract.getDeliveryInterval().getSteps());
	}

	@Test
	public void testExpirationTime() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 0L, 0L, 22L, 0L, Products.NoProduct);
		assertEquals(22L, contract.getExpirationTime().getStep());
	}

	@Test
	public void testGetNextTimeOfDeliveryAfterBeforeFirstDelivery() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 10L, 100L, 1000L, 0L, Products.NoProduct);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(3L));
		assertEquals(10L, nextDeliveryTime.getStep());
	}

	@Test
	public void testGetNextTimeOfDeliveryAfterAtFirstDelivery() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 10L, 100L, 1000L, 0L, Products.NoProduct);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(10L));
		assertEquals(110L, nextDeliveryTime.getStep());
	}

	@Test
	public void testGetNextTimeOfDeliveryAfterBetweenDeliveries() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 10L, 100L, 1000L, 0L, Products.NoProduct);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(50L));
		assertEquals(110L, nextDeliveryTime.getStep());
	}

	@Test
	public void testGetNextTimeOfDeliveryAfterBeyondExpiration() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 10L, 100L, 1000L, 0L, Products.NoProduct);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(950L));
		assertEquals(TimeStamp.LATEST.getStep(), nextDeliveryTime.getStep());
	}

	@Test
	public void testGetNextTimeOfDeliveryAfterOnExpiration() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 0L, 100L, 1000L, 0L, Products.NoProduct);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(950L));
		assertEquals(1000L, nextDeliveryTime.getStep());
	}

	@Test
	public void testGetNextTimeOfDeliveryAfterOnFirstDeliveryAfterExpiration() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 10L, 100L, 100L, 0L, Products.NoProduct);
		TimeStamp nextDeliveryTime = contract.getNextTimeOfDeliveryAfter(new TimeStamp(10L));
		assertEquals(TimeStamp.LATEST.getStep(), nextDeliveryTime.getStep());
	}

	@Test
	public void testIsActiveAt() {
		Contract contract = buildContract(0L, 0L, Products.NoProduct.name(), 10L, 1L, 1000L, 0L, Products.NoProduct);
		assertFalse(contract.isActiveAt(new TimeStamp(9L)));
		assertTrue(contract.isActiveAt(new TimeStamp(10L)));
		assertTrue(contract.isActiveAt(new TimeStamp(11L)));
		assertTrue(contract.isActiveAt(new TimeStamp(999L)));
		assertTrue(contract.isActiveAt(new TimeStamp(1000L)));
		assertFalse(contract.isActiveAt(new TimeStamp(1001L)));
	}

	@Test
	public void testFulfilAfter() {
		long contractId = 99L;
		Contract contract = buildContract(0L, 0L, Products.ProductA.name(), 0L, 10L, 20L, contractId, Products.ProductA);
		ContractData result = contract.fulfilAfter(new TimeStamp(15L));
		assertEquals(contractId, result.contractId);
		assertEquals(20L, result.deliveryTime.getStep());
	}
}