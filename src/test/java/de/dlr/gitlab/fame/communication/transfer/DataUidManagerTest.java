package de.dlr.gitlab.fame.communication.transfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.DummyBrokenPortable;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableWithSubcomponents;
import de.dlr.gitlab.fame.communication.transfer.portablesB.DummyPortableSingleBoolean;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;

/** Tests for {@link DataUidManager} */
public class DataUidManagerTest {
	private DataUidManager dataUidManager;
	private LogChecker logChecker = new LogChecker(DataUidManager.class);

	@Before
	public void setupTest() {
		Setup mockSetup = prepareMockSetup("de.dlr.gitlab.fame.communication.transfer.portablesA");
		dataUidManager = new DataUidManager(mockSetup);
		logChecker.clear();
	}

	/** Creates a mocked {@link Setup} returning given Strings as "portablePackages" */
	private Setup prepareMockSetup(String... packages) {
		List<String> portablePackages = Arrays.asList(packages);
		Setup mockSetup = mock(Setup.class);
		when(mockSetup.getPortablePackages()).thenReturn(portablePackages);
		return mockSetup;
	}

	@Test
	public void testConstructorNoPortablesInPackage() {
		Setup mockSetup = prepareMockSetup("de.dlr.gitlab.fame.testUtils");
		dataUidManager = new DataUidManager(mockSetup);
		logChecker.assertLogsContain(DataUidManager.WARN_NO_PORTABLES);
	}

	@Test
	public void testConstructorNoPackage() {
		Setup mockSetup = prepareMockSetup();
		dataUidManager = new DataUidManager(mockSetup);
		logChecker.assertLogsContain(DataUidManager.WARN_NO_PORTABLES);
	}

	@Test
	public void testConstructor() {
		@SuppressWarnings("unchecked")
		HashMap<Class<? extends Portable>, Integer> mapClassToId = (HashMap<Class<? extends Portable>, Integer>) AccessPrivates
				.getPrivateFieldOf("mapClassToId", dataUidManager);
		assertTrue(mapClassToId.containsKey(DummyPortablePrimitives.class));
		assertTrue(mapClassToId.containsKey(DummyPortableWithSubcomponents.class));
		assertTrue(mapClassToId.containsKey(DummyPortableEmpty.class));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSortClassByTypeName() {
		Set<Class<? extends Object>> setA = createHashSet(String.class, Composer.class, Portable.class, Integer.class,
				Math.class);
		ArrayList<Class<? extends Object>> result = DataUidManager.sortClassTypesByName(setA);
		for (int i = 1; i < result.size(); i++) {
			String classNameBefore = result.get(i - 1).getName();
			String classNameAfter = result.get(i).getName();
			assertTrue(classNameAfter.compareTo(classNameBefore) > 0);
		}
	}

	@SuppressWarnings("unchecked")
	/** @return a HashSet with the given classes */
	private Set<Class<? extends Object>> createHashSet(Class<? extends Object>... classes) {
		HashSet<Class<? extends Object>> set = new HashSet<>();
		for (Class<? extends Object> clas : classes) {
			set.add(clas);
		}
		return set;
	}

	@Test
	public void testCreateClassToIdMap() {
		ArrayList<Class<? extends Object>> list = new ArrayList<>();
		list.add(String.class);
		list.add(Composer.class);
		list.add(Portable.class);
		list.add(Math.class);
		list.add(Integer.class);
		HashMap<Class<? extends Object>, Integer> result = DataUidManager.createClassToIdMap(list);
		for (int i = 0; i < list.size(); i++) {
			Class<?> clas = list.get(i);
			int index = result.get(clas);
			assertEquals(i, index);
		}
	}

	@Test
	public void testCreateConstructorListNoConstructor() {
		ArrayList<Class<? extends Portable>> classes = new ArrayList<>();
		classes.add(DummyBrokenPortable.class);
		try {
			AccessPrivates.callPrivateMethodOn("createConstructorList", dataUidManager, classes);
			fail("Exception expected!");
		} catch (RuntimeException e) {
			assertEquals(DataUidManager.NO_DEFAULT_CONSTRUCTOR + "DummyBrokenPortable", e.getMessage());
		}
	}

	@Test
	public void testGetTypeIdOfClass_UnknownClass() {
		try {
			dataUidManager.getTypeIdOfClass(DummyPortableSingleBoolean.class);
			fail("Exception expected!");
		} catch (RuntimeException e) {
			assertEquals(DataUidManager.CLASS_NOT_KNOWN + DummyPortableSingleBoolean.class.getSimpleName(),
					e.getMessage());
		}
	}
	
	@Test
	public void testGetTypeIdOfClass_KnownClass_NoCrash() {
		Setup mockSetup = prepareMockSetup("de.dlr.gitlab.fame.communication.transfer.portablesB");
		dataUidManager = new DataUidManager(mockSetup);
		dataUidManager.getTypeIdOfClass(DummyPortableSingleBoolean.class);
	}
}