package de.dlr.gitlab.fame.communication.transfer.portablesB;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/**
 * Dummy object for testing of {@link Portable}
 * 
 * @author Christoph Schimeczek, Marc Deissenroth
 */
public class DummyPortableSingleBoolean implements Portable {
	private int value = 0;
	
	public DummyPortableSingleBoolean () {}
	
	public DummyPortableSingleBoolean (int value) {
		this.value = value;
	}
	
	/** adds no components */
	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeInts(value);
	}

	/** Does nothing */
	@Override
	public void populate(ComponentProvider provider) {
		value = provider.nextInt();
	}
	
	public int getValue() {
		return value;
	}
}