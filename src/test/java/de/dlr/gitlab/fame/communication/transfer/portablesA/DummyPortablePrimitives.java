package de.dlr.gitlab.fame.communication.transfer.portablesA;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.ComposerTest;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.data.TimeSeries;

/**
 * Dummy object for testing of {@link Portable} - contains only portable-"primitive" types 
 * 
 * @author Christoph Schimeczek, Marc Deissenroth
 */
public class DummyPortablePrimitives implements Portable {
	public int count;
	public boolean[] booleans;
	public int[] ints;
	public long[] longs;
	public float[] floats;
	public double[] doubles;
	public String[] strings;
	public TimeSeries[] timeSeries;

	public DummyPortablePrimitives() {}

	public DummyPortablePrimitives(int count) {
		this.count = count;
		prepareArrays();
		for (int i = 0; i < count; i++) {
			booleans[i] = (i % 2 == 0);
			longs[i] = ((long) Integer.MAX_VALUE) + i;
			floats[i] = 1f / ((float) i + 1);
			doubles[i] = 1d / ((float) i + 1);
			strings[i] = "" + i;
			timeSeries[i] = ComposerTest.timeSeries[i];
		}
	}

	private void prepareArrays() {
		booleans = new boolean[count];
		ints = new int[count];
		longs = new long[count];
		floats = new float[count];
		doubles = new double[count];
		strings = new String[count];
		timeSeries = new TimeSeries[count];
	}

	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeInts(count);
		collector.storeBooleans(booleans);
		collector.storeInts(ints);
		collector.storeLongs(longs);
		collector.storeFloats(floats);
		collector.storeDoubles(doubles);
		collector.storeStrings(strings);
		collector.storeTimeSeries(timeSeries);
	}

	@Override
	public void populate(ComponentProvider provider) {
		count = provider.nextInt();
		prepareArrays();
		for (int i = 0; i < count; i++) {
			booleans[i] = provider.nextBoolean();
			ints[i] = provider.nextInt();
			longs[i] = provider.nextLong();
			floats[i] = provider.nextFloat();
			doubles[i] = provider.nextDouble();
			strings[i] = provider.nextString();
			timeSeries[i] = provider.nextTimeSeries();
		}
	}
}