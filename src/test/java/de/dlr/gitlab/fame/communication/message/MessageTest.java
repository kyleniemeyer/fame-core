package de.dlr.gitlab.fame.communication.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Agent.NestedItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoMessage;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.communication.transfer.Composer;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableWithSubcomponents;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableSingleValue;
import de.dlr.gitlab.fame.service.TimeSeriesProvider;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;

/**
 * Tests for {@link Message}
 * 
 * @author Christoph Schimeczek
 */
public class MessageTest {
	private ProtoDataItem[] dataItems;
	private NestedItem[] nestedItems;
	private Message message;
	private Composer composer;

	@Before
	public void setup() {
		Setup mockSetup = mock(Setup.class);
		when(mockSetup.getPortablePackages()).thenReturn(Arrays.asList("de.dlr.gitlab.fame.communication.transfer.portablesA"));
		when(mockSetup.getMaxPortableHierarchyDepth()).thenReturn(1);
		TimeSeriesProvider mockSeriesProvider = mock(TimeSeriesProvider.class);
		composer = new Composer(mockSetup, mockSeriesProvider);
		Message.setComposer(composer);
		dataItems = new ProtoDataItem[] {new DummyDataItem().getProtobuf()};
		nestedItems = new NestedItem[] {composer.decompose(new DummyPortableEmpty())};
		message = (Message) AccessPrivates.usePrivateConstructorOn(Message.class, 0L, 1L, dataItems, nestedItems);
	}

	@Test
	public void testPrivateConstructor() {
		assertEquals(0L, message.senderId);
		assertEquals(1L, message.receiverId);
		assertNotNull(message.getDataItemOfType(DummyDataItem.class));
		assertEquals(1, message.getAllPortableItemsOfType(DummyPortableEmpty.class).size());
	}

	@Test
	public void testConstructor() {
		ProtoMessage protoMessage = ProtoMessage.newBuilder().setSenderId(0L).setReceiverId(1L)
				.addDataItem(new DummyDataItem().getProtobuf()).addNestedItem(composer.decompose(new DummyPortableEmpty()))
				.build();
		Message message = new Message(protoMessage);
		assertEquals(0L, message.senderId);
		assertEquals(1L, message.receiverId);
		assertNotNull(message.getDataItemOfType(DummyDataItem.class));
		assertEquals(1, message.getAllPortableItemsOfType(DummyPortableEmpty.class).size());
	}

	@Test
	public void testCreateProtobufRepresentationWithReceiver() {
		ProtoMessage protoMessage = (ProtoMessage) AccessPrivates
				.callPrivateMethodOn("createProtobufRepresentationWithReceiver", message, 11L);
		assertEquals(11L, protoMessage.getReceiverId());
		assertEquals(message.senderId, protoMessage.getSenderId());
		assertEquals(1, protoMessage.getDataItemCount());
		assertEquals(1, protoMessage.getNestedItemCount());
	}

	@Test
	public void testCreateCopyWithNewReceiver() {
		Message copy = message.createCopyWithNewReceiver(11L);
		assertNotEquals(message, copy);
		assertEquals(11L, copy.receiverId);
		assertEquals(message.senderId, copy.senderId);
		assertEquals(message.getAllPortableItemsOfType(DummyPortableEmpty.class).size(),
				copy.getAllPortableItemsOfType(DummyPortableEmpty.class).size());
		assertEquals(message.containsType(DummyDataItem.class), copy.containsType(DummyDataItem.class));
	}

	@Test
	public void testContainsType() {
		assertFalse(message.containsType(Signal.class));
		assertTrue(message.containsType(DummyDataItem.class));
	}

	@Test
	public void testGetDataItemsOfType() {
		DummyDataItem dataItem = message.getDataItemOfType(DummyDataItem.class);
		assertNotNull(dataItem);
	}

	@Test
	public void testGetDataItemsOfTypeNotFound() {
		Signal dummy = message.getDataItemOfType(Signal.class);
		assertNull(dummy);
	}

	@Test
	public void testCreateProtobufRepresentation() {
		ProtoMessage proto = message.createProtobufRepresentation();
		assertEquals(message.senderId, proto.getSenderId());
		assertEquals(message.receiverId, proto.getReceiverId());
		assertEquals(1, proto.getDataItemCount());
		assertEquals(1, proto.getNestedItemCount());
	}

	@Test
	public void testGetAllPortableItemsOfType() {
		nestedItems = new NestedItem[] {composer.decompose(new DummyPortableEmpty()),
				composer.decompose(new DummyPortableEmpty()), composer.decompose(new DummyPortableWithSubcomponents(0))};
		message = (Message) AccessPrivates.usePrivateConstructorOn(Message.class, 0L, 1L, dataItems, nestedItems);

		assertEquals(2, message.getAllPortableItemsOfType(DummyPortableEmpty.class).size());
		assertEquals(1, message.getAllPortableItemsOfType(DummyPortableWithSubcomponents.class).size());
		assertEquals(0, message.getAllPortableItemsOfType(DummyPortablePrimitives.class).size());
	}

	@Test
	public void testGetFirstPortableItemOfType_ReturnNullOnNoItem() {
		nestedItems = new NestedItem[] {};
		message = (Message) AccessPrivates.usePrivateConstructorOn(Message.class, 0L, 1L, dataItems, nestedItems);
		assertNull(message.getFirstPortableItemOfType(DummyPortableEmpty.class));
	}

	@Test
	public void testGetFirstPortableItemOfType_ReturnNullOnWrongItem() {
		nestedItems = new NestedItem[] {composer.decompose(new DummyPortableEmpty())};
		message = (Message) AccessPrivates.usePrivateConstructorOn(Message.class, 0L, 1L, dataItems, nestedItems);
		assertNull(message.getFirstPortableItemOfType(DummyPortablePrimitives.class));
	}

	@Test
	public void testGetFirstPortableItemOfType_ReturnFirstOfTwo() {
		nestedItems = new NestedItem[] {composer.decompose(new DummyPortableSingleValue(42)),
				composer.decompose(new DummyPortableSingleValue(99))};
		message = (Message) AccessPrivates.usePrivateConstructorOn(Message.class, 0L, 1L, dataItems, nestedItems);
		DummyPortableSingleValue result = message.getFirstPortableItemOfType(DummyPortableSingleValue.class);
		assertThat(result.getValue(), is(42));
	}
}