package de.dlr.gitlab.fame.communication;

import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;

/** Dummy DataItem Class to emulate errors caused by class not in package list */
public class DummyBrokenDataItem extends DataItem {
	public DummyBrokenDataItem() {}
	public DummyBrokenDataItem(ProtoDataItem protoData) {}
	@Override
	protected void fillDataFields(Builder builder) {}
}