package de.dlr.gitlab.fame.communication.transfer.portablesA;

import de.dlr.gitlab.fame.communication.transfer.ComponentCollector;
import de.dlr.gitlab.fame.communication.transfer.ComponentProvider;
import de.dlr.gitlab.fame.communication.transfer.Portable;

/**
 * Dummy object for testing of {@link Portable} - contains only sub-components
 * 
 * @author Christoph Schimeczek, Marc Deissenroth
 */
public class DummyPortableWithSubcomponents implements Portable {
	public DummyPortablePrimitives subcomponentA;
	public DummyPortablePrimitives subcomponentB;
	
	public DummyPortableWithSubcomponents() {}
	
	public DummyPortableWithSubcomponents(int count) {
		subcomponentA = new DummyPortablePrimitives(count);
		subcomponentB = new DummyPortablePrimitives(count);		
	}
	
	@Override
	public void addComponentsTo(ComponentCollector collector) {
		collector.storeComponents(subcomponentA, subcomponentB);
	}

	/** Does nothing */
	@Override
	public void populate(ComponentProvider provider) {
		subcomponentA = provider.nextComponent(DummyPortablePrimitives.class);
		subcomponentB = provider.nextComponent(DummyPortablePrimitives.class);
	}
}