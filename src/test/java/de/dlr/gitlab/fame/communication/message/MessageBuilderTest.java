package de.dlr.gitlab.fame.communication.message;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;
import de.dlr.gitlab.fame.communication.transfer.Portable;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.service.TimeSeriesProvider;
import de.dlr.gitlab.fame.setup.Setup;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;

/** Tests for {@link MessageBuilder}
 * 
 * @author Christoph Schimeczek */
public class MessageBuilderTest {
	private long senderId = 42L;
	private long receiverId = 21L;
	private MessageBuilder builder;
	private ArrayList<DataItem> dataItems;
	private ArrayList<Portable> portables;
	private LogChecker logChecker = new LogChecker(MessageBuilder.class);

	@Before
	public void setup() {
		Setup mockSetup = mock(Setup.class);
		when(mockSetup.getPortablePackages()).thenReturn(Arrays.asList("de.dlr.gitlab.fame.communication.transfer"));
		TimeSeriesProvider mockSeriesProvider = mock(TimeSeriesProvider.class);
		builder = new MessageBuilder(mockSetup, mockSeriesProvider);
		builder.setReceiverId(receiverId).setSenderId(senderId);
		dataItems = new ArrayList<>();
		dataItems.add(new DummyDataItem());
		AccessPrivates.setPrivateField("dataItems", builder, dataItems);
		portables = new ArrayList<>();
		portables.add(new DummyPortableEmpty());
		AccessPrivates.setPrivateField("portables", builder, portables);
		logChecker.clear();
	}

	@Test
	public void testAdd() {
		Signal signal = new Signal(0);
		builder.add(signal);
		assertTrue(dataItems.contains(signal));
	}

	@Test
	public void testAddFailsDuplicate() {
		assertThrowsFatalMessage(MessageBuilder.TYPE_ALREADY_EXISTS, () -> builder.add(new DummyDataItem()));
		logChecker.assertLogsContain(MessageBuilder.TYPE_ALREADY_EXISTS);
	}

	@Test
	public void testIsNotComplete() {
		assertTrue(!(boolean) AccessPrivates.callPrivateMethodOn("isNotComplete", builder));
	}

	@Test
	public void testIsNotCompleteMissingSender() {
		builder.setSenderId(Long.MIN_VALUE);
		assertTrue((boolean) AccessPrivates.callPrivateMethodOn("isNotComplete", builder));
	}

	@Test
	public void testIsNotCompleteMissingReceiver() {
		builder.setReceiverId(Long.MIN_VALUE);
		assertTrue((boolean) AccessPrivates.callPrivateMethodOn("isNotComplete", builder));
	}

	@Test
	public void testIsNotCompleteMissingDataItems() {
		dataItems.clear();
		assertTrue((boolean) AccessPrivates.callPrivateMethodOn("isNotComplete", builder));
	}

	@Test
	public void testClear() {
		builder.clear();
		assertEquals(0, dataItems.size());
		assertEquals(Long.MIN_VALUE, (long) AccessPrivates.getPrivateFieldOf("senderId", builder));
		assertEquals(Long.MIN_VALUE, (long) AccessPrivates.getPrivateFieldOf("receiverId", builder));
	}

	@Test
	public void testBuildIncomplete() {
		builder.clear();
		assertThrowsFatalMessage(MessageBuilder.INCOMPLETE, () -> builder.build());
		logChecker.assertLogsContain(MessageBuilder.INCOMPLETE);
	}

	@Test
	public void testBuild() {
		Message message = builder.build();
		assertEquals(senderId, message.senderId);
		assertEquals(receiverId, message.receiverId);
		ArrayList<DummyPortableEmpty> portableItems = message.getAllPortableItemsOfType(DummyPortableEmpty.class);
		assertEquals(portables.size(), portableItems.size());
	}
}