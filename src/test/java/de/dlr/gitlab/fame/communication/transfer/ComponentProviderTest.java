package de.dlr.gitlab.fame.communication.transfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableEmpty;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortablePrimitives;
import de.dlr.gitlab.fame.communication.transfer.portablesA.DummyPortableWithSubcomponents;
import de.dlr.gitlab.fame.data.TimeSeries;

/**
 * Tests for {@link ComponentProvider}
 *
 * @author Christoph Schimeczek
 */
public class ComponentProviderTest {
	private ComponentProvider provider;
	private Boolean[] boolInput = new Boolean[] {true, true, false, true};
	private Integer[] intInput = new Integer[] {1, 6, 2, 3};
	private Long[] longInput = new Long[] {1L, 23987729384L, 19019190910L, 44L};
	private Float[] floatInput = new Float[] {0f, -6f, 11f, 42.42f};
	private Double[] doubleInput = new Double[] {-0d, -6d, 11d, 42.42d};
	private String[] stringInput = new String[] {"Da", "Lai", "Lam", "A"};
	private TimeSeries[] timeSeriesInput = new TimeSeries[] {mock(TimeSeries.class), mock(TimeSeries.class),
			mock(TimeSeries.class)};
	private Portable[] portableInput = new Portable[] {new DummyPortableWithSubcomponents(),
			new DummyPortableWithSubcomponents(), new DummyPortableEmpty(), new DummyPortableWithSubcomponents()};

	@Before
	public void setup() {
		provider = new ComponentProvider();
		provider.setBoolArray(Arrays.asList(boolInput));
		provider.setIntArray(Arrays.asList(intInput));
		provider.setLongArray(Arrays.asList(longInput));
		provider.setFloatArray(Arrays.asList(floatInput));
		provider.setDoubleArray(Arrays.asList(doubleInput));
		provider.setStringArray(Arrays.asList(stringInput));
		provider.setTimeSeriesArray(Arrays.asList(timeSeriesInput));
		provider.setComponentArray(Arrays.asList(portableInput));
	}

	@Test
	public void testNextBoolean() {
		for (int i = 0; i < boolInput.length; i++) {
			assertEquals(boolInput[i], provider.nextBoolean());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextBooleanNoElement() {
		for (int i = 0; i < boolInput.length; i++) {
			provider.nextBoolean();
		}
		provider.nextBoolean();
	}

	@Test
	public void testNextInt() {
		for (int i = 0; i < intInput.length; i++) {
			assertEquals((int) intInput[i], provider.nextInt());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextIntNoElement() {
		for (int i = 0; i < intInput.length; i++) {
			provider.nextInt();
		}
		provider.nextInt();
	}

	@Test
	public void testNextLong() {
		for (int i = 0; i < longInput.length; i++) {
			assertEquals((long) longInput[i], provider.nextLong());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextLongNoElement() {
		for (int i = 0; i < longInput.length; i++) {
			provider.nextLong();
		}
		provider.nextLong();
	}

	@Test
	public void testNextFloat() {
		for (int i = 0; i < floatInput.length; i++) {
			assertEquals(floatInput[i], provider.nextFloat(), 1E-6);
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextFloatNoElement() {
		for (int i = 0; i < floatInput.length; i++) {
			provider.nextFloat();
		}
		provider.nextFloat();
	}

	@Test
	public void testNextDouble() {
		for (int i = 0; i < doubleInput.length; i++) {
			assertEquals(doubleInput[i], provider.nextDouble(), 1E-14);
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextDoubleNoElement() {
		for (int i = 0; i < doubleInput.length; i++) {
			provider.nextDouble();
		}
		provider.nextDouble();
	}

	@Test
	public void testNextString() {
		for (int i = 0; i < stringInput.length; i++) {
			assertEquals(stringInput[i], provider.nextString());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextStringNoElement() {
		for (int i = 0; i < stringInput.length; i++) {
			provider.nextString();
		}
		provider.nextString();
	}

	@Test
	public void testNextTimeSeries() {
		for (int i = 0; i < timeSeriesInput.length; i++) {
			assertEquals(timeSeriesInput[i], provider.nextTimeSeries());
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextTimeSeriesNoElement() {
		for (int i = 0; i < timeSeriesInput.length; i++) {
			provider.nextTimeSeries();
		}
		provider.nextTimeSeries();
	}

	@Test
	public void testHasNextComponent() {
		for (int i = 0; i < portableInput.length; i++) {
			assertTrue(provider.hasNextComponent());
			provider.nextComponent(Portable.class);
		}
		assertFalse(provider.hasNextComponent());
	}

	@Test
	public void testHasNextComponentExactMatchClass() {
		for (int i = 0; i < portableInput.length; i++) {
			Class<? extends Portable> targetClass = portableInput[i].getClass();
			assertTrue(provider.hasNextComponent(targetClass));
			assertFalse(provider.hasNextComponent(DummyPortablePrimitives.class));
			provider.nextComponent(targetClass);
		}
	}

	@Test
	public void testHasNextComponentForCastableClass() {
		for (int i = 0; i < portableInput.length; i++) {
			Class<? extends Portable> targetClass = portableInput[i].getClass();
			assertTrue(provider.hasNextComponent(Portable.class));
			provider.nextComponent(targetClass);
		}
	}

	@Test
	public void testHasNextComponentForClassNoComponent() {
		for (int i = 0; i < portableInput.length; i++) {
			provider.nextComponent(Portable.class);
		}
		assertFalse(provider.hasNextComponent(Portable.class));
	}

	@Test
	public void testNextComponentListCutOffByOtherElement() {
		ArrayList<DummyPortableWithSubcomponents> result = provider.nextComponentList(DummyPortableWithSubcomponents.class);
		assertEquals(2, result.size());
		assertEquals(portableInput[0], result.get(0));
		assertEquals(portableInput[1], result.get(1));
	}

	@Test
	public void testNextComponentListReturnsEmptyOnWrongType() {
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		ArrayList<DummyPortableWithSubcomponents> result = provider.nextComponentList(DummyPortableWithSubcomponents.class);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testNextComponentListReturnsEmptyOnEnd() {
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		provider.nextComponent(Portable.class);
		ArrayList<DummyPortableWithSubcomponents> result = provider.nextComponentList(DummyPortableWithSubcomponents.class);
		assertTrue(result.isEmpty());
	}
}