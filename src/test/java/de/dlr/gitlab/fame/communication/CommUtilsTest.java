package de.dlr.gitlab.fame.communication;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.communication.message.dataItemsA.DummyDataItem;

public class CommUtilsTest {
	AutoCloseable closeable;

	@Before
	public void setUp() {
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void releaseMock() throws Exception {
		closeable.close();
	}

	@Test
	public void getExactlyOneEntry_Null() {
		assertThrowsFatalMessage(CommUtils.NOT_ONE_ENTRY, () -> CommUtils.getExactlyOneEntry(null));
	}

	@Test
	public void getExactlyOneEntry_Empty() {
		assertThrowsFatalMessage(CommUtils.NOT_ONE_ENTRY, () -> CommUtils.getExactlyOneEntry(new ArrayList<Double>()));
	}

	@Test
	public void getExactlyOneEntry_One() {
		Object value = new Object();
		assertEquals(value, CommUtils.getExactlyOneEntry(Arrays.asList(value)));
	}

	@Test
	public void getExactlyOneEntry_MoreThanOne() {
		assertThrowsFatalMessage(CommUtils.NOT_ONE_ENTRY, () -> CommUtils.getExactlyOneEntry(Arrays.asList(5, 5)));
	}

	@Test
	public void extractMessagesFrom_Null() {
		assertTrue(CommUtils.extractMessagesFrom(null, 12L).isEmpty());
	}

	@Test
	public void extractMessageFrom_Empty() {
		assertTrue(CommUtils.extractMessagesFrom(makeMessagesFrom(), 12L).isEmpty());
	}

	/** @return new ArrayList of messages; one message from each given id */
	private ArrayList<Message> makeMessagesFrom(long... ids) {
		ArrayList<Message> list = new ArrayList<>();
		for (long id : ids) {
			Message message = mock(Message.class);
			when(message.getSenderId()).thenReturn(id);
			list.add(message);
		}
		return list;
	}

	@Test
	public void extractMessageFrom_NotFound() {
		ArrayList<Message> input = makeMessagesFrom(1, 2, 3);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 99L);
		assertTrue(result.isEmpty());
		assertEquals(3, input.size());
	}

	@Test
	public void extractMessageFrom_One() {
		ArrayList<Message> input = makeMessagesFrom(1, 2, 3);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 1L);
		assertEquals(1, result.size());
		assertEquals(2, input.size());
	}

	@Test
	public void extractMessageFrom_Some() {
		ArrayList<Message> input = makeMessagesFrom(1, 1, 3);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 1L);
		assertEquals(2, result.size());
		assertEquals(1, input.size());
	}

	@Test
	public void extractMessageFrom_All() {
		ArrayList<Message> input = makeMessagesFrom(1, 1, 1);
		ArrayList<Message> result = CommUtils.extractMessagesFrom(input, 1L);
		assertEquals(3, result.size());
		assertEquals(0, input.size());
	}

	@Test
	public void extractMessagesWith_NullMessage() {
		assertTrue(CommUtils.extractMessagesWith(null, DataItem.class).isEmpty());
	}

	@Test
	public void extractMessagesWith_EmptyMessage() {
		assertTrue(CommUtils.extractMessagesWith(new ArrayList<>(), DataItem.class).isEmpty());
	}

	@Test
	public void extractMessagesWith_NullType() {
		assertThrowsFatalMessage(CommUtils.NO_TYPE, () -> CommUtils.extractMessagesWith(new ArrayList<>(), null));
	}

	@Test
	public void extractMessagesWith_NotFound() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, false, false, false);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(0, result.size());
		assertEquals(3, input.size());
	}

	/** @return new List of Messages; each Message returning corresponding "haveItem" when asked containsType(given DataItem) */
	private ArrayList<Message> makeMessagesWith(Class<? extends DataItem> clas, boolean... haveItem) {
		ArrayList<Message> list = new ArrayList<>();
		for (boolean hasItem : haveItem) {
			Message message = mock(Message.class);
			when(message.containsType(clas)).thenReturn(hasItem);
			list.add(message);
		}
		return list;
	}

	@Test
	public void extractMessagesWith_OneFound() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, false, true, false);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(1, result.size());
		assertEquals(2, input.size());
	}

	@Test
	public void extractMessagesWith_SomeFound() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, false, true, true);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(2, result.size());
		assertEquals(1, input.size());
	}

	@Test
	public void extractMessagesWith_AllFound() {
		ArrayList<Message> input = makeMessagesWith(DummyDataItem.class, true, true, true);
		ArrayList<Message> result = CommUtils.extractMessagesWith(input, DummyDataItem.class);
		assertEquals(3, result.size());
		assertEquals(0, input.size());
	}
}
