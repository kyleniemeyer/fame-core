package de.dlr.gitlab.fame.communication.message.dataItemsA;

import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem;
import de.dlr.gitlab.fame.protobuf.Agent.ProtoDataItem.Builder;

public class DummyDataItem extends DataItem {
	public DummyDataItem() {}
	public DummyDataItem(ProtoDataItem protoData) {}
	@Override
	protected void fillDataFields(Builder builder) {}
}