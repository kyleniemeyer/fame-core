package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public class Mother extends Agent {
	public Mother(DataProvider dataProvider) {
		super(dataProvider);
	}

	@Dummy
	public enum MotherEnum {
		MotherA, MotherB, MotherC
	}

	@Output
	public enum MotherColumns {
		A, B, C
	};

	public enum MotherSubindex {
		T, L
	}

	public static final ComplexIndex<MotherSubindex> complexIndexMother = ComplexIndex.build(MotherColumns.A,
			MotherSubindex.class);

	public class InnerClass {}
}