package de.dlr.gitlab.fame.reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.google.protobuf.DescriptorProtos.GeneratedCodeInfo.Annotation;

/**
 * Dummy {@link Annotation} used for tests in {@link ReflectionTest} 
 * 
 * @author Christoph Schimeczek
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface Dummy {}