package de.dlr.gitlab.fame.reflection.indexNonFinal;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public class NonFinal extends Agent {
	@Output
	public enum Columns {
		A, B, C
	};

	public enum Subindex {
		T, L
	}

	public static ComplexIndex<Subindex> complexIndexMother = ComplexIndex.build(Columns.A, Subindex.class);

	public NonFinal(DataProvider dataProvider) {
		super(dataProvider);
	}
}
