package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.service.output.Output;

public class GrandChild extends Child {
	public GrandChild(DataProvider dataProvider) {
		super(dataProvider);
	}

	@Dummy
	public enum GrandChildEnum {
		GrandChildA
	}

	@Output
	public enum ChildColumns {
		X, Y, Z
	};

	public enum ChildSubindex1 {
		U, V
	}

	public enum ChildSubindex2 {
		R, S
	}

	public static final ComplexIndex<ChildSubindex1> complexIndexChild1 = ComplexIndex.build(ChildColumns.Y,
			ChildSubindex1.class);
	public static final ComplexIndex<ChildSubindex2> complexIndexChild2 = ComplexIndex.build(ChildColumns.Z,
			ChildSubindex2.class);
}