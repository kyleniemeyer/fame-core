package de.dlr.gitlab.fame.reflection.doubleEnum;

import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.reflection.packA.Child;

public class GrandSon extends Child {
	public GrandSon(DataProvider dataProvider) {
		super(dataProvider);
	}

	@Dummy
	public enum GrandSonEnum {MotherA}
}