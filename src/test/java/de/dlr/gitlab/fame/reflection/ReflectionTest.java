package de.dlr.gitlab.fame.reflection;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.reflection.packA.Child;
import de.dlr.gitlab.fame.reflection.packA.GrandChild;
import de.dlr.gitlab.fame.reflection.packA.GrandChild.GrandChildEnum;
import de.dlr.gitlab.fame.reflection.packA.GrandDaughter;
import de.dlr.gitlab.fame.reflection.packA.Mother;
import de.dlr.gitlab.fame.reflection.packA.Mother.MotherEnum;
import de.dlr.gitlab.fame.service.output.ComplexIndex;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.util.Reflection;
import de.dlr.gitlab.fame.util.Reflection.ReflectionType;

/** Tests for {@link Reflection}
 * 
 * @author Christoph Schimeczek, Achraf El Ghazi */
public class ReflectionTest {
	private List<String> packagesNames = new ArrayList<>();

	@Before
	public void setup() {
		packagesNames.clear();
	}

	@Test
	public void testConstructor() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		testReflectionCreated(packagesNames, ReflectionType.AGENT, 2); // +1 FAME standard AGENT package
		testReflectionCreated(packagesNames, ReflectionType.COMMUNICATION, 2); // +1 FAME standard COMM package
		testReflectionCreated(packagesNames, ReflectionType.PORTABLE, 1); // No FAME standard PORTABLE package
	}

	/** Creates Reflection with given packageNames of given Type and tests if length of Reflection#reflectionList matches given
	 * expectedListLength */
	private void testReflectionCreated(List<String> packagesNames, ReflectionType type, int expectedListLength) {
		Reflection reflection = new Reflection(type, packagesNames);
		ArrayList<?> reflectionList = (ArrayList<?>) AccessPrivates.getPrivateFieldOf("reflectionList", reflection);
		assertEquals(expectedListLength, reflectionList.size()); // including FAME standard package
	}

	@Test
	public void testConstructorEmptyPackages() {
		testReflectionCreated(packagesNames, ReflectionType.AGENT, 1); // +1 FAME standard AGENT package
	}

	@Test
	public void testConstructorNoPackages() {
		testReflectionCreated(null, ReflectionType.AGENT, 1); // +1 FAME standard AGENT package
	}

	@Test
	public void testConstructorNoPackageName() {
		packagesNames.add(null);
		testReflectionCreated(packagesNames, ReflectionType.AGENT, 1); // +1 FAME standard AGENT package
	}

	@Test
	public void testConstructorFailOnIllegalPackage() {
		packagesNames.add("IllegalPackageName");
		assertThrowsFatalMessage(Reflection.ERR_ILLEGAL_PACKAGE, () -> new Reflection(ReflectionType.AGENT, packagesNames));
	}

	@Test
	public void testFindAllChildsOfByName() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		HashMap<String, Class<? extends Mother>> childrenByName = reflection.findAllChildsOfByName(Mother.class);
		assertTrue(childrenByName.containsKey("Child"));
		Class<?> classOfChild = childrenByName.get("Child");
		assertEquals(Child.class, classOfChild);
	}

	@Test
	public void testFindAllInstantiableChildClassesOf() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		ArrayList<Class<?>> classes = reflection.findAllInstantiableChildClassesOf(Mother.class);
		assertTrue(classes.contains(GrandChild.class));
		assertFalse(classes.contains(Child.class));
	}

	@Test
	public void testMapAnnotatedEnumsToEnclosingClass() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		@SuppressWarnings("unchecked") HashMap<Class<?>, ArrayList<Enum<?>>> enumMap = (HashMap<Class<?>, ArrayList<Enum<?>>>) AccessPrivates
				.callPrivateMethodOn("mapAnnotatedEnumsToEnclosingClass", reflection, Dummy.class);
		assertTrue(enumMap.containsKey(Mother.class));
		assertTrue(enumMap.containsKey(GrandChild.class));
		ArrayList<Enum<?>> enums = enumMap.get(Mother.class);
		for (Enum<?> e : MotherEnum.values()) {
			assertTrue(enums.contains(e));
		}
	}

	@Test
	public void mapAnnotatedEnumsToEnclosingClass_FailIfMultipleUsage() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.doubleAnnotation");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_MULTI_USE,
				() -> AccessPrivates.callPrivateMethodOn("mapAnnotatedEnumsToEnclosingClass", reflection, Dummy.class));
	}

	@Test
	public void findEnumHierarchyAnnotatedWithForSubtypes_MissingAnnotationInChildClass() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		HashMap<Class<? extends Mother>, ArrayList<Enum<?>>> result = reflection
				.findEnumHierarchyAnnotatedWithForSubtypes(Dummy.class, Mother.class);
		assertTrue(result.containsKey(GrandDaughter.class));
		ArrayList<Enum<?>> enums = result.get(GrandDaughter.class);
		assertTrue(enums.contains(MotherEnum.MotherA));
		assertTrue(result.containsKey(GrandChild.class));
		enums = result.get(GrandChild.class);
		assertTrue(enums.contains(MotherEnum.MotherA));
		assertTrue(enums.contains(GrandChildEnum.GrandChildA));
	}

	@Test
	public void findEnumHierarchyAnnotatedWithForSubtypes_FailOnDoubleDeclaration() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		packagesNames.add("de.dlr.gitlab.fame.reflection.doubleEnum");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_DOUBLE_DECLARATION,
				() -> reflection.findEnumHierarchyAnnotatedWithForSubtypes(Dummy.class, Mother.class));
	}

	@Test
	public void getEnclosingClass_FailIfNotInnerClass() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_NOT_ENUM,
				() -> AccessPrivates.callPrivateMethodOn("getEnclosingClass", reflection, Mother.class, "ANNOTATION"));
	}

	@Test
	public void getEnclosingClass_FailIfNotEnum() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_NOT_ENUM, () -> AccessPrivates
				.callPrivateMethodOn("getEnclosingClass", reflection, Mother.InnerClass.class, "ANNOTATION"));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_failIfNotStatic() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.indexNonStatic");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_MODIFIERS_MISSING,
				() -> reflection.findComplexIndexHierarchyForAgents());
	}

	@Test
	public void findComplexIndexHierarchyForAgents_failIfNotFinal() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.indexNonFinal");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		assertThrowsFatalMessage(Reflection.ERR_MODIFIERS_MISSING,
				() -> reflection.findComplexIndexHierarchyForAgents());
	}

	@Test
	public void findComplexIndexHierarchyForAgents_AgentsHaveOwnIndex() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> results = reflection
				.findComplexIndexHierarchyForAgents();

		ArrayList<ComplexIndex<? extends Enum<?>>> motherResults = results.get(Mother.class.getSimpleName());
		assertTrue(motherResults.contains(Mother.complexIndexMother));

		ArrayList<ComplexIndex<? extends Enum<?>>> grandChildResults = results.get(GrandChild.class.getSimpleName());
		assertTrue(grandChildResults.contains(GrandChild.complexIndexChild1));
		assertTrue(grandChildResults.contains(GrandChild.complexIndexChild2));
	}

	@Test
	public void findComplexIndexHierarchyForAgents_AgentsHaveParentIndex() {
		packagesNames.add("de.dlr.gitlab.fame.reflection.packA");
		Reflection reflection = new Reflection(ReflectionType.AGENT, packagesNames);
		HashMap<String, ArrayList<ComplexIndex<? extends Enum<?>>>> results = reflection
				.findComplexIndexHierarchyForAgents();

		ArrayList<ComplexIndex<? extends Enum<?>>> motherResults = results.get(Mother.class.getSimpleName());
		assertEquals(1, motherResults.size());
		
		ArrayList<ComplexIndex<? extends Enum<?>>> childResults = results.get(Child.class.getSimpleName());
		assertTrue(childResults.contains(Mother.complexIndexMother));
		assertEquals(1, childResults.size());
		
		ArrayList<ComplexIndex<? extends Enum<?>>> grandChildResults = results.get(GrandChild.class.getSimpleName());
		assertTrue(grandChildResults.contains(Mother.complexIndexMother));
		assertEquals(3, grandChildResults.size());
		
		ArrayList<ComplexIndex<? extends Enum<?>>> grandDaughterResults = results.get(GrandDaughter.class.getSimpleName());
		assertTrue(grandDaughterResults.contains(Mother.complexIndexMother));
		assertEquals(1, grandDaughterResults.size());		
	}
}