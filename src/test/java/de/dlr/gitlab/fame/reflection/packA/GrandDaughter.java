package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.reflection.Dummy;
import de.dlr.gitlab.fame.reflection.ReflectionTest;

/** Test class for {@link ReflectionTest}: Contains no {@link Dummy} annotation */
public class GrandDaughter extends Child {

	public GrandDaughter(DataProvider dataProvider) {
		super(dataProvider);
	}

}