package de.dlr.gitlab.fame.reflection.packA;

import de.dlr.gitlab.fame.agent.input.DataProvider;

public abstract class Child extends Mother {

	public Child(DataProvider dataProvider) {
		super(dataProvider);
	}

}