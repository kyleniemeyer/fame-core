package de.dlr.gitlab.fame.reflection.doubleAnnotation;

import de.dlr.gitlab.fame.reflection.Dummy;

public class DummyClass {
	@Dummy
	public enum FirstEnum {
		A, B, C
	}

	@Dummy
	public enum SecondEnum {
		X, Y, Z
	}
}