package de.dlr.gitlab.fame.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao.Builder;
import de.dlr.gitlab.fame.protobuf.Input.InputData.TimeSeriesDao.Row;
import de.dlr.gitlab.fame.time.TimeStamp;

public class TimeSeriesTest {
	private Builder builder = TimeSeriesDao.newBuilder();
	private TimeSeries timeSeries;
	private String seriesName = "MySeries";
	private int seriesId = 15;

	@Before
	public void setup() {
		builder.clear();
		builder.addRow(Row.newBuilder().setTimeStep(-10L).setValue(-20.0))
				.addRow(Row.newBuilder().setTimeStep(0L).setValue(-10.0))
				.addRow(Row.newBuilder().setTimeStep(10L).setValue(30.0));
		builder.setSeriesId(seriesId);
		builder.setSeriesName(seriesName);
		timeSeries = new TimeSeries(builder.build());
	}

	@Test
	public void testFailOnDuplicateTime() {
		builder.addRow(Row.newBuilder().setTimeStep(0L).setValue(5.0));
		try {
			new TimeSeries(builder.build());
			fail("Exception expected");
		} catch (RuntimeException e) {
			assertEquals("Duplicate time steps found in series " + seriesName, e.getMessage());
		}
	}

	@Test
	public void testToString() {
		assertEquals("Series(" + seriesId + ": " + seriesName + ")", timeSeries.toString());
	}

	@Test
	public void testLinearInterpolation() {
		assertEquals(-20.0, timeSeries.getValueLinear(new TimeStamp(-15L)), 1E-12);
		assertEquals(-20.0, timeSeries.getValueLinear(new TimeStamp(-10L)), 1E-12);
		assertEquals(-15.0, timeSeries.getValueLinear(new TimeStamp(-5L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueLinear(new TimeStamp(0L)), 1E-12);
		assertEquals(-6.0, timeSeries.getValueLinear(new TimeStamp(1L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLinear(new TimeStamp(10L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLinear(new TimeStamp(15L)), 1E-12);
	}

	@Test
	public void testLowerEqual() {
		assertEquals(-20.0, timeSeries.getValueLowerEqual(new TimeStamp(-15L)), 1E-12);
		assertEquals(-20.0, timeSeries.getValueLowerEqual(new TimeStamp(-10L)), 1E-12);
		assertEquals(-20.0, timeSeries.getValueLowerEqual(new TimeStamp(-5L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueLowerEqual(new TimeStamp(0L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueLowerEqual(new TimeStamp(1L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLowerEqual(new TimeStamp(10L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueLowerEqual(new TimeStamp(15L)), 1E-12);
	}

	@Test
	public void testHigherEqual() {
		assertEquals(-20.0, timeSeries.getValueHigherEqual(new TimeStamp(-15L)), 1E-12);
		assertEquals(-20.0, timeSeries.getValueHigherEqual(new TimeStamp(-10L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueHigherEqual(new TimeStamp(-5L)), 1E-12);
		assertEquals(-10.0, timeSeries.getValueHigherEqual(new TimeStamp(0L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueHigherEqual(new TimeStamp(1L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueHigherEqual(new TimeStamp(10L)), 1E-12);
		assertEquals(30.0, timeSeries.getValueHigherEqual(new TimeStamp(15L)), 1E-12);
	}

	@Test
	public void testGetId() {
		assertEquals(seriesId, timeSeries.getId());
	}
}