package de.dlr.gitlab.fame.data;

import static org.junit.Assert.*;
import org.junit.Test;

public class DataEntryTest {

	@Test
	public void testZero() {
		DataEntry item = new DataEntry(0L, 0.0);
		testItem(item, 0L, 0.0);
	}

	/** tests given {@link DataEntry} for equality to specified timeIndex, & double value */
	private void testItem(DataEntry item, long l, double d) {
		assertEquals(l, item.getTimeStep());
		assertEquals(d, item.getValue(), 1E-15);
	}

	@Test
	public void testNegative() {
		DataEntry item = new DataEntry(-1L, -1.0);
		testItem(item, -1L, -1.0);
	}
	
	@Test
	public void testPositive() {
		DataEntry item = new DataEntry(2L, 10.0);
		testItem(item, 2L, 10.0);		
	}
	
	@Test
	public void testToString() {
		DataEntry item = new DataEntry(9L, 27.0);
		assertEquals("(9, 27.0)", item.toString());
	}
	
	@Test
	public void testCompareLowerTime() {
		DataEntry lowerItem = new DataEntry(-1L, 0);
		DataEntry higherItem = new DataEntry(2L, 1);
		assertTrue(lowerItem.compareTo(higherItem) < 0);
	}
	
	@Test
	public void testCompareHigherTime() {
		DataEntry lowerItem = new DataEntry(-1L, 0);
		DataEntry higherItem = new DataEntry(2L, 1);
		assertTrue(higherItem.compareTo(lowerItem) > 0);
	}
	
	@Test
	public void testCompareEqualTime() {
		DataEntry item = new DataEntry(2L, 0);
		DataEntry otherItem = new DataEntry(2L, 1);
		assertEquals(0, item.compareTo(otherItem));
	}	
}