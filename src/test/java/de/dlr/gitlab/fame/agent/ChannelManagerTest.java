package de.dlr.gitlab.fame.agent;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.communication.Channel;
import de.dlr.gitlab.fame.communication.Constants.MessageContext;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin;
import de.dlr.gitlab.fame.communication.message.ChannelAdmin.Topic;
import de.dlr.gitlab.fame.communication.message.DataItem;
import de.dlr.gitlab.fame.communication.message.Message;
import de.dlr.gitlab.fame.service.PostOffice;
import de.dlr.gitlab.fame.testUtils.AccessPrivates;
import de.dlr.gitlab.fame.testUtils.LogChecker;

public class ChannelManagerTest {
	private long dummySender = 0L;
	private ChannelManager manager;
	private Agent mockOwner;
	private LogChecker logChecker = new LogChecker(ChannelManager.class);
	private PostOffice mockPostOffice;
	private final long ownerAgentId = 1701L;

	@Before
	public void setup() {
		mockPostOffice = mock(PostOffice.class);
		mockOwner = mock(Agent.class);
		when(mockOwner.getPostOffice()).thenReturn(mockPostOffice);
		when(mockOwner.getId()).thenReturn(ownerAgentId);
		manager = new ChannelManager(mockOwner);
		logChecker.clear();
	}

	@Test
	public void testOpenChannel() {
		@SuppressWarnings("unchecked") HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates
				.getPrivateFieldOf("channels", manager);
		manager.openChannel(MessageContext.NONE);
		assertTrue(channels.containsKey(MessageContext.NONE));
	}

	@Test
	public void testOpenChannelTwice() {
		manager.openChannel(MessageContext.CLEARING_PRICE);
		manager.openChannel(MessageContext.NONE);
		manager.openChannel(MessageContext.DEMAND_ASK);
		assertThrowsFatalMessage(ChannelManager.DOUBLE_CHANNEL, () -> manager.openChannel(MessageContext.NONE));
		logChecker.assertLogsContain(ChannelManager.DOUBLE_CHANNEL);
	}

	@Test
	public void testCloseChannel() {
		@SuppressWarnings("unchecked") HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates
				.getPrivateFieldOf("channels", manager);
		channels.put(MessageContext.NONE, mock(Channel.class));
		manager.closeChannel(MessageContext.NONE);
		assertFalse(channels.containsKey(MessageContext.NONE));
		manager.closeChannel(MessageContext.NONE);
		logChecker.assertLogsContain(ChannelManager.REMOVE_MISSING);
	}

	@Test
	public void testPublishWithContextNoChannel() {
		DataItem mockDataItem = mock(DataItem.class);
		assertThrowsFatalMessage(ChannelManager.PUBLISH_MISSING, () -> manager.publish(MessageContext.NONE, mockDataItem));
		logChecker.assertLogsContain(ChannelManager.PUBLISH_MISSING);
	}

	@Test
	public void testPublish() {
		Channel mockChannel = mock(Channel.class);
		@SuppressWarnings("unchecked") HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates
				.getPrivateFieldOf("channels", manager);
		channels.put(MessageContext.NONE, mockChannel);
		DataItem mockDataItem = mock(DataItem.class);
		manager.publish(MessageContext.NONE, mockDataItem);
		verify(mockChannel).publish(mockDataItem);
	}

	@Test
	public void testHandleMessageNoMessage() {
		Message result = manager.handleMessage(null);
		assertNull(result);
	}

	@Test
	public void testHandleMessageNoChannelMessage() {
		Message input = mock(Message.class);
		doReturn(false).when(input).containsType(ChannelAdmin.class);
		Message result = manager.handleMessage(input);
		assertEquals(input, result);
	}

	@Test
	public void testHandleMessageNoSubscription() {
		Message input = mockMessage(ChannelAdmin.class, new ChannelAdmin(Topic.CLOSE, MessageContext.NONE));
		Message result = manager.handleMessage(input);
		assertEquals(input, result);
	}

	private <T extends DataItem> Message mockMessage(Class<T> type, T content) {
		Message message = mock(Message.class);
		when(message.getSenderId()).thenReturn(dummySender);
		when(message.getDataItemOfType(type)).thenReturn(content);
		doReturn(true).when(message).containsType(type);
		return message;
	}

	@Test
	public void testManageSubscriptionMessageWrongTopic() {
		manager.openChannel(MessageContext.NONE);
		Message input = mockMessage(ChannelAdmin.class, new ChannelAdmin(Topic.CLOSE, MessageContext.NONE));
		try {
			AccessPrivates.callPrivateMethodOn("manageSubscriptionMessage", manager, input);
			fail("Exception expected");
		} catch (RuntimeException e) {
			assertEquals(ChannelManager.ERROR_WRONG_TOPIC, e.getMessage());
		}
	}

	@Test
	public void testHandleMessageChannelMissing() {
		Message input = mockMessage(ChannelAdmin.class, new ChannelAdmin(Topic.SUBSCRIBE, MessageContext.NONE));
		Message result = manager.handleMessage(input);
		assertNull(result);
		logChecker.assertLogsContain(ChannelManager.SUBSCRIBE_MISSING);
		verify(mockPostOffice).sendMessage(eq(ownerAgentId), eq(dummySender), eq(null), any());
	}

	@Test
	public void testHandleMessageSubscribe() {
		Channel mockChannel = injectChannel(MessageContext.NONE);
		Message input = mockMessage(ChannelAdmin.class, new ChannelAdmin(Topic.SUBSCRIBE, MessageContext.NONE));
		Message result = manager.handleMessage(input);
		assertNull(result);
		verify(mockChannel).subscribe(0L);
	}

	/** injects a mocked {@link Channel} with given {@link MessageContext} into the {@link #manager}s channel list
	 * 
	 * @return injected mocked channel */
	private Channel injectChannel(MessageContext context) {
		@SuppressWarnings("unchecked") HashMap<MessageContext, Channel> channels = (HashMap<MessageContext, Channel>) AccessPrivates
				.getPrivateFieldOf("channels", manager);
		Channel mockChannel = mock(Channel.class);
		channels.put(context, mockChannel);
		return mockChannel;
	}

	@Test
	public void testHandleMessageUnsubscribe() {
		Channel mockChannel = injectChannel(MessageContext.NONE);
		Message input = mockMessage(ChannelAdmin.class, new ChannelAdmin(Topic.UNSUBSCRIBE, MessageContext.NONE));
		Message result = manager.handleMessage(input);
		assertNull(result);
		verify(mockChannel).unsubscribe(0L);
	}
}