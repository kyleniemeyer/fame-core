package de.dlr.gitlab.fame.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.testUtils.ExceptionTesting;
import de.dlr.gitlab.fame.time.TimeStamp;

public class ProductTimesTest {
	private enum MyProducts {
		A, B, C
	};

	private ProductTimes productTimes;
	private final TimeStamp[] times = new TimeStamp[] {new TimeStamp(1L), new TimeStamp(2L), new TimeStamp(3L)};

	@Before
	public void setup() {
		productTimes = new ProductTimes();
	}

	@Test
	public void getProductsForTime_EmptyListOnDefault() {
		assertTrue(productTimes.drawProductsForTime(times[0]).isEmpty());
	}

	@Test
	public void getProductsForTime_EmptyListOnMissingEntry() {
		productTimes.linkProductToTime(times[1], MyProducts.A);
		productTimes.linkProductToTime(times[2], MyProducts.A);
		assertTrue(productTimes.drawProductsForTime(times[0]).isEmpty());
	}

	@Test
	public void getProductsForTime_GetSingleEntry() {
		productTimes.linkProductToTime(times[0], MyProducts.A);
		ArrayList<Enum<?>> result = productTimes.drawProductsForTime(times[0]);
		assertEquals(1, result.size());
		assertTrue(result.contains(MyProducts.A));
	}

	@Test
	public void getProductsForTime_GetMultipleEntries() {
		productTimes.linkProductToTime(times[0], MyProducts.A);
		productTimes.linkProductToTime(times[0], MyProducts.B);
		productTimes.linkProductToTime(times[1], MyProducts.C);
		ArrayList<Enum<?>> result = productTimes.drawProductsForTime(times[0]);
		assertEquals(2, result.size());
		assertTrue(result.contains(MyProducts.A));
		assertTrue(result.contains(MyProducts.B));
	}

	@Test
	public void getProductsForTime_SecondCallReturnsEmptyList() {
		productTimes.linkProductToTime(times[0], MyProducts.A);
		productTimes.drawProductsForTime(times[0]);
		ArrayList<Enum<?>> result = productTimes.drawProductsForTime(times[0]);
		assertTrue(result.isEmpty());
	}

	@Test
	public void linkProductToTime_FailOnNullProduct() {
		ExceptionTesting.assertThrowsFatalMessage(ProductTimes.ERROR_NULL_PRODUCT, () ->  {
			productTimes.linkProductToTime(times[0], null);	
		});
	}

	@Test
	public void linkProductToTime_FailOnNullTime() {
		ExceptionTesting.assertThrowsFatalMessage(ProductTimes.ERROR_NULL_TIME, () ->  {
			productTimes.linkProductToTime(null, MyProducts.A);	
		});
	}
}
