package de.dlr.gitlab.fame.agent.dummy;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;

public abstract class AgentDummyAbstract extends Agent {
	public AgentDummyAbstract(DataProvider param) {
		super(param);
	}
}