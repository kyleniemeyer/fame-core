package de.dlr.gitlab.fame.agent.dummy;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;

public class AgentDummyConstructorThrows extends Agent {
	public static final String INTERNAL_ERROR = "This is the internal error message.";
	
	public AgentDummyConstructorThrows(DataProvider param) throws Exception {
		super(param);
		throw new Exception(INTERNAL_ERROR);
	}
}