package de.dlr.gitlab.fame.agent.input;

import static org.mockito.Mockito.mockStatic;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;

/** Test for {@link Tree}
 *
 * @author Christoph Schimeczek */
public class TreeTest {
	@Mock private DataProvider dataProviderMock;
	AutoCloseable closeable;

	private Tree tree;

	@Before
	public void setUp() {
		tree = new Tree();
		closeable = MockitoAnnotations.openMocks(this);
	}

	@After
	public void closeMocks() throws Exception {
		closeable.close();
	}

	@Test
	public void join_CallsJoiner() {
		try (MockedStatic<Joiner> joinerMock = mockStatic(Joiner.class)) {
			ParameterData resultMock = mock(ParameterData.class);
			joinerMock.when(() -> Joiner.join(tree, dataProviderMock)).thenReturn(resultMock);
			ParameterData result = tree.join(dataProviderMock);
			assertEquals(resultMock, result);
		}
	}
}
