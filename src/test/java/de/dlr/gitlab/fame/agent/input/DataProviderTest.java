package de.dlr.gitlab.fame.agent.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.google.protobuf.ProtocolStringList;
import de.dlr.gitlab.fame.agent.input.Make.Type;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.protobuf.Field.NestedField;
import de.dlr.gitlab.fame.protobuf.Input.InputData.AgentDao;
import de.dlr.gitlab.fame.service.LocalServices;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Unit tests for {@link DataProvider}
 *
 * @author Christoph Schimeczek */
public class DataProviderTest {
	private enum DummyEnum {
		A, B
	};

	private enum OtherEnum {
		C, D
	}

	@Mock private LocalServices serviceMock;
	@Mock private AgentDao agentDaoMock;
	@Mock private NestedField fieldMock;
	@Mock private Parameter parameterMock;
	private AutoCloseable closeable;
	private DataProvider dataProvider;
	private Map<Integer, TimeSeries> mapOfTimeSeries;

	@Before
	public void setUp() {
		closeable = MockitoAnnotations.openMocks(this);
		mapOfTimeSeries = new HashMap<>();
		dataProvider = new DataProvider(agentDaoMock, mapOfTimeSeries, serviceMock);
	}

	@After
	public void closeMocks() throws Exception {
		closeable.close();
	}

	@Test
	public void getAgentId() {
		long agentId = 42L;
		when(agentDaoMock.getId()).thenReturn(agentId);
		assertEquals(agentId, dataProvider.getAgentId());
	}

	@Test
	public void getFields() {
		@SuppressWarnings("unchecked") List<NestedField> fieldListMock = (List<NestedField>) mock(List.class);
		when(agentDaoMock.getFieldList()).thenReturn(fieldListMock);
		assertEquals(fieldListMock, dataProvider.getFields());
	}

	@Test
	public void getLocalServices() {
		assertEquals(serviceMock, dataProvider.getLocalServices());
	}

	@Test
	public void getValue_Double() {
		double value = 99.9;
		setParameter(Type.DOUBLE, false);
		setupDoubleFieldMock(value);
		assertEquals(value, retrieve(Double.class), 1e-12);
	}

	/** sets {@link #mockDetail} to return given DataType */
	private void setParameter(Type type, boolean isList) {
		when(parameterMock.getDataType()).thenReturn(type);
		when(parameterMock.isList()).thenReturn(isList);
	}

	/** adds given double values to be returned by {@link #fieldMock} */
	private void setupDoubleFieldMock(double... values) {
		when(fieldMock.getDoubleValueCount()).thenReturn(values.length);
		for (int i = 0; i < values.length; i++) {
			when(fieldMock.getDoubleValue(i)).thenReturn(values[i]);
		}
		when(fieldMock.getDoubleValueList()).thenReturn(Arrays.asList(ArrayUtils.toObject(values)));
	}

	/** retrieves the given Type from the {@link #dataProvider} using {@link #mockDetail} and {@link #fieldMock} */
	private <T> T retrieve(Class<T> returnType) {
		return returnType.cast(dataProvider.getValue(parameterMock, fieldMock));
	}

	@Test
	public void getValue_FailNoValue() {
		for (Type type : Type.values()) {
			setParameter(type, false);
			assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
		}
	}

	@Test
	public void getValue_FailNoList() {
		for (Type type : Type.values()) {
			if (type == Type.TIMESERIES) {
				continue;
			}
			setParameter(type, true);
			assertThrowsFatalMessage(DataProvider.NO_LIST, () -> getValue());
		}
	}

	/** calls getValue ignoring the return value */
	private void getValue() {
		dataProvider.getValue(parameterMock, fieldMock);
	}

	@Test
	public void getValue_DoubleFailMultipleValues() {
		setParameter(Type.DOUBLE, false);
		setupDoubleFieldMock(1.0, 2.0, 3.0);
		assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
	}

	@Test
	public void getValue_DoubleList() {
		setParameter(Type.DOUBLE, true);
		setupDoubleFieldMock(1.0, 2.0, 3.0);
		@SuppressWarnings("unchecked") List<Double> result = (List<Double>) retrieve(List.class);
		assertThat(result, hasItems(1.0, 2.0, 3.0));
		assertEquals(3, result.size());
	}

	@Test
	public void getValue_Int() {
		int value = 21;
		setParameter(Type.INTEGER, false);
		setIntFieldMock(value);
		assertEquals(value, (int) retrieve(Integer.class));
	}

	/** sets given int value to be returned by {@link #fieldMock} or no value if Null is given */
	private void setIntFieldMock(int... values) {
		when(fieldMock.getIntValueCount()).thenReturn(values.length);
		for (int i = 0; i < values.length; i++) {
			when(fieldMock.getIntValue(i)).thenReturn(values[i]);
		}
		when(fieldMock.getIntValueList()).thenReturn(Arrays.asList(ArrayUtils.toObject(values)));
	}

	@Test
	public void getValue_IntFailMultipleValues() {
		setParameter(Type.INTEGER, false);
		setIntFieldMock(1, 2, 3);
		assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
	}

	@Test
	public void getValue_IntList() {
		setParameter(Type.INTEGER, true);
		setIntFieldMock(4, 5, 6);
		@SuppressWarnings("unchecked") List<Integer> result = (List<Integer>) retrieve(List.class);
		assertThat(result, hasItems(4, 5, 6));
		assertEquals(3, result.size());
	}

	@Test
	public void getValue_String() {
		String value = "MyValue";
		setParameter(Type.STRING, false);
		setStringFieldMock(value);
		assertEquals(value, retrieve(String.class));
	}

	/** sets given String values to be returned by {@link #fieldMock} or no value if Null is given */
	private void setStringFieldMock(String... values) {
		when(fieldMock.getStringValueCount()).thenReturn(values.length);
		for (int i = 0; i < values.length; i++) {
			when(fieldMock.getStringValue(i)).thenReturn(values[i]);
		}
		ProtocolStringList listMock = mock(ProtocolStringList.class);
		when(listMock.stream()).thenReturn(Arrays.asList(values).stream());
		when(fieldMock.getStringValueList()).thenReturn(listMock);
	}

	@Test
	public void getValue_StringFailMultipleValues() {
		setParameter(Type.STRING, false);
		setStringFieldMock("La", "Le", "Lu");
		assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
	}

	@Test
	public void getValue_StringList() {
		setParameter(Type.STRING, true);
		setStringFieldMock("La", "Le", "Lu");
		List<String> result = retrieve(ProtocolStringList.class).stream().collect(Collectors.toList());
		assertThat(result, hasItems("La", "Le", "Lu"));
		assertEquals(3, result.size());
	}

	@Test
	public void getValue_Enum() {
		setParameter(Type.ENUM, false);
		setStringFieldMock(DummyEnum.A.toString());
		doReturn(DummyEnum.values()).when(parameterMock).getAllowedValues();
		assertEquals(DummyEnum.A, retrieve(DummyEnum.class));
	}

	@Test
	public void getValue_EnumFailMultipleValues() {
		setParameter(Type.ENUM, false);
		setStringFieldMock(DummyEnum.A.toString(), DummyEnum.B.toString());
		doReturn(DummyEnum.values()).when(parameterMock).getAllowedValues();
		assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
	}

	@Test
	public void getValue_EnumFailInvalidEnum() {
		setParameter(Type.ENUM, false);
		setStringFieldMock("NotAnEnum");
		doReturn(DummyEnum.values()).when(parameterMock).getAllowedValues();
		assertThrowsFatalMessage(DataProvider.VALUE_NOT_ALLOWED, () -> getValue());
	}

	@Test
	public void getValue_EnumList() {
		setParameter(Type.ENUM, true);
		setStringFieldMock(DummyEnum.A.toString(), OtherEnum.C.toString());
		Enum<?>[] allowedValues = join(DummyEnum.values(), OtherEnum.values());
		doReturn(allowedValues).when(parameterMock).getAllowedValues();
		@SuppressWarnings("unchecked") List<Enum<?>> result = (List<Enum<?>>) retrieve(List.class);
		assertEquals(2, result.size());
		assertThat(result, hasItems(DummyEnum.A, OtherEnum.C));
	}

	/** joins two different Enum arrays into a new single one */
	private Enum<?>[] join(Enum<?>[] values, Enum<?>[] values2) {
		Enum<?>[] joined = new Enum<?>[values.length + values2.length];
		for (int i = 0; i < values.length; i++) {
			joined[i] = values[i];
		}
		for (int i = 0; i < values2.length; i++) {
			joined[i + values.length] = values2[i];
		}
		return joined;
	}

	@Test
	public void getValue_TimeSeries() {
		TimeSeries timeSeriesMock = mock(TimeSeries.class);
		int seriesId = 55;
		mapOfTimeSeries.put(seriesId, timeSeriesMock);
		setParameter(Type.TIMESERIES, false);
		setSeriesFieldMock(seriesId);
		assertEquals(timeSeriesMock, retrieve(TimeSeries.class));
	}

	/** sets given SeriesId value to be returned by {@link #fieldMock} or no value if Null is given */
	private void setSeriesFieldMock(Integer seriesId) {
		when(fieldMock.hasSeriesId()).thenReturn(seriesId != null);
		when(fieldMock.getSeriesId()).thenReturn(seriesId != null ? seriesId : 0);
	}

	@Test
	public void getValue_TimeSeriesFailNoSeries() {
		setParameter(Type.TIMESERIES, false);
		setSeriesFieldMock(84);
		assertThrowsFatalMessage(DataProvider.MISSING_SERIES, () -> retrieve(TimeSeries.class));
	}

	@Test
	public void getValue_Long() {
		setParameter(Type.LONG, false);
		setLongFieldMock(55L);
		assertEquals(55L, (long) retrieve(Long.class));
	}

	/** sets given long value to be returned by {@link #fieldMock} or no value if Null is given */
	private void setLongFieldMock(long... values) {
		when(fieldMock.getLongValueCount()).thenReturn(values.length);
		for (int i = 0; i < values.length; i++) {
			when(fieldMock.getLongValue(i)).thenReturn(values[i]);
		}
		when(fieldMock.getLongValueList()).thenReturn(Arrays.asList(ArrayUtils.toObject(values)));
	}

	@Test
	public void getValue_LongFailMultipleValues() {
		setParameter(Type.LONG, false);
		setLongFieldMock(55L, 99L, 22L);
		assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
	}

	@Test
	public void getValue_LongList() {
		setParameter(Type.LONG, true);
		setLongFieldMock(55L, 99L, 22L);
		@SuppressWarnings("unchecked") List<Long> result = (List<Long>) retrieve(List.class);
		assertEquals(3, result.size());
		assertThat(result, hasItems(55L, 99L, 22L));
	}

	@Test
	public void getValue_TimeStamp() {
		setParameter(Type.TIMESTAMP, false);
		setLongFieldMock(55L);
		assertEquals(55L, retrieve(TimeStamp.class).getStep());
	}

	@Test
	public void getValue_TimeStampFailMultipleValues() {
		setParameter(Type.TIMESTAMP, false);
		setLongFieldMock(55L, 99L, 22L);
		assertThrowsFatalMessage(DataProvider.NOT_SINGLE_VALUE, () -> getValue());
	}

	@Test
	public void getValue_TimeStampList() {
		setParameter(Type.TIMESTAMP, true);
		setLongFieldMock(55L, 99L, 22L);
		@SuppressWarnings("unchecked") List<TimeStamp> result = (List<TimeStamp>) retrieve(List.class);
		assertEquals(3, result.size());
		assertThat(result, hasItems(new TimeStamp(55L), new TimeStamp(99L), new TimeStamp(22L)));
	}
}
