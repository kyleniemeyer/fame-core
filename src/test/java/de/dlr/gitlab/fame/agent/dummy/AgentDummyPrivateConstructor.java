package de.dlr.gitlab.fame.agent.dummy;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;

public class AgentDummyPrivateConstructor extends Agent {
	private AgentDummyPrivateConstructor(DataProvider param) {
		super(param);
	}
}