package de.dlr.gitlab.fame.agent.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiFunction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import de.dlr.gitlab.fame.agent.input.ParameterData.MissingDataException;
import de.dlr.gitlab.fame.data.TimeSeries;
import de.dlr.gitlab.fame.time.TimeStamp;

/** Tests for {@link ParameterData} Since the Constructor is called only by the {@link Joiner} - it can safely be assumed that its
 * argument are not null */
public class ParameterDataTest {
	private enum Dummy {
		A, B, C
	}

	private final Double dble = 4.2;
	private final String string = "LaLeLu";
	private final Integer integer = 42;
	private final Long lng = 99L;
	private final TimeStamp timeStamp = new TimeStamp(57);
	private List<Double> doubleList = Arrays.asList(1.0, 2.0, 3.0);
	@Mock private TimeSeries timeSeries;
	private final Enum<?> enm = Dummy.A;
	private AutoCloseable closeable;

	private ParameterData parameterData;
	private HashMap<String, Object> input;

	@Before
	public void setUp() {
		closeable = MockitoAnnotations.openMocks(this);
		input = new HashMap<>();
		parameterData = new ParameterData(input, "");
	}

	@After
	public void releaseMocks() throws Exception {
		closeable.close();
	}

	@Test
	public void getAvailableParameters_Empty() {
		assertTrue(parameterData.getAvailableParameters().isEmpty());
	}

	@Test
	public void getAvailableParameters_NotEmpty() {
		String address = "An.Address";
		input.put(address, null);
		assertTrue(parameterData.getAvailableParameters().contains(address));
	}

	@Test
	public void getGroup_FailMissing() {
		assertThrowsMessage(MissingDataException.class, ParameterData.GROUP_MISSING,
				() -> parameterData.getGroup("Missing.Group"));
	}

	@Test
	public void getGroup_FailList() {
		input.put("Group.0.A", null);
		assertThrowsFatalMessage(ParameterData.GROUP_IS_LIST, () -> parameterData.getGroup("Group"));
	}

	@Test
	public void getGroup_Outer() throws MissingDataException {
		input.put("Outer.Inner.Group.Item.A", null);
		ParameterData result = parameterData.getGroup("Outer");
		assertEquals("Outer.", result.parentPath);
	}

	@Test
	public void getGroup_Inner() throws MissingDataException {
		input.put("Outer.Inner.Group.Item.A", null);
		ParameterData result = parameterData.getGroup("Outer.Inner.Group");
		assertEquals("Outer.Inner.Group.", result.parentPath);
	}

	@Test
	public void getGroup_FailIsNoGroup() {
		input.put("Outer.Group.A", null);
		assertThrowsMessage(MissingDataException.class, ParameterData.GROUP_MISSING,
				() -> parameterData.getGroup("Outer.Group.A"));
	}

	@Test
	public void getGroupList_FailMissing() {
		assertThrowsMessage(MissingDataException.class, ParameterData.GROUP_MISSING,
				() -> parameterData.getGroupList("Missing.Group"));
	}

	@Test
	public void getGroupList_FailNoList() {
		input.put("Outer.Inner.Group.Item.A", null);
		assertThrowsFatalMessage(ParameterData.GROUP_NOT_LIST, () -> parameterData.getGroupList("Outer"));
	}

	@Test
	public void getGroupList_Outer() throws MissingDataException {
		input.put("Outer.0.Item.A", 1);
		input.put("Outer.1.Item.A", 2);
		input.put("Outer.2.Item.A", 3);
		List<ParameterData> result = parameterData.getGroupList("Outer");
		assertEquals(3, result.size());
		assertEquals("Outer.0.", result.get(0).parentPath);
		assertEquals("Outer.1.", result.get(1).parentPath);
		assertEquals("Outer.2.", result.get(2).parentPath);
	}

	@Test
	public void getGroupList_Inner() throws MissingDataException {
		input.put("Outer.Inner.0.Group.Item.A", 1);
		input.put("Outer.Inner.1.Group.Item.A", 2);
		List<ParameterData> result = parameterData.getGroupList("Outer.Inner");
		assertEquals(2, result.size());
		assertEquals("Outer.Inner.0.", result.get(0).parentPath);
		assertEquals("Outer.Inner.1.", result.get(1).parentPath);
	}

	@Test
	public void get_AnyFailMissing() {
		String invalidPath = "Any.Path";
		assertMissing(() -> parameterData.getTimeSeries(invalidPath));
		assertMissing(() -> parameterData.getString(invalidPath));
		assertMissing(() -> parameterData.getInteger(invalidPath));
		assertMissing(() -> parameterData.getDouble(invalidPath));
		assertMissing(() -> parameterData.getLong(invalidPath));
		assertMissing(() -> parameterData.getTimeStamp(invalidPath));
		assertMissing(() -> parameterData.getEnum(invalidPath, Dummy.class));
		assertMissing(() -> parameterData.getList(invalidPath, Double.class));
	}

	/** asserts that the given runnable throws a MissingDataException caused by an unknown parameter */
	private void assertMissing(ThrowingRunnable runnable) {
		assertThrowsMessage(MissingDataException.class, ParameterData.EX_UNKNOWN_PARAM, runnable);
	}

	@Test
	public void get_Any() {
		assertPresent(timeSeries, parameterData::getTimeSeries);
		assertPresent(dble, parameterData::getDouble);
		assertPresent(integer, parameterData::getInteger);
		assertPresent(string, parameterData::getString);
		assertPresent(lng, parameterData::getLong);
		assertPresent(timeStamp, parameterData::getTimeStamp);
		assertPresent(enm, Dummy.class, parameterData::getEnum);
	}

	@FunctionalInterface
	public interface CheckedFunction<T, R> {
		R apply(T t) throws MissingDataException;
	}

	/** asserts that given function returns the given value (stored in input) of same type */
	private <R> void assertPresent(R storedValue, CheckedFunction<String, R> function) {
		input.put("Group.A", storedValue);
		try {
			assertEquals(storedValue, function.apply("Group.A"));
		} catch (MissingDataException e) {
			fail("Exception occured.");
		}
	}

	@FunctionalInterface
	public interface CheckedBiFunction<T, U, R> {
		R apply(T t, U u) throws MissingDataException;
	}

	private <R, U> void assertPresent(R storedValue, U type, CheckedBiFunction<String, U, R> function) {
		input.put("Group.A", storedValue);
		try {
			assertEquals(storedValue, function.apply("Group.A", type));
		} catch (MissingDataException e) {
			fail("Exception occured.");
		}
	}

	@Test
	public void getList() {
		assertPresent(doubleList, Double.class, parameterData::getList);
		assertPresent(Arrays.asList(1, 2, 3), Integer.class, parameterData::getList);
		assertPresent(Arrays.asList(Dummy.A, Dummy.B), Enum.class, parameterData::getList);
		assertPresent(Arrays.asList("La", "Le", "Lu"), String.class, parameterData::getList);
		assertPresent(Arrays.asList(1L, 2L, 4L), Long.class, parameterData::getList);
		assertPresent(Arrays.asList(new TimeStamp(55), new TimeStamp(22)), TimeStamp.class, parameterData::getList);
	}

	@Test
	public void getString_WrongClass() {
		input.put("Group.A", integer);
		assertThrowsFatalMessage(ParameterData.WRONG_TYPE, () -> parameterData.getString("Group.A"));
	}

	@Test
	public void getOrDefault_ReturnsDefault() {
		assertReturnsDefaultOnMissing(timeSeries, parameterData::getTimeSeriesOrDefault);
		assertReturnsDefaultOnMissing(dble, parameterData::getDoubleOrDefault);
		assertReturnsDefaultOnMissing(string, parameterData::getStringOrDefault);
		assertReturnsDefaultOnMissing(integer, parameterData::getIntegerOrDefault);
		assertReturnsDefaultOnMissing(lng, parameterData::getLongOrDefault);
		assertReturnsDefaultOnMissing(timeStamp, parameterData::getTimeStampOrDefault);
		assertReturnsDefaultOnMissing(Dummy.A, Dummy.class, parameterData::getEnumOrDefault);
	}

	/** asserts that given function returns given default for a missing item */
	private <R> void assertReturnsDefaultOnMissing(R defaultValue, BiFunction<String, R, R> function) {
		assertEquals(defaultValue, function.apply("Missing.Item", defaultValue));
	}

	/** asserts that given function returns given default for a missing item */
	private <R, T> void assertReturnsDefaultOnMissing(R defaultValue, T type, TriFunction<String, T, R, R> function) {
		assertEquals(defaultValue, function.apply("Missing.Item", type, defaultValue));
	}

	@Test
	public void getOrDefault_ReturnsValue() {
		assertReturnsValueIfPresent(timeSeries, parameterData::getTimeSeriesOrDefault);
		assertReturnsValueIfPresent(dble, parameterData::getDoubleOrDefault);
		assertReturnsValueIfPresent(string, parameterData::getStringOrDefault);
		assertReturnsValueIfPresent(integer, parameterData::getIntegerOrDefault);
		assertReturnsValueIfPresent(lng, parameterData::getLongOrDefault);
		assertReturnsValueIfPresent(timeStamp, parameterData::getTimeStampOrDefault);
		assertReturnsValueIfPresent(Dummy.A, Dummy.class, parameterData::getEnumOrDefault);
	}

	/** asserts that given function returns the stored value but not the default */
	private <R> void assertReturnsValueIfPresent(R value, BiFunction<String, R, R> function) {
		input.put("Group.A", value);
		assertEquals(value, function.apply("Group.A", null));
	}

	/** asserts that given function returns the stored value but not the default */
	private <R, T> void assertReturnsValueIfPresent(R value, T type, TriFunction<String, T, R, R> function) {
		input.put("Group.A", value);
		assertEquals(value, function.apply("Group.A", type, null));
	}

	@FunctionalInterface
	public interface TriFunction<T, U, V, R> {
		R apply(T t, U u, V v);
	}

	@Test
	public void getListOrDefault_ReturnsDefault() {
		assertListReturnsValue(Arrays.asList(1.0, 2.0, 3.0), Double.class, parameterData::getListOrDefault);
		assertListReturnsValue(Arrays.asList(1, 2, 3), Integer.class, parameterData::getListOrDefault);
		assertListReturnsValue(Arrays.asList(Dummy.A, Dummy.B), Dummy.class, parameterData::getListOrDefault);
		assertListReturnsValue(Arrays.asList("La", "Le", "Lu"), String.class, parameterData::getListOrDefault);
		assertListReturnsValue(Arrays.asList(1L, 2L, 4L), Long.class, parameterData::getListOrDefault);
		assertListReturnsValue(Arrays.asList(new TimeStamp(55), new TimeStamp(22)), TimeStamp.class,
				parameterData::getListOrDefault);
	}

	private <R, T> void assertListReturnsValue(List<R> value, T type, TriFunction<String, T, List<R>, List<R>> function) {
		input.put("Group.A", value);
		assertEquals(value, function.apply("Group.A", type, null));
	}

	@Test
	public void getListOrDefault_ReturnsList() {
		assertListReturnsDefault(Arrays.asList(1.0, 2.0, 3.0), Double.class, parameterData::getListOrDefault);
		assertListReturnsDefault(Arrays.asList(1, 2, 3), Integer.class, parameterData::getListOrDefault);
		assertListReturnsDefault(Arrays.asList(Dummy.A, Dummy.B), Dummy.class, parameterData::getListOrDefault);
		assertListReturnsDefault(Arrays.asList("La", "Le", "Lu"), String.class, parameterData::getListOrDefault);
		assertListReturnsDefault(Arrays.asList(1L, 2L, 4L), Long.class, parameterData::getListOrDefault);
		assertListReturnsDefault(Arrays.asList(new TimeStamp(55), new TimeStamp(22)), TimeStamp.class,
				parameterData::getListOrDefault);
	}

	private <R, T> void assertListReturnsDefault(List<R> value, T type,
			TriFunction<String, T, List<R>, List<R>> function) {
		assertEquals(value, function.apply("Missing.Value", type, value));
	}
}
