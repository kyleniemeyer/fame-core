package de.dlr.gitlab.fame.agent.input;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import de.dlr.gitlab.fame.agent.input.Make.Type;

/** Tests for {@link Parameter}
 * 
 * @author Christoph Schimeczek */
public class ParameterTest {
	private enum Dummy {
		A, B, C
	}

	private Parameter parameter;

	@Before
	public void setUp() {
		parameter = new Parameter();
	}

	@Test
	public void getInnerElements() {
		assertTrue(parameter.getInnerElements().isEmpty());
	}

	@Test
	public void isPropagatedMandatory() {
		parameter.setOptional(true);
		assertFalse(parameter.isPropagatedMandatory());
		parameter.setOptional(false);
		assertTrue(parameter.isPropagatedMandatory());
	}

	@Test
	public void deepCopy_ReturnsOtherObject() {
		Parameter copy = parameter.deepCopy();
		assertNotEquals(parameter, copy);
	}

	@Test
	public void deepCopy_copiesAttributes() {
		parameter.setDataType(Type.ENUM);
		parameter.setAllowedValues(Dummy.values());
		parameter.setComment("Comment");
		parameter.setDefaultValue("Default");
		parameter.setIsList(true);
		parameter.setName("MyName");
		parameter.setParent(mock(TreeElement.class));
		parameter.setOptional(true);

		Parameter copy = parameter.deepCopy();
		assertEquals(Type.ENUM, copy.getDataType());
		assertThat(Arrays.asList(copy.getAllowedValues()), hasItems(Dummy.A, Dummy.B, Dummy.C));
		assertEquals("Comment", copy.getComment());
		assertEquals("Default", copy.getDefaultValue());
		assertTrue(copy.isList());
		assertEquals("MyName", copy.getName());
		assertNull(copy.getParent());
		assertTrue(copy.isOptional());
	}

	@Test
	public void getRoot_hasParent() {
		TreeElement parent = mockParent(null);
		assertEquals(parent, parameter.getRoot());
	}

	@Test
	public void getRoot_IsRoot() {
		assertEquals(parameter, parameter.getRoot());
	}

	/** injects a newly mocked TreeElement into {@link #parameter} as parent */
	private TreeElement mockParent(String address) {
		TreeElement parent = mock(TreeElement.class);
		when(parent.getRoot()).thenReturn(parent);
		when(parent.getAddress()).thenReturn(address);
		parameter.setParent(parent);
		return parent;
	}

	@Test
	public void isRoot() {
		assertTrue(parameter.isRoot());
		mockParent(null);
		assertFalse(parameter.isRoot());
	}

	@Test
	public void getAddress_WithParent() {
		parameter.setName("MyName");
		mockParent("Parent.Address");
		assertEquals("Parent.Address.MyName", parameter.getAddress());
	}

	@Test
	public void getAddress_EmptyForRoot() {
		parameter.setName("MyName");
		assertEquals("", parameter.getAddress());
	}

	@Test
	public void getAddress_RootParentThenParentAddressIgnored() {
		parameter.setName("MyName");
		TreeElement parent = mockParent("Parent.Address");
		when(parent.isRoot()).thenReturn(true);
		assertEquals("MyName", parameter.getAddress());
	}

	@Test
	public void get_ReturnsOtherObject() {
		assertNotEquals(parameter, parameter.get());
	}
}
