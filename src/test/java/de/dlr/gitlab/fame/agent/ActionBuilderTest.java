package de.dlr.gitlab.fame.agent;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import de.dlr.gitlab.fame.communication.Contract;
import de.dlr.gitlab.fame.communication.message.Message;

/**
 * Tests of {@link ActionBuilder}
 * 
 * @author Christoph Schimeczek
 */
public class ActionBuilderTest {
	private enum Dummy {
		None, One, Two
	};
	
	@Mock private ContractManager contractManagerMock;
	@Mock private ActionManager actionManagerMock;
	@Mock private MessageManager messageManagerMock;
	@Captor private ArgumentCaptor<List<Enum<?>>> captor;
	
	
	private AutoCloseable closeable;
	private ActionBuilder actionBuilder;
	private BiConsumer<ArrayList<Message>, List<Contract>> action = this::dummyMethod;

	@Before
	public void setup() {
		closeable = MockitoAnnotations.openMocks(this);
		actionBuilder = new ActionBuilder(action);
	}
	
	@After
	public void tearDown() throws Exception {
		closeable.close();
	}

	/** used to test calls of {@link BiConsumer} functions */
	private void dummyMethod(ArrayList<Message> m, List<Contract> c) {
		// DoNothing
	};

	@Test
	public void testConstructorFailNull() {
		assertThrowsFatalMessage(ActionBuilder.ARGUMENT_IS_NULL, () -> new ActionBuilder(null));
	}

	@Test
	public void testOnReturnsBuilder() {
		assertEquals(actionBuilder, actionBuilder.on(Dummy.One));
	}

	@Test
	public void testOnNotAcceptFailNull() {
		assertThrowsFatalMessage(ActionBuilder.ARGUMENT_IS_NULL, () -> actionBuilder.on(null));
	}

	@Test
	public void testArmFailNoTrigger() {
		assertThrowsFatalMessage(ActionBuilder.MISSING_TRIGGER,
				() -> actionBuilder.arm(contractManagerMock, actionManagerMock, messageManagerMock));
	}

	@Test
	public void testArmAfterUseNoInput() {
		actionBuilder.on(Dummy.One);
		actionBuilder.use();
		actionBuilder.arm(contractManagerMock, actionManagerMock, messageManagerMock);
		assertArmed(Dummy.One);
	}

	/** asserts that associated managers haven been called with given trigger and input products */
	private void assertArmed(Enum<?> trigger, Enum<?>... inputs) {
		verify(contractManagerMock, times(1)).consider(trigger);
		verify(actionManagerMock, times(1)).configure(trigger, action);
		verify(messageManagerMock, times(1)).connectInputsToTrigger(eq(trigger), captor.capture());
		List<Enum<?>> inputList = captor.getValue();
		if (inputs.length == 0) {
			assertThat(inputList.isEmpty(), is(true));
		} else {
			for (Enum<?> input : inputs) {
				assertThat(inputList, hasItem(input));
			}
		}
	}

	@Test
	public void testUseOneInput() {
		actionBuilder.on(Dummy.None);
		actionBuilder.use(Dummy.One);
		actionBuilder.arm(contractManagerMock, actionManagerMock, messageManagerMock);
		assertArmed(Dummy.None, Dummy.One);
	}

	@Test
	public void testUseTwoInputs() {
		actionBuilder.on(Dummy.None);
		actionBuilder.use(Dummy.One, Dummy.Two);
		actionBuilder.arm(contractManagerMock, actionManagerMock, messageManagerMock);
		assertArmed(Dummy.None, Dummy.One);
	}

	@Test
	public void testUseTriggerAsInput() {
		actionBuilder.on(Dummy.None);
		actionBuilder.use(Dummy.None);
		actionBuilder.arm(contractManagerMock, actionManagerMock, messageManagerMock);
		assertArmed(Dummy.None, Dummy.None);
	}
}