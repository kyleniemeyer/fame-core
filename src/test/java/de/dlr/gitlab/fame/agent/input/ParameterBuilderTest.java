package de.dlr.gitlab.fame.agent.input;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.verify;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedConstruction;
import de.dlr.gitlab.fame.agent.input.Make.Type;

/** Tests for {@link ParameterBuilder}
 *
 * @author Christoph Schimeczek */
public class ParameterBuilderTest {
	private enum Dummy {
		A, B, C
	}

	private ParameterBuilder builder;
	private MockedConstruction<Parameter> mockedConstruction;

	@Before
	public void setUp() {
		builder = new ParameterBuilder();
		mockedConstruction = mockConstruction(Parameter.class);
	}

	@After
	public void closeMocks() {
		mockedConstruction.close();
	}

	@Test
	public void setName_FailNull() {
		assertThrowsFatalMessage(ElementBuilder.NAME_NEEDED, () -> builder.setName(null));
	}

	@Test
	public void setName_FailEmpty() {
		assertThrowsFatalMessage(ElementBuilder.NAME_NEEDED, () -> builder.setName(""));
	}
	
	@Test
	public void setName_FailIllegal() {
		assertThrowsFatalMessage(ElementBuilder.NAME_ILLEGAL, () -> builder.setName("class"));
		assertThrowsFatalMessage(ElementBuilder.NAME_ILLEGAL, () -> builder.setName("0"));
		assertThrowsFatalMessage(ElementBuilder.NAME_ILLEGAL, () -> builder.setName("false"));
	}

	@Test
	public void setName() {
		assertEquals(builder, builder.setName("MyName"));
		verify(builder.build()).setName("MyName");
		assertEquals("MyName", builder.getName());
	}

	@Test
	public void setType_FailNull() {
		assertThrowsFatalMessage(ParameterBuilder.DATA_TYPE_NEEDED, () -> builder.setType(null));
	}

	@Test
	public void setType() {
		assertEquals(builder, builder.setType(Type.DOUBLE));
		verify(builder.build()).setDataType(Type.DOUBLE);
	}

	@Test
	public void setType_FailEnumNotProvided() {
		builder.setType(Type.ENUM);
		assertThrowsFatalMessage(ParameterBuilder.ENUM_REQUIRED, () -> builder.build());
	}

	@Test
	public void setType_Enum() {
		builder.setType(Type.ENUM);
		assertEquals(builder, builder.setEnum(Dummy.class));
		verify(builder.build()).setAllowedValues(any(Dummy[].class));
	}

	@Test
	public void list() {
		verify(builder.build()).setIsList(false);
		assertEquals(builder, builder.list());
		verify(builder.build()).setIsList(true);
	}

	@Test
	public void list_Disallowed() {
		builder.setType(Type.TIMESERIES);
		assertThrowsFatalMessage(ParameterBuilder.LIST_DISALLOWED, () -> builder.list());
	}

	@Test
	public void optional() {
		verify(builder.build()).setOptional(false);
		assertEquals(builder, builder.optional());
		verify(builder.build()).setOptional(true);
	}

	@Test
	public void def() {
		assertEquals(builder, builder.fill("Some value"));
		verify(builder.build()).setDefaultValue("Some value");
	}

	@Test
	public void help() {
		assertEquals(builder, builder.help("Some comment"));
		verify(builder.build()).setComment("Some comment");
	}
}
