package de.dlr.gitlab.fame.agent.dummy;

import de.dlr.gitlab.fame.agent.Agent;
import de.dlr.gitlab.fame.agent.input.DataProvider;
import de.dlr.gitlab.fame.communication.Product;

public class AgentDummy extends Agent {
	@Product
	public enum MyProducts{ProdA, ProdB};

	public AgentDummy(DataProvider param) {
		super(param);
	}
}