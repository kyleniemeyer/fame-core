package de.dlr.gitlab.fame.testUtils;

import java.util.Random;

/** @author A. Achraf El Ghazi */
public class RandSelect {

	private static Random rand = new Random();

	/** @return one value of given array*/
	public static <T> T pickOneOf(T[] items) {
		return items[rand.nextInt(items.length)];
	}
}
