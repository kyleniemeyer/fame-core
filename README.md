# FAME-Core
FAME's core library written in Java. 

## What is FAME?
FAME is an open **F**ramework for distributed **A**gent-based **M**odels of **E**nergy systems.
Its purpose is to support the rapid development and fast execution of complex agent-based energy system simulations.
All components of FAME are open source.
These components are:
- **FAME-Core**: A Java library that provides classes and methods to create and run your own agent-based simulations in single-core or multi-core mode.
- **FAME-Io**: A set of Python tools that enable you to feed input data to your simulations and to extract simulation results in a convenient way.
- **FAME-Gui**: A graphical user interface for convenient configuration of FAME-based models.
- **FAME-Protobuf**: Basic definitions for input and output formats of FAME-Core.
- **FAME-Demo**: A simplistic FAME-based simulation demonstrating the most important features of FAME and their application in energy systems analysis.
- **FAME-Mpi**: Components for parallelisation of FAME-Core applications

Please visit the [FAME-Wiki](https://gitlab.com/fame-framework/wiki/-/wikis/home) to access detailed manuals and explanations for FAME-components.

## Statement of need
FAME is a general framework intended for, but not exclusive to, energy systems analysis.
It facilitates the development of complex agent-based simulations.
FAME enables parallel execution of simulations without any prior knowledge about parallelisation techniques.
Models built with FAME can be executed on computer systems of any capacity – from a laptop to a high-performance computing cluster.
Furthermore, FAME applications are binary data files are portable across Windows and Linux systems.
FAME's highly adaptable contracting system offers broad configuration options to control simulations without changing its code.

FAME-Core has been thoroughly tested to fulfil the demand of scientific rigor and to provide a stable and reliable simulation framework.
It provides thoroughly tested functionality common to all simulations (e.g. input, output, scheduling, communication), hence resulting in

1. less code &rarr; less errors &rarr; less maintenance, 
1. higher transparency of existing models, due to common interfaces and concepts of the respective framework, and 
1. lower threshold for the development of new models by providing a well-defined starting point.

## Installation instructions
### Prerequisites
As long as the repository is not integrated with Maven-central, you need to package and install FAME-Protobuf locally, first. Please follow install instructions of FAME-Protobuf.

### Usage with models based on FAME (e.g. FAME-Demo)
#### Package and install
As long as the repository is not integrated with Maven-central, the Java source files should to be packaged into a library file and installed to local Maven repository.

##### With Eclipse
Run the provided launchers

1. "PrepareToInstallFameCore.launch"
2. "InstallFameCore.launch"

in that order by selecting "Run" -> "Run Configuration" -> "Maven Build" -> <LauncherName> and clicking on "Run".

##### With Linux console
To compile, package and install FAME-Core to you local Maven repository run

1. `mvn validate`
2. `mvn install`

in that order.
  
#### Maven Dependency
Use the following Maven dependency to refer to FAME-Core in your model project

```
<dependency>
  <groupId>de.dlr.fame</groupId>
  <artifactId>core</artifactId>
  <version>1.4.0</version>
</dependency>
```

## Available Support
This is a purely scientific project, hence there will be no paid technical support.
Limited support is available, e.g. to enhance FAME, fix bugs, etc. 
To report bugs or pose enhancement requests, please file issues following the provided templates (see also [CONTRIBUTING.md](CONTRIBUTING.md))
For substantial enhancements, we recommend that you contact us via [fame@dlr.de](mailto:fame@dlr.de) for working together on the code in common projects or towards common publications and thus further develop FAME.
